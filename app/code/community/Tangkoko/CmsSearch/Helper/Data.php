<?php
/**
 * Tangkoko-Fidesio Cms Search Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tangkoko.com or info@fidesio.com and you will be sent
 * a copy immediately.
 *
 * @category   Tangkoko & Fidesio
 * @package    CmsSearch
 * @author     Vincent Decrombecque
 * @copyright  Copyright (c) 2009 Tangkoko sarl (http://www.tangkoko.com) & Fidesio sarl (http://www.fidesio.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tangkoko_CmsSearch_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Retrieve search type
     *
     * @param int $storeId
     * @return int
     */
    public function getSearchType($storeId = null)
    {
        return Mage::getStoreConfig(Mage_CatalogSearch_Model_Fulltext::XML_PATH_CATALOG_SEARCH_TYPE, $storeId);
    }

   public function toAbstract($text)
	{	
		$search=array('@&lt;script.*?&gt;.*?&lt;/script&gt;@si', '@&lt;style.*?&gt;.*?&lt;/style&gt;@si');
		$replace=array('','');
		$text=trim(preg_replace($search,$replace,$text));

		preg_match('/^([^.!?\s]*[\.!?\s]+){0,30}/', trim(strip_tags($text)),$abstract);
		$result=trim(count($abstract) > 0 ? $abstract[0] : '').'...';
		return $result;
	} 
}