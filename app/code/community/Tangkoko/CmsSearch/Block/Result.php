<?php
/**
 * Tangkoko-Fidesio Cms Search Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tangkoko.com or info@fidesio.com and you will be sent
 * a copy immediately.
 *
 * @category   Tangkoko & Fidesio
 * @package    CmsSearch
 * @author     Vincent Decrombecque
 * @copyright  Copyright (c) 2009 Tangkoko sarl (http://www.tangkoko.com) & Fidesio sarl (http://www.fidesio.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tangkoko_CmsSearch_Block_Result extends Mage_CatalogSearch_Block_Result
{
	/**
	 * Cms Page collection
	 *
	 * @var Tangkoko_CmsSearch_Model_Mysql4_Fulltext_Collection
	 */
	protected $_pageCollection;

	/**
	 * Retrieve search result count
	 *
	 * @return string
	 */
	public function getPageResultCount()
	{
		if (!$this->getData('page_result_count')) {
			$size = $this->_getPageCollection()->getSize();
			$this->setPageResultCount($size);
		}
		return $this->getData('page_result_count');
	}

	/**
	 * Retrieve cms page collection
	 *
	 * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext_Collection
	 */
	protected function _getPageCollection()
	{
		if (is_null($this->_pageCollection)) {
			$this->_pageCollection = Mage::getResourceModel('cmssearch/fulltext_collection');
			$this->_pageCollection->addSearchFilter(Mage::helper('catalogSearch')->getEscapedQueryText())
			->addStoreFilter(Mage::app()->getStore());
		}
		return $this->_pageCollection;
	}

	protected function _toAbstract($page)
	{
		return Mage::helper('cmssearch')->toAbstract($this->_toHtmlContent($page));
	}
	
	protected function _toHtmlContent($page)
	{
		$processor = Mage::getModel('cmssearch/core_email_template_filter');
		$designConfig = $processor->getDesignConfig();
		$arStoreId = $page->getStoreId();
		$designConfig->setStore($arStoreId[0]);
		$designConfig->setArea('frontend');			
		$html = $processor->process($page->getContent());
		
		return $html;
	}
}