<?php
/**
 * Tangkoko-Fidesio Cms Search Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tangkoko.com or info@fidesio.com and you will be sent
 * a copy immediately.
 *
 * @category   Tangkoko & Fidesio
 * @package    CmsSearch
 * @author     Vincent Decrombecque
 * @copyright  Copyright (c) 2009 Tangkoko sarl (http://www.tangkoko.com) & Fidesio sarl (http://www.fidesio.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tangkoko_CmsSearch_Model_Observer
{	
    public function refreshPageIndex($observer)
    {
    	$page = $observer->getEvent()->getObject();

        Mage::getModel('cmssearch/fulltext')
            ->rebuildIndex(null, $page->getId())
            ->resetSearchResults();

        return $this;
    }

    public function cleanPageIndex($observer)
    {
    	$page = $observer->getEvent()->getObject();

        Mage::getModel('cmssearch/fulltext')
            ->cleanIndex(null, $page->getId())
            ->resetSearchResults();

        return $this;
    }

    /**
     * Refresh fulltext index when we add new store
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogSearch_Model_Fulltext_Observer
     */
    public function refreshStoreIndex($observer)
    {
        $store = $observer->getEvent()->getStore();
        if (!isset($store))
        	$store = $observer->getEvent()->getObject();
        if (isset($store))
        {
        	$storeId = $store->getId();
        	Mage::getModel('cmssearch/fulltext')->rebuildIndex($storeId);
        }
        return $this;
    }

    /**
     * Refresh fulltext index
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogSearch_Model_Fulltext_Observer
     */
    public function refreshFullIndex($observer)
    {
        Mage::getModel('cmssearch/fulltext')->rebuildIndex();
        return $this;
    }
    
}