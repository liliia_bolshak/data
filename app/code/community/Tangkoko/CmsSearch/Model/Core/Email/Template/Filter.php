<?php
/**
 * Tangkoko-Fidesio Cms Search Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tangkoko.com or info@fidesio.com and you will be sent
 * a copy immediately.
 *
 * @category   Tangkoko & Fidesio
 * @package    CmsSearch
 * @author     Vincent Decrombecque
 * @copyright  Copyright (c) 2009 Tangkoko sarl (http://www.tangkoko.com) & Fidesio sarl (http://www.fidesio.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tangkoko_CmsSearch_Model_Core_Email_Template_Filter extends Mage_Core_Model_Email_Template_Filter
{
    /**
     * Configuration of desing package for template
     *
     * @var Varien_Object
     */
    protected $_designConfig;

    public function process($content)
    {
        $this->_applyDesignConfig();
        try{
        	$processedResult = $this->filter($content);
        }
        catch (Exception $e)   {
        	$this->_cancelDesignConfig();
            throw $e;
        }
        $this->_cancelDesignConfig();
        return $processedResult;
    }
    
	/**
     * Initialize design information for email template and subject processing
     *
     * @param   array $config
     * @return  Mage_Core_Model_Email_Template
     */
    public function setDesignConfig(array $config)
    {
        $this->getDesignConfig()->setData($config);
        return $this;
    }

    /**
     * Get design configuration data
     *
     * @return Varien_Object
     */
    public function getDesignConfig()
    {
        if(is_null($this->_designConfig)) {
            $this->_designConfig = new Varien_Object();
        }
        return $this->_designConfig;
    }

    /**
     * Apply declared configuration for design
     *
     * @return Mage_Core_Model_Email_Template
     */
    protected function _applyDesignConfig()
    {
        if ($this->getDesignConfig()) {
            $design = Mage::getDesign();
            $this->getDesignConfig()
                ->setOldArea($design->getArea())
                ->setOldStore($design->getStore());

            if ($this->getDesignConfig()->getArea()) {
                Mage::getDesign()->setArea($this->getDesignConfig()->getArea());
            }

            if ($this->getDesignConfig()->getStore()) {
                Mage::app()->getLocale()->emulate($this->getDesignConfig()->getStore());
                $design->setStore($this->getDesignConfig()->getStore());
                $design->setTheme('');
                $design->setPackageName('');
            }

        }
        return $this;
    }

    /**
     * Revert design settings to previous
     *
     * @return Mage_Core_Model_Email_Template
     */
    protected function _cancelDesignConfig()
    {
        if ($this->getDesignConfig()) {
            if ($this->getDesignConfig()->getOldArea()) {
                Mage::getDesign()->setArea($this->getDesignConfig()->getOldArea());
            }

            if ($this->getDesignConfig()->getOldStore()) {
                Mage::getDesign()->setStore($this->getDesignConfig()->getOldStore());
                Mage::getDesign()->setTheme('');
                Mage::getDesign()->setPackageName('');
            }
        }
        Mage::app()->getLocale()->revert();
        return $this;
    }

}