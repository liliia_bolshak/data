<?php
/**
 * Tangkoko-Fidesio Cms Search Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@tangkoko.com or info@fidesio.com and you will be sent
 * a copy immediately.
 *
 * @category   Tangkoko & Fidesio
 * @package    CmsSearch
 * @author     Vincent Decrombecque
 * @copyright  Copyright (c) 2009 Tangkoko sarl (http://www.tangkoko.com) & Fidesio sarl (http://www.fidesio.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tangkoko_CmsSearch_Model_Mysql4_Fulltext extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Index values separator
     *
     * @var string
     */
    protected $_separator = ' ';

	public function _construct()
    {
        $this->_init('cmssearch/fulltext', 'page_id');
    }

    /**
     * Regenerate search index for store(s)
     *
     * @param int $storeId Store View Id
     * @param int|array $pageIds Page Entity Id(s)
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     */
    public function rebuildIndex($storeId = null, $pageIds = null)
    {
    	if (is_null($storeId)) {
            foreach (Mage::app()->getStores(false) as $store) {
                $this->_rebuildStoreIndex($store->getId(), $pageIds);
            }
        } else {
            $this->_rebuildStoreIndex($storeId, $pageIds);
        }
        return $this;
    }

     /**
     * Regenerate search index for specific store
     *
     * @param int $storeId Store View Id
     * @param int|array $pageIds Page Entity Id
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     */
    protected function _rebuildStoreIndex($storeId, $pageIds = null)
    {
		try
		{
	    	$this->cleanIndex($storeId, $pageIds);
	
			$processor = Mage::getModel('cmssearch/core_email_template_filter');
			$designConfig = $processor->getDesignConfig();
			$designConfig->setStore($storeId);
			$designConfig->setArea('frontend');
	                
			$lastPageId = 0;
	        while (true) {
	            $pages = $this->_getSearchablePages($storeId, $pageIds, $lastPageId);
	            if (!$pages) {
	                break;
	            }
	
	            $pageIndexes = array();
	            foreach ($pages as $pageData) {
	                $lastPageId = $pageData['page_id'];
	            	if (!isset($pageData['page_id'])) {
	                    continue;
	                }
	                $index = array();
	                if (isset($pageData['title']))
	                	$index[] = $pageData['title'];
	                if (isset($pageData['content']))
	                {
	        			$html = "";
	        			try
	        			{	       				
	        				$html = $processor->process($pageData['content']);
	        			}
	        			catch (Exception $e)
	        			{
	        				Mage::log("Tangkoko_CmsSearch_Model_Mysql4_Fulltext::_rebuildStoreIndex Following page ignored");
	        				Mage::log($pageData);
	        				Mage::log($e);
		                    continue;
	        			}
	        			$search=array('@&lt;script.*?&gt;.*?&lt;/script&gt;@si', '@&lt;style.*?&gt;.*?&lt;/style&gt;@si');
						$replace=array('','');
						$html=trim(preg_replace($search,$replace,$html)); 
		               	$html = preg_replace("#\s+#si", " ", trim(strip_tags($html)));
		               	$index[] = html_entity_decode($html,ENT_QUOTES,"UTF-8");
	                }
	                $pageIndexes[$pageData['page_id']] = join($this->_separator, $index);
	            }
	            $this->_savePageIndexes($storeId, $pageIndexes);
	        }
	        $this->resetSearchResults();
		}
		catch (Exception $e)
		{
			Mage::log($e);
			throw $e;
		}
        return $this;
    }

    /**
     * Retrieve searchable pages per store
     *
     * @param int $storeId
     * @param array|int $pageIds
     * @param int $lastPageId
     * @param int $limit
     * @return array
     */
    protected function _getSearchablePages($storeId, $pageIds = null, $lastPageId = 0, $limit = 100)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(
                array('p' => $this->getTable('cms/page')),
                array('page_id', 'title', 'identifier', 'content'))
            ->joinInner(
                array('store' => $this->getTable('cms/page_store')),
                $this->_getReadAdapter()->quoteInto('store.page_id=p.page_id AND (store.store_id=? OR store.store_id=0)', $storeId),
                array()
            );

        if (!is_null($pageIds)) {
            $select->where('p.page_id IN(?)', $pageIds);
        }

        $select->where('p.is_active');
        $select->where('p.page_id>?', $lastPageId)
            ->limit($limit)
            ->order('p.page_id');
        return $this->_getReadAdapter()->fetchAll($select);
    }

    /**
     * Save Multiply Page indexes
     *
     * @param int $storeId
     * @param array $pageIndexes
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     */
    protected function _savePageIndexes($storeId, $pageIndexes)
    {
        $values = array();
        $bind   = array();
        foreach ($pageIndexes as $pageId => &$index) {
            $values[] = sprintf('(%s,%s,%s)',
                $this->_getWriteAdapter()->quoteInto('?', $pageId),
                $this->_getWriteAdapter()->quoteInto('?', $storeId),
                '?'
            );
            $bind[] = $index;
        }

        if ($values) {
            $sql = "REPLACE INTO `{$this->getMainTable()}` VALUES"
                . join(',', $values);
            $this->_getWriteAdapter()->query($sql, $bind);
        }

        return $this;
    }

    /**
     * Delete search index data for store
     *
     * @param int $storeId Store View Id
     * @param int $pageId Page Entity Id
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     */
    public function cleanIndex($storeId = null, $pageId = null)
    {
        $where = array();

        if (!is_null($storeId)) {
            $where[] = $this->_getWriteAdapter()->quoteInto('store_id=?', $storeId);
        }
        if (!is_null($pageId)) {
            $where[] = $this->_getWriteAdapter()->quoteInto('page_id IN(?)', $pageId);
        }

        $this->_getWriteAdapter()->delete($this->getMainTable(), $where);
        return $this;
    }

    /**
     * Reset search results
     *
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     * @TODO
     */
    public function resetSearchResults()
    {
        $this->beginTransaction();
        try {
            $this->_getWriteAdapter()->update($this->getTable('catalogsearch/search_query'), array('is_cmsprocessed' => 0));
            $this->_getWriteAdapter()->query("TRUNCATE TABLE {$this->getTable('cmssearch/result')}");

            $this->commit();
        }
        catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }

        Mage::dispatchEvent('cmssearch_reset_search_result');

        return $this;
	}

     /**
     * Prepare results for query
     *
     * @param Tangkoko_CmsSearch_Model_Fulltext $object
     * @param string $queryText
     * @param Mage_CatalogSearch_Model_Query $query
     * @return Tangkoko_CmsSearch_Model_Mysql4_Fulltext
     */
    public function prepareResult($object, $queryText, $query)
    {	
    	if (!$query->getIsCmsprocessed()) {
            $searchType = $object->getSearchType($query->getStoreId());

            $stringHelper = Mage::helper('core/string');
            /* @var $stringHelper Mage_Core_Helper_String */

            $bind = array(
                ':query'     => $queryText
            );
            $like = array();
            $unLike = array();

            $fulltextCond   = '';
            $likeCond       = '';
            $separateCond   = '';

            if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_LIKE
                || $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE) {
                $words = $stringHelper->splitWords($queryText, true, $query->getMaxQueryWords());
                $likeI = 0;
                foreach ($words as $word) {
                    $like[] = '`data_index` LIKE :likew' . $likeI;
                    $bind[':likew' . $likeI] = '%' . $word . '%';    
                    $likeI ++;
                }
                if ($like) {
                    $likeCond = '(' . join(' AND ',$like) . ')';
                }
            }
            if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_FULLTEXT
                || $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE) {
                $fulltextCond = 'MATCH (`data_index`) AGAINST (:query IN BOOLEAN MODE)';
            }
            if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE && $likeCond) {
                $separateCond = ' OR ';
            }

            $sql = sprintf("REPLACE INTO `{$this->getTable('cmssearch/result')}` "
                . "(SELECT '%d', `page_id`, MATCH (`data_index`) AGAINST (:query IN BOOLEAN MODE) "
                . "FROM `{$this->getMainTable()}` WHERE (%s%s%s) AND `store_id`='%d')",
                $query->getId(),
                $fulltextCond,
                $separateCond,
                $likeCond,
                $query->getStoreId()
            );
            $this->_getWriteAdapter()->query($sql, $bind);
            $query->setIsCmsprocessed(1);
        }

        return $this;
    }

}