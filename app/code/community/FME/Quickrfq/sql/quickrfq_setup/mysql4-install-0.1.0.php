<?php

/**
 * Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package    FME_Quickrfq
 * @author     Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 * 
 * campi eliminati:
 * `kjedekunde` varchar(255) NOT NULL default '',
 * `innsender` varchar(255) NOT NULL default '',
 * `estimertomsening` varchar(255) NOT NULL default '',
 * `kredittgrense` varchar(255) NOT NULL default '',
 * `generellrabattbetingelse` varchar(255) NOT NULL default '',
 * `betalingsbetingelse` varchar(255) NOT NULL default '',
 * `erdettergjort` varchar(255) NOT NULL default '',
 * `samlefakturer` varchar(255) NOT NULL default '',
 * `kundensbransje` varchar(255) NOT NULL default '',
 * `kundenslokalebyggvarekontakt` varchar(255) NOT NULL default '',
 * `spesiellerestriksjonerforvareuttak` varchar(255) NOT NULL default '',
 */

$installer = $this;
$installer->startSetup();
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('quickrfq')};
CREATE TABLE {$this->getTable('quickrfq')} (
  `quickrfq_id` int(11) unsigned NOT NULL auto_increment,
  `status` varchar(255) NOT NULL default 'No',
  `foretak` varchar(255) NOT NULL default '',
  `privatkunde` varchar(255) NOT NULL default '',
  `navn` varchar(255) NOT NULL default '',
  `tlf1` varchar(255) NOT NULL default '',
  `adresse` varchar(255) NOT NULL default '',
  `tlf2` varchar(255) NOT NULL default '',
  `postboks` varchar(255) NOT NULL default '',
  `mobil` varchar(255) NOT NULL default '',
  `postnummer` varchar(255) NOT NULL default '',
  `sted` varchar(255) NOT NULL default '',
  `faksnr` varchar(255) NOT NULL default '',
  `foretaksform` varchar(255) NOT NULL default '',
  `mailadr` varchar(255) NOT NULL default '',
  `kontaktpersonmobiltlf` varchar(255) NOT NULL default '',
  `kontaktpersonstilling` varchar(255) NOT NULL default '',
  `osnketkreditt` varchar(255) NOT NULL default '',
  `notes`  text NOT NULL default '',
  `create_date` date NOT NULL,
  `update_date` date NULL,
  PRIMARY KEY (`quickrfq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
$installer->setConfigData('quickrfq/email/recipient','someone@example.com');
$installer->setConfigData('quickrfq/option/enable','1');
$installer->setConfigData('quickrfq/upload/allow','jpg,doc,docx,pdf');
$installer->setConfigData('quickrfq/option/date','5');

$installer->endSetup();