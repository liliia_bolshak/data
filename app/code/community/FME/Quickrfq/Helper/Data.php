<?php

 /**
 * Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package	FME_Quickrfq
 * @author	 Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 */
class FME_Quickrfq_Helper_Data extends Mage_Core_Helper_Abstract
{
	private $_countries = null;

	public function getUserName()
	{
		if (!Mage::getSingleton('customer/session')->isLoggedIn()) return '';
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		return trim($customer->getFirstname());
	}

	public function getUserSurname()
	{
		if (!Mage::getSingleton('customer/session')->isLoggedIn()) return '';
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		return trim($customer->getLastname());
	}

	public function getUserEmail()
	{
		if (!Mage::getSingleton('customer/session')->isLoggedIn()) return '';
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		return $customer->getEmail();
	}

	public function getCountries()
	{
		if (is_null($this->_countries))
		{
			$countries = Mage::getResourceModel('directory/country_collection')
				->loadData()
				->toOptionArray(false);
			$this->_countries = array();
			foreach ($countries as $country) {
				$this->_countries[$country['value']] = $country['label'];
			}
		}
		return $this->_countries;
	}

}