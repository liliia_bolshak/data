<?php

 /* Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package	Quickrfq
 * @author	 Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 */
class FME_Quickrfq_IndexController extends Mage_Core_Controller_Front_Action
{
	const XML_PATH_EMAIL_RECIPIENT = 'quickrfq/email/recipient';
	const XML_PATH_EMAIL_SENDER    = 'quickrfq/email/sender';
	const XML_PATH_EMAIL_TEMPLATE  = 'quickrfq/email/template';
	const XML_PATH_ENABLED         = 'quickrfq/option/enable';
	const XML_PATH_UPLOAD          = 'quickrfq/upload/allow';
	const XML_PATH_SUBJECT         = 'quickrfq/email_reply/subject';
	const XML_PATH_BODY            = 'quickrfq/email_reply/body';

	public function preDispatch()
	{
		parent::preDispatch();
		if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) )
		{
			Mage::getSingleton('core/session')->addError(
				Mage::helper('quickrfq')->__('Sorry This Feature is disabled temporarily')
			);
			$this->norouteAction();
		}
	}

	public function indexAction()
	{
		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setTitle($this->__('Bli proff kunde hos Norfloor'));
		$this->renderLayout();
	}

	/**
	 * this function validates and saves posted rfq data from frontend
	 * and also sends the emails to admin and the client who has
	 * requested the quote
	 */
	public function postAction()
	{
		// $rfqdata saves the posted data as an array
		$rfqdata = $this->getRequest()->getPost();
		$params = $this->getRequest()->getParams();
		// check whether any data posted or not
		if ( $rfqdata )
		{
			// check servers date and save it as create date in database
			$todaydate=date("Y-m-d");
			$rfqdata['create_date']=$todaydate;
			$translate = Mage::getSingleton('core/translate');
			/* @var $translate Mage_Core_Model_Translate */
			$translate->setTranslateInline(false);
			try {
				//$postObject is to set all the posted data to send emails
				$postObject = new Varien_Object();
				//these variables are set default value as false
				//further they will be used as to check which required fields
				//are not validating
				$required_error = false;
				$phone_error = false;
				$email_error = false;
				$captcha_error = false;
				$hideit_error = false;
				// zend validator validates the required fields
				if (!Zend_Validate::is(trim($rfqdata['foretak']) , 'NotEmpty'))								$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['privatkunde']) , 'NotEmpty'))							$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['kjedekunde']) , 'NotEmpty'))							$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['innsender']) , 'NotEmpty'))							$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['navn']) , 'NotEmpty'))								$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['tlf1']) , 'NotEmpty'))								$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['adresse']) , 'NotEmpty'))								$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['tlf2']) , 'NotEmpty'))								$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['postboks']) , 'NotEmpty'))							$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['mobil']) , 'NotEmpty'))								$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['postnummer']) , 'NotEmpty'))							$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['sted']) , 'NotEmpty'))								$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['faksnr']) , 'NotEmpty'))								$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['foretaksform']) , 'NotEmpty'))						$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['mailadr']) , 'NotEmpty'))								$required_error = true;
				elseif (!Zend_Validate::is(trim($rfqdata['mailadr']) , 'EmailAddress'))						$email_error = true;
				if (!Zend_Validate::is(trim($rfqdata['kontaktpersonmobiltlf']) , 'NotEmpty'))				$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['estimertomsening']) , 'NotEmpty'))					$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['kredittgrense']) , 'NotEmpty'))						$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['generellrabattbetingelse']) , 'NotEmpty'))			$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['betalingsbetingelse']) , 'NotEmpty'))					$required_error = true;
				//if (!in_array(trim($rfqdata['erdettergjort']), array('yes', 'no')))							$required_error = true;
				//if (!in_array(trim($rfqdata['samlefakturer']), array('yes', 'no')))							$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['kundensbransje']) , 'NotEmpty'))						$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['kundenslokalebyggvarekontakt']) , 'NotEmpty'))		$required_error = true;
				//if (!Zend_Validate::is(trim($rfqdata['spesiellerestriksjonerforvareuttak']) , 'NotEmpty'))	$required_error = true;
				if (!Zend_Validate::is(trim($rfqdata['security_code']) , 'NotEmpty'))	$captcha_error = true;
				if (Zend_Validate::is(trim($rfqdata['hideit']), 'NotEmpty')) 			$hideit_error = true;
				//if error returned by zend validator then add an error message
			 	if ($required_error)
			 	{
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter all required fields'));
				}
				if ($captcha_error)
				{
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter the Security Code'));
				}
				if ($phone_error)
				{
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter a valid Phone number'));
				}
				if ($email_error)
				{
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Please Enter a valid Email Address'));
				}
				// if any error occurs then throw an exception so not to move
				// from this forward this if condition
				if ($hideit_error || $required_error  || $phone_error  || $email_error || $captcha_error) throw new Exception();
				if (!$captcha_error && $rfqdata['security_code'] != $rfqdata['captacha_code'])
				{
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Sorry The Security Code You Entered Was Incorrect'));
					throw new Exception();
				}
				// if any any none required field is left empty then use a
				// default text of "Not Mentioned" in it instead of empty field
				// in database.
				foreach($rfqdata as $key => $value)
				{
					if($key=='hideit') break;
					if(empty($value) && $key != 'notes') $rfqdata[$key] = '--';
					if ($key == 'country')
					{
						$_countries = Mage::helper('quickrfq')->getCountries();
						$rfqdata['countryLabel'] = $_countries[$value];
					}
				}

				$postObject->setData($rfqdata);
				// Now after all workings save the data to DB
				$model = Mage::getModel('quickrfq/quickrfq');
				$model->setData($rfqdata)->setId($this->getRequest()->getParam('id'));
				try {
					$model->save();
					// Send all data to the email address saved at backend
					// (system/configuration/{quickrfq}configurations->email setup options
					// for the rfq reciever)
					$mailTemplate = Mage::getModel('core/email_template');
					 /* @var $mailTemplate Mage_Core_Model_Email_Template */
					$mailTemplate->setDesignConfig(array('area' => 'frontend'))
						->setReplyTo($rfqdata['mailadr'])
						->sendTransactional(
							Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
							Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
							Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
							null,
							array('data' => $postObject)
						);
					if (!$mailTemplate->getSentSuccess()) throw new Exception();
					// Send an email to the client also and get its email subject and
					// the body from backend
					// (system/configuration/{quickrfq}configurations->Reply to customer)

					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addSuccess(Mage::helper('quickrfq')->__('Your Request was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
					$this->_redirect('*/*/');
					return;
				}
				catch (Exception $e)
				{
					/* ERROR DEBUGGIN ---------------------------------------------
					$this->_sendDotcomANotify($e, __LINE__);
					// ERROR DEBUGGIN --------------------------------------------- */
					$translate->setTranslateInline(true);
					Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Your request cannont be sent this time, Please, try again later'));
					var_dump($e);
					$this->_redirect('*/*/');
					return;
				}
			}
			catch (Exception $e)
			{
				/* ERROR DEBUGGIN ---------------------------------------------
				$this->_sendDotcomANotify($e, __LINE__);
				// ERROR DEBUGGIN --------------------------------------------- */
				$translate->setTranslateInline(true);
				Mage::getSingleton('core/session')->addError(Mage::helper('quickrfq')->__('Unable to submit your request. Please, try again later'));
					var_dump($e);
				$this->_redirect('*/*/');
				return;
			}
		}
		else
		{
			$this->_redirect('*/*/');
		}
	}

	/**
	 * @param Exception $error
	 * @param int $line
	 */
	private function _sendDotcomANotify($error, $line = null)
	{
		$trinline = Mage::getSingleton('core/translate')->getTranslateInline();
		Mage::getSingleton('core/translate')->setTranslateInline(false);

		Mage::log($error->getMessage());
		Mage::log(mageDebugBacktrace(true, true, true));

		$mail = new Zend_Mail();
		$body = $error->getMessage();
		$body .= "\r\n\r\n";
		$body .= $error->getTraceAsString();
		$mail->setBodyText($body);
		$mail->setFrom('infor@olisails.it');
		$mail->addTo('apeternelli@dotcom.ts.it');
		$mail->setSubject('ERROR DUMP '.$line);
		try {
			$mail->send();
		}
		catch(Exception $e) {
			Mage::log('Errore nella spedizione emails '.$line);
			Mage::log($e->getMessage());
		}
		Mage::getSingleton('core/translate')->setTranslateInline($trinline);
	}
}