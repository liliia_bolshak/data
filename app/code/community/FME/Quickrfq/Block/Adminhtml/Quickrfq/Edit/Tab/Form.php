<?php

 /**
 * Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category FME
 * @package	FME_Quickrfq
 * @author	 Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyrightCopyright 2010 © free-magentoextensions.com All right reserved
 */

class FME_Quickrfq_Block_Adminhtml_Quickrfq_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$id= $this->getRequest()->getParam('id');
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('quickrfq_form', array('legend' => Mage::helper('quickrfq')->__('Senders information')));
		$_curRFQ = Mage::getModel('quickrfq/quickrfq')->load($id);

		/* get the status of this rfq */
		$status = $_curRFQ->getStatus();
		$status = empty($status) ? 'No':$status;
		$statuses = Mage::getSingleton('quickrfq/status')->getOptionArray();
		/* this variable will set the saved status of this rfq */
		$origin = $statuses[$status];

		/* display the status of this rfq */
		$fieldset->addField('status', 'select', array(
			'label' => Mage::helper('quickrfq')->__('Change Status'),
			'name' => 'status',
			'values' => $statuses,
			'after_element_html' => '<small>' . Mage::helper('quickrfq')->__('......Originally It was ') . '</small><span Style="text-decoration: none;">'. $origin .'</span>',
		));

		/* display the date of creation of this rfq */
		$fieldset->addField('create_date', 'label', array(
			'label' => Mage::helper('quickrfq')->__('Quote requested'),
			'required' => false,
			'name' => 'create_date',
			'after_element_html' => '<small>' . Mage::helper('quickrfq')->__('......YYYY-MM-DD') . '</small>',
		));

		/* display the date of last update of this rfq */
		$fieldset->addField('update_date', 'label', array(
			'label' => Mage::helper('quickrfq')->__('Quote last updated'),
			'required' => false,
			'name' => 'update_date',
			'after_element_html' => '<small>' . Mage::helper('quickrfq')->__('......YYYY-MM-DD') . '</small>',
		));

		$fieldset->addField('foretak', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Foretak Org. nr'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'foretak',
		));

		$fieldset->addField('privatkunde', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Privatkunde Fødslsnr'),
			//'class' => 'required-entry',
			'required' => false,
			'name' => 'privatkunde',
		));

		/*$fieldset->addField('kjedekunde', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Kjedekunde Kjede nr'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'kjedekunde',
		));

		$fieldset->addField('innsender', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Innsender Avd. nr'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'innsender',
		));*/

		$fieldset->addField('navn', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Navn'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'navn',
		));

		$fieldset->addField('tlf1', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Tlf 1'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'tlf1',
		));

		$fieldset->addField('adresse', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Adresse'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'adresse',
		));

		$fieldset->addField('tlf2', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Tlf 2'),
			//'class' => 'required-entry',
			'required' => false,
			'name' => 'tlf2',
		));

		$fieldset->addField('postboks', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Postboks'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'postboks',
		));

		$fieldset->addField('mobil', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Mobil'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'mobil',
		));

		$fieldset->addField('postnummer', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Postnummer'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'postnummer',
		));

		$fieldset->addField('sted', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Sted'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'sted',
		));

		$fieldset->addField('faksnr', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Faksnr'),
			//'class' => 'required-entry',
			'required' => false,
			'name' => 'faksnr',
		));

		$fieldset->addField('foretaksform', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Foretaksform'),
			//'class' => 'required-entry',
			'required' => false,
			'name' => 'foretaksform',
		));

		$fieldset->addField('mailadr', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Mailadr'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'mailadr',
		));

		$fieldset->addField('kontaktpersonmobiltlf', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Kontaktperson'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'kontaktpersonmobiltlf',
		));
		
		$fieldset->addField('kontaktpersonstilling', 'text', array(
					'label' => Mage::helper('quickrfq')->__('Kontaktperson Stilling'),
					//'class' => 'required-entry',
					'required' => false,
					'name' => 'kontaktpersonstilling',
		));
		
		$fieldset->addField('osnketkreditt', 'text', array(
					'label' => Mage::helper('quickrfq')->__('Øsnket Kreditt'),
					//'class' => 'required-entry',
					'required' => false,
					'name' => 'osnketkreditt',
		));

		/*$fieldset->addField('estimertomsening', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Estimert omsening (kr)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'estimertomsening',
		));

		$fieldset->addField('kredittgrense', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Kredittgrense (kr)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'kredittgrense',
		));

		$fieldset->addField('generellrabattbetingelse', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Generell rabattbetingelse (%)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'generellrabattbetingelse',
		));

		$fieldset->addField('betalingsbetingelse', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Betalingsbetingelse (dgr)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'betalingsbetingelse',
		));

		$fieldset->addField('erdettergjort', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Er dette gjort'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'erdettergjort',
		));

		$fieldset->addField('samlefakturer', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Samlefakturer'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'samlefakturer',
		));

		$fieldset->addField('kundensbransje', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Kundens bransje'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'kundensbransje',
		));

		$fieldset->addField('kundenslokalebyggvarekontakt', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Kundens lokale byggvarekontakt'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'kundenslokalebyggvarekontakt',
		));

		$fieldset->addField('spesiellerestriksjonerforvareuttak', 'text', array(
			'label' => Mage::helper('quickrfq')->__('Spesielle restriksjoner for vareuttak (ID-kort, personer m.m)'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'spesiellerestriksjonerforvareuttak',
		));*/

		$fieldset->addField('notes', 'editor', array(
			'name' => 'notes',
			'label' => Mage::helper('quickrfq')->__('Notes'),
			'style' => 'width:600px; height:500px;',
			'wysiwyg' => false,
			'required' => false,
		));

		if (Mage::getSingleton('adminhtml/session')->getQuickrfqData())
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getQuickrfqData());
			Mage::getSingleton('adminhtml/session')->setQuickrfqData(null);
		} elseif ( Mage::registry('quickrfq_data') ) {
			$form->setValues(Mage::registry('quickrfq_data')->getData());
		}
		return parent::_prepareForm();
	}
}