<?php
/**
 * Quickrfq extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   FME
 * @package	FME_Quickrfq
 * @author	 Malik Tahir Mehmood<malik.tahir786@gmail.com>
 * @copyright  Copyright 2010 © free-magentoextensions.com All right reserved
 */

class FME_Quickrfq_Block_Quickrfq extends Mage_Core_Block_Template
{
	const XML_PATH_UPLOAD = 'quickrfq/upload/allow';
	const XML_PATH_DATE = 'quickrfq/option/date';

	protected function _prepareLayout()
	{
		if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))
		{
			$breadcrumbs->addCrumb('home', array('label'=>'Home', 'title'=>'Home', 'link'=>Mage::getBaseUrl()))
				->addCrumb('collaboration-request', array('label'=>$this->__('Bli proff kunde hos Norfloor'), 'title'=>$this->__('Bli proff kunde hos Norfloor')));
		}
		return parent::_prepareLayout();
	}

	public function getQuickrfq()
	{
		if (!$this->hasData('quickrfq')) {
			$this->setData('quickrfq', Mage::registry('quickrfq'));
		}
		return $this->getData('quickrfq');
	}

	/**
	 * +---------------------------------------------------------------+
	 * | this function returns the action of the form while submitting |
	 * +---------------------------------------------------------------+
	 */
	public function getPostActionUrl()
	{
		return Mage::getUrl('quickrfq/index/post');
	}

	/**
	 * +--------------------------------------------------------------+
	 * | this function draws the path of FME_quickrfq folder on local |
	 * | and returns the path to the frontend from where it is called |
	 * +--------------------------------------------------------------+
	 */
	public function getSecureImageUrl()
	{
		$path = Mage::getBaseUrl('media');
		$pos =strripos($path,'media');
		$apppath =substr($path,0, $pos) . 'FME_quickrfq' . DS . 'captcha/';
		return $apppath;
	}

	/**
	 * +-----------------------------------------------------------------+
	 * | this function gets a new unique value by sending request to the |
	 * | assign_rand_value() function which returns a character and it   |
	 * | adds the character in its variable and returns to the form at   |
	 * | frontend                                                        |
	 * +-----------------------------------------------------------------+
	 */
	function getNewrandCode($length)
	{
		if($length>0)
		{
			$rand_id="";
			for($i=1; $i<=$length; $i++)
			{
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1,36);
				$rand_id .= $this->assign_rand_value($num);
			}
		}
		return $rand_id;
	}

	function assign_rand_value($num)
	{
		$map = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
		return $map[$num];
	}
}