<?php
/**
 * Hachmang & Otten pagecache module
 *
 * DISCLAIMER
 *
 * Alle intellectuele en andere eigendomsrechten met betrekking tot deze module
 * behoren toe aan Hachmang & Otten en worden uitdrukkelijk voorbehouden. � Copyright 2010
 * 
 * Deze module mag onder geen omstandigheden worden gebruikt door 3e partijen,
 * tenzij door Hachmang & Otten anders bepaald. *
 */

class HO_Analyticsdash_Model_Processor extends HO_Analyticsdash_Model_Api
{
	const XML_PATH_SETTINGS_PROFILEID		= 'analyticsdash/settings/profileid';
    const XML_PATH_SETTINGS_ACCOUNT    		= 'analyticsdash/settings/account';
    const XML_PATH_SETTINGS_PASSWORD    	= 'analyticsdash/settings/password';
    const XML_PATH_SETTINGS_CACHELIFETIME   = 'analyticsdash/settings/cachelifetime';

	function __construct()
	{
		parent::__construct();	
	}

    public function getToday() {
   		$this->requestReportData(
			Mage::getStoreConfig(self::XML_PATH_SETTINGS_PROFILEID),
			array('date'),
			array('pageviews','visits', 'timeOnSite'),
			'','',
			date('Y-m-d'),
			date('Y-m-d')
		);
		
		return $this;
	}
	
	public function getYesterday()
	{
		$this->requestReportData(
			Mage::getStoreConfig(self::XML_PATH_SETTINGS_PROFILEID),
			array('date'),
			array('pageviews','visits', 'timeOnSite'),
			'','',
			date('Y-m-d', strtotime('yesterday')),
			date('Y-m-d', strtotime('yesterday'))
		);
		
		return $this;		
	}  
	
	public function getLastmonth()
	{
		$this->requestReportData(
		Mage::getStoreConfig(self::XML_PATH_SETTINGS_PROFILEID),
		array('date'),
		array('pageviews','visits', 'newVisits', 'timeOnSite', 'bounces', 'entrances'),
		'date', '',
		date('Y-m-d', strtotime('31 days ago')),
		date('Y-m-d', strtotime('yesterday'))
		);  
		
		return $this;
	}
	
	public function getTopContent()
	{
		$this->requestReportData(
		Mage::getStoreConfig(self::XML_PATH_SETTINGS_PROFILEID),
		array('hostname', 'pagePath'),
		array('pageviews'),
		'-pageviews', '',
		date('Y-m-d', strtotime('31 days ago')),
		date('Y-m-d', strtotime('yesterday')),
		null, 10
		);
		return $this;
	}
	
	public function getTopReferrers()
	{
		$this->requestReportData(
		Mage::getStoreConfig(self::XML_PATH_SETTINGS_PROFILEID),
		array('source', 'referralPath', 'medium'),
		array('visits'),
		'-visits', '',
		date('Y-m-d', strtotime('31 days ago')),
		date('Y-m-d', strtotime('yesterday')),
		null, 10
		);
		return $this;
	}

}

