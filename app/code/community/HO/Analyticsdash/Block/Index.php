<?php

class HO_Analyticsdash_Block_Index extends Mage_Adminhtml_Block_Template
{
	public $auth_token;
	
    public function _construct()
    {   	
        parent::_construct();
        $this->setTemplate('Analyticsdash/index.phtml');

    	$this->addData(array(
            'cache_lifetime'    => Mage::getStoreConfig(HO_Analyticsdash_Model_Processor::XML_PATH_SETTINGS_CACHELIFETIME)*60,
            'cache_tags'        => array('ho_analyticsdash', Mage::getStoreConfig(HO_Analyticsdash_Model_Processor::XML_PATH_SETTINGS_PROFILEID)),
        ));
    }
    
    public function getAuthToken(){
    	if(isset($this->auth_token)){
    		return $this->auth_token;
    	}else{
			try { 
				return $this->auth_token = Mage::getModel('analyticsdash/processor')->authenticateUser(Mage::getStoreConfig(HO_Analyticsdash_Model_Processor::XML_PATH_SETTINGS_ACCOUNT),Mage::getStoreConfig(HO_Analyticsdash_Model_Processor::XML_PATH_SETTINGS_PASSWORD))->getAuthToken();	
			} catch (Exception $e) { 
			   echo '<ul class="messages"><li class="error-msg"><ul><li><span>'.$e->getMessage().'</span></li></ul></li></ul>';
			    exit;
			} 	
    	}
    }
    
    public function getProfile()
    {
    	return Mage::getStoreConfig(HO_Analyticsdash_Model_Processor::XML_PATH_SETTINGS_PROFILEID);
    }
    
    
    public function getToday()
    {
    	$processor = Mage::getModel('analyticsdash/processor')->setAuthToken($this->getAuthToken());
    	try { 
				return $processor->getToday();
			} catch (Exception $e) {  
			   echo '<ul class="messages"><li class="error-msg"><ul><li><span>'.$e->getMessage().'</span></li></ul></li></ul>';
			   exit;
			} 	
    	
    	}
    
    public function getYesterday()
    {
    	$processor = Mage::getModel('analyticsdash/processor')->setAuthToken($this->getAuthToken());
    	return $processor->getYesterday();
    }
    
    public function getLastmonth ()
    {
    	$processor = Mage::getModel('analyticsdash/processor')->setAuthToken($this->getAuthToken());
    	return $processor->getLastMonth();
    }
    
    public function getTopContent ()
    {
    	$processor = Mage::getModel('analyticsdash/processor')->setAuthToken($this->getAuthToken());
		return $processor->getTopContent()->getResults();
    }
    
    public function getTopReferrers ()
    {
    	$processor = Mage::getModel('analyticsdash/processor')->setAuthToken($this->getAuthToken());
		return $processor->getTopReferrers()->getResults();
    }
    
    public function getVisits()
    {
    	return $this->processor->getVisits();
    }
    
    public function getPageviews()
    {
    	return $this->processor->getPageviews();
    } 
    
    public function getAvgPages($pageviews, $visits)
    {
    	return ($pageviews > 0 && $visits > 0) ? round($pageviews / $visits, 2) : 0;
    }
    
    public function getAvgTime($seconds, $visits)
    {
		if($seconds > 0 && $visits > 0)
		{
			$avg_secs = $seconds / $visits;
			$hours = floor($avg_secs / (60 * 60));
			$minutes = floor(($avg_secs - ($hours * 60 * 60)) / 60);
			$seconds = $avg_secs - ($minutes * 60) - ($hours * 60 * 60);
			return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
		}
		else
		{
			return '00:00:00';
		}
   	}
   	
   	public function getDatespan()
   	{
   		return date('j F Y', strtotime('31 days ago')).' &ndash; '.date('j F Y', strtotime('yesterday'));
   	}
   	
   	public function getBouncesPercentage($bounces,$entrances)
   	{
   		if($bounces > 0 && $bounces > 0){
			return round( ($bounces / $entrances) * 100, 2 ).'%';
		}
		else
		{
			return '0%';
		}
   	}
   	
   	public function getNewVisits()
   	{
   		return $this->processor->getnewVisits();
   	}
   	
   	public function graphicSparkline($data_array, $metric)
	{
		$max = 0; $stats = '';
		foreach($data_array as $result)
		{
			switch($metric) {
				case "pageviews":
					$datapoint = $result->getPageviews();
					break;
				case "visits":	
					$datapoint = $result->getVisits();
					break;
				case "time":
					$datapoint = $result->getTimeOnSite();
					break;
				case "avgpages":
					$datapoint = ($result->getVisits() > 0 && $result->getPageViews() > 0) ? $result->getPageviews() / $result->getVisits() : 0;
					break;
				case "bouncerate":
					$datapoint = ($result->getEntrances() > 0 && $result->getBounces() > 0) ? $result->getBounces() / $result->getEntrances() : 0;
					break;
				case "newvisits":
					$datapoint =  ($result->getNewVisits() > 0 && $result->getVisits() > 0) ? $result->getNewVisits() / $result->getVisits() : 0;
					break;
			}		
			
			if($max < $datapoint)
			{
				$max = $datapoint;
			}
			$stats .= $datapoint . ',';
		}
		$stats = rtrim($stats, ',');
		
		return '<img src="http://chart.apis.google.com/chart?cht=ls&amp;chs=100x20&amp;chm=B,e6f2fa,0,0.0,0.0&amp;chco=0077cc&amp;chd=t:'.$stats.'&amp;chds=0,'.$max.'" alt="" />';
	}	
	

     
}