<?php

class HO_Analyticsdash_IndexController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();        
		$this->_addContent($this->getLayout()->createBlock('analyticsdash/index'));
		$this->_setActiveMenu('analyticsdash');
        $this->renderLayout();
    }	
    
}