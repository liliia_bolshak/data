<?php
/**
 * Helper file: Data
 * Mostly fetches module settings data from System -> Configuration
 * General functionalities used frequently in the module is written here
 *
 * @category   Sf_
 * @package    Sf_Bnm
 * @author     Sf_ Magento Developer <magento.developer@javra.com>  
 * @version    0.1.0           
 * @changed date : 06/03/2010 4:50 PM
 */

class MO_Bnm_Helper_Data extends Mage_Core_Helper_Abstract
{
	const STORE_CONFIG_BESTSELLING_HOMEPAGE = 'bestsellingproduct/bestselling/bestselling_homepage';
	const STORE_CONFIG_BESTSELLING_SIDEBAR = 'bestsellingproduct/bestselling/bestselling_sidebar';
	
	const STORE_CONFIG_NEWPRODUCT_HOMEPAGE = 'bestsellingproduct/newproduct/newproduct_homepage';
	const STORE_CONFIG_NEWPRODUCT_SIDEBAR = 'bestsellingproduct/newproduct/newproduct_sidebar';
	
	const STORE_CONFIG_MOSTVIEWED_HOMEPAGE = 'bestsellingproduct/mostviewed/mostviewed_homepage';
	const STORE_CONFIG_MOSTVIEWED_SIDEBAR = 'bestsellingproduct/mostviewed/mostviewed_sidebar';    
    	
		
	/**
	 * Get number of bestselling products to be displayed in homepage
	 *
	 * @return integer
	 */
	public function getBestsellingHomepage()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_BESTSELLING_HOMEPAGE);
        return $num >= 0 ? $num : 3;
    }
	
	/**
	 * Get number of bestselling products to be displayed in sidebar
	 *
	 * @return integer
	 */
	public function getBestsellingSidebar()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_BESTSELLING_SIDEBAR);
        return $num >= 0 ? $num : 5;
    }
	
	/**
	 * Get number of new products to be displayed in homepage
	 *
	 * @return integer
	 */
	public function getNewproductHomepage()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_NEWPRODUCT_HOMEPAGE);
        return $num >= 0 ? $num : 3;
    }
	
	/**
	 * Get number of new products to be displayed in sidebar
	 *
	 * @return integer
	 */
	public function getNewproductSidebar()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_NEWPRODUCT_SIDEBAR);
        return $num >= 0 ? $num : 5;
    }	
	
	/**
	 * Get number of most viewed products to be displayed in homepage
	 *
	 * @return integer
	 */
	public function getMostviewedHomepage()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_MOSTVIEWED_HOMEPAGE);
        return $num >= 0 ? $num : 3;
    }
	
	/**
	 * Get number of most viewed products to be displayed in sidebar
	 *
	 * @return integer
	 */
	public function getMostviewedSidebar()
    {
        $num = (int)Mage::getStoreConfig(self::STORE_CONFIG_MOSTVIEWED_SIDEBAR);
        return $num >= 0 ? $num : 5;
    }	
}