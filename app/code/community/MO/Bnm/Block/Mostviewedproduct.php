<?php
/**
 * Most viewed products block
 *
 * @category   MO
 * @package    MO_Bnm
 * @author     Magento Oursourcing Team <info@magento-outsourcing.com>
 */ 
class MO_Bnm_Block_Mostviewedproduct extends Mage_Catalog_Block_Product_Abstract
{
    public function getProductsLimit() 
    { 
		$count = Mage::helper('bestsellingproduct')->getMostviewedSidebar();
		if($count) 		
			return $count;	
			
		/* if($this->count)
			return $this->count; */
        
		return 4;
    }	
	
    public function getProductCollection()
    {    	
        $storeId    = Mage::app()->getStore()->getId();       
		
		$today = time()- (60*60*24*35);
		$yesterday = $today - (60*60*24*1);
				
		$from = date("Y-m-d", $yesterday);
		$to = date("Y-m-d", $today);		
		//echo $from."  ".$to; exit;
				
		if(Mage::registry('current_category')) {		
			$products = Mage::getResourceModel('reports/product_collection')
				->addAttributeToSelect('*')
				//->addAttributeToSelect(array('name', 'price', 'small_image', 'short_description', 'description'))
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				//->addViewsCount($from, $to)
				->addViewsCount()
				->addCategoryFilter(Mage::registry('current_category'))
				->setPageSize($this->getProductsLimit()); 
        }
		else {
			$products = Mage::getResourceModel('reports/product_collection')
				->addAttributeToSelect('*')
				//->addAttributeToSelect(array('name', 'price', 'small_image', 'short_description', 'description'))
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				//->addViewsCount($from, $to)
				->addViewsCount()
				->setPageSize($this->getProductsLimit());
		}
		
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
		
		return $products; 				
    }			
} 


	


