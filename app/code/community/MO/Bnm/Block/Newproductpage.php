<?php
/**
 * New products page block
 *
 * @category   MO
 * @package    MO_Bnm
 * @author     Magento Oursourcing Team <info@magento-outsourcing.com>
 */ 
class MO_Bnm_Block_Newproductpage extends Mage_Catalog_Block_Product_Abstract
{
    protected $_productsCount = null;

    const DEFAULT_PRODUCTS_COUNT = 5;

    protected function _beforeToHtml()
    {
    
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        
        $collection = Mage::getResourceModel('catalog/product_collection');
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('news_from_date', array('date' => true, 'to' => $todayDate))
            ->addAttributeToFilter('news_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
            ), 'left')
            ->addAttributeToSort('news_from_date', 'desc')
            //->setPageSize($this->getProductsCount())			
            ->setCurPage(1)
        ;
		
		$this->setToolbar($this->getLayout()->createBlock('catalog/product_list_toolbar', 'Toolbar'));	
		$toolbar = $this->getToolbar();		
		$toolbar->setDefaultOrder('news_from_date')
		->setDefaultDirection('desc')
		->setCollection($collection);
		
		return $this;        
    }

    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
	
	public function getToolbarHtml() {	
		return $this->getToolbar()->_toHtml();
	}
	
	public function getMode() {
		return $this->getToolbar()->getCurrentMode();
	}
	
	public function getProductCollection() {
		return $this->getToolbar()->getCollection();
	} 	
}
