<?php
/**
 * Bestselling products page block
 *
 * @category   MO
 * @package    MO_Bnm
 * @author     Magento Oursourcing Team <info@magento-outsourcing.com>
 */ 
class MO_Bnm_Block_Bestsellingproduct extends Mage_Catalog_Block_Product_Abstract
{
    public function getProductsLimit() 
    { 		
		if($this->displaylocation == 'homepage') {
			$count = Mage::helper('bestsellingproduct')->getBestsellingHomepage();
		}
		else {
			$count = Mage::helper('bestsellingproduct')->getBestsellingSidebar();
		}
		
		if($count) 		
			return $count;		
			
		/* if($this->count)
			return $this->count; */
        
		//return 4;
    }	
	
    public function getProductCollectionInitial()
    {    
		/* $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId(100000005);
		echo "<pre>"; print_r($invoice->getData()); 
		$invoice->capture()->save();
		echo "<pre>"; print_r($invoice->getData()); exit; */
		
		/* $prod = Mage::getModel('catalog/product')->load(198);
		echo "<pre>"; print_r($prod->getStoreIds()); exit; */
		
				
		
        $storeId    = Mage::app()->getStore()->getId();
		
		if(Mage::registry('current_category')) {
			$products = Mage::getResourceModel('reports/product_collection')
				->addOrderedQty()
				->addAttributeToSelect('*')
				//->addAttributeToSelect(array('name', 'price', 'small_image', 'short_description', 'description')) //edit to suit tastes
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				->addAttributeToFilter('status','1')
				//->addAttributeToFilter(array(array('attribute'=>'visibility', 'neq'=>1)))
				->addCategoryFilter(Mage::registry('current_category'))
				->setOrder('ordered_qty', 'desc');
				//->setPageSize(20); //best sellers on top
		}
		else {
			$products = Mage::getResourceModel('reports/product_collection')
				->addOrderedQty()
				->addAttributeToSelect('*')
				//->addAttributeToSelect(array('name', 'price', 'small_image', 'short_description', 'description')) //edit to suit tastes
				->setStoreId($storeId)
				->addStoreFilter($storeId)
				->addAttributeToFilter('status','1')
				//->addAttributeToFilter(array(array('attribute'=>'visibility', 'neq'=>1)))				
				->setOrder('ordered_qty', 'desc');
				//->setPageSize(20); //best sellers on top
		}
		
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
		return $products; 
    }
	
	public function getProductCollection()
    {		
		$storeId = Mage::app()->getStore()->getId();	
		$productCollection = $this->getProductCollectionInitial();
		
		//echo count($productCollection); exit;
		$sameProduct = array(); 
 		$checkedProducts = new Varien_Data_Collection();		
		foreach($productCollection as $key => $prod) { 			
			
			$parentId = $this->getParentId($prod);
			
			if($parentId == '') {
				continue; 
			}
						
			$product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($this->getParentId($prod));			
			
			// if the product is not visible or is disabled
			if(!$product->isVisibleInCatalog()) {							
				continue; 
			}
			
			// if two or more simple products of the same configurable product are ordered
			if(in_array($product->getId(),$sameProduct)) {							
				continue; 
			}
			
			$sameProduct[] = $product->getId();
			
			if (!$checkedProducts->getItemById($parentId)) {
				$checkedProducts->addItem($product);			
			}
			
			if(count($checkedProducts) >= $this->getProductsLimit()) {
				break;
			}
		}
		$productCollection = $checkedProducts; 
		//echo "<pre>"; print_r(count($checkedProducts)); exit; 
		return $productCollection; 

	}	
	
	public function getParentId($product)
	{
		$parentId = '';
				
		// if the product visibility is not set to "Nowhere"
		// i.e. if the product is visible
		if($product->getVisibility() != '1') {
			$parentId = $product->getId(); 
			
			/* $parentIdArray = $product->loadParentProductIds()->getData('parent_product_ids');
			echo $product->getId(); 
			print_r($parentIdArray); 
			echo "***********<br/>"; */
		}		
		else {
			// get parent id if the product is not visible
			// this means that the product is associated with a configurable product
			$parentIdArray = $product->loadParentProductIds()->getData('parent_product_ids');
			
			/* echo $product->getId(); 
			print_r($parentIdArray); 
			echo "***********<br/>"; */
			
			if(!empty($parentIdArray)) {
				$parentId = $parentIdArray[0];
			}
		}		
		return $parentId; 
	}
	
	
    
	
} 


