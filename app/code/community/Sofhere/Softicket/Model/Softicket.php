<?php

class Sofhere_Softicket_Model_SofTicket extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('softicket/softicket');
    }

	
    public function getAllTickets($customerId){

    	$customer = Mage::getModel('customer/customer');
    	if ($customerId) {
    		$customer->load($customerId);
    		$email = $customer->getEmail();
    		
    		// Get softickets for this customer with the email...
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$softicketTable = $resource->getTableName('ticket_tickets');
			
			$select = $read->select()
			   ->from($softicketTable,array('ID','email','subject','status','timestamp', 'cat', 'priority'))
			   ->where('email='."'".$email."'")
			   ->order('timestamp DESC') ;
			return $read->fetchAll($select);
    	}
    	return array();
    }

    
     //save and load methods
	public function save() {
		
		$resource = Mage::getSingleton('core/resource');
		$connection= $resource->getConnection('softicket_write');
		$connection->beginTransaction();

		try {
			$this->_beforeSave();
			if ($this->getID()==0)
				$this->setID(Mage::helper('softicket')->getTicketID());
				
			$query= 'insert into ticket_tickets (ID, name, subject, cat, rep, phone, timestamp, ip, priority, email) VALUES('
				."'".$this->getID()."',"
				."'".$this->getName()."',"
				."'".$this->getSubject()."',"
				."'".$this->getCat()."',"
				."'".$this->getRep()."',"
				."'".$this->getPhone()."',"
				."'".$this->getTimestamp()."',"
				."'".$this->getIp()."',"
				."'".$this->getPriority()."',"
				."'".$this->getEmail()."')";
			
				$connection->query($query);

				$connection->commit();
				$this->_afterSave();
		}
		catch (Exception $e) {
			Mage::log('Exception:'.$e);
			$connection->rollBack();
			throw $e;
		}
		return $this;
	}
	
	public function update() {
		parent::save();
	}

}