<?php

class Sofhere_Softicket_Model_Mysql4_Softicket extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the softicket_id refers to the key field in your database table.
        $this->_init('softicket/softicket', 'ID');
    }
    

}