<?php

class Sofhere_Softicket_Model_Status extends Varien_Object
{
	const STATUS_EMPTY	= '';
    const STATUS_NEW	= 'new';
    const STATUS_ONHOLD	= 'onhold';
    const STATUS_CUSTREPLIED	= 'custreplied';
    const STATUS_AWAITING	= 'awaitingcustomer';
    const STATUS_REOPENED	= 'reopened';
    const STATUS_CLOSED	= 'closed';
    
    
    static public function getOptionArray()
    {
        return array(
            self::STATUS_EMPTY    => Mage::helper('softicket')->__(''),
            self::STATUS_NEW    => Mage::helper('softicket')->__('New'),
            self::STATUS_ONHOLD   => Mage::helper('softicket')->__('On Hold'),
            self::STATUS_CUSTREPLIED   	=> Mage::helper('softicket')->__('Customer Replied'),
            self::STATUS_AWAITING   	=> Mage::helper('softicket')->__('Awaiting Customer'),
            self::STATUS_REOPENED   	=> Mage::helper('softicket')->__('Reopened'),
            self::STATUS_CLOSED   	=> Mage::helper('softicket')->__('Closed'),
        );
    }
}