<?php
/**
 * Sofhere SofTicket Magento Component
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU (3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sofhere.com so we can send you a copy immediately.
 * 
 * @category	design_default
 * @author 		sofhere.com
 * @package		Sofhere_SofTicket
 * @copyright  	Copyright (c) 2008-2009 Sofhere IT Solutions.(http://www.sofhere.com)
 * @version 	0.5 beta
 * @license		http://opensource.org/licenses/gpl-3.0.html GNU GENERAL PUBLIC LICENSE (GNU 3.0) 
 */

class Sofhere_Softicket_Adminhtml_RepController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('softicket/rep')
			->_addBreadcrumb(Mage::helper('softicket')->__('SofTicket'), Mage::helper('softicket')->__('Representative'));
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction();
		$this->renderLayout();
	}
	

	public function editAction() {
		$this->loadLayout();
		$this->_setActiveMenu('softicket/rep');
		$this->_addBreadcrumb(Mage::helper('softicket')->__('SofTicket'), Mage::helper('softicket')->__('Representative'));
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		

		$id = $this->getRequest()->getParam('id');
		// Edit
		if ($id > 0) {
			$admin_model  = Mage::getModel('admin/user')->load($id);
			$email=$admin_model->getEmail();
			$name=$admin_model->getFirstname().' '.$admin_model->getLastname();
			if ($email){
				$admin_model->setName($name)
					->setLblName($name)
					->setLblEmail($admin_model->getEmail())
					->setLblUsername($admin_model->getUsername())
					->setLblPassword($admin_model->getPassword());
					
				$reps= Mage::getModel('softicket/reps')->getCollection()->addFieldToFilter('username',$admin_model->getUsername());
				if ($reps->getItems()){
					foreach ($reps as $rep){
						$admin_model->setSignature($rep->getSignature());
					}
				}else{
					Mage::log('editAction:');
					$reps = Mage::getModel('softicket/reps');
					$reps->setData('name',$name);
					$reps->setData('email',$admin_model->getData('email'));
					$reps->setData('username',$admin_model->getData('username'));
					$reps->setData('password',$admin_model->getData('password'));
					$reps->save();
				}
			}

			// set data for the front.
			Mage::register('rep_data', $admin_model);
		}

		$this->_addContent($this->getLayout()->createBlock('softicket/adminhtml_rep_edit'))
			 ->_addLeft($this->getLayout()->createBlock('softicket/adminhtml_rep_edit_tabs'));
		$this->renderLayout();
	}
 

	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {

		$id=$this->getRequest()->getParam('id');
		$data = $this->getRequest()->getPost();
		Mage::log($data);
		try{
			$admin_model  = Mage::getModel('admin/user')->load($id);
			if ($admin_model && $admin_model->getUsername()){
				$reps= Mage::getModel('softicket/reps')->getCollection()->addFieldToFilter('username',$admin_model->getUsername())->load();
				if ($reps->getItems()){
					foreach ($reps as $rep){
						if ($data['signature'])
							$rep->setData('signature',$data['signature']);
						$rep->save();
					}
				}
			}
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('softicket')->__('Representative was successfully updated'));
			$this->_redirect('*/*/', array('id' => $id));
	
		} catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            $this->_redirect('*/*/', array('id' => $id));
        }	
       
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('softicket/reps');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('softicket')->__('Representative was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $softicketIds = $this->getRequest()->getParam('softicket');
        if(!is_array($softicketIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('softicket')->__('Please select representative(s)'));
        } else {
            try {
                foreach ($softicketIds as $softicketId) {
                    $softicket = Mage::getModel('softicket/reps')->load($softicketId);
                    $softicket->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('softicket')->__(
                        'Total of %d record(s) were successfully deleted', count($softicketIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    
  
    public function exportCsvAction()
    {
        $fileName   = 'softicket.csv';
        $content    = $this->getLayout()->createBlock('softicket/adminhtml_softicket_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'softicket.xml';
        $content    = $this->getLayout()->createBlock('softicket/adminhtml_softicket_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}