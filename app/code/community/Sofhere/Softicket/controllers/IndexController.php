<?php
/**
 * Sofhere SofTicket Magento Component
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU (3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sofhere.com so we can send you a copy immediately.
 * 
 * @category	design_default
 * @author 		sofhere.com
 * @package		Sofhere_SofTicket
 * @copyright  	Copyright (c) 2008-2009 Sofhere IT Solutions.(http://www.sofhere.com)
 * @version 	0.5 beta
 * @license		http://opensource.org/licenses/gpl-3.0.html GNU GENERAL PUBLIC LICENSE (GNU 3.0) 
 */

require 'Sofhere/Softicket/Helper/Ticket.php';

class Sofhere_Softicket_IndexController extends Mage_Core_Controller_Front_Action
{
	
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    public function preDispatch()
    {
        parent::preDispatch();

        if (! $this->_getSession()->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    public function indexAction()
    {
    	$softicketModel = Mage::getModel('softicket/softicket');
		$customerId= $this->_getSession()->getCustomerId();
		$softicket= $softicketModel->getAllTickets($customerId);
		Mage::register('softicket_all', $softicket, true);
		
    	if ((is_null($softicket) == false) && (empty($softicket) == false) ) {
	        $this->loadLayout();
	        $this->_initLayoutMessages('customer/session');
	        $this->renderLayout();
    	}
        else {
            $this->getResponse()->setRedirect(Mage::getUrl('*/*/add'));
        }
    }

    public function addAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
    	if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
	            $navigationBlock->setActive('softicket');
	    }
        $this->renderLayout();
    }
    
    public function editAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
    	if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
	            $navigationBlock->setActive('softicket');
	    }

        $this->renderLayout();
    }

    public function editPostAction()
    {
    	$data= $this->getRequest()->getPost();

    	if ($data && $data['ID'] && $data['ID'] >0 ) {
            try {
            	$repid=$data['rep'];
            	$id=$data['ID'];
            	$answer=$data['answer'];
            	$rep_name='';
		    	if ($repid>0){
		    		$reps  = Mage::getModel('softicket/reps')->load($repid);
			        $rep_name = $reps->getData('name');
		    	}

				$ticket = new Ticket();
		    	$ticket->postAnswer($answer, $repid, $rep_name, $id, 'custreplied');
		    	
		        $this->_getSession()->addSuccess($this->__('Ticket was successfully updated')); 
			    $this->_redirectSuccess(Mage::getUrl('*/*/', array('_secure'=>true, 'id'=>$id)));
			    return;
            }
            catch (Mage_Core_Exception $e) {
                $this->_getSession()->setsofticketFormData($this->getRequest()->getPost())
                    ->addException($e, $e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->setsofticketFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Ticket cannot be saved'));
            }
        }
        $this->_getSession()->addError($this->__('Ticket cannot be saved'));
		$this->_redirectError(Mage::getUrl('*/*/'));
    }

   
    public function addPostAction()
    {
    	$data= $this->getRequest()->getPost();
    	if ($data) {
            try {
		    	$ticket = new Ticket();
	    		$id=Mage::helper('softicket')->getTicketID();
				$ticket->id=$id;
				$ticket->message=$data['message'];
				$ticket->timestamp=now();
                $ticket->subject=$data['subject'];
				$ticket->name=$data['name'];
				$ticket->email=$data['email'];
				$ticket->priority=$data['priority'];
				$ticket->category=$data['cat'];
				$ticket->ip=$_SERVER['REMOTE_ADDR'];
				$ticket->phone=$data['phone'];
				
		    	if(!$ticket->create(true)){
					$this->_getSession()->addError($this->__('Ticket cannot be saved'));
					$this->_redirectError(Mage::getUrl('*/*/'));
					return;
				}else{
		        	$ticket->postMessage($id, $ticket->message, '', true, 'new');
		        	$this->_getSession()->addSuccess($this->__('Ticket was successfully saved')); 
		        	$this->_redirectSuccess(Mage::getUrl('*/*/', array('_secure'=>true)));
		        	return;
				}
            }
            catch (Mage_Core_Exception $e) {
                $this->_getSession()->setsofticketFormData($this->getRequest()->getPost())
                    ->addException($e, $e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->setsofticketFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Ticket cannot be saved'));
            }
        }
        $this->_redirectError(Mage::getUrl('*/*/edit', array('id'=>$id)));
    }
    
}