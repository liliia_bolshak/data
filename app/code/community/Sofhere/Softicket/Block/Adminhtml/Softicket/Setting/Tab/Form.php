<?php
/**
 * Sofhere SofTicket Magento Component
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU (3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sofhere.com so we can send you a copy immediately.
 * 
 * @category	design_default
 * @author 		sofhere.com
 * @package		Sofhere_SofTicket
 * @copyright  	Copyright (c) 2008-2009 Sofhere IT Solutions.(http://www.sofhere.com)
 * @version 	0.5 beta
 * @license		http://opensource.org/licenses/gpl-3.0.html GNU GENERAL PUBLIC LICENSE (GNU 3.0) 
 */

class Sofhere_Softicket_Block_Adminhtml_Softicket_Setting_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('softicket_setting', array('legend'=>Mage::helper('softicket')->__('Settings')));
	  $data =Mage::registry('softicket_settingdata');

      $fieldset->addField('answer_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Answer Subject'),
          'required'  => false,
          'name'      => 'answer_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('answer_msg', 'text', array(
          'label'     => Mage::helper('softicket')->__('Answer Message'),
          'required'  => false,
          'name'      => 'answer_msg',
          'style'		=> 'width: 600px',
      ));

      $fieldset->addField('rep_trans_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Representative Transfer Subject'),
          'required'  => false,
          'name'      => 'rep_trans_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('rep_trans_msg', 'text', array(
          'label'     => Mage::helper('softicket')->__('Representative Transfer Message'),
          'required'  => false,
          'name'      => 'rep_trans_msg',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('remove_tag', 'text', array(
          'label'     => Mage::helper('softicket')->__('Remove Tag'),
          'required'  => false,
          'name'      => 'remove_tag',
          'style'		=> 'width: 600px',
      ));
	  
      $fieldset->addField('ticket_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Ticket Subject'),
          'required'  => false,
          'name'      => 'ticket_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('ticket_msg', 'textarea', array(
          'label'     => Mage::helper('softicket')->__('Ticket Message'),
          'required'  => false,
          'name'      => 'ticket_msg',
          'style'		=> 'width: 600px',
      ));
      
      /*
      $fieldset->addField('limit_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Limit Subject'),
          'required'  => false,
          'name'      => 'limit_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('limit_msg', 'textarea', array(
          'label'     => Mage::helper('softicket')->__('Limit Message'),
          'required'  => false,
          'name'      => 'limit_msg',
          'style'		=> 'width: 600px',
      ));
	  */
      
      $fieldset->addField('alert_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Alert Subject'),
          'required'  => false,
          'name'      => 'alert_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('alert_msg', 'text', array(
          'label'     => Mage::helper('softicket')->__('Alert Message'),
          'required'  => false,
          'name'      => 'alert_msg',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('alert_email', 'text', array(
          'label'     => Mage::helper('softicket')->__('Alert Emails of Admins'),
          'required'  => false,
          'name'      => 'alert_email',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('alert_user', 'text', array(
          'label'     => Mage::helper('softicket')->__('Alert Emails of Users'),
          'required'  => false,
          'name'      => 'alert_user',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('alert_new', 'checkbox', array(
          'label'     => Mage::helper('softicket')->__('Alert New'),
          'required'  => false,
          'name'      => 'alert_new',
          'style'		=> 'text-align:left;width: 20px',
          'checked'   => $data->getData('alert_new'),
          'value'   => 1,
      ));
      
      $fieldset->addField('message_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Message Subject'),
          'required'  => false,
          'name'      => 'message_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('message_msg', 'textarea', array(
          'label'     => Mage::helper('softicket')->__('Message Message'),
          'required'  => false,
          'name'      => 'message_msg',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('trans_subj', 'text', array(
          'label'     => Mage::helper('softicket')->__('Transfer Subject'),
          'required'  => false,
          'name'      => 'trans_subj',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('trans_msg', 'textarea', array(
          'label'     => Mage::helper('softicket')->__('Transfer Message'),
          'required'  => false,
          'name'      => 'trans_msg',
          'style'		=> 'width: 600px',
      ));
      
      $fieldset->addField('root_url', 'text', array(
          'label'     => Mage::helper('softicket')->__('Root Url'),
          'required'  => false,
          'name'      => 'root_url',
          'style'		=> 'width: 300px',
      ));
      /*
      $fieldset->addField('mail_method', 'text', array(
          'label'     => Mage::helper('softicket')->__('Mail Method'),
          'required'  => false,
          'name'      => 'mail_method',
          'style'		=> 'width: 300px',
      ));*/
      
      $fieldset->addField('smtp_host', 'text', array(
          'label'     => Mage::helper('softicket')->__('Smtp Host'),
          'required'  => false,
          'name'      => 'smtp_host',
          'style'		=> 'width: 300px',
      ));
      
      $fieldset->addField('smtp_port', 'text', array(
          'label'     => Mage::helper('softicket')->__('Smtp Port'),
          'required'  => false,
          'name'      => 'smtp_port',
          'style'		=> 'width: 60px',
      ));
      
      $fieldset->addField('smtp_auth', 'checkbox', array(
          'label'     => Mage::helper('softicket')->__('Smtp Auth'),
          'required'  => false,
          'name'      => 'smtp_auth',
          'style'		=> 'text-align:left;width: 20px',
      	  'checked'   => $data->getData('smtp_auth'),
          'value'   => 1,
      ));
      
      $fieldset->addField('smtp_user', 'text', array(
          'label'     => Mage::helper('softicket')->__('Smtp User'),
          'required'  => false,
          'name'      => 'smtp_user',
          'style'		=> 'width: 150px',
      ));
      
      $fieldset->addField('smtp_pass', 'password', array(
          'label'     => Mage::helper('softicket')->__('Smtp Password'),
          'required'  => false,
          'name'      => 'smtp_pass',
          'style'		=> 'width: 150px',
      ));
      
      $fieldset->addField('sendmail_path', 'text', array(
          'label'     => Mage::helper('softicket')->__('Send Mail Path'),
          'required'  => false,
          'name'      => 'sendmail_path',
          'style'		=> 'width: 300px',
      ));
      
      if ( Mage::getSingleton('adminhtml/session')->getSofTicketSettingData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSofTicketSettingData());
          Mage::getSingleton('adminhtml/session')->getSofTicketSettingData(null);
      } elseif ( Mage::registry('softicket_settingdata') ) {
          $form->setValues(Mage::registry('softicket_settingdata')->getData());
      }
      return parent::_prepareForm();
  }
}