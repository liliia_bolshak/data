<?php
/**
 * Sofhere SofTicket Magento Component
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU (3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sofhere.com so we can send you a copy immediately.
 * 
 * @category	design_default
 * @author 		sofhere.com
 * @package		Sofhere_SofTicket
 * @copyright  	Copyright (c) 2008-2009 Sofhere IT Solutions.(http://www.sofhere.com)
 * @version 	0.5 beta
 * @license		http://opensource.org/licenses/gpl-3.0.html GNU GENERAL PUBLIC LICENSE (GNU 3.0) 
 */
class Sofhere_Softicket_Block_Adminhtml_Rep_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{


  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('rep_form', array('legend'=>Mage::helper('softicket')->__('Edit Representative')));
      $fieldset->addField('name', 'hidden', array('name'      => 'name'));
      $fieldset->addField('email', 'hidden', array('email'      => 'email'));
      $fieldset->addField('username', 'hidden', array('username'      => 'username'));
      $fieldset->addField('password', 'hidden', array('password'      => 'password'));
      
      /*$fieldset->addField('lbl_name', 'label', array(
          'label'     => Mage::helper('softicket')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lbl_name',
      ));*/
      
      $fieldset->addField('lbl_name', 'text', array(
      		'label'     => Mage::helper('softicket')->__('Name'),
      		'title'    => Mage::helper('softicket')->__('Name'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'lbl_name',
      ));
      
      /*$fieldset->addField('lbl_email', 'label', array(
          'label'     => Mage::helper('softicket')->__('Email'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lbl_email',
      ));*/
      
      $fieldset->addField('lbl_email', 'text', array(
      		'label'     => Mage::helper('softicket')->__('Email'),
      		'title'    => Mage::helper('softicket')->__('Email'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'lbl_email',
      ));

      /*$fieldset->addField('lbl_username', 'label', array(
          'label'     => Mage::helper('softicket')->__('User Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lbl_username',
      ));*/
      
      $fieldset->addField('lbl_username', 'text', array(
      		'label'     => Mage::helper('softicket')->__('User Name'),
      		'title'    => Mage::helper('softicket')->__('User Name'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'lbl_username',
      ));
      
      /*$fieldset->addField('lbl_password', 'label', array(
          'label'     => Mage::helper('softicket')->__('Password'),
      	  'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'lbl_password',
      ));*/
      
      $fieldset->addField('lbl_password', 'text', array(
      		'label'     => Mage::helper('softicket')->__('Password'),
      		'title'    => Mage::helper('softicket')->__('Password'),
      		'class'     => 'required-entry',
      		'required'  => true,
      		'name'      => 'lbl_password',
      ));

      $fieldset->addField('signature', 'textarea', array(
          'label'     => Mage::helper('softicket')->__('Signature'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'signature',
      	 'style'		=> 'width: 600px',
      ));

      if ( Mage::getSingleton('adminhtml/session')->getRepData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getRepData());
          Mage::getSingleton('adminhtml/session')->setRepData(null);
      } elseif ( Mage::registry('rep_data') ) {
          $form->setValues(Mage::registry('rep_data')->getData());
      }
      return parent::_prepareForm();
  }
}