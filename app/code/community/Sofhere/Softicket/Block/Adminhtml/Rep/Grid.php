<?php
/**
 * Sofhere SofTicket Magento Component
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GNU (3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sofhere.com so we can send you a copy immediately.
 * 
 * @category	design_default
 * @author 		sofhere.com
 * @package		Sofhere_SofTicket
 * @copyright  	Copyright (c) 2008-2009 Sofhere IT Solutions.(http://www.sofhere.com)
 * @version 	0.5 beta
 * @license		http://opensource.org/licenses/gpl-3.0.html GNU GENERAL PUBLIC LICENSE (GNU 3.0) 
 */
class Sofhere_Softicket_Block_Adminhtml_Rep_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('repGrid');
      $this->setDefaultSort('ID');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }


  protected function _prepareCollection()
  {
  	  $users = Mage::getModel("admin/user")->getCollection();
  	  if ($users)
      	$this->setCollection($users);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
  	
  	  $this->addColumn('username', array(
          'header'    => Mage::helper('softicket')->__('Username'),
          'align'     =>'left',
          'index'     => 'username',
      ));
      
      $this->addColumn('firstname', array(
          'header'    => Mage::helper('softicket')->__('First Name'),
          'align'     =>'left',
          'index'     => 'firstname',
      ));
      
      $this->addColumn('lastname', array(
          'header'    => Mage::helper('softicket')->__('Last Name'),
          'align'     =>'left',
          'index'     => 'lastname',
      ));
      
      $this->addColumn('email', array(
          'header'    => Mage::helper('softicket')->__('Email'),
          'align'     =>'left',
          'index'     => 'email',
      ));
      

      
      $this->addColumn('Action',
            array(
                'header'    =>  Mage::helper('softicket')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('softicket')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id',
                    ),
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('softicket')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('softicket')->__('XML'));
	  
      return parent::_prepareColumns();
  }

 		
  public function getRowUrl($row)
  {
  		$user=Mage::getSingleton('admin/session')->getUser();
  		$userId=$user->getUserId();
  		if (Mage::helper('softicket')->isAdmin() || $row->getId() == $userId )
      		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
      	return "javascript:alert('".Mage::helper('softicket')->__('Not enough rights')."');";
  }

}