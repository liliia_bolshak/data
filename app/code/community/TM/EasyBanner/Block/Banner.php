<?php
/**
 * DO NOT REMOVE OR MODIFY THIS NOTICE
 *
 * EasyBanner module for Magento - flexible banner management
 *
 * @author Templates-Master Team <www.templates-master.com>
 */

class TM_EasyBanner_Block_Banner extends Mage_Core_Block_Template
{
    public function getTemplate()
    {
        if (!$this->hasData('template')) {
            $this->setData('template', "easybanner/banner/{$this->getMode()}.phtml");
        }
        return $this->_getData('template');
    }

    protected function _toHtml()
    {
        if (!$this->getBannerId()) {
            if (!$name = $this->getBannerName()) {
                return '';
            }
            // inline banner call
            $banner = Mage::getModel('easybanner/banner')->load($name, 'identifier');
            if ($banner->getId()) {
                $this->addData($banner->getData());
            } else {
                return '';
            }
        }

        $html = parent::_toHtml();

        $statRes = Mage::getResourceModel('easybanner/banner_statistic')
            ->incrementDisplayCount($this->getBannerId());

        $processor = new Mage_Cms_Model_Template_Filter();
        return $processor->filter($html);
    }

    public function getBannerUrl()
    {
        $url = 'click/id/' . $this->_getData('banner_id');
        if (!$this->getHideUrl()) {
            $url .= '/url/' . $this->_getData('url');
        }
        if (Mage::getStoreConfig('aitpagecache')) { ///aitpagecache_config_cron/aitpagecache_config_cron_frequency')) {
            $url .= '?noMagentoBoosterCache';
        }
        return $url;
    }

    public function getHtml()
    {
        return $this->getData('html');
    }
}
