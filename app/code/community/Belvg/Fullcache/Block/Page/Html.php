<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
  /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 * *************************************** */
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
  /***************************************
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 * ****************************************************
 * @category   Belvg
 * @package    Belvg_Fullcache
 * @author     Yauheni Katkouski <support@yauheni-katkouski.com>
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Fullcache_Block_Page_Html extends Mage_Page_Block_Html
{
    /**
     * Processing block html after rendering
     *
     * @param   string $html
     * @return  string
     */    
    protected function _afterToHtml($html)
    {    
        if (!Mage::app()->useCache('fullcache') || !Mage::helper('fullcache')->isFullcacheEnabled()) {
            return $html;
        }

        if (Mage::helper('fullcache')->pagesCacheEnabled()) {
            $this->setFullcache(TRUE);
            
            $expires = (int)Mage::getStoreConfig('fullcache/settings/expires');
            $this->setExpires(($expires)? $expires : NULL);        
        }
        
        if (Mage::getModel('core/session')->getMessageWasAdded()) {
            Mage::getModel('core/session')->setMessageWasAdded(FALSE);
            $this->setFullcache(FALSE);
        }
        
        if ($this->getFullcache()) {            
            if (Mage::app()->getRequest()->getActionName() != 'noRoute') {
                $key = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                    $key = 'SECURE_' . $key;
                }

                $key = Mage::helper('fullcache')->removeExcludesFromKey($key);

                $key = Mage::app()->getStore()->getCurrentCurrencyCode() . '_'  . $key;
                $key = Mage::app()->getStore()->getName() . '_'  . $key;
                $key = Mage::getSingleton('core/design_package')->getPackageName() . '_'  . $key;
                $key = Mage::getSingleton('core/design_package')->getTheme('template') . '_'  . $key;
                
                if (Mage::registry('fullcache_is_category_view')) {
                    $key = Mage::helper('fullcache')->checkCategoryUrlKey($key);           
                }

                $data = (string)$html;

                if (Mage::helper('fullcache')->isTestMode()) {
                    $testModeMessage = Mage::helper('fullcache')->getTestModeMessage('Fullcache: saving page with key = "' . $key . '"');
                    echo Mage::helper('fullcache')->wrapTestModeBlock($testModeMessage, TRUE);
                }
                
                if (Mage::helper('fullcache')->getSource()) {
                    Mage::helper('fullcache')->getSource()->save($data, $key, array('BELVG_FULL_CACHE'), $this->getExpires());
                }
            }
        }

        return $html;
    }	
}
