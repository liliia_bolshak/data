<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
  /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 * *************************************** */
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
  /***************************************
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 * ****************************************************
 * @category   Belvg
 * @package    Belvg_Fullcache
 * @author     Yauheni Katkouski <contact@yauheni-katkouski.com>
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Fullcache_Model_Observers
{    
    /**
     * Array of placeholders with contents
     *
     * @var array
     */   
    static private $holeContent = array();
    
    /**
     * Array of placeholders with aliases
     *
     * @var array
     */ 
    static private $placeholders = array(); 

    /**
    * Observer for event "controller_action_layout_generate_blocks_after"
    * 
    * Get cached page
    *
    * @param  Varien_Event_Observer $observer
    * @return mixed
    */     
    public static function start(Varien_Event_Observer $observer)
    {        
        try {
            self::renderCachedPage();
            die;
        } catch (Exception $e) {
            return FALSE;
        }
    }
 
    /**
    * Observer for event "core_session_abstract_add_message"
    *
    * Set info about messages in session
    * 
    * @param  Varien_Event_Observer $observer
    */    
    public function addMessage(Varien_Event_Observer $observer)
    {
        Mage::getModel('core/session')->setMessageWasAdded(TRUE);
    }

    /**
    * Observer for event "core_block_abstract_to_html_after"
    *
    * Set '<!-- fullcache_' wrapper for blocks
    * 
    * @param  Varien_Event_Observer $observer
    */     
    public function afterToHtml(Varien_Event_Observer $observer)
    {
        $placeholders = self::getPlaceholders();
        foreach ($placeholders as $placeholder) {            
            if (($placeholder == $observer->getEvent()->getBlock()->getNameInLayout()) && $placeholder) {             
                $html = '<!-- fullcache_' . $placeholder . ' -->';                                      
                $html .= $observer->getEvent()->getTransport()->getHtml();        
                $html .= '<!-- /fullcache_' . $placeholder . ' -->';
                $observer->getEvent()->getTransport()->setHtml($html);
            }
        }        
    }
    
    /**
    * Observer for event "application_clean_cache"
    *
    * Flush all cache
    * 
    * @param  Varien_Event_Observer $observer
    */    
    public function delete(Varien_Event_Observer $observer)
    {
        if (!is_null($observer) && is_array($observer->getEvent()->getTags()) && in_array('BACKEND_MAINMENU', $observer->getEvent()->getTags())) {
            return;
        } elseif (Mage::helper('fullcache')->getSource() && !Mage::registry('no_fullcache_clear')) {
            Mage::helper('fullcache')->getSource()->flush();
        }
    }

    /**
     * Observer for event "sales_order_item_save_after"
     * 
     * @param  Varien_Event_Observer $observer
     */
    public function salesOrderItemSaveAfter(Varien_Event_Observer $observer)
    {
        $this->_disableFullcacheClear();
    }
    
    /**
     * Observer for event "sales_order_save_after"
     * 
     * @param  Varien_Event_Observer $observer
     */
    public function salesOrderSaveAfter(Varien_Event_Observer $observer)
    {
        $items = $observer->getEvent()->getOrder()->getAllItems();
        foreach ($items as $_item) {
            $_product = Mage::getModel('catalog/product')->load($_item->getProductId());
            $_stock = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
            if (!$_stock) {
                $this->_enableFullcacheClear();                
                $this->delete($observer);
                break;
            }
        } 
        
        $this->_disableFullcacheClear();
    }
    
    /**
     * Disable flush full cache
     * 
     */
    protected function _disableFullcacheClear()
    {
        if (!Mage::registry('no_fullcache_clear')) {
            Mage::unregister('no_fullcache_clear');
            Mage::register('no_fullcache_clear', TRUE);
        }
    }
    
    /**
     * Enable flush full cache
     * 
     */
    protected function _enableFullcacheClear()
    {
        if (Mage::registry('no_fullcache_clear')) {
            Mage::unregister('no_fullcache_clear');
            Mage::register('no_fullcache_clear', FALSE);
        }
    }

    /**
     * Observer for event "review_save_after"
     * 
     * @param  Varien_Event_Observer $observer
     */
    public function deleteOrSaveAfter(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $this->delete($observer);
        } else {
            $this->_disableFullcacheClear();
        }
    }

    /**
    * Get cached page
    *
    * @return mixed
    */      
    public static function getCachedPage()
    {        
        if (Mage::getModel('core/session')->getMessageWasAdded()) {
            return FALSE;
        }
            
        if (!Mage::helper('fullcache')->pagesCacheEnabled()) {
            return FALSE;
        }
        
        $key = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $key = 'SECURE_' . $key;
        }
        
        $key = Mage::helper('fullcache')->removeExcludesFromKey($key);
     
        $key = Mage::app()->getStore()->getCurrentCurrencyCode() . '_'  . $key;
        $key = Mage::app()->getStore()->getName() . '_'  . $key;
        $key = Mage::getSingleton('core/design_package')->getPackageName() . '_'  . $key;
        $key = Mage::getSingleton('core/design_package')->getTheme('template') . '_'  . $key;
        
        
        if (Mage::registry('fullcache_is_category_view')) {
            $key = Mage::helper('fullcache')->checkCategoryUrlKey($key);           
        }
        
        if (Mage::helper('fullcache')->getSource() && ($data = Mage::helper('fullcache')->getSource()->load($key))) {                
            if (Mage::helper('fullcache')->isTestMode()) {
                $testModeMessage = Mage::helper('fullcache')->getTestModeMessage('Fullcache: getting page with key = "' . $key . '"', TRUE);
                echo Mage::helper('fullcache')->wrapTestModeBlock($testModeMessage, TRUE);
            } 
            
            $placeholders = self::getPlaceholders();

            $pattern = array();
            $testModePre = '';            
            foreach ($placeholders as $patternKey => $placeholder) {                
                if ($placeholderNotAliasHtml = Mage::getSingleton('core/layout')->getBlock($placeholder)) {
                    if (!($placeholderNotAliasHtml = $placeholderNotAliasHtml->toHtml())) {
                        continue;
                    }                    
                } elseif ($placeholder == 'header.welcome') {
                    $placeholderNotAliasHtml = Mage::getSingleton('core/layout')->getBlock('header')->getWelcome();
                } else {
                    continue;
                }                    

                if (Mage::helper('fullcache')->isTestMode()) {
                    $testModePre = Mage::helper('fullcache')->getTestModeMessage('Fullcache placeholder: ' . $placeholder);
                }

                self::$holeContent[$placeholder] = $testModePre . $placeholderNotAliasHtml;
                if (Mage::helper('fullcache')->isTestMode()) {
                    self::$holeContent[$placeholder] = Mage::helper('fullcache')->wrapTestModeBlock(self::$holeContent[$placeholder]);
                }
                
                $pattern[$patternKey] = '/\<!\-\- fullcache_(';
                $pattern[$patternKey] .= $placeholder;
                $pattern[$patternKey] .= ') \-\-\>(.*?)\<!\-\- \/fullcache_';
                $pattern[$patternKey] .= $placeholder . ' \-\-\>/si';
            }

            return self::fillNoCachePlaceholders($data, $pattern);   
        } else {
            return FALSE;
        }
    }

    /**
    * Fill no cache placeholders
    * 
    * @param string $html
    * @param string $pattern
    * @return string
    */    
    public static function fillNoCachePlaceholders($html, $pattern)
    {
        return preg_replace_callback($pattern, 'Belvg_Fullcache_Model_Observers::replaceNoCacheBlocks', $html);        
    }

    /**
    * Replace no cache blocks
    * 
    * @param array $matches
    * @return string
    */   
    public static function replaceNoCacheBlocks($matches)
    {
        $key = $matches[1];
        
        if (isset(self::$holeContent[$key])) {
            return self::$holeContent[$key]; 
        } else {
            return $matches[0];
        }         
    }

    /**
    * Render cached page
    *
    * @return string
    */     
    public static function renderCachedPage()
    {
        if (!Mage::app()->useCache('fullcache') || !Mage::helper('fullcache')->isFullcacheEnabled()) {
            throw new Exception('Fullcache is not enabled.');
        } elseif ($page = self::getCachedPage()) {
            echo $page;
        } else {
            throw new Exception('No cache matches at this url.');
        }
    }	

    /**
    * Render placeholders to ignore
    *
    * @return array
    */     
    public static function getPlaceholders()
    {
        if (!self::$placeholders) {
            $placeholders = array(
                'header.welcome',
                'top.links',
                'cart_sidebar',
                'catalog.compare.sidebar',
                'right.poll',
                'wishlist_sidebar',
                'left.reports.product.viewed',
                'right.reports.product.viewed',
                'right.reports.product.compared',
                'sale.reorder.sidebar',
                'home.catalog.product.new',
                'home.reports.product.compared',
                'home.reports.product.viewed',                           
            );

            $placeholdersFromConfig = (string)Mage::getStoreConfig('fullcache/settings/blocks'); 
            $placeholdersFromConfigArray = $placeholdersFromConfig ? explode(',', $placeholdersFromConfig) : array();            
            $placeholdersFromConfigArray = array_map('trim', $placeholdersFromConfigArray);            
            $placeholdersFromConfigArray = array_unique($placeholdersFromConfigArray);

            foreach ($placeholdersFromConfigArray as $placeholderFromConfig) {
                $placeholders[] = $placeholderFromConfig;
            }
            
            self::$placeholders = $placeholders;
        }
        
        return self::$placeholders;
    }
}