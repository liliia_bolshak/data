<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
  /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 * *************************************** */
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
  /***************************************
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 * ****************************************************
 * @category   Belvg
 * @package    Belvg_Fullcache
 * @author     Yauheni Katkouski <contact@yauheni-katkouski.com>
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Fullcache_Model_Host extends Mage_Core_Model_Config_Data
{
    /**
    * Redefine the save method
    *
    * @return mixed
    */ 
    public function save()
    {
        $host = explode(':', $this->getValue());
        
        if (isset($host[0]) && isset($host[1]) && is_numeric($host[1])) {
            $host = array_map('trim', $host);
            $memcache = memcache_connect($host[0], $host[1]);
            if (memcache_get_server_status($memcache, $host[0], $host[1])) {
                return parent::save();
            }            
        }
        
        Mage::throwException('Memcache server is failed.');                
    }
}