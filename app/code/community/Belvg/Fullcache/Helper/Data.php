<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
  /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 * *************************************** */
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
  /***************************************
 *         DISCLAIMER   *
 * *************************************** */
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 * ****************************************************
 * @category   Belvg
 * @package    Belvg_Fullcache
 * @author     Yauheni Katkouski <contact@yauheni-katkouski.com>
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Fullcache_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Cache model
     *
     * @var Mage_Core_Model_Cache
     */
    protected $_source = NULL;
    
    /**
     * Check cache enabled
     *
     * @var mixed
     */    
    protected $_pagesCacheEnabled = NULL;
    
    /**
     * Get cache model
     *
     * @return Mage_Core_Model_Cache
     */    
    public function getSource()
    {
        try {
            if (!$this->_source) {
                $this->_source = new Mage_Core_Model_Cache($this->_loadConfiguration());
            }
        } catch (Exception $e) {
            $this->_source = NULL;
        }       

        return $this->_source;
    }
    
    /**
    * Check test mode
    *
    * @return bool
    */    
    public function isTestMode()
    {
        return Mage::getStoreConfig('fullcache/settings/test_mode');
    }
        
    /**
    * Check module fullcache enabled
    *
    * @return bool
    */    
    public function isFullcacheEnabled()
    {
        return Mage::getStoreConfig('fullcache/settings/enabled');
    }
    
    /**
    * Get test mode message
    *
    * @param  string $message
    * @return string
    */     
    public function getTestModeMessage($message, $is_get_save = FALSE)
    {
        return '<span onmouseover="this.style.backgroundColor=\'transparent\';this.style.color=\'transparent\';" onmouseout="this.style.backgroundColor=\'' . ($is_get_save ? 'green' : 'orangeRed') . '\';this.style.color=\'white\';" style="cursor:default;display:block;background:' . ($is_get_save ? 'green' : 'orangeRed') . ';color:white;border-radius:2px;font:11px Arial;left:0;padding: 2px 5px;position:absolute;text-align: left !important;top:0;z-index:9999;">' . $message . '</span>';
    }
    
    /**
    * Wrap test mode block
    *
    * @param  string $html
    * @return string
    */
    public function wrapTestModeBlock($html, $is_get_save = FALSE)
    {
        if ($is_get_save) {
            return  $html;
        }
        return '<span style="display:block;border:1px dotted orangeRed;border-radius:2px;position:relative;min-height:17px;">' . $html . '</span>';
    }
 
    /**
    * Remove the excluded url params
    *
    * @param  string $key
    * @return string
    */    
    public function removeExcludesFromKey($key)
    {
        $excludes = array('___SID', '___store', '___from_store');

        $excludeParams = (string)Mage::getStoreConfig('fullcache/settings/params');        

        $excludeArr = $excludeParams ? explode(',', $excludeParams) : array();

        foreach ($excludeArr as $param) {
            $excludes[] = $param;
        }
        
        if (Mage::registry('fullcache_is_category_view')) {
            $excludeArr = array('dir', 'limit', 'mode', 'order');
            
            foreach ($excludeArr as $param) {
                $excludes[] = $param;
            }
        }

        $keyPart = explode('?', $key);
        if (isset($keyPart[1]) && $keyPart[1]) {
            $keyParse = parse_url($key);
            $newParams = explode('&', $keyParse['query']);
            $newString = '?';
            foreach ($newParams as $iteration => $newParam) {
                $paramArr = explode('=', $newParam);
                if (!in_array($paramArr[0], $excludes)) {
                    $newString .= $paramArr[0] . '=' . $paramArr[1] . '&';
                }                                                           
            }
            
            $key = $keyPart[0] . $newString;
            $key = rtrim($key, '?&'); 
        }
        
        return $key;
    }

    /**
    * Load cache configuration
    */     
    protected function _loadConfiguration()
    {
        switch(Mage::getStoreConfig('fullcache/settings/type')) {
            case 'memcached':
                $host = explode(':', (string)Mage::getStoreConfig('fullcache/settings/host'));
                $options = array(
                    'servers' => array(
                        array(
                            'host' => $host[0],
                            'port' => $host[1],
                            'persistant' => Mage::getStoreConfig('fullcache/settings/persistant'),
                        ),
                    ),
                );
                return array('backend' => 'memcached', 'backend_options' => $options);
                
            case 'files':
            default:
                // default
                break;             
        }
        
        return array();
    }
 
    /**
    * Check cache enabled
    *
    * @return bool
    */ 
    public function pagesCacheEnabled()
    {
        if ($this->_pagesCacheEnabled) {
            return TRUE;
        } elseif ($this->_pagesCacheEnabled === FALSE) {
            return FALSE;
        }
            
        $cmsPagesCacheEnabled = $layeredCatalogPagesCacheEnabled = $productViewPagesCacheEnabled = FALSE;
        $request  = Mage::app()->getFrontController()->getRequest();
        
        if (Mage::registry('fullcache_is_category_view')) {
            Mage::unregister('fullcache_is_category_view');
            Mage::register('fullcache_is_category_view', FALSE);
        }
        
        switch ($request->getModuleName()) {
            case 'cms':
                $cmsPagesCacheEnabled = Mage::getStoreConfig('fullcache/settings/cms_pages');
                break;
            
            case 'catalog':
                if ($request->getActionName() == 'view') {
                    switch ($request->getControllerName()) {
                        case 'category':                        
                            $layeredCatalogPagesCacheEnabled = Mage::getStoreConfig('fullcache/settings/layered_catalog_pages');
                            if (!Mage::registry('fullcache_is_category_view')) {
                                Mage::unregister('fullcache_is_category_view');
                                Mage::register('fullcache_is_category_view', TRUE);
                            }
                            break;
                            
                        case 'product':
                            $productViewPagesCacheEnabled = Mage::getStoreConfig('fullcache/settings/product_view_pages'); 
                        default:
                            // default
                            break;
                    }
                }
                
            default:
                // default
                break;
        }
        
        if ($cmsPagesCacheEnabled || $layeredCatalogPagesCacheEnabled || $productViewPagesCacheEnabled) {
            $this->_pagesCacheEnabled = TRUE;
            return TRUE;
        }
        
        $this->_pagesCacheEnabled = FALSE;
        return FALSE;
    }
    
    /**
    * Check category url key
    *
    * @param  string $key
    * @return string
    */
    function checkCategoryUrlKey($key) {
        $get_params = Mage::app()->getFrontController()->getRequest()->getParams();

        $sort_direction_default = 'asc';
        $sort_direction = isset($get_params['dir']) ? $get_params['dir'] : Mage::getSingleton('catalog/session')->getSortDirection();
        $sort_direction = $sort_direction ? $sort_direction : $sort_direction_default;
        Mage::getSingleton('catalog/session')->setSortDirection($sort_direction);

        $display_mode_default = (string)Mage::getStoreConfig('catalog/frontend/list_mode');            
        $display_mode = isset($get_params['mode']) ? $get_params['mode'] : Mage::getSingleton('catalog/session')->getDisplayMode();
        $display_mode = $display_mode ? $display_mode : $display_mode_default;            
        $display_mode = explode('-', $display_mode);
        $display_mode = $display_mode[0];
        Mage::getSingleton('catalog/session')->setDisplayMode($display_mode);

        $limit_page_default = ($display_mode == 'grid') ? (int)Mage::getStoreConfig('catalog/frontend/grid_per_page') : (int)Mage::getStoreConfig('catalog/frontend/list_per_page');
        $limit_page = isset($get_params['limit']) ? $get_params['limit'] : Mage::getSingleton('catalog/session')->getLimitPage();
        $limit_page = $limit_page ? $limit_page : $limit_page_default;
        Mage::getSingleton('catalog/session')->setLimitPage($limit_page);

        $sort_order_default = (string)Mage::getStoreConfig('catalog/frontend/default_sort_by');
        $sort_order = isset($get_params['order']) ? $get_params['order'] : Mage::getSingleton('catalog/session')->getSortOrder();
        $sort_order = $sort_order ? $sort_order : $sort_order_default;
        Mage::getSingleton('catalog/session')->setSortOrder($sort_order);

        $key .= '_dir=' . $sort_direction . '&limit=' . $limit_page . '&mode=' . $display_mode . '&order=' . $sort_order;
        
        return $key;
    }
}