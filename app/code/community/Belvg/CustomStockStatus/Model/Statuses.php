<?php

/**
 * BelVG LLC.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*
********************************************
* MAGENTO EDITION USAGE NOTICE *
********************************************
* This package designed for Magento COMMUNITY edition
* BelVG does not guarantee correct work of this extension
* on any other Magento edition except Magento COMMUNITY edition.
* BelVG does not provide extension support in case of
* incorrect edition usage.
************************
*       DISCLAIMER     *
************************
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future.
********************************************
*
* @category   Belvg
* @package    Belvg_CustomStockStatus
* @author     Victor Potseluyonok
* @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
* @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
*/
class Belvg_CustomStockStatus_Model_Statuses extends Mage_Core_Model_Abstract
{

    public function _construct ()
    {
        parent::_construct();
        $this->_init('customstockstatus/statuses');
    }

    public function saveRange (Varien_Object $obj)
    {
        $model = Mage::getModel('customstockstatus/statuses');
        $model->setData($obj->getData());
        $model->save();
    }

    public function deleteAll ()
    {
        $this->getResource()->deleteAll();
    }

    public function getOptions ()
    {
        $options = Mage::getResourceModel(
                'eav/entity_attribute_option_collection')->setAttributeFilter(
                Mage::registry('entity_attribute')->getId())
            ->setPositionOrder('asc', TRUE);
        $options->getSelect()->joinLeft(
                array(
                        'statuses' => $this->getResource()
                            ->getMainTable()
                ), 'main_table.option_id=statuses.option_id',
                array(
                        'qty_from' => 'statuses.qty_from',
                        'qty_to' => 'statuses.qty_to'
                ));

        $options->load();

        foreach ($options as &$option) {
            $option->setValue(
                    Mage::helper('core')->htmlEscape($option->getValue()));
        }

        return $options;
    }
}