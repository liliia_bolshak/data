<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Model_Mysql4_Statuses extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct ()
    {
        $this->_init('customstockstatus/statuses', 'id');
    }

    public function deleteAll ()
    {
        $this->_getWriteAdapter()->truncateTable($this->getMainTable());
    }

    public function getStatusByRangeQty ($qty)
    {
        $status = Mage::getResourceModel(
                'eav/entity_attribute_option_collection');

        $status->getSelect()->join(
                array(
                        'statuses' => $this->getMainTable()
                ),
                'main_table.option_id=statuses.option_id AND statuses.qty_from <= ' .
                         $qty . ' AND statuses.qty_to >= ' . $qty);
        $stat = $status->getFirstItem();

        if ($stat->getOptionId()) {
            $opt = $this->getOptionById($stat->getOptionId());

            if (! $opt) {
                return FALSE;
            }

            $stat->setData('label', $opt['label']);
            return $stat;
        }

        return FALSE;
    }

    public function getOptionById ($option_id)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute(
                'catalog_product',
                Mage::helper('customstockstatus')->getCustomAttrStatusCode());

        if (! $attribute) {
            return FALSE;
        }

        foreach ($attribute->getSource()->getAllOptions(TRUE, FALSE) as $opt) {
            if ($opt['value'] == $option_id) {
                return $opt;
            }
        }

        return FALSE;
    }
}