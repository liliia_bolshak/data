<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Model_Resource_Eav_Mysql4_Setup extends Mage_Eav_Model_Entity_Setup
{

    public function getDefaultEntities ()
    {
        return array(
                'catalog_product' => array(
                        'entity_model' => 'catalog/product',
                        'attribute_model' => 'catalog/resource_eav_attribute',
                        'table' => 'catalog/product',
                        'additional_attribute_table' => 'catalog/eav_attribute',
                        'entity_attribute_collection' => 'catalog/product_attribute_collection',
                        'attributes' => array(
                                'belvg_custom_stock_status' => array(
                                        'group' => 'Custom Stock Status',
                                        'label' => 'Select stock status',
                                        'type' => 'int',
                                        'input' => 'select',
                                        'default' => 0,
                                        'class' => '',
                                        'source' => 'eav/entity_attribute_source_table',
                                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                                        'visible' => TRUE,
                                        'required' => FALSE,
                                        'user_defined' => FALSE,
                                        'default' => '',
                                        'searchable' => FALSE,
                                        'filterable' => FALSE,
                                        'comparable' => FALSE,
                                        'visible_on_front' => FALSE,
                                        'unique' => FALSE,
                                        'apply_to' => '',
                                        'is_configurable' => FALSE,
                                        'note' => 'Select a stock message here if wanted. If custom availability text is entered below, it will override this message. Add more messages by going to <b>Catalog->Attributes->Manage Attributes</b> and clicking on the \'belvg_custom_stock_status\' attribute.'
                                ),
                                'belvg_custom_stock_text' => array(
                                        'group' => 'Custom Stock Status',
                                        'label' => 'Custom availability text',
                                        'type' => 'varchar',
                                        'input' => 'text',
                                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                                        'visible' => TRUE,
                                        'required' => FALSE,
                                        'user_defined' => TRUE,
                                        'default' => '',
                                        'searchable' => FALSE,
                                        'filterable' => FALSE,
                                        'comparable' => FALSE,
                                        'visible_on_front' => FALSE,
                                        'unique' => FALSE,
                                        'apply_to' => '',
                                        'is_configurable' => FALSE,
                                        'note' => 'Enter custom availability text here if wanted.'
                                )
                        )
                )
        );
    }
}