<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Model_Product_Configurable extends Belvg_CustomStockStatus_Model_Product_Abstract
{

    public function getAvailabilityText ()
    {
        if ($text = $this->getCustomAvailabilityText(TRUE)) {
            return $text;
        }

        return $this->getStandartStockStatusText();
    }

    public function getOptionLabel ($product)
    {
        if (! is_object($product)) {
            $product = Mage::getModel('catalog/product')->load($product);
        } elseif (! ($product instanceof Mage_Catalog_Model_Product)) {
            return FALSE;
        } else {
            $product = Mage::getModel('catalog/product')->load($product->getId());
        }

        $cust = Mage::getModel('customstockstatus/product_simple', $product);

        if ($text = $cust->getCustomAvailabilityText(FALSE,
                $this->getProductType())) {
            return $text;
        }

        if ($text = $cust->getStatusByRangeQty(FALSE, $this->getProductType())) {
            return $text;
        }

        if ($text = $cust->getActualQtyText($this->getProductType())) {
            return $text;
        }

        if (! $product->getStockItem()->getIsInStock()) {
            return $cust->getStandartStockStatusText(TRUE);
        }

        return '';
    }
}