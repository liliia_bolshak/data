<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
abstract class Belvg_CustomStockStatus_Model_Product_Abstract
{

    /**
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product;

    public function __construct ()
    {
        $this->_product = func_get_arg(0);

        return $this;
    }

    abstract public function getAvailabilityText();

    protected function getCustomAvailabilityText ($with_image = FALSE,
            $parent_product_type = NULL)
    {
        $img = '';
        
        if (Mage::helper('customstockstatus')->isUseCustAvailableText(
                $this->getProductType(), $parent_product_type) &&
                 $this->isShowTextIfOutStock()) {
            $text = $this->getProduct()->getData(
                    Mage::helper('customstockstatus')->getCustomAttrTextCode());
            if (! empty($text)) {
                return $this->prepareText($text, $parent_product_type != NULL);
            } elseif ($option_id = $this->getProduct()->getData(
                    Mage::helper('customstockstatus')->getCustomAttrStatusCode())) {
                $text = '';

                if ($with_image && ($url = Mage::helper(
                        'customstockstatus/image')->getImageUrl($option_id))) {
                    $img = Mage::helper('customstockstatus/image')->getImageHtml(
                            $url);
                }

                $opt = Mage::getModel('customstockstatus/statuses')->getResource()->getOptionById(
                        $option_id);

                if ($opt) {
                    $text = $opt['label'];
                } else {
                    $text = '';
                    $img = '';
                }

                return $img . $this->prepareText($text, $parent_product_type != NULL);
            } else {
                return FALSE;
            }
        }

        return FALSE;
    }

    protected function getStatusByRangeQty ($with_image = FALSE,
            $parent_product_type = NULL)
    {
        if ((! $this->isInStock() &&
                 ! Mage::helper('customstockstatus')->isShowStockStatusForOutStock()) ||
                 $this->getProductType() == 'virtual') {
            return FALSE;
        }

        $status = Mage::getResourceModel('customstockstatus/statuses')->getStatusByRangeQty(
                $this->getQty());

        if (is_object($status)) {
            $text = '';

            if ($with_image && ($url = Mage::helper('customstockstatus/image')->getImageUrl(
                    $status->getOptionId()))) {
                $text = Mage::helper('customstockstatus/image')->getImageHtml(
                        $url);
            }

            $text .= $this->prepareText($status->getLabel(),
                    $parent_product_type != NULL);

            return $text;
        }

        return FALSE;
    }

    protected function getActualQtyText ($parent_product_type = NULL)
    {
        if ($this->isInStock() && Mage::helper('customstockstatus')->isShowActualQty(
                $this->getProductType(), $parent_product_type) &&
                 $this->getProductType() != 'virtual') {
            return $this->prepareText(
                    Mage::helper('customstockstatus')->getQtyText(
                            $this->getQty()), $parent_product_type != NULL);
        }

        return FALSE;
    }

    protected function isShowTextIfOutStock ()
    {
        if ($this->isInStock()) {
            return TRUE;
        }
        
        return ! $this->isInStock() ==
                 Mage::helper('customstockstatus')->isShowStockStatusForOutStock();
    }

    protected function getProductType ()
    {
        return $this->getProduct()->getTypeId();
    }

    protected function getProduct ()
    {
        return $this->_product;
    }

    protected function prepareText ($text, $for_option = FALSE)
    {
        $text = str_replace('{qty}', $this->getQty(), $text);

        if ($for_option) {
            $text = ' (' . $text . ')';
        }

        return Mage::helper('core')->htmlEscape($text);
    }

    protected function correctingTextForOption ($text)
    {
        return ' (' . $text . ')';
    }

    protected function getQty ()
    {
        return (int) $this->getStockItem()->getQty();
    }

    protected function isInStock ()
    {
        return $this->getStockItem()->getIsInStock();
    }

    protected function getManageStock ()
    {
        return $this->getStockItem()->getManageStock();
    }

    protected function getStockItem ()
    {
        return $this->getProduct()->getStockItem();
    }

    protected function getStandartStockStatusText ($for_option = FALSE)
    {
        if ($this->isInStock()) {
            return $this->prepareText(
                    Mage::helper('customstockstatus')->getInStockText(),
                    $for_option);
        } else {
            return $this->prepareText(
                    Mage::helper('customstockstatus')->getOutStockText(),
                    $for_option);
        }
    }
}