<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Adminhtml_StatusesController extends Mage_Adminhtml_Controller_Action
{

    protected function _delete ($post)
    {
        if ($this->isCorrectPostData($post)) {
            foreach ($post as $option_id => $v) {
                Mage::helper('customstockstatus/image')->deleteImage($option_id);
            }
        }
    }

    protected function _saveRange ($post)
    {
        if ($this->isCorrectPostData($post)) {
            Mage::getModel('customstockstatus/statuses')->deleteAll();

            foreach ($post as $option_id => $v) {
                $obj = new Varien_Object();
                $obj->setOptionId($option_id);
                $obj->setQtyFrom(
                        (int) $v['from'] ? abs((int) $v['from']) : NULL);
                $obj->setQtyTo((int) $v['to'] ? abs((int) $v['to']) : NULL);
                Mage::getModel('customstockstatus/statuses')->saveRange($obj);
            }
        }
    }

    protected function _upload ()
    {
        try {
            if (! empty($_FILES['icon'])) {
                foreach ($_FILES['icon']['name'] as $k => $v) {
                    Mage::helper('customstockstatus/image')->uploadImage($k);
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                    $this->__($e->getMessage()));
        }
    }

    public function saveAction ()
    {
        $attribute_id = Mage::app()->getRequest()->getParam('attribute_id');

        $this->_delete(
                Mage::app()->getRequest()
                    ->getParam('delete'));

        $this->_saveRange(
                Mage::app()->getRequest()
                    ->getParam('qty'));

        $this->_upload();

        $this->_forward('edit', 'catalog_product_attribute', 'admin',
                array(
                        'attribute_id' => $attribute_id
                ));

        /*
         * $this->_redirect('catalog_product_attribute/edit', array(
         * 'attribute_id' => $attribute_id ));
         */
    }

    public function isCorrectPostData ($data)
    {
        return ! empty($data) && is_array($data);
    }
}