<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$tableName = $installer->getTable('customstockstatus/statuses');
$eavTableName = $installer->getTable('eav/attribute_option');

$connection = $installer->getConnection();

if (! $connection->isTableExists($tableName)) {
    $maintable = $connection->newTable($tableName)
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL,
            array(
                    'nullable' => TRUE,
                    'primary' => TRUE,
                    'identity' => TRUE
            ))
        ->addColumn('option_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL,
            array(
                    'unique' => TRUE,
                    'nullable' => FALSE
            ))
        ->addColumn('qty_from', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL,
            array(
                    'nullable' => TRUE
            ))
        ->addColumn('qty_to', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL,
            array(
                    'nullable' => TRUE
            ));
    $connection->createTable($maintable);
}

$installer->installEntities();

