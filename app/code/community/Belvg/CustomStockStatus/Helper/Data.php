<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Helper_Data extends Mage_Core_Helper_Abstract
{

    const ENABLED_PATH = 'customstockstatus/general/enabled';

    const SHOW_STOCK_STATUS_FOR_OUTOFSTOCK_PATH = 'customstockstatus/general/outofstock';

    const ONLY_RANGED_STATUS_PATH = 'customstockstatus/general/rangestatusonly';

    const CUSTOM_STATUS_ATTRIBUTE_CODE = 'belvg_custom_stock_status';

    const CUSTOM_TEXT_ATTRIBUTE_CODE = 'belvg_custom_stock_text';

    const SHOW_UNORDERABLE_PRODUCT_PATH = 'customstockstatus/configurableproduct/showunorder';

    const SHOW_STOCK_STATUS_ABOVE_ADD_TO_CART_PATH = 'customstockstatus/configurableproduct/displaystockaboveaddtocart';

    const QTY_TEXT = '%s in stock';

    const INSTOCK_TEXT = 'In stock';

    const OUTSTOCK_TEXT = 'Out of stock';

    public function isEnabled ()
    {
        return Mage::getStoreConfig(self::ENABLED_PATH) ? TRUE : FALSE;
    }

    public function isOnlyRangedStatus ()
    {
        return Mage::getStoreConfig(self::ONLY_RANGED_STATUS_PATH) ? TRUE : FALSE;
    }

    public function isShowStockStatusForOutStock ()
    {
        return Mage::getStoreConfig(self::SHOW_STOCK_STATUS_FOR_OUTOFSTOCK_PATH) ? TRUE : FALSE;
    }

    public function isUseCustAvailableText ($product_type, $product_parent_type)
    {
        if ($product_parent_type == NULL || $product_type == 'virtual') {
            return Mage::getStoreConfig(
                    'customstockstatus/' . $product_type . 'product/showcusttext') ? ! $this->isOnlyRangedStatus() : FALSE;
        } else {
            return Mage::getStoreConfig(
                    'customstockstatus/' . $product_parent_type .
                             'product/showcusttext_option') ? ! $this->isOnlyRangedStatus() : FALSE;
        }
    }

    public function isUseCustAvailableTextOptionConfigurable ()
    {
        return Mage::getStoreConfig(
                'customstockstatus/configurableproduct/showcusttext_option') ? ! $this->isOnlyRangedStatus() : FALSE;
    }

    public function isShowActualQty ($product_type, $product_parent_type)
    {
        if ($product_parent_type == NULL) {
            return Mage::getStoreConfig(
                    'customstockstatus/' . $product_type . 'product/showqty') ? ! $this->isOnlyRangedStatus() : FALSE;
        } else {
            return Mage::getStoreConfig(
                    'customstockstatus/' . $product_parent_type .
                             'product/showqty_option') ? ! $this->isOnlyRangedStatus() : FALSE;
        }
    }

    public function isShowUnOrderableProduct ()
    {
        return Mage::getStoreConfig(self::SHOW_UNORDERABLE_PRODUCT_PATH) ? TRUE : FALSE;
    }

    public function isShowStockStatusAboveAddToCart ()
    {
        return Mage::getStoreConfig(
                self::SHOW_STOCK_STATUS_ABOVE_ADD_TO_CART_PATH) ? TRUE : FALSE;
    }

    public function getCustomAttrStatusCode ()
    {
        return self::CUSTOM_STATUS_ATTRIBUTE_CODE;
    }

    public function getCustomAttrTextCode ()
    {
        return self::CUSTOM_TEXT_ATTRIBUTE_CODE;
    }

    public function getInStockText ()
    {
        return $this->__(self::INSTOCK_TEXT);
    }

    public function getOutStockText ()
    {
        return $this->__(self::OUTSTOCK_TEXT);
    }

    public function getQtyText ($qty)
    {
        return $this->__(self::QTY_TEXT, $qty);
    }

    public function getAvailabilityText ($_product)
    {
        $model_name = $this->getModelName($_product->getTypeId());
        if($model_name == "Belvg_CustomStockStatus_Model_Product_Subscription_simple"){
            $model_name = "Belvg_CustomStockStatus_Model_Product_Simple";
        }
        return Mage::getModel($model_name, $_product)->getAvailabilityText();
    }

    public function getModelName ($_product_type)
    {
        return 'Belvg_CustomStockStatus_Model_Product_' . ucfirst(
                $_product_type);
    }
}