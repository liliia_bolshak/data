<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Helper_Image extends Mage_Core_Helper_Abstract
{

    const MEDIA_PATH = 'customstockstatus';

    protected $_allowedImageExt = array(
            'jpg',
            'jpeg',
            'gif',
            'png'
    );

    public function getAllowedImageExtensions ()
    {
        return $this->_allowedImageExt;
    }

    public function getImagesPath ()
    {
        return Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . self::MEDIA_PATH .
                 DIRECTORY_SEPARATOR;
    }

    public function getImageName ($option_id, $ext)
    {
        return $option_id . "." . strtolower($ext);
    }

    public function getImageFilePath ($option_id)
    {
        if ($filename = $this->checkImageExists($option_id)) {
            return $this->getImagesPath() . $filename;
        }

        return FALSE;
    }

    public function getImageUrl ($option_id)
    {
        $filename = $this->checkImageExists($option_id);
        $filename = $this->__($filename);
        if ($filename) {
            return Mage::getBaseUrl('media') . self::MEDIA_PATH . '/' .
                     $filename;
        }

        return FALSE;
    }

    public function getImageHtml ($url)
    {
        return '<img src="' . $url . '" alt=""/>';
    }

    public function checkImageExists ($option_id)
    {
        $io = new Varien_Io_File();

        foreach ($this->_allowedImageExt as $ext) {
            if ($io->fileExists(
                    $this->getImagesPath() .
                             $this->getImageName($option_id, $ext), TRUE)) {
                return $this->getImageName($option_id, $ext);
            }
        }

        return FALSE;
    }

    public function deleteImage ($option_id)
    {
        $filename = $this->getImageFilePath($option_id);

        $io = new Varien_Io_File();

        if ($io->fileExists($filename, TRUE)) {
            return $io->rm($filename);
        }

        return FALSE;
    }

    public function uploadImage ($option_id)
    {
        $adapter = new Zend_File_Transfer_Adapter_Http();

        if ($adapter->isUploaded("icon_{$option_id}_")) {
            $this->deleteImage($option_id);
            $upload = new Varien_File_Uploader("icon[$option_id]");
            $upload->setAllowCreateFolders(TRUE);
            $upload->setAllowedExtensions($this->getAllowedImageExtensions());
            $upload->setAllowRenameFiles(FALSE);
            $upload->setFilesDispersion(FALSE);

            $imagename = $this->getImageName($option_id, $upload->getFileExtension());

            $path = $this->getImagesPath();

            if ($upload->save($path, $imagename)) {
                return $upload->getUploadedFileName();
            }
        }

        return FALSE;
    }
}