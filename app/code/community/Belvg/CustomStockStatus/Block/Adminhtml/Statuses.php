<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Block_Adminhtml_Statuses extends Mage_Core_Block_Template
{

    protected $_message = array();

    public function __construct ()
    {
        parent::__construct();
        $this->setTemplate('belvg/customstockstatus/statuses.phtml');
    }

    public function getMessage ()
    {
        if (! empty($this->_message)) {
            return $this->_message;
        }

        return FALSE;
    }

    public function getOptions ()
    {
        return Mage::getModel('customstockstatus/statuses')->getOptions();
    }

    public function getImageUrl ($option_id)
    {
        return Mage::helper('customstockstatus/image')->getImageUrl($option_id);
    }

    public function getUpdateUrl ()
    {
        $attribute_id = Mage::app()->getRequest()->getParam('attribute_id');
        return $this->getUrl('customstockstatus/adminhtml_statuses/save',
                array(
                        'attribute_id' => $attribute_id
                ));
    }
}