<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Block_Product_View_Type_Bundle_Option extends Mage_Bundle_Block_Catalog_Product_View_Type_Bundle_Option

{

    public function getSelectionQtyTitlePrice ($_selection,
            $includeContainer = TRUE)
    {
        if (! Mage::helper('customstockstatus')->isEnabled()) {
            $priceTitle = parent::getSelectionQtyTitlePrice($_selection,
                    $includeContainer);
        } else {
            $cust = Mage::getModel('customstockstatus/product_bundle',
                    $this->getProduct());

            $price = $this->getProduct()
                ->getPriceModel()
                ->getSelectionPreFinalPrice($this->getProduct(), $_selection);
            $this->setFormatProduct($_selection);
            $priceTitle = $_selection->getSelectionQty() * 1 . ' x ' .
                     $this->escapeHtml($_selection->getName());
            /*
             * BELVG START
             */
            $priceTitle .= $cust->getOptionLabel($_selection);

            /*
             * BELVG END
             */

            $priceTitle .= ' &nbsp; ' .
                     ($includeContainer ? '<span class="price-notice">' : '') .
                     '+' . $this->formatPriceString($price, $includeContainer) .
                     ($includeContainer ? '</span>' : '');
        }

        return $priceTitle;
    }

    public function getSelectionTitlePrice ($_selection,
            $includeContainer = TRUE)
    {
        if (! Mage::helper('customstockstatus')->isEnabled()) {
            $priceTitle = parent::getSelectionTitlePrice($_selection,
                    $includeContainer);
        } else {
            $cust = Mage::getModel('customstockstatus/product_bundle',
                    $this->getProduct());

            $price = $this->getProduct()
                ->getPriceModel()
                ->getSelectionPreFinalPrice($this->getProduct(), $_selection, 1);
            $this->setFormatProduct($_selection);
            $priceTitle = $this->escapeHtml($_selection->getName());

            /*
             * BELVG START
             */
            $priceTitle .= $cust->getOptionLabel($_selection);
            /*
             * BELVG END
             */

            $priceTitle .= ' &nbsp; ' .
                     ($includeContainer ? '<span class="price-notice">' : '') .
                     '+' . $this->formatPriceString($price, $includeContainer) .
                     ($includeContainer ? '</span>' : '');
        }

        return $priceTitle;
    }
}