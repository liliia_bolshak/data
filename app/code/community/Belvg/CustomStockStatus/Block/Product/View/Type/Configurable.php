<?php

/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 ********************************************
 * MAGENTO EDITION USAGE NOTICE *
 ********************************************
 * This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 ************************
 *       DISCLAIMER     *
 ************************
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 ********************************************
 *
 * @category   Belvg
 * @package    Belvg_CustomStockStatus
 * @author     Victor Potseluyonok
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_CustomStockStatus_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{

    public function getAllowProducts ()
    {
        if (! $this->hasAllowProducts()) {
            $products = array();
            $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
            $allProducts = $this->getProduct()
                ->getTypeInstance(TRUE)
                ->getUsedProducts(NULL, $this->getProduct());
            foreach ($allProducts as $product) {
                /*
                 * BELVG START
                 */
                if (Mage::helper('customstockstatus')->isShowUnOrderableProduct()) {
                    $products[] = $product;
                } else {
                    if ($product->isSaleable() || $skipSaleableCheck) {
                        $products[] = $product;
                    }
                }

                /*
                 * BELVG END
                 */
            }

            $this->setAllowProducts($products);
        }

        return $this->getData('allow_products');
    }

    public function getJsonConfig ()
    {
        if (! Mage::helper('customstockstatus')->isEnabled()) {
            return parent::getJsonConfig();
        }

        /*
         * BELVG START
         */
        $cust = Mage::getModel('customstockstatus/product_configurable',
                $this->getProduct());
        $stockStatus = array();
        $stockStatus['displayAddToStatus'] = Mage::helper('customstockstatus')->isShowStockStatusAboveAddToCart();
        /*
         * BELVG END
         */

        $attributes = array();
        $options = array();
        $store = $this->getCurrentStore();
        $taxHelper = Mage::helper('tax');
        $currentProduct = $this->getProduct();

        $preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
        if ($preconfiguredFlag) {
            $preconfiguredValues = $currentProduct->getPreconfiguredValues();
            $defaultValues = array();
        }

        foreach ($this->getAllowProducts() as $product) {
            $productId = $product->getId();

            /*
             * BELVG START
             */
            $stockStatus['products'][$productId]['status_cart'] = $cust->getOptionLabel(
                    $productId);
            
            $stockStatus['products'][$productId]['status'] = htmlspecialchars_decode($stockStatus['products'][$productId]['status_cart']);
            
            $stockStatus['products'][$productId]['add_to_cart_enabled'] = $product->getStockItem()->getIsInStock();
            /*
             * BELVG END
             */

            foreach ($this->getAllowAttributes() as $attribute) {
                $productAttribute = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue = $product->getData(
                        $productAttribute->getAttributeCode());
                if (! isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (! isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }

                $options[$productAttributeId][$attributeValue][] = $productId;
            }
        }

        $this->_resPrices = array(
                $this->_preparePrice($currentProduct->getFinalPrice())
        );

        foreach ($this->getAllowAttributes() as $attribute) {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();
            $info = array(
                    'id' => $productAttribute->getId(),
                    'code' => $productAttribute->getAttributeCode(),
                    'label' => $attribute->getLabel(),
                    'options' => array()
            );

            $optionPrices = array();
            $prices = $attribute->getPrices();
            if (is_array($prices)) {
                foreach ($prices as $value) {
                    if (! $this->_validateAttributeValue($attributeId, $value,
                            $options)) {
                        continue;
                    }

                    $currentProduct->setConfigurablePrice(
                            $this->_preparePrice($value['pricing_value'],
                                    $value['is_percent']));
                    $currentProduct->setParentId(TRUE);
                    Mage::dispatchEvent(
                            'catalog_product_type_configurable_price',
                            array(
                                    'product' => $currentProduct
                            ));
                    $configurablePrice = $currentProduct->getConfigurablePrice();

                    if (isset($options[$attributeId][$value['value_index']])) {
                        $productsIndex = $options[$attributeId][$value['value_index']];
                    } else {
                        $productsIndex = array();
                    }

                    $info['options'][] = array(
                            'id' => $value['value_index'],
                            'label' => $value['label'],
                            'price' => $configurablePrice,
                            'oldPrice' => $this->_prepareOldPrice(
                                    $value['pricing_value'],
                                    $value['is_percent']),
                            'products' => $productsIndex
                    );
                    $optionPrices[] = $configurablePrice;
                }
            }

            /**
             * Prepare formated values for options choose
             */
            foreach ($optionPrices as $optionPrice) {
                foreach ($optionPrices as $additional) {
                    $this->_preparePrice(abs($additional - $optionPrice));
                }
            }

            if ($this->_validateAttributeInfo($info)) {
                $attributes[$attributeId] = $info;
            }

            // Add attribute default value (if set)
            if ($preconfiguredFlag) {
                $configValue = $preconfiguredValues->getData(
                        'super_attribute/' . $attributeId);
                if ($configValue) {
                    $defaultValues[$attributeId] = $configValue;
                }
            }
        }

        $taxCalculation = Mage::getSingleton('tax/calculation');
        if (! $taxCalculation->getCustomer() &&
                 Mage::registry('current_customer')) {
            $taxCalculation->setCustomer(Mage::registry('current_customer'));
        }

        $_request = $taxCalculation->getRateRequest(FALSE, FALSE, FALSE);
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $defaultTax = $taxCalculation->getRate($_request);

        $_request = $taxCalculation->getRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $currentTax = $taxCalculation->getRate($_request);

        $taxConfig = array(
                'includeTax' => $taxHelper->priceIncludesTax(),
                'showIncludeTax' => $taxHelper->displayPriceIncludingTax(),
                'showBothPrices' => $taxHelper->displayBothPrices(),
                'defaultTax' => $defaultTax,
                'currentTax' => $currentTax,
                'inclTaxTitle' => Mage::helper('catalog')->__('Incl. Tax')
        );

        $config = array(
                'attributes' => $attributes,
                'template' => str_replace('%s', '#{price}',
                        $store->getCurrentCurrency()->getOutputFormat()),
                'basePrice' => $this->_registerJsPrice(
                        $this->_convertPrice($currentProduct->getFinalPrice())),
                'oldPrice' => $this->_registerJsPrice(
                        $this->_convertPrice($currentProduct->getPrice())),
                'productId' => $currentProduct->getId(),
                'chooseText' => Mage::helper('catalog')->__(
                        'Choose an Option...'),
                'taxConfig' => $taxConfig
        );

        $config['stockStatus'] = $stockStatus;

        if ($preconfiguredFlag && ! empty($defaultValues)) {
            $config['defaultValues'] = $defaultValues;
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return Mage::helper('core')->jsonEncode($config);
    }
}