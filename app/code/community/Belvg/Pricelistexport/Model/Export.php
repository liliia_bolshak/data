<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Pricelistexport
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */ 
class Belvg_Pricelistexport_Model_Export extends Mage_Catalog_Model_Category
{
    
    public function getCategorysProducts($parent = FALSE, $attributes)
    {
        $result = array();
        $full_name = array();
        $last_level = 0;
        if ($parent) { 
            $parentCategory = Mage::getModel('catalog/category')->load($parent);
            $curr_level = $parentCategory->getLevel();
        }
        
        $recursionLevel  = max(0, (int) Mage::app()->getStore()->getConfig('catalog/navigation/max_depth'));      
        $toLoad = TRUE;
        $sorted = 'position';
        $asCollection = TRUE;
        $storeCategories = $this->getCategories($parent, $recursionLevel, $sorted, $asCollection, $toLoad);
        $storeCategories = $this->sortCat($storeCategories, $curr_level);
        foreach ($storeCategories as $key=>$category) {    
            $curr_level = $category->getLevel();
            $result[$key]['level'] = $curr_level;
            $result[$key]['name'] = $category->getName();
            if ($last_level > $curr_level) {
                for ($i = $last_level; $i > $curr_level; $i--) {
                    unset($full_name[$i]);
                }
            }
            
            $full_name[$category->getLevel()] = $category->getName();
            $result[$key]['full_name'] = implode(' - ', $full_name);
            $last_level = $curr_level;
            $result[$key]['products'] = $this->_getCategoryProducts($category, $attributes);
        }
        
        if (empty($storeCategories)) {
            $result['-1']['products'] = $this->_getCategoryProducts($parentCategory, $attributes);
        }
        
        return $result;
    }
    
    protected function _getCategoryProducts($category, $attributes)
    {
        $items = Mage::getModel('catalog/product')->getCollection()->addCategoryFilter($category);
        $items->addAttributeToSelect('visibility')->addAttributeToFilter('visibility', array(2,3,4));
        foreach ($attributes as $attribute) {
            $items->addAttributeToSelect($attribute['attribute_code']);
        }
        
        if (Mage::getStoreConfigFlag('pricelistexport/content/assimple')) {
            $cat_ids = $items->getColumnValues('entity_id');
            $tmp = array();
            foreach ($items as $item) {
                if ($item->getTypeId() == 'simple') {
                    $tmp[] = $item;
                } elseif ($item->getTypeId() == 'configurable') {
                    $ids = $this->getAssociatedProductsIds($item->getEntityId());
                    if ($ids) {
                        $products = Mage::getModel('catalog/product')->getCollection()
                                                                     ->addFieldToFilter('entity_id', $ids);
                        foreach ($attributes as $attribute) {
                            $products->addAttributeToSelect($attribute['attribute_code']);
                        }
                        
                        foreach ($products as $product) {
                            if (!in_array($product->getEntityId(), $cat_ids)) {
                                $tmp[] = $product;
                            }
                        }
                    }
                }
            }
            
            $items = $tmp;
        }        
        
        $result = array();
        foreach ($items as $key=>$item) {
            if ($tmp = $item->getData()) {
                if ($item->getTypeId() == 'configurable' || $item->getTypeId() == 'simple') {
                    foreach ($attributes as $attribute) {
                        if ($attribute['attribute_code'] == 'stock') {
                            if ($item->getTypeId() == 'configurable') {
                                $result[$key]['stock'] = $this->getConfProductStock($item->getEntityId());
                            } else {
                                $result[$key]['stock'] = Mage::getModel('cataloginventory/stock_item')->getCollection()
                                                             ->addFieldToFilter('product_id', $item->getEntityId())
                                                             ->getLastItem()->getQty();
                            }                        
                        } elseif ($attribute['attribute_code'] == 'price') {
                            if ($item->getTypeId() == 'configurable') {
                                $result[$key]['price'] = Mage::helper('pricelistexport')->__('from') . ' ' . number_format(Mage::app()->getStore()->convertPrice($item->getFinalPrice(1)), 2, ',', '');
                            } else {
                                $result[$key]['price'] = number_format(Mage::app()->getStore()->convertPrice($item->getFinalPrice(1)), 2, ',', '');
                            }
                        } else {
                            if (isset($tmp[$attribute['attribute_code']])) {
                                if ($attribute['frontend_input'] == 'select') {
                                     $attributeModel = Mage::getSingleton('eav/config')
                                                           ->getAttribute('catalog_product', $attribute['attribute_code']);
                                     $result[$key][$attribute['attribute_code']] = $attributeModel->getSource()->getOptionText($tmp[$attribute['attribute_code']]);
                                } else {
                                     $result[$key][$attribute['attribute_code']] = $tmp[$attribute['attribute_code']];
                                }
                            } else {
                                $result[$key][$attribute['attribute_code']] = '';
                            }
                        }
                    }
                }
            }   
        } 
                    
        return $result;
    }
    
    
    /**
     *
     *
     */
    public function getConfProductStock($product_id)
    {   
        $ids = $this->getAssociatedProductsIds($product_id);
        if ($ids) {
            $products = Mage::getModel('catalog/product')->getCollection()
                                                         ->addFieldToFilter('entity_id', $ids);
            $stock = Mage::getModel('cataloginventory/stock_item')->getCollection()
                         ->addFieldToFilter('product_id', $ids)
                         ->getColumnValues('qty');
            return array_sum($stock);
        } else {
            return 0;
        }
    }
    
    /**
     *
     *
     */
    public function getAssociatedProductsIds($product_id)
    {
        $ids = array();
        $childIds = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product_id);
        foreach ($childIds[0] as $key=>$childId) { 
            $ids[] = $childId; 
        }
        
        return $ids;
    }
    
    /**
     *
     *
     */
    public function sortCat($storeCategories, $lev)
    {
        function sortByPosition($a, $b) 
        {
            if ($a->getPosition() == $b->getPosition()) {
                return 0;
            }
            
            return ($a->getPosition() > $b->getPosition()) ? -1 : 1;
        }
        
        function sortByName($a, $b) 
        {
            if ($a->getName() == $b->getName()) {
                return 0;
            }
            
            return ($a->getName() > $b->getName()) ? -1 : 1;
        }
        
        $lev++;
        $result = array();
        do {
            $tmp = array();
            foreach ($storeCategories as $category) {
                if ($category->getLevel() == $lev) { 
                    $tmp[] = $category;
                }
            }
            
            $lev++;
            if (Mage::getStoreConfig('pricelistexport/content/catsort') == 'name') {
                usort($tmp, "sortByName");
            } else {
                usort($tmp, "sortByPosition");
            }
            
            foreach ($tmp as $cat) {
                $par = $cat->getParentId();
                foreach ($result as $key=>$cat2) {
                    if ($cat2->getEntityId() == $par) {
                        array_splice($result, $key + 1, 0, array($cat));
                    }
                }
            }
            
            if (empty($result)) {        
                $result = array_reverse($tmp);
            }
        } while (!empty($tmp));
        
        return $result;
    }
    
}