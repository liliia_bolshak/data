<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Pricelistexport
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Pricelistexport_Model_Source_Logo
{
    
    public function toOptionArray()
    {
        $helper = Mage::helper('pricelistexport');
        return array(array('value' => 0, 'label' => $helper->__('Default Store Logo')),
                     array('value' => 1, 'label' => $helper->__('Logo Url')),
                     array('value' => 2, 'label' => $helper->__('Upload Logo')));
    }
    
}