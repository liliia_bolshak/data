<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Pricelistexport
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Pricelistexport_Model_Source_Attributes
{
    
    public function toOptionArray()
    {  
        $types = array('text', 'select', 'decimal');
        $exclAttributeCodes = array('name', 'price');        
        $attrs = Mage::getResourceModel('catalog/product_attribute_collection')
                     ->addFieldToFilter('frontend_input', $types)
                     ->getData();
        $data = array();
        foreach ($attrs as $attr) {
            if (!empty($attr['frontend_label']) && !in_array($attr['attribute_code'], $exclAttributeCodes)) {
                array_push($data, array('value' => $attr['attribute_code'], 
                                        'label' => $attr['frontend_label']));
            }
        }
        
        return $data;
    }
    
}