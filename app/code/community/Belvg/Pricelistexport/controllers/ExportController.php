<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Pricelistexport
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */ 
class Belvg_Pricelistexport_ExportController extends Mage_Core_Controller_Front_Action
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $attributes = Mage::helper('pricelistexport')->getAttributes();
        if (!$parent = $this->getRequest()->getParam('cat_id')) {
            $parent = Mage::app()->getStore()->getRootCategoryId();
        }

        $pExcel = Mage::helper('pricelistexport')->getStructure($attributes, $parent);
        $format = $this->getRequest()->getParam('format');
        if ($format == 'xls') {
            include("PHPExcel/PHPExcel/Writer/Excel5.php");
            $objWriter = new PHPExcel_Writer_Excel5($pExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="pricelist.xls"');
        }    

        if ($format == 'pdf') {
            include("PHPExcel/PHPExcel/Writer/PDF.php");
            $objWriter = new PHPExcel_Writer_PDF($pExcel);
            header('Content-Type: application/vnd.pdf');
            header('Content-Disposition: attachment;filename="pricelist.pdf"');
        } 
    
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        
    }
 
}