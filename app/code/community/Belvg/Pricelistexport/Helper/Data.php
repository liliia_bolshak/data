<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Pricelistexport
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Pricelistexport_Helper_Data extends Mage_Core_Helper_Data 
{

    public function isEnabled()
    {
        if (Mage::getStoreConfigFlag('pricelistexport/settings/full') 
        || Mage::getStoreConfigFlag('pricelistexport/settings/cat')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isFull()
    {
        return Mage::getStoreConfigFlag('pricelistexport/settings/full');
    }

    public function isCat()
    {
        return Mage::getStoreConfigFlag('pricelistexport/settings/cat');
    }    

    public function getAttributes()
    {
        if (Mage::getStoreConfigFlag('pricelistexport/content/enable_attr')) {
            $tmp = explode(',', Mage::getStoreConfig('pricelistexport/content/attributes'));
            $tmp = Mage::getResourceModel('catalog/product_attribute_collection')
                       ->addFieldToFilter('attribute_code', array('in' => $tmp));
        } else {
            $tmp = array();
        }
        
        $helper = Mage::helper('pricelistexport');             
        $result = array();
        $result[] = array('attribute_code' => 'name',
                                    'name' => $helper->__('Product Name'),
                          'frontend_input' => 'text');
        foreach ($tmp as $item) {
            $result[] = array('attribute_code' => $item->getAttributeCode(),
                                        'name' => $item->getFrontendLabel(),
                              'frontend_input' => $item->getFrontendInput());
        }
        
        if (Mage::getStoreConfig('pricelistexport/content/stock')) {
            $result[] = array('attribute_code' => 'stock',
                                        'name' => 'Qty',
                              'frontend_input' => 'decimal');   
        }
        
        $result[] = array('attribute_code' => 'price',
                                    'name' => $helper->__('Price') . ', ' . $this->getCurrencySymbol(),
                          'frontend_input' => 'decimal');
        return $result;
    }
    
    public function getCurrencySymbol()
    {
        $currCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $currSymbol = Mage::app()->getLocale()->currency($currCode)->getSymbol();
        if ($currSymbol) {
            return $currSymbol;
        } else {
            return $currCode;
        }

    }
    
    public function getExportUrl($type = 'xls')
    {
        $result = Mage::getUrl('pricelistexport/export/index');
        switch ($type) {
            case 'xls': 
                $result .= 'format/xls';
                break;
                
            case 'pdf':
                $result .= 'format/pdf';
                break;
                
            default: 
                break;
        }
        
        return $result;
    }
    
    public function getLogoUrl()
    {
        if (Mage::getStoreConfig('pricelistexport/content/custom_logo')) {
            $url = Mage::getUrl('media/pricelistexport') . Mage::getStoreConfig('pricelistexport/content/custom_logo');
            $tmp = explode('/', $url);
            $url = array_slice($tmp, array_search('media', $tmp));
            return './' . implode('/', $url);
        } else {
            $url = Mage::getDesign()->getSkinUrl(Mage::getStoreConfig('design/header/logo_src'));
            $tmp = explode('/', $url);
            $url = array_slice($tmp, array_search('skin', $tmp));
            return './' . implode('/', $url);
        }        
    }
    
    
    public function getStructure($attributes, $parent = FALSE)
    {
        $result = Mage::getModel('pricelistexport/export')->getCategorysProducts($parent, $attributes);
        
        include_once 'PHPExcel/PHPExcel.php';
        $pExcel = new PHPExcel();
        $pExcel->setActiveSheetIndex(0);
        $aSheet = $pExcel->getActiveSheet();
        
        $headStyle = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                     'startcolor' => array('argb' => 'D0D0D0D0')));
        $categoryStyle = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                         'startcolor' => array('argb' => 'E5E5E5E5')));
        $oddStyle = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'startcolor' => array('argb' => 'FFFFFFFF')),
                          'font' => array('name' => 'Arial Cyr',
                                          'size' => '8',
                                          'bold' => FALSE),
                      'alignment'=> array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
        $evenStyle = array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                     'startcolor' => array('argb' => 'F7F7F7F7')),
                           'font' => array('name' => 'Arial Cyr',
                                           'size' => '8',
                                           'bold' => FALSE),
                      'alignment'=> array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));                                    

        $y=1;
        $x='A';
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setPath($this->getLogoUrl());
        $objDrawing->setHeight(40);
        $objDrawing->setOffsetX(8);
        $objDrawing->setOffsetY(8);
        $objDrawing->setCoordinates('A1');
        $objDrawing->setWorksheet($aSheet);
        $aSheet->getRowDimension('1')->setRowHeight(40);
        $tmp = 'A';
        for ($i=0; $i < (count($attributes)-1); $i++) {
            $tmp++;
        }
        
        $aSheet->mergeCells($x . $y . ':' . --$tmp . $y);
        
        $y=2;
        $x='A';
        foreach ($attributes as $attribute) {
            $aSheet->setCellValue($x . $y, $attribute['name']);
            $aSheet->getStyle($x . $y)->applyFromArray($headStyle);
            $x++;
        }
        
        $y=3;
        foreach ($result as $key=>$row) {
            if ($key >= 0) {;
                $x = 'A';
                $aSheet->setCellValue($x . $y, $row['full_name']);
                $aSheet->getStyle($x . $y)->applyFromArray($categoryStyle);
                $tmp = 'A';
                for ($i=0; $i < (count($attributes)-1); $i++) {
                    $tmp++;
                }
                
                $aSheet->mergeCells($x . $y . ':' . --$tmp . $y);
                $y++;
            }
            
            if (!empty($row['products'])) {
                foreach ($row['products'] as $product) {
                    $x = 'A';
                    foreach ($attributes as $attribute) {
                        $aSheet->setCellValue($x . $y, $product[$attribute['attribute_code']]);
                        if (($y % 2) == 0) {
                            $aSheet->getStyle($x . $y)->applyFromArray($evenStyle);
                        } else {
                            $aSheet->getStyle($x . $y)->applyFromArray($oddStyle);
                        }
                        
                        $x++;
                    }
                    
                    $y++;                    
                }
            }
        }
       
        $x='A';
        foreach ($attributes as $attribute) {
            $aSheet->getColumnDimension($x)->setAutoSize(TRUE);
            $x++;
        }

        return $pExcel;
    }
 
}