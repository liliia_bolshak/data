<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Block_Designer_Options extends Mage_Core_Block_Template
{

    protected $options;

    protected $design_options = array(
        'bandamagnetica_front',
        'bandamagnetica_back',

        'chipcontatto_front',
        'chipcontatto_back',

        'chipcrfid_front',
        'chipcrfid_back',

        'barcode_front',
        'barcode_back',

        'scratch_front',
        'scratch_back',

        'firma_front',
        'firma_back',

        'asolaturacard',

        'punzonatura_front',
        'punzonatura_back',

        'numbering_front',
        'numbering_back',

        'persstampafoto_front',
        'persstampafoto_back',

        'silkbackground_front',
        'silkbackground_back'

    );

    protected function _construct()
    {
        parent::_construct();
        $this->options = Mage::getModel('gomage_designer/options');
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getDesignConfig()
    {
        $configs = array();

        foreach ($this->design_options as $option) {
            $option_model = $this->options->getOption($option);
            if ($config = $option_model->getDesignConfig()) {
                $configs[$option_model->getKey()] = $config;
            }
        }

        return json_encode($configs);
    }

    public function getPriceUrl()
    {
        return $this->getUrl('designer/index/calcOptionsPrice');
    }

}