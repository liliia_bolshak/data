<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Block_Designer_UploadFile extends Mage_Core_Block_Template
{
    public function hasFiles()
    {
        return count(Mage::getModel('gomage_designer/uploadedFile')->getCustomerUploadedFiles()) > 0;
    }

    public function getRemoveFileUrl()
    {
        return $this->getUrl('designer/index/removeUploadedFiles');
    }
}