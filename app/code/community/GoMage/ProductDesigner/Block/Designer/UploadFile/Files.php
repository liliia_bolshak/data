<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Block_Designer_UploadFile_Files extends Mage_Core_Block_Template
{
    /**
     * Return upload files
     *
     * @return GoMage_ProductDesigner_Model_Mysql4_UploadedFile_Collection
     */
    public function getUploadedFiles()
    {
        return Mage::getModel('gomage_designer/uploadedFile')->getCustomerUploadedFiles();
    }


    public function getFileUrl($file)
    {
        return Mage::getSingleton('gomage_designer/uploadedFile_config')->getMediaUrl($file);
    }
}