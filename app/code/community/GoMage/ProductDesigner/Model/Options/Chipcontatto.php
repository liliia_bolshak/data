<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Chipcontatto extends GoMage_ProductDesigner_Model_Options_Abstract
{

    protected $prices = array(
        'FM4442'   => array(
            '100'   => 3.171,
            '250'   => 1.268,
            '500'   => 0.634,
            '1000'  => 0.317073171,
            '2000'  => 0.304878049,
            '3000'  => 0.292682927,
            '4000'  => 0.280487805,
            '5000'  => 0.268292683,
            '8000'  => 0.268292683,
            '10000' => 0.231707317,
            '20000' => 0.231707317,
            '50000' => 0.220,
        ),
        'SLE5542'  => array(
            '100'   => 2.622,
            '250'   => 1.049,
            '500'   => 0.524,
            '1000'  => 0.390,
            '2000'  => 0.378,
            '3000'  => 0.366,
            '4000'  => 0.366,
            '5000'  => 0.354,
            '8000'  => 0.354,
            '10000' => 0.341,
            '20000' => 0.341,
            '50000' => 0.317,
        ),
        'FM4428'   => array(
            '100'   => 2.622,
            '250'   => 1.049,
            '500'   => 0.524,
            '1000'  => 0.390,
            '2000'  => 0.378,
            '3000'  => 0.366,
            '4000'  => 0.366,
            '5000'  => 0.354,
            '8000'  => 0.354,
            '10000' => 0.341,
            '20000' => 0.341,
            '50000' => 0.317,
        ),
        'SLE5528'  => array(
            '100'   => 3.537,
            '250'   => 1.415,
            '500'   => 0.707317073,
            '1000'  => 0.585365854,
            '2000'  => 0.573170732,
            '3000'  => 0.56097561,
            '4000'  => 0.56097561,
            '5000'  => 0.548780488,
            '8000'  => 0.548780488,
            '10000' => 0.536585366,
            '20000' => 0.512195122,
            '50000' => 0.500,
        ),
        'AT24C02'  => array(
            '100'   => 2.500,
            '250'   => 1.000,
            '500'   => 0.500,
            '1000'  => 0.378,
            '2000'  => 0.378,
            '3000'  => 0.378,
            '4000'  => 0.366,
            '5000'  => 0.354,
            '8000'  => 0.354,
            '10000' => 0.329,
            '20000' => 0.317,
            '50000' => 0.305,
        ),
        'AT24C016' => array(
            '100'   => 2.988,
            '250'   => 1.195,
            '500'   => 0.598,
            '1000'  => 0.476,
            '2000'  => 0.463,
            '3000'  => 0.451,
            '4000'  => 0.451,
            '5000'  => 0.439,
            '8000'  => 0.439,
            '10000' => 0.427,
            '20000' => 0.415,
            '50000' => 0.390,
        ),
        'AT24C064' => array(
            '100'   => 3.476,
            '250'   => 1.390,
            '500'   => 0.695,
            '1000'  => 0.573,
            '2000'  => 0.561,
            '3000'  => 0.549,
            '4000'  => 0.549,
            '5000'  => 0.537,
            '8000'  => 0.537,
            '10000' => 0.512,
            '20000' => 0.500,
            '50000' => 0.488,
        ),
    );

    CONST DEFAULT_VALUE = 'no';

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('Without chip'),
            'FM4442'            => $helper->__('FM4442'),
            'SLE5542'           => $helper->__('SLE5542'),
            'FM4428'            => $helper->__('FM4428'),
            'SLE5528'           => $helper->__('SLE5528'),
            'AT24C02'           => $helper->__('AT24C02'),
            'AT24C016'          => $helper->__('AT24C016'),
            'AT24C064'          => $helper->__('AT24C064'),
        );
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Contact chip');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[$this->getValue()][GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'chipcontatto';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                'FM4442'   => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'SLE5542'  => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'FM4428'   => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'SLE5528'  => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'AT24C02'  => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'AT24C016' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
                'AT24C064' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/chip.jpg',
            ),
            'position'      => array(
                'left' => 450,
                'top'  => 180,
            )
        );
    }

}