<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Abstract extends Varien_Object
{

    protected $value;

    /**
     * @return string
     */
    abstract public function getKey();

    /**
     * @return mixed
     */
    abstract public function getValues();

    /**
     * @return string
     */
    abstract public function getLabel();

    /**
     * @param  int $qty
     * @return float
     */
    abstract public function getPrice($qty = 1);

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValueLabel($value)
    {
        $values = $this->getValues();
        if (isset($values[$value])) {
            return $values[$value];
        }
        return $value;
    }

    public function getDesignConfig()
    {
        return false;
    }

}