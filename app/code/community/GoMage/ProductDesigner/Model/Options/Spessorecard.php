<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options_Spessorecard extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = '80';

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('0,80 mm'),
            '50'                => $helper->__('0,50 mm'),
            '40'                => $helper->__('0,40 mm'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Card Thickness');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'spessorecard';
    }
}