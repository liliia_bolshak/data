<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options_Flowpack extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = '0';

    protected $prices = array(
        '100'   => 0.201,
        '250'   => 0.080,
        '500'   => 0.080,
        '1000'  => 0.080,
        '2000'  => 0.080,
        '3000'  => 0.080,
        '4000'  => 0.080,
        '5000'  => 0.070,
        '8000'  => 0.070,
        '10000' => 0.070,
        '20000' => 0.070,
        '50000' => 0.070,
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No'),
            '1'                 => $helper->__('Yes'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Flow Pack');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'flowpack';
    }
}