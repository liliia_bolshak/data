<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options_Asolaturacard extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        '100'   => 0.183,
        '250'   => 0.073,
        '500'   => 0.018,
        '1000'  => 0.018,
        '2000'  => 0.018,
        '3000'  => 0.018,
        '4000'  => 0.018,
        '5000'  => 0.018,
        '8000'  => 0.018,
        '10000' => 0.018,
        '20000' => 0.018,
        '50000' => 0.018,
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No Punching'),
            'hole'              => $helper->__('Hole Punch'),
            'slot'              => $helper->__('Slot Punch'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Card Punching');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'asolaturacard';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                'hole' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/hole_punch.png',
                'slot' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/slot_punch.png',
            )
        );
    }

    public function getPositions()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            'top_middle'    => $helper->__('Top middle'),
            'left_middle'   => $helper->__('Left middle'),
            'right_middle'  => $helper->__('Right middle'),
            'bottom_middle' => $helper->__('Bottom middle'),
        );
    }

}