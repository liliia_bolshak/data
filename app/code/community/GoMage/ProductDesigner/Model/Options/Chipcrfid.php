<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Chipcrfid extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        '125RO'      => array(
            '100'   => 2.134,
            '250'   => 0.854,
            '500'   => 0.427,
            '1000'  => 0.317,
            '2000'  => 0.305,
            '3000'  => 0.293,
            '4000'  => 0.280,
            '5000'  => 0.262,
            '8000'  => 0.262,
            '10000' => 0.213,
            '20000' => 0.207,
            '50000' => 0.207,
        ),
        '1K_S50'     => array(
            '100'   => 5.976,
            '250'   => 2.390,
            '500'   => 1.195,
            '1000'  => 0.598,
            '2000'  => 0.585,
            '3000'  => 0.561,
            '4000'  => 0.549,
            '5000'  => 0.537,
            '8000'  => 0.537,
            '10000' => 0.512,
            '20000' => 0.512,
            '50000' => 0.494,
        ),
        'COMP'       => array(
            '100'   => 2.134,
            '250'   => 0.854,
            '500'   => 0.427,
            '1000'  => 0.317,
            '2000'  => 0.305,
            '3000'  => 0.293,
            '4000'  => 0.280,
            '5000'  => 0.262,
            '8000'  => 0.262,
            '10000' => 0.213,
            '20000' => 0.207,
            '50000' => 0.207,
        ),
        '4K_S70'     => array(
            '100'   => 9.512,
            '250'   => 3.805,
            '500'   => 1.902439024,
            '1000'  => 0.951,
            '2000'  => 0.939,
            '3000'  => 0.927,
            '4000'  => 0.915,
            '5000'  => 0.902,
            '8000'  => 0.902,
            '10000' => 0.866,
            '20000' => 0.866,
            '50000' => 0.854,
        ),
        'ULTRALIGHT' => array(
            '100'   => 2.561,
            '250'   => 1.024,
            '500'   => 0.512195122,
            '1000'  => 0.390,
            '2000'  => 0.378,
            '3000'  => 0.366,
            '4000'  => 0.366,
            '5000'  => 0.354,
            '8000'  => 0.354,
            '10000' => 0.341,
            '20000' => 0.329,
            '50000' => 0.317,
        ),
        'T5577'      => array(
            '100'   => 5.488,
            '250'   => 2.195,
            '500'   => 1.097560976,
            '1000'  => 0.549,
            '2000'  => 0.537,
            '3000'  => 0.524,
            '4000'  => 0.512,
            '5000'  => 0.488,
            '8000'  => 0.488,
            '10000' => 0.463,
            '20000' => 0.463,
            '50000' => 0.451,
        ),
        'ICODE'      => array(
            '100'   => 2.744,
            '250'   => 1.098,
            '500'   => 0.548780488,
            '1000'  => 0.426829268,
            '2000'  => 0.414634146,
            '3000'  => 0.402439024,
            '4000'  => 0.402439024,
            '5000'  => 0.390243902,
            '8000'  => 0.390243902,
            '10000' => 0.365853659,
            '20000' => 0.353658537,
            '50000' => 0.305,
        ),
        'UHF_GEN2'   => array(
            '100'   => 0.000,
            '250'   => 0.000,
            '500'   => 0,
            '1000'  => 0,
            '2000'  => 0,
            '3000'  => 0,
            '4000'  => 0,
            '5000'  => 0,
            '8000'  => 0,
            '10000' => 0,
            '20000' => 0,
            '50000' => 0.000,
        )
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('Without chip'),
            '125RO'             => $helper->__('125RO'),
            '1K_S50'            => $helper->__('Mifare 1K S50'),
            'COMP'              => $helper->__('Mifare Comp'),
            '4K_S70'            => $helper->__('Mifare 4K S70'),
            'ULTRALIGHT'        => $helper->__('Ultralight'),
            'T5577'             => $helper->__('T5577'),
            'ICODE'             => $helper->__('ICODE'),
            'UHF_GEN2'          => $helper->__('UHF Gen2'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Chip RFID');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[$this->getValue()][GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'chipcrfid';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                '125RO'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                '1K_S50'     => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                'COMP'       => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                '4K_S70'     => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                'ULTRALIGHT' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                'T5577'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                'ICODE'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
                'UHF_GEN2'   => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/rfid.jpg',
            )
        );
    }


}