<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Bandamagnetica extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        'loco' => array(
            '100'   => 0.037,
            '250'   => 0.015,
            '500'   => 0.007,
            '1000'  => 0.004,
            '2000'  => 0.004,
            '3000'  => 0.004,
            '4000'  => 0.004,
            '5000'  => 0.004,
            '8000'  => 0.004,
            '10000' => 0.004,
            '20000' => 0.004,
            '50000' => 0.004,
        ),
        'hico' => array(
            '100'   => 0.110,
            '250'   => 0.044,
            '500'   => 0.022,
            '1000'  => 0.011,
            '2000'  => 0.011,
            '3000'  => 0.011,
            '4000'  => 0.011,
            '5000'  => 0.011,
            '8000'  => 0.011,
            '10000' => 0.011,
            '20000' => 0.011,
            '50000' => 0.011,

        ),
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    public function getKey()
    {
        return 'bandamagnetica';
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No Stripe'),
            'loco'              => $helper->__('Lo-Co (300 Oe)'),
            'hico'              => $helper->__('Hi-Co (2750 Oe)'),
        );
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Magnetic stripe');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[$this->getValue()][GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                'loco' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/loco.png',
                'hico' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/hico.png',
            ),
            'position'      => array(
                'left' => 275,
                'top'  => 50,
            )
        );
    }

}