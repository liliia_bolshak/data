<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_UploadedFile extends Mage_Core_Model_Abstract
{
    protected $_urlModel;

    protected function _construct()
    {
        $this->_init('gomage_designer/uploadedFile');
    }

    private function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    public function getUrl($filePath)
    {
        return $this->getConfig()->getBaseTmpMediaUrl() . $filePath;
    }

    public function getDestinationPath($filePath)
    {
        return $this->getConfig()->getBaseMediaPath() . $filePath;
    }

    public function getTempPath($filePath)
    {
        return $this->getConfig()->getBaseTmpMediaPath() . $filePath;
    }

    public function getDestinationDir($filePath)
    {
        $expFilePath = explode('/', $filePath);
        array_pop($expFilePath);
        $filePath = implode('/', $expFilePath);
        if (strpos($filePath, $this->getConfig()->getBaseMediaPath()) === false) {
            $filePath = $this->getConfig()->getBaseMediaPath() . $filePath;
        }

        return $filePath;
    }

    public function getConfig()
    {
        return Mage::getSingleton('gomage_designer/uploadedFile_config');
    }

    public function getCustomerUploadedFiles()
    {
        return $this->_getUploadedFilesFromSession()->getItems();
    }

    protected function _getUploadedFilesFromSession()
    {
        /**
         * @var $collection GoMage_ProductDesigner_Model_Mysql4_UploadedFile_Collection
         */
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $designerSessionId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        } else {
            $designerSessionId = $this->_getDesignerSessionId();
        }
        $collection = $this->getResourceCollection();
        $collection
            ->addFieldToFilter('session_id', $designerSessionId)
            ->addFieldToFilter('design_id', array('null' => true));

        return $collection;
    }

    public function getDesignUploadedFiles($design_id = 0)
    {
        $collection = $this->getResourceCollection();
        $collection
            ->addFieldToFilter('design_id', $design_id);

        return $collection->getItems();
    }

    protected function _getDesignerSessionId()
    {
        return Mage::helper('gomage_designer')->getDesignerSessionId();
    }

    public function removeUploadedFiles()
    {
        $ids = $this->_getUploadedFilesFromSession()->getAllIds();
        if (!empty($ids)) {
            $this->getResource()->removeFilesByIds($ids);
        }

        return $this;
    }

}