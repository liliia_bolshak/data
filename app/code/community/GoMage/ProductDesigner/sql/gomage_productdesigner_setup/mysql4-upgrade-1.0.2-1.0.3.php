<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

$installer = $this;
/* @var $installer GoMage_ProductDesigner_Model_Resource_Setup */

$installer->startSetup();

try {

    $installer->getConnection()->addColumn(
        $installer->getTable('gomage_designer/uploadedFile'),
        'original_name',
        array(
            'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'comment' => 'File Name'
        )

    );

    $installer->getConnection()->addColumn(
        $installer->getTable('gomage_designer/uploadedFile'),
        'design_id',
        array(
            'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'unsigned' => true,
            'nullable' => true,
            'comment'  => 'Design Id'
        )
    );

    $installer->endSetup();

} catch (Exception $e) {
    Mage::logException($e);
} catch (Mage_Core_Exception $e) {
    Mage::logException($e);
}