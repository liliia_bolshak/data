<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

/**
 * Designer controller
 *
 * @category   GoMage
 * @package    GoMage_ProductDesigner
 * @subpackage controllers
 * @author     Roman Bublik <rb@gomage.com>
 */
class GoMage_ProductDesigner_IndexController extends Mage_Core_Controller_Front_Action
{
    public function dispatch($action)
    {
        $moduleEnabled = Mage::getStoreConfig('gomage_designer/general/enabled', Mage::app()->getStore());
        if (!$moduleEnabled) {
            $action = 'noRoute';
        }
        parent::dispatch($action);
    }

    /**
     * Inititalize product
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initializeProduct()
    {
        return Mage::helper('gomage_designer')->initializeProduct();
    }

    /**
     * Index action
     *
     * @return void
     */
    public function indexAction()
    {
        $product = $this->_initializeProduct();
        if ($product->getId() && (!$product->getEnableProductDesigner() || !$product->hasImagesForDesign())
            && !Mage::helper('gomage_designer')->isNavigationEnabled()
        ) {
            $this->_redirectReferer();
        } elseif (!$product->getId() && !Mage::helper('gomage_designer')->isNavigationEnabled()) {
            $this->_redirectReferer();
        }
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->_getTitle());
        $this->renderLayout();
    }

    /**
     * Change product action
     *
     * @return void
     */
    public function changeProductAction()
    {
        $request = $this->getRequest();
        $isAjax  = (bool)$request->getParam('ajax');
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        try {
            $product = $this->_initializeProduct();
            if (!$product || !$product->getId()) {
                throw new Exception(Mage::helper('gomage_designer')->__('Product with id %d not found',
                    $this->getRequest()->getParam('id')
                ));
            }
            $settings     = Mage::helper('gomage_designer')->getProductSettingForEditor($product);
            $responseData = array(
                'product_settings' => $settings,
                'design_price'     => $this->_getDesignPriceHtml(),
                'price_config'     => Mage::helper('gomage_designer')->getProductPriceConfig(),
            );
            if ($productColors = $product->getProductColors()) {
                $responseData['product_colors'] = $productColors;
            }
            Mage::helper('gomage_designer/ajax')->sendSuccess($responseData);
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
        }
    }

    /**
     * Filter products action
     *
     * @return void
     */
    public function filterProductsAction()
    {
        $isAjax = (bool)$this->getRequest()->getPost('ajax');
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        $this->loadLayout();
        $navigationBlock = $this->getLayout()->getBlock('productNavigator');
        $responseData    = array(
            'navigation_filters'  => $navigationBlock->getFiltersHtml(),
            'navigation_prodcuts' => $navigationBlock->getProductListHtml()
        );

        Mage::helper('gomage_designer/ajax')->sendSuccess($responseData);
    }

    /**
     * Filter cliparts action
     *
     * @return void
     */
    public function filterClipartsAction()
    {
        $isAjax = (bool)$this->getRequest()->getPost('ajax');
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        $this->loadLayout();
        $designBlock  = $this->getLayout()->getBlock('design');
        $responseData = array(
            'filters'  => $designBlock->getChildHtml('design.filters'),
            'cliparts' => $designBlock->getChildHtml('design.cliparts')
        );
        Mage::helper('gomage_designer/ajax')->sendSuccess($responseData);
    }

    /**
     * Save product designed images and redirect to product view page
     *
     * @return void
     */
    public function continueAction()
    {
        /**
         * @var $request Mage_Core_Controller_Request_Http
         * @var $product Mage_Catalog_Model_Product
         */
        $request = $this->getRequest();
        $isAjax  = $request->isAjax();
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }
        try {
            $design  = $this->_saveDesign();
            $product = Mage::registry('product');
            Mage::helper('gomage_designer/ajax')->sendRedirect(array(
                    'url'       => $product->getDesignedProductUrl($design->getId()),
                    'design_id' => $design->getId()
                )
            );
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
            Mage::logException($e);
        }
    }

    protected function _saveDesign()
    {
        $design = Mage::helper('gomage_designer')->saveProductDesignedImages();

        $design_id = Mage::app()->getRequest()->getParam('design_id', 0);

        $files = Mage::getModel('gomage_designer/UploadedFile')->getCustomerUploadedFiles();
        if (count($files)) {
            foreach ($files as $file) {
                if (!$file->getData('design_id')) {
                    $file->setData('design_id', $design->getDesignId())->save();
                }
            }
        }

        if ($design_id) {
            $product = Mage::getModel('catalog/product')->load($design->getData('associated_product_id'));

            $qty = $this->getRequest()->getParam('qty', 1);
            $product->setPrice($design->getPrice($qty));

            $mediaApi      = Mage::getModel("catalog/product_attribute_media_api");
            $mediaApiItems = $mediaApi->items($product->getId());
            foreach ($mediaApiItems as $item) {
                $mediaApi->remove($product->getId(), $item['file']);
            }
            $product->save();

            $product = Mage::getModel('catalog/product')->load($design->getData('associated_product_id'));

            $images = $design->getImages($design->getId())->getItems();

            if ($images) {
                foreach ($images as $image) {
                    $filePath = Mage::getModel('gomage_designer/design_config')->getMediaPath($image->getImage());
                    $product->addImageToMediaGallery($filePath, null, false, false);
                }
            }

            $product->save();

            $mediaGallery = $product->getMediaGallery();

            if (isset($mediaGallery['images'])) {
                foreach ($mediaGallery['images'] as $image) {
                    Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array(
                            'image'       => $image['file'],
                            'small_image' => $image['file'],
                            'thumbnail'   => $image['file'],
                        ), 0
                    );
                    break;
                }
            }

        } else {
            $design = $this->_associateProduct($design);
        }

        return $design;
    }

    protected function _associateProduct($design)
    {
        $qty = $this->getRequest()->getParam('qty', 1);

        $design_product = Mage::getModel('catalog/product')->load($design->getData('product_id'));

        $product = Mage::getModel('catalog/product');
        $product->setSku(sprintf("CARD-%05d", $design->getDesignId()));
        $product->setAttributeSetId($design_product->getAttributeSetId());
        $product->setTypeId('simple');
        $product->setName($design_product->getName());
        $product->setCategoryIds(array(626));
        $product->setWebsiteIds($design_product->getWebsiteIds());
        $product->setDescription($design_product->getDescription());
        $product->setShortDescription($design_product->getShortDescription());
        $product->setPrice($design->getPrice($qty));
        $product->setWeight($design_product->getWeight());
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $product->setTaxClassId($design_product->getTaxClassId());
        $product->setCreatedAt(strtotime('now'));

        $product->setData('codcat', 'IS');
        $product->setData('type_product_ariete', 'PD');

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $product->setData('ariete_allowed_customers', Mage::getSingleton('customer/session')->getCustomer()->getId());
        }

        $images = $design->getImages($design->getId())->getItems();

        if ($images) {
            foreach ($images as $image) {
                $filePath = Mage::getModel('gomage_designer/design_config')->getMediaPath($image->getImage());
                $product->addImageToMediaGallery($filePath, null, false, false);
            }
        }

        $product->save();

        $mediaGallery = $product->getMediaGallery();

        if (isset($mediaGallery['images'])) {
            foreach ($mediaGallery['images'] as $image) {
                Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array(
                        'image'       => $image['file'],
                        'small_image' => $image['file'],
                        'thumbnail'   => $image['file'],
                    ), 0
                );
                break;
            }
        }

        $design->setData('associated_product_id', $product->getId())->save();

        $content       = '';
        $options       = json_decode($design->getOptions());
        $options_model = Mage::getModel('gomage_designer/options');
        foreach ($options as $key => $value) {
            $option = $options_model->getOption($key);
            $content .= $option->getLabel() . ': ' . $option->getValueLabel($value) . '; ';
        }

        $erp_resource = Mage::getSingleton('core/resource');
        $connection   = $erp_resource->getConnection('erp_db');
        $date         = date("Y-m-d");
        $connection->query("insert into `jartana` (`codice`, `tipart`, `keyrif`, `descri`, `codgru`, `codrag`, `codcat`, `codtip`, `codpap`, `gesdbp`, `gesdba`, `tipdba`, `newart`, `ubicaz`, `tiputf`, `omaggi`, `codbar`, `status`, `desart`, `desar2`, `desar3`, `unitam`, `codiva`, `kconai`, `codacq`, `codven`, `incsta`, `tippro`, `numpez`, `numcof`, `pervol`, `perzuc`, `capaci`, `kgpeso`, `volspe`, `tipcon`, `sogcnt`, `codutf`, `coddog`, `costos`, `persco`, `perric`, `lotrio`, `multiplominimo`, `scomin`, `scomax`, `codsec`, `codcal`, `codczu`, `conplt`, `cspmat`, `csplav`, `modell`, `verspe`, `impre1`, `giaril`, `datins`, `daturd`, `uteurd`, `tipurd`, `cartec`, `codcnt`, `tipcnt`, `kitdba`, `skeocl`, `artacq`, `blkacq`, `artven`, `blkven`, `desrid`, `indlav`, `nogiac`, `codimm`, `IdArticoloMagazzino`, `IndiceTrasfArticoloMag`, `gestlottiserial`, `idForPrinc`, `revisionedbt`, `nomeimmagine`, `nomepdf`, `immagine`, `filepdf`, `pubblicaweb`, `noteweb`, `codvpco`, `codcrim`, `utedis`, `datdis`, `codprod`, `idRiordino`, `lavconsegnare`, `idcoddog`, `desing1`, `uming`, `nwebing`, `filepdfing`, `nomefilepdfing`, `priori`, `artaccisa`, `idregprofin`, `idavvisodoc`, `idproduttore`, `firmware`, `rapunsupintra`, `imbutf`, `utemod`, `datmod`, `genautosn`, `idcassa`, `disaccisadoc`)
                            values(null,'9','{$product->getSku()}','CARDPRINTINGJOB{$product->getSku()}','61','6','IS','PD','','0','N','','0','','','','','0','CARD PRINTING JOB {$product->getSku()}','','','PZ','','','A01','V01','0','','0.000','0.000','0.000','0.000','0.000','0.00600','0.00000','0','0','0','','0.00000','','','0.000','0.000','0.000','0.000','0','0','0','0.00000','0.00000','0.00000','','','0.000','0.000','{$date}',NULL,'','',NULL,'0','','N','N','S','0','1','0','','','N','0','0','0','Nessuno','0','0','','',NULL,NULL,'0','{$content}','0','0','',NULL,'','0','0','0','','',NULL,NULL,'','0','0','0','0','0','','0.00000','0','WEB','{$date}','0','0','0');"
        );

        return $design;
    }

    public function uploadImagesAction()
    {
        $this->loadLayout();
        $block                = $this->getLayout()->getBlock('uploadedImages');
        $baseMediaPath        = Mage::getSingleton('gomage_designer/uploadedImage_config')->getBaseMediaPath();
        $allowedFormatsString = str_replace('/', ',', Mage::getStoreConfig('gomage_designer/upload_image/format'));
        $maxUploadFileSize    = Mage::getStoreConfig('gomage_designer/upload_image/size');
        $allowedFormats       = explode(',', $allowedFormatsString);
        $sessionId            = $this->getSessionId();
        $customerId           = (int)$this->_getCustomerSession()->getCustomerId();
        $uploadedFilesCount   = 0;
        $errors               = array();

        try {
            if (!isset($_FILES['filesToUpload'])) {
                throw new Exception(Mage::helper('gomage_designer')->__('Please, select files for upload'));
            }
            $files = $this->prepareFilesArray($_FILES['filesToUpload']);
            foreach ($files as $file) {
                if (!$file['name']) {
                    continue;
                }
                if ($file['error'] === UPLOAD_ERR_INI_SIZE || $file['error'] === UPLOAD_ERR_FORM_SIZE
                    || $file['size'] > $maxUploadFileSize * 1024 * 1024
                ) {
                    $errors['size'] = Mage::helper('gomage_designer')->__('You can not upload files larger than %d MB', $maxUploadFileSize);
                    continue;
                }

                $imageExtension = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                if (!in_array($imageExtension, $allowedFormats)) {
                    $errors['type'] = Mage::helper('gomage_designer')->__('Cannot upload the file. The format is not supported. Supported file formats are: %s', $allowedFormatsString);
                    continue;
                }

                $fileName       = substr(sha1(microtime()), 0, 20) . Mage::helper('gomage_designer')->formatFileName($file['name']);
                $fileDir        = '/' . ($customerId ? $customerId : $sessionId) . '/';
                $destinationDir = $baseMediaPath . $fileDir;
                if (!file_exists($destinationDir)) {
                    mkdir($destinationDir, 0777, true);
                }
                $destinationFile = $destinationDir . $fileName;

                if (move_uploaded_file($file['tmp_name'], $destinationFile)) {

                    if ($imageExtension == 'pdf') {
                        $img = new Imagick($destinationFile);
                        $img->setImageFormat('png');
                        $img->writeImage(str_replace('.pdf', '.png', $destinationFile));
                        $fileName = str_replace('.pdf', '.png', $fileName);
                    }

                    $imageData   = array(
                        'image'       => $fileDir . $fileName,
                        'customer_id' => $customerId,
                        'session_id'  => $customerId ? $customerId : $sessionId
                    );
                    $uploadImage = Mage::getModel('gomage_designer/uploadedImage');
                    $uploadImage->setData($imageData);
                    $uploadImage->resizeImage(Mage::getSingleton('gomage_designer/uploadedImage_config')
                            ->getMediaPath($fileDir . $fileName)
                    );
                    $uploadImage->save();
                    $uploadedFilesCount++;
                }
            }

            if (!empty($errors)) {
                throw new Exception(implode("\n", $errors));
            }
            if ($uploadedFilesCount == 0) {
                throw new Exception(Mage::helper('gomage_designer')->__('Please, select files for upload'));
            }

        } catch (Exception $e) {
            $block->setError($e->getMessage());
        }

        $content = preg_replace('/\t+|\n+|\s{2,}/', '', $block->toHtml());

        $this->getResponse()->setBody($content);
    }

    public function removeUploadedImagesAction()
    {
        $isAjax = (bool)$this->getRequest()->getPost('ajax');
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        try {
            Mage::getModel('gomage_designer/uploadedImage')->removeCustomerUploadedImages();
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
            return;
        }

        $this->loadLayout();
        $block = $this->getLayout()->getBlock('uploadedImages');
        Mage::helper('gomage_designer/ajax')->sendSuccess(array(
                'content' => $block->toHtml()
            )
        );
    }

    public function uploadFilesAction()
    {
        $this->loadLayout();
        $block         = $this->getLayout()->getBlock('uploadedFiles');
        $baseMediaPath = Mage::getSingleton('gomage_designer/uploadedFile_config')->getBaseMediaPath();

        $sessionId          = $this->getSessionId();
        $customerId         = (int)$this->_getCustomerSession()->getCustomerId();
        $uploadedFilesCount = 0;
        $errors             = array();

        try {
            if (!isset($_FILES['dbfilesToUpload'])) {
                throw new Exception(Mage::helper('gomage_designer')->__('Please, select files for upload'));
            }
            $files = $this->prepareFilesArray($_FILES['dbfilesToUpload']);
            foreach ($files as $file) {
                if (!$file['name']) {
                    continue;
                }
                if ($file['error'] === UPLOAD_ERR_INI_SIZE || $file['error'] === UPLOAD_ERR_FORM_SIZE) {
                    $errors['size'] = Mage::helper('gomage_designer')->__('File size is too large for destination.');
                    continue;
                }

                $fileName       = substr(sha1(microtime()), 0, 20) . Mage::helper('gomage_designer')->formatFileName($file['name']);
                $fileDir        = '/' . ($customerId ? $customerId : $sessionId) . '/';
                $destinationDir = $baseMediaPath . $fileDir;
                if (!file_exists($destinationDir)) {
                    mkdir($destinationDir, 0777, true);
                }
                $destinationFile = $destinationDir . $fileName;

                if (move_uploaded_file($file['tmp_name'], $destinationFile)) {
                    $fileData = array(
                        'file'          => $fileDir . $fileName,
                        'original_name' => $file['name'],
                        'customer_id'   => $customerId,
                        'session_id'    => $customerId ? $customerId : $sessionId
                    );
                    $upload   = Mage::getModel('gomage_designer/uploadedFile');
                    $upload->setData($fileData);
                    $upload->save();
                    $uploadedFilesCount++;
                }
            }

            if (!empty($errors)) {
                throw new Exception(implode("\n", $errors));
            }
            if ($uploadedFilesCount == 0) {
                throw new Exception(Mage::helper('gomage_designer')->__('Please, select files for upload'));
            }

        } catch (Exception $e) {
            $block->setError($e->getMessage());
        }

        $content = preg_replace('/\t+|\n+|\s{2,}/', '', $block->toHtml());
        $this->getResponse()->setBody($content);
    }

    public function removeUploadedFilesAction()
    {
        $isAjax = (bool)$this->getRequest()->getPost('ajax');
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        try {
            Mage::getModel('gomage_designer/uploadedFile')->removeUploadedFiles();
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
            return;
        }

        $this->loadLayout();
        $block = $this->getLayout()->getBlock('uploadedFiles');
        Mage::helper('gomage_designer/ajax')->sendSuccess(array(
                'content' => $block->toHtml()
            )
        );
    }

    public function saveDesignAction()
    {
        $request = $this->getRequest();
        $isAjax  = $request->isAjax();
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        try {
            $design = $this->_saveDesign();
            Mage::helper('gomage_designer/ajax')->sendSuccess(array('design_id' => $design->getId()));
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
        }
    }

    protected function _getDesignPriceHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('gomage_designer_design_price');
        $layout->generateXml();
        $layout->generateBlocks();
        return $layout->getOutput();
    }

    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    protected function getSessionId()
    {
        return $this->_getCustomerSession()->getEncryptedSessionId();
    }

    protected function prepareFilesArray($files)
    {
        $filesArray = array();
        foreach ($files as $key => $values) {

            foreach ($values as $valueKey => $value) {
                $filesArray[$valueKey][$key] = $value;
            }
        }
        return $filesArray;
    }

    protected function _getTitle()
    {
        $title = Mage::getStoreConfig('gomage_designer/general/page_title');
        return $title ? : Mage::getStoreConfig('design/head/default_title');
    }

    public function calcOptionsPriceAction()
    {
        $request = $this->getRequest();
        $isAjax  = $request->isAjax();
        if (!$isAjax) {
            $this->norouteAction();
            return;
        }

        try {
            $model = Mage::getModel('gomage_designer/options');

            $options = json_decode($request->getParam('options'));
            $qty     = $request->getParam('qty', 1);

            Mage::helper('gomage_designer/ajax')->sendSuccess(array('price' => $model->getOptionsPrice($options, $qty)));
        } catch (Exception $e) {
            Mage::helper('gomage_designer/ajax')->sendError($e->getMessage());
        }
    }

}
