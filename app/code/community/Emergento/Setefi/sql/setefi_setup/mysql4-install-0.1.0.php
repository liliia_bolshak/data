<?php
$installer = $this;
$prefix = substr(preg_replace('#[aeiou\s]+#i', '',current(array_slice(explode("_", strtolower($installer->_resourceConfig->setup->module)), 1,1))),0,3);
$module_name = current(array_slice(explode("_", strtolower($installer->_resourceConfig->setup->module)), 1,1));
$installer->startSetup();

/*
$flag = false;

$page = Mage::getModel('cms/page')->load('error-payment', 'identifier')->getId();


if(!$page) {
$cmsPageData = array(
    'title' => 'Pagamento Non riuscito',
    'root_template' => 'one_column',
    'meta_keywords' => '',
    'meta_description' => '',
    'identifier' => 'error-payment',
    'content_heading' => 'content heading',
    'stores' => array(0),
    'content' => "<p>Il tuo acquisto &egrave; stato interrotto. Ti &egrave; stata inviata una mail con un link che ti permetter&agrave; di continuare l'acquisto da dove lo hai lasciato!</p>"
);

Mage::getModel('cms/page')->setData($cmsPageData)->save();
}
*/

$flag_tmail=false;
$templates_mail = Mage::getModel('core/email_template')->getCollection();

foreach ($templates_mail as $key => $tm) {
    if($tm->getTemplateCode() == 'payment_request' ) $flag_tmail=true;
}

if(!$flag_tmail) {
$template_mail = Mage::getModel('core/email_template')
		->setTemplateCode('payment_request')
		->setTemplateSubject('richiesta di pagamento')
		->setTemplateText('Gentile {{var customer}},
In riferimento al suo ordine #{{var order}}, effettuato su le inviamo un link per effettuare il pagamento con la sua carta di credito.
<a href="{{var url}}">{{var url}}</a>.
Per informazioni ci contatti al numero {{config path="general/store_information/phone"}} o al nostro indirizzo email: {{config path="trans_email/ident_general/email"}}
Cordiali Saluti,
{{config path="general/store_information/name"}}')
		->setTemplateType(2)
		->save();
}

$rand_prefix = new Mage_Core_Model_Config();
$rand_prefix ->saveConfig('payment/' . $module_name . '_cc/transaction_prefix', randomPrefix('stf'), 'default', 0);



function randomPrefix($fixed_prefix="emerg",$str_length = 0, $int_leingth = 2, $char_sep = 1) {
    $str = $fixed_prefix;
    $ints = "0123456789";
    $chars = "abcdefghijklmnopqrstuvwxyz";

    for ($i = 0; $i < $str_length; $i++){
        $str .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    for ($j = 0; $j < $int_leingth; $j++){
        $str .= $ints[mt_rand(0, strlen($ints) - 1)];
    }
    for ($k = 0; $k < $char_sep; $k++){
        $str .= $chars[mt_rand(0, strlen($chars) - 1)];
    }

    return $str;
}
$installer->endSetup();
