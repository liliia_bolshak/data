<?php

$installer = $this;
$installer->startSetup();

$installer->run("

    DROP TABLE IF EXISTS `{$this->getTable('tierprices/tierpricesdataset')}`;
    CREATE TABLE `{$this->getTable('tierprices/tierpricesdataset')}` (
    `id` int(11) unsigned NOT NULL auto_increment,
    `website_id` SMALLINT(5) UNSIGNED NOT NULL,
    `cust_group` SMALLINT(5) UNSIGNED NOT NULL,
    `price_qty` int(11) unsigned,
    `product_id` INT(10) UNSIGNED NOT NULL,
    `tier_price_index` int(11) unsigned NOT NULL,
    `percent` int(11) unsigned NOT NULL,  
    PRIMARY KEY (`id`,`product_id`,`website_id`,`cust_group`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();