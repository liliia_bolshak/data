<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Model_Observer extends Mage_Core_Block_Abstract {

    public function productLoadAfter(Varien_Event_Observer $observer)
    {
        /** @var $product Mage_Catalog_Model_Product */
        $product = $observer->getEvent()->getProduct();

        /** @var $tierPriceModel Magpleasure_Tierprices_Model_Tierpricesdataset  */
        $tierPriceModel = Mage::getModel('tierprices/tierpricesdataset');
        $tierPrices = $tierPriceModel->prepareTierPrices($product->getId());
        $product->setCustomTierPrice($tierPrices);


    }

    public function productSaveBefore(Varien_Event_Observer $observer)
    {
        /** @var $product Mage_Catalog_Model_Product */
        $product = $observer->getEvent()->getProduct();

        /** @var $tierPriceModel Magpleasure_Tierprices_Model_Tierpricesdataset  */
        $tierPriceModel = Mage::getModel('tierprices/tierpricesdataset');

        $product->setCustomTierPrice($product->getData('tier_price'));
        $product->setTierPrice(
                        $tierPriceModel->getNativeFromCustom(
                            (($product->getData('tier_price') && is_array($product->getData('tier_price'))) ?
                            $product->getData('tier_price') :
                            array()),
                            $product
                        ));

        return $this;
    }

    public function productSaveAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $product = Mage::getModel('tierprices/tierpricesdataset')
            ->updateProduct($product);

        return $this;
    }

    public function productDublicate(Varien_Event_Observer $observer)
    {

        $currentProduct = $observer->getEvent()->getCurrentProduct();
        $newProduct = $observer->getEvent()->getNewProduct();
        $currentProductId = $currentProduct->getId();
        $tierPricesArray = $currentProduct->getTierPrice();
        $tierPriceCollection = Mage::getModel('tierprices/tierpricesdataset')
            ->getCollection()
            ->addProductFilter($currentProductId);

        $index = 0;

        foreach ($tierPriceCollection as $tierPriceItem) {

            if ($tierPriceItem->getTierPriceIndex()) {
                $tierPricesArray[$index]['dinamic_tier'] =
                    Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT;
            } else {
                $tierPricesArray[$index]['dinamic_tier'] =
                    Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED;
            }

            $tierPricesArray[$index]['cust_group'] = $tierPriceItem->getCustGroup();
            $tierPricesArray[$index]['website_id'] = $tierPriceItem->getWebsiteId();
            $tierPricesArray[$index]['price_qty'] = $tierPriceItem->getPriceQty();
            $tierPricesArray[$index]['price'] = $tierPriceItem->getPercent();
            $tierPricesArray[$index]['percent'] = $tierPriceItem->getPercent();

            if (isset($tierPricesArray[$index]['price_id'])){
                unset($tierPricesArray[$index]['price_id']);
            }

            $index++;
        }

        /** @var $tierPriceModel Magpleasure_Tierprices_Model_Tierpricesdataset  */
        $tierPriceModel = Mage::getModel('tierprices/tierpricesdataset');

        $newProduct->setData('tier_price', $tierPricesArray);
        return $this;
    }

    public function productFormPrepareBefore(Varien_Event_Observer $observer)
    {

        $form = $observer->getEvent()->getForm();
        $productEntity = $form->getDataObject();
        $productId = $productEntity->getEntityId();

        $tierPricesArray = $productEntity->getData('tier_price');

        $tierPriceCollection = Mage::getModel('tierprices/tierpricesdataset')
                ->getCollection()
                ->addProductFilter($productId);

        $index = 0;
        foreach ($tierPriceCollection as $tierPriceItem) {

            if (!isset($tierPricesArray[$index])){
                $tierPricesArray[$index] = array();
            }

            if ($tierPriceItem->getTierPriceIndex()) {
                $tierPricesArray[$index]['dinamic_tier'] = Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT;
            } else {
                $tierPricesArray[$index]['dinamic_tier'] = Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED;
            }

            $tierPricesArray[$index]['cust_group'] = $tierPriceItem->getCustGroup();
            $tierPricesArray[$index]['website_id'] = $tierPriceItem->getWebsiteId();
            $tierPricesArray[$index]['price_qty'] = $tierPriceItem->getPriceQty();
            $tierPricesArray[$index]['price'] = $tierPriceItem->getPercent();

            $index++;
        }

        $productEntity->setData('tier_price', $tierPricesArray);
        $form->setData('data_object', $productEntity);

        return $this;
    }

    protected function _setIndexUpdateFlag()
    {
        try {

            /** @var $process Mage_Index_Model_Process  */
            $process = Mage::getModel('index/process');
            $process->load('catalog_product_price', 'indexer_code');
            $process->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX)->save();

        } catch (Exception $e) {
            Mage::logException($e);
        }


        return $this;
    }

    protected function _validateTierPrices($tierPriceData)
    {
        $id = 0;
        $dataArray = array();
        foreach ($tierPriceData as $row){
            $dataArray[$id] = $row;
            $dataArray[$id]['id'] = $id + 1;
            $id++;
        }

        foreach ($dataArray as $row){
            if (is_array($row) && !$row['delete']){
                foreach ($dataArray as $subRow){
                    if (is_array($subRow) && !$subRow['delete']){
                        if (
                            ($row['id'] !== $subRow['id']) &&
                            ($row['website_id'] == $subRow['website_id']) &&
                            ($row['cust_group'] == $subRow['cust_group']) &&
                            ($row['price_qty'] == $subRow['price_qty'])
                        ){
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function PreDespatch($observer)
    {

        $event = $observer->getData();
        $action = $event['controller_action'];
        $post = $action->getRequest()->getPost();

        if ($event['controller_action'] instanceof Mage_Adminhtml_Catalog_Product_Action_AttributeController) {
            $productIds = $this->helper('tierprices/adminhtml_catalog_product_edit_action_attribute')->getProductIds();
            $tierPriceData = $action->getRequest()->getParam('tier_price', array());

            # Fast Tier Prices Update

            if (isset($post['attributes']['price']) && !$action->getRequest()->getPost('tierprice_need_change')){
                $price = $post['attributes']['price'];

                /** @var $tierPriceModel Magpleasure_Tierprices_Model_Mysql4_Tierpricesdataset */
                $tierPriceModel = Mage::getResourceModel('tierprices/tierpricesdataset');

                $productData  = $tierPriceModel->prepareFastProductData($productIds);

                foreach ($productData as $productId=>$data){
                    $tierPriceModel->fastUpdateNativeTierPrices($productId, $price, $data['type_id']);
                }

                if (count($productIds)){
                    $this->_setIndexUpdateFlag();
                }

            # Update of Tier Prices and Native Prices
            } elseif ($needChange = $action->getRequest()->getPost('tierprice_need_change')) {

                if (!$this->_validateTierPrices($tierPriceData)){
                    Mage::throwException("Duplicate website tier price customer group and quantity.");
                }

                foreach ($productIds as $productId) {
                    $product = Mage::getModel('catalog/product')->load($productId);

                    if (isset($post['attributes']['price'])){
                        $price = $post['attributes']['price'];
                        $product->setData('price', $price);
                    }

                    /** @var $tierPriceModel Magpleasure_Tierprices_Model_Mysql4_Tierpricesdataset */
                    $tierPriceModel = Mage::getResourceModel('tierprices/tierpricesdataset');
                    $productData  = $tierPriceModel->prepareFastProductData($productIds);

                    # Save Magpleasure Tier Prices
                    foreach ($productData as $productId=>$data){
                        $tierPriceModel->fastUpdateMagpleasureTierPrices($tierPriceData , $productId, (isset($price) ? $price : $data['price']), $data['type_id']);
                    }

                    # Save Native Tier Prices
                    foreach ($productData as $productId=>$data){
                        $tierPriceModel->fastUpdateNativeTierPrices($productId, (isset($price) ? $price : $data['price']), $data['type_id'], $tierPriceData);
                    }

                    if (isset($price)){
                        unset($price);
                    }
                }

                try {
                    if (count($productIds)){
                        $this->_setIndexUpdateFlag();
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
    }

}

