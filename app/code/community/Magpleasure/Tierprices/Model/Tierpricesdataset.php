<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Model_Tierpricesdataset extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('tierprices/tierpricesdataset');
    }

    public function prepareTierPrices($productId)
    {
        return $this->getResource()->prepareTierPrices($productId);
    }

    public function loadByProductId($productId)
    {
        $this->getResource()->loadByProductId($this, $productId);
        return $this;
    }

    public function loadByTierIndexForProduct($tierPriceIndex, $productId)
    {
        $this->getResource()->loadByTierIndexForProduct($this, $tierPriceIndex, $productId);
        return $this;
    }

    public function loadByTierIndexForProductByWiCgPq($productId, $websiteId, $customergroup, $productQty)
    {
        $this->getResource()->loadByTierIndexForProductByWiCgPq($this, $productId, $websiteId, $customergroup, $productQty);
        return $this;
    }

    public function getNativeFromCustom(array $customPrices, $product = null, $price = null, $pType = null)
    {
        $nativePrices = array();

        if ($product){
            $productPrice = $product->getPrice();
            $productType = $product->getTypeId();
        } else {
            $productPrice = $price;
            $productType = $pType;
        }

        foreach ($customPrices as $tierPriceIndex => $productTierPrice) {

            if (isset($productTierPrice['dinamic_tier']) && $productTierPrice['dinamic_tier']) {
                $nativePrices[$tierPriceIndex] = $productTierPrice;
                $nativePrices['customer_group_id'] = $productTierPrice['cust_group'];
                $percentage = $productTierPrice['price'];

                if ($percentage > 100){
                    $percentage = 100;
                }

                if ($productType == "bundle") {
                    $priceToPay = $productTierPrice['price'];
                } else {
                    # update price for product, this price goes to Magento database as fixed price
                    $discount = ($percentage - 0.0001) / 100;
                    $save = $productPrice * $discount;
                    $priceToPay = $productPrice - $save;
                }
                $nativePrices[$tierPriceIndex]['price'] = $priceToPay;

            } else {
                if ($productType != "bundle") {
                    $nativePrices[$tierPriceIndex] = $productTierPrice;
                }
            }
        }
        return $nativePrices;
    }

    public function getCustomFromNative(array $nativePrices, Mage_Catalog_Model_Product $product)
    {
        ///TODO



    }

    // update collection by new tierPrice
    public function updateProduct($product)
    {
        $productTierPrices = $product->getData('custom_tier_price') ? $product->getData('custom_tier_price') : array();
        $indexesToUnset = array();
        $message = "";
        foreach ($productTierPrices as $tierPriceIndex => $productTierPrice) {

            $tirePriceToSave = Mage::getModel('tierprices/tierpricesdataset')
                ->loadByTierIndexForProductByWiCgPq(
                                                    $product->getId(),
                                                    $productTierPrice['website_id'],
                                                    $productTierPrice['cust_group'],
                                                    $productTierPrice['price_qty']
                                                );

            $tirePriceToSave
                ->setProductId($product->getId())
                ->setWebsiteId($productTierPrice['website_id'])
                ->setCustGroup($productTierPrice['cust_group'])
                ->setPriceQty($productTierPrice['price_qty']);

            if (isset($productTierPrice['dinamic_tier']) && $productTierPrice['dinamic_tier']) {
                # save percent
                $tirePriceToSave
                    ->setTierPriceIndex(Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT)
                    ->setPercent($productTierPrice['price'])
                    ->save();

            } else {

                if ($product->getTypeId() != "bundle") {
                    $tirePriceToSave
                        ->setTierPriceIndex(Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED)
                        ->setPercent($productTierPrice['price'])
                        ->save();
                } else {
                    $productTierPrice['delete'] = '1';
                    $message = Mage::helper('tierprices')->__("Fixed price will no applied to %s because it's bundle.", $product->getName());
                }
            }

            if (isset($productTierPrice['delete']) && $productTierPrice['delete']) {

                $toDel = Mage::getModel('tierprices/tierpricesdataset')
                    ->loadByTierIndexForProductByWiCgPq(
                                                            $product->getId(),
                                                            $productTierPrice['website_id'],
                                                            $productTierPrice['cust_group'],
                                                            $productTierPrice['price_qty']
                                                        );

                if ($toDel->getId()){
                    $toDel->delete();
                }
            }
        }

        if ($message){
            Mage::getSingleton('core/session')->addError($message);
        }

        return $product;
    }

}
