<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Model_Process extends Mage_Core_Block_Abstract
{
    protected $_cartData = array();

    /**
     * Helper
     *
     * @return Magpleasure_Tierprices_Helper_Data
     */
    protected function _helper()
    {
        return Mage::helper('tierprices');
    }

    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get quote object associated with cart. By default it is current customer session quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->_quote) {
            $this->_quote = $this->getCheckoutSession()->getQuote();
        }
        return $this->_quote;
    }

    /**
     * List of shopping cart items
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract|array
     */
    public function getItems()
    {
        if (!$this->getQuote()->getId()) {
            return array();
        }

        if (!$this->hasData('items')){
            $this->setData('items', $this->getQuote()->getItemsCollection());
        }
        return $this->getData('items');
    }

    protected function _hasChildInCart($parent)
    {
        foreach ($this->getItems() as $item){
            if ($item->getParentItemId() && ($item->getParentItemId() == $parent->getId())){
                return true;
            }
        }
        return false;
    }

    public function getRealQtyInCart(Mage_Catalog_Model_Product $product)
    {
        if (!isset($this->_cartData[$product->getId()])){

            $data = array();
            if (!isset($data[$product->getId()])){
                foreach ($this->getItems() as $item){

                    /** @var $item Mage_Sales_Model_Order_Item */
                    if ($parentItem = $item->getParentItem()){
                        $prodId = $parentItem->getProductId();
                        $prodQty = $parentItem->getQty();
                    } else {
                        if ($this->_hasChildInCart($item)){
                            continue;
                        }
                        $prodId = $item->getProductId();
                        $prodQty = $item->getQty();
                    }


                    if (isset($data[$prodId])){
                        $data[$prodId] = $data[$prodId] + $prodQty;
                    } else {
                        $data[$prodId] = $prodQty;
                    }
                }
            }
            $this->_cartData[$product->getId()] = isset($data[$product->getId()]) ? $data[$product->getId()] : 0;
        }

        return $this->_cartData[$product->getId()];
    }

    protected function _isCheckout()
    {
        return (in_array(Mage::app()->getRequest()->getModuleName(), array('checkout','onestepcheckout')));
    }

    public function processFinalPrice($event)
    {
        if ($this->_helper()->confApplyForGrouped()){

            /** @var $product Mage_Catalog_Model_Product */
            $product = $event->getProduct();
            $qty = $event->getQty();

            if ($this->_isCheckout() && $product && count($product->getTierPriceCount())){

                if (!Mage::registry('mp_tier_prices_is_nesting_'.$product->getId())){

                    Mage::register('mp_tier_prices_is_nesting_'.$product->getId(), true, true);
                    $realQty = $this->getRealQtyInCart($product);
                    $realTierPrice = $product->getTierPrice($realQty ? $realQty : $qty);

                    $tierPrice = $product->getTierPrice($qty);

                    if ($product->getData('special_price')){
                        try {
                            $finalPrice = (float)max(0, min($product->getData('special_price'), $realTierPrice));
                        } catch (Exception $e){
                            $finalPrice = $product->getData('special_price');
                        }

                    } else {
                        try {
                            $finalPrice = (float)max(0, $realTierPrice);
                        } catch (Exception $e) {
                            $finalPrice = null;
                        }


                    }

                    Mage::register('mp_tier_prices_final_price_'.$product->getId(), $finalPrice, true);
                    if ($finalPrice){
                        $product->setFinalPrice($finalPrice);
                    }

                } else {

                    if ($finalPrice = Mage::registry('mp_tier_prices_final_price_'.$product->getId())){
                        $product->setFinalPrice($finalPrice);
                    }
                }

            }
        }
    }

}

