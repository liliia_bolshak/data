<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Model_Mysql4_Tierpricesdataset extends Mage_Core_Model_Mysql4_Abstract {

    protected $_read;

    public function _construct() {
        $this->_init('tierprices/tierpricesdataset', 'id');
        $this->_read = $this->_getReadAdapter();
    }

    /**
     * Retrives name of table in DB
     * @param string $tableName
     * @return string
     */
    public function getDbTable($tableName)
    {
        if (!isset($this->_tables[$tableName])) {
            $this->_tables[$tableName] = Mage::getSingleton('core/resource')->getTableName($tableName);
        }
        return $this->_tables[$tableName];
    }
    
    public function loadByProductId($tirePriceData, $productId) {

        $select = $this->_read->select()
                ->from($this->getDbTable('mp_tierprices_dataset'))
                ->where("product_id=?", $productId);

        if ($data = $this->_read->fetchRow($select)) {
            $tirePriceData->addData($data);
        }
        $this->_afterLoad($tirePriceData);
        return $this;
    }
    
    public function loadByTierIndexForProduct($tirePriceData, $tierPriceIndex, $productId) {
                
        $select = $this->_read->select()
                ->from($this->getDbTable('mp_tierprices_dataset'))
                ->where("product_id=?", $productId)
                ->where("tier_price_index=?", $tierPriceIndex);

        if ($data = $this->_read->fetchRow($select)) {
            $tirePriceData->addData($data);
        }
        $this->_afterLoad($tirePriceData);
        return $this;
        
    }
    
    public function loadByTierIndexForProductByWiCgPq($tirePriceData, $productId, $websiteId, $customergroup, $productQty) {
                
        $select = $this->_read->select()
                ->from($this->getDbTable('mp_tierprices_dataset'))
                ->where("product_id=?", $productId)
                ->where("website_id=?", $websiteId)
                ->where("cust_group=?", $customergroup)
                ->where("price_qty=?", $productQty);

        if ($data = $this->_read->fetchRow($select)) {
            $tirePriceData->addData($data);
        }
        $this->_afterLoad($tirePriceData);
        return $this;
        
    }

    public function prepareTierPrices($productId)
    {
        $tierPricesArray = array();
        /** @var $tirePrices Magpleasure_Tierprices_Model_Mysql4_Tierpricesdataset_Collection */
        $tierPrices = Mage::getModel('tierprices/tierpricesdataset')->getCollection();
        $tierPrices->addProductFilter($productId);
        $index = 0;

        foreach ($tierPrices as $tierPriceItem) {

            if ($tierPriceItem->getTierPriceIndex()) {
                $tierPricesArray[$index]['dinamic_tier'] =
                    Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT;
            } else {
                $tierPricesArray[$index]['dinamic_tier'] =
                    Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED;
            }

            $tierPricesArray[$index]['cust_group'] = $tierPriceItem->getCustGroup();
            $tierPricesArray[$index]['website_id'] = $tierPriceItem->getWebsiteId();
            $tierPricesArray[$index]['price_qty'] = $tierPriceItem->getPriceQty();
            $tierPricesArray[$index]['price'] = $tierPriceItem->getPercent();
            $tierPricesArray[$index]['percent'] = $tierPriceItem->getPercent();

            if (isset($tierPricesArray[$index]['price_id'])){
                unset($tierPricesArray[$index]['price_id']);
            }

            $index++;
        }
        return $tierPricesArray;
    }

    public function fastUpdateNativeTierPrices($productId, $actualPrice, $productType = 'simple', $tierPrices = null)
    {
        /** @var $tpModel Magpleasure_Tierprices_Model_Tierpricesdataset */
        $tpModel = Mage::getModel('tierprices/tierpricesdataset');

        if (!$tierPrices){
            $tierPrices = $this->prepareTierPrices($productId);
        }

        $productTable = $this->getDbTable('catalog_product_entity');
        $nativePricesTable = $productTable."_tier_price";

        $nativePrices = $tpModel->getNativeFromCustom($tierPrices, null, $actualPrice, $productType);

        $write = $this->_getWriteAdapter();
        $write->beginTransaction();

        $write->delete($nativePricesTable, "entity_id = '{$productId}'");

        foreach ($nativePrices as $data){
            if (is_array($data)){

                $insertData = array();
                if ($data['cust_group'] == 32000){
                    $insertData['all_groups'] = 1;
                    $insertData['customer_group_id'] = 0;
                } else {
                    $insertData['all_groups'] = 0;
                    $insertData['customer_group_id'] = $data['cust_group'];
                }

                $insertData['value'] = $data['price'];
                $insertData['qty'] = $data['price_qty'];
                $insertData['entity_id'] = $productId;
                $insertData['website_id'] = $data['website_id'];


                $write->insert($nativePricesTable, $insertData);
            }
        }

        $write->commit();

        return $this;
    }

    public function fastUpdateMagpleasureTierPrices($tierPrices, $productId, $actualPrice, $productType = 'simple')
    {

        /** @var $tpModel Magpleasure_Tierprices_Model_Tierpricesdataset */
        $tpModel = Mage::getModel('tierprices/tierpricesdataset');
        $tierPricesTable = $this->getMainTable();

        $write = $this->_getWriteAdapter();
        $write->beginTransaction();

        $write->delete($tierPricesTable, "product_id = '{$productId}'");

        foreach ($tierPrices as $data){
            if (is_array($data)){

                $insertData = array();

                $insertData['website_id'] = $data['website_id'];
                $insertData['cust_group'] = $data['cust_group'];
                $insertData['price_qty'] = $data['price_qty'];
                $insertData['product_id'] = $productId;
                $insertData['tier_price_index'] = $data['dinamic_tier'];
                $insertData['percent'] = $data['price'];

                $write->insert($tierPricesTable, $insertData);
            }
        }

        $write->commit();
        return $this;
    }


    public function prepareFastProductData($productIds)
    {
        $array = array();

        $productTable = $this->getDbTable('catalog_product_entity');
        $read = $this->getReadConnection();
        $select = new Zend_Db_Select($read);

        $pIdsFilter = implode("','", $productIds);

        $catalogEntityTypeTable = $this->getDbTable('eav_entity_type');
        $catalogAttributeTable = $this->getDbTable('eav_attribute');
        $catalogValueTable = $productTable."_decimal";

        $select
            ->from(array('e' => $productTable), array('entity_id' => 'e.entity_id', 'type_id' => 'e.type_id', 'price' => 'eVal.value'))
            ->join(array('eType'=>$catalogEntityTypeTable), "eType.entity_type_code = 'catalog_product'", array())
            ->join(array('eAttr'=>$catalogAttributeTable), "eType.entity_type_id = eAttr.entity_type_id AND eAttr.attribute_code = 'price'", array())
            ->joinLeft(array('eVal' =>$catalogValueTable), "eVal.attribute_id = eAttr.attribute_id AND eVal.store_id = '0' AND eVal.entity_id = e.entity_id", array())
            ->where(new Zend_Db_Expr("e.entity_id IN ('{$pIdsFilter}')"))
            ;

        foreach ($read->fetchAll($select) as $data){
            $array[$data['entity_id']] = array(
                'type_id' =>  $data['type_id'],
                'price' =>  $data['price'],
            );
        }

        return $array;
    }

}