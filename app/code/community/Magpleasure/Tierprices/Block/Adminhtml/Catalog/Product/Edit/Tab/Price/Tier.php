<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Block_Adminhtml_Catalog_Product_Edit_Tab_Price_Tier
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Tier
{

    public function __construct()
    {
        $this->setTemplate('mp_tierprices/catalog/product/edit/price/tierprices.phtml');
    }

    public function getPriceTypes()
    {
        $types = array(
            Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED => Mage::helper('tierprices')->__('Fixed'),
            Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT => Mage::helper('tierprices')->__('Percent')
        );

        if ($this->hasData('price_column_header')){ #only bundle product sets this var
            unset($types[Magpleasure_Tierprices_Helper_Data::TIER_PRICE_FIXED]);
        }

        return $types;
    }

    public function getDefaultPriceTypes()
    {
        return Magpleasure_Tierprices_Helper_Data::TIER_PRICE_PERCENT;
    }

    /**
     * Show tier prices grid website column
     *
     * @return bool
     */
    public function isShowWebsiteColumn()
    {
        if (Mage::app()->isSingleStoreMode()) {
            return false;
        }
        return true;
    }
}

