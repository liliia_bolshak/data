<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */
class Magpleasure_Tierprices_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Tier 
extends Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    protected $_websites;
    protected $_customerGroups;

    public function _construct() {
        parent::_construct();
        $this->setTemplate('mp_tierprices/massaction/tier.phtml');        
    }
    
    /**
     * ######################## TAB settings #################################
     */
    //Label to be shown in the tab
    public function getTabLabel() {
        return Mage::helper('tierprices')->__('Tier Prices');
    }

    public function getTabTitle() {
        return Mage::helper('tierprices')->__('Tier Prices');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }
}
