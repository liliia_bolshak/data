<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Tierprices
 * @version    1.0.3
 * @copyright  Copyright (c) 2012 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */

class Magpleasure_Tierprices_Block_Adminhtml_Catalog_Product_Edit_Action_Attribute_Tab_Tier_TierForm extends Mage_Adminhtml_Block_Widget_Form {

    protected $_websites;

    protected function _construct() {
        parent::_construct();
    }


    public function initForm() {

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('tierprices_fieldset', array('legend' => Mage::helper('tierprices')->__('Tierprices')));

        $attributes = $this->getAttributes();

        $form->setDataObject(Mage::getModel('catalog/product'));

        $fieldset->addField('tier_prices', 'text', array(
            'name' => 'tier_price',
            'class' => 'requried-entry',
        ));
        
        $fieldset->addField('tier_massaction','hidden',array(
            'name' => 'tier_massaction',
            
        ));

        $form->getElement('tier_prices')->setRenderer(
                $this->getLayout()->createBlock('tierprices/adminhtml_catalog_product_edit_action_attribute_tab_tier_Renderer_TierPrice')
        );
                
        $this->setForm($form);

        return $this;
    }
  
}

