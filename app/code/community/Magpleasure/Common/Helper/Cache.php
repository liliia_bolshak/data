<?php
/**
 * Magpleasure Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-CE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magpleasure.com/LICENSE-CE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * Magpleasure does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magpleasure does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Magpleasure
 * @package    Magpleasure_Common
 * @version    0.6.3
 * @copyright  Copyright (c) 2012-2013 Magpleasure Ltd. (http://www.magpleasure.com)
 * @license    http://www.magpleasure.com/LICENSE-CE.txt
 */
class Magpleasure_Common_Helper_Cache extends Mage_Core_Helper_Abstract
{
    const MAGPLEASURE_CACHE_KEY = 'MAGPLEASURE_DATA';

    /**
     * Helper
     *
     * @return Magpleasure_Common_Helper_Data
     */
    protected function _commonHelper()
    {
        return Mage::helper('magpleasure');
    }

    public function resetCache()
    {
        if (Mage::app()->useCache('magpleasure')){
            Mage::app()->cleanCache(self::MAGPLEASURE_CACHE_KEY);
        }

        return $this;
    }

    /**
     * Get Cached Html
     *
     * @param string $key
     * @return string|mixed
     */
    public function getPreparedHtml($key)
    {
        if (Mage::app()->useCache('magpleasure')){
            if ($html = Mage::app()->loadCache($key)){
                return $html;
            }
        }

        return false;
    }

    /**
     * Save Cached Html
     *
     * @param string $key
     * @param string $content
     * @param int $timeout
     * @return $this
     */
    public function savePreparedHtml($key, $content, $timeout = 3600)
    {
        if (Mage::app()->useCache('magpleasure')){
            Mage::app()->saveCache($content, $key, array(self::MAGPLEASURE_CACHE_KEY), $timeout);
        }

        return $this;
    }

}