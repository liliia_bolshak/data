<?php 
class AW_Sarp_Model_Payment_Method_Contrassegno extends AW_Sarp_Model_Payment_Method_Abstract
{
    public function processOrder(Mage_Sales_Model_Order $PrimaryOrder, Mage_Sales_Model_Order $Order = null)
    {
        // Set order as pending
        $Order->addStatusToHistory('pending', '', false)->save();
        // Throw exception to suspend subscription
        throw new AW_Sarp_Exception("Suspending subscription till order status change to completed");
    }
    
    /**
     * Returns service subscription service id for specified quote
     * @param mixed $quoteId
     * @return int
     */
    public function getSubscriptionId($OrderItem)
    {
        return 1;
    }  
}
