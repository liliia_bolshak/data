<?php

class AW_Points_Model_Total_Quote_After_Discount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $usePointsOnAll = true;

    public function __construct()
    {
        $this->setCode('points');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $isApplyBefore = Mage::helper('points/config')->getPointsSpendingCalculation() == AW_Points_Helper_Config::BEFORE_TAX;
        if(!$isApplyBefore) return $this;

        $quote = $address->getQuote();

        $session = Mage::getSingleton('checkout/session');
        if (is_null($session->getQuoteId())) {
            $session = Mage::getSingleton('adminhtml/session_quote');
        }
        $is_customer_logedIn = (bool) $quote->getCustomer()->getId();

        if ($session->getData('use_points') && $address->getBaseSubtotal() && $is_customer_logedIn) {
            $pointsAmountUsed = abs($session->getData('points_amount'));

            $pointsAmountAllowed = Mage::getModel('points/summary')
                    ->loadByCustomer($quote->getCustomer())
                    ->getPoints();

            $customer = $session->getQuote()->getCustomer();
            $storeId = $session->getQuote()->getStoreId();
            $website = Mage::app()->getWebsite(Mage::app()->getStore($storeId)->getWebsiteId());

            $baseSubtotalWithDiscount = $address->getData('base_subtotal') + $address->getData('base_discount_amount');
            $subtotalWithDiscount = $address->getData('subtotal') + $address->getData('discount_amount');

            $limitedPoints = Mage::helper('points')->getLimitedPoints($baseSubtotalWithDiscount, $customer, $storeId);
            $pointsAmountUsed = min($pointsAmountUsed, $pointsAmountAllowed, $limitedPoints);
           
            $session->setData('points_amount', $pointsAmountUsed);
            $rate = Mage::getModel('points/rate')
                        ->setCurrentCustomer($customer)
                        ->setCurrentWebsite($website)
                        ->loadByDirection(AW_Points_Model_Rate::POINTS_TO_CURRENCY);
          
            $moneyBaseCurrencyForPoints = $rate->exchange($pointsAmountUsed);
            $moneyCurrentCurrencyForPoints = Mage::app()->getStore()->convertPrice($moneyBaseCurrencyForPoints);
            
            /* If points amount is more then needed to pay for subtotal with disccount for order, we need to set new points amount */
            if ($moneyBaseCurrencyForPoints > $baseSubtotalWithDiscount) {
                $neededAmount = ceil($baseSubtotalWithDiscount * $rate->getPoints() / $rate->getMoney());
                $moneyBaseCurrencyForPoints = $rate->exchange($neededAmount);
                $moneyCurrentCurrencyForPoints = Mage::app()->getStore()->convertPrice($moneyBaseCurrencyForPoints);
                $session->setData('points_amount', $neededAmount);
            }
            $address->setPointsBeforeTax($moneyCurrentCurrencyForPoints);
            $address->setBasePointsBeforeTax($moneyBaseCurrencyForPoints);
            $quote->setPointsBeforeTax($moneyCurrentCurrencyForPoints);
            $quote->setBasePointsBeforeTax($moneyBaseCurrencyForPoints);

            $items = $quote->getAllItems();
            $taxableSubtotal = 0;
            $taxableBaseSubtotal = 0;
            if (!$this->usePointsOnAll) {
                // Split point between taxable items only
                foreach($items as $item){
                    /** @var Mage_Sales_Model_Quote_Item $item */
                    $taxableSubtotal += $item->getData('taxable_amount');
                    $taxableBaseSubtotal += $item->getData('base_taxable_amount');
                }
            }
            else {
                $taxableSubtotal = $address->getSubtotal();
                $taxableBaseSubtotal = $address->getBaseSubtotal();
            }

            if($taxableSubtotal and $taxableBaseSubtotal){
                foreach($items as $item){
                    /** @var Mage_Sales_Model_Quote_Item $item */
                    $item->setData('taxable_amount',$item->getData('taxable_amount')-round($item->getData('taxable_amount')/$taxableSubtotal*$moneyCurrentCurrencyForPoints,2)) ;
                    $item->setData('base_taxable_amount', $item->getData('base_taxable_amount')-round($item->getData('base_taxable_amount')/$taxableBaseSubtotal*$moneyBaseCurrencyForPoints,2));
                }
            }
        }
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $session = Mage::getSingleton('checkout/session');
        if (is_null($session->getQuote()->getId())) {
            $session = Mage::getSingleton('adminhtml/session_quote');
        }
        $quote = $address->getQuote();
        if ($address->getPointsBeforeTax()) {
            $description = $session->getData('points_amount');
            $moneyForPoints = $address->getPointsBeforeTax();
            $textForPoints = Mage::helper('points/config')->getPointUnitName($quote->getStoreId());
            if ($description) {
                $title = Mage::helper('sales')->__('%s (%s)', $textForPoints, $description);
            } else {
                $title = Mage::helper('sales')->__('%s', $textForPoints);
            }
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => -$moneyForPoints
            ));
        }
        return $this;
    }

}
