<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 10.02.15
 * Time: 13:31
 */

$installer = $this;
$installer->startSetup();

$installer->run("

    ALTER TABLE {$this->getTable('awrma/entity')} ADD `without_order` BOOLEAN DEFAULT FALSE;

");

$installer->endSetup();