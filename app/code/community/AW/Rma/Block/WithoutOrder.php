<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 12.02.15
 * Time: 12:40
 */

class AW_Rma_Block_WithoutOrder extends Mage_Core_Block_Template
{

    /**
     * product for rendering.
     */
    protected $_product;

    /**
     * quantity of RMA product
     */
    protected $_qty;

    public function __construct()
    {
        parent::__construct();
        $_template = 'aw_rma/without_order/items/renderer/default.phtml';
        $this->setTemplate($_template);
    }

    /**
     * Set product
     *
     * @param $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    /**
     * Get product
     *
     * @return mixed
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * Set product qty
     */
    public function setProductQty($qty)
    {
        $this->_qty = $qty;
    }

    /**
     * Get product qty
     */
    public function getProductQty()
    {
        return $this->_qty;
    }

    /**
     * Get Product html
     *
     * @return mixed
     */
    public function getProductHtml()
    {
        return $this->_toHtml();
    }
}