<?php
/**
 * Created by PhpStorm.
 * User: ShutEmDown
 * Date: 15.02.2015
 * Time: 0:33
 */

class AW_Rma_Block_Adminhtml_Rma_Without_Tab_Customeraddress extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $contactInfo = $form->addFieldset('contactinformation', array(
            'legend' => $this->__('Contact Information')
        ));

        $contactInfo->addField('firstname', 'text', array(
            'name' => 'printlabel[firstname]',
            'label' => $this->__('First Name'),
            'required' => TRUE
        ));

        $contactInfo->addField('lastname', 'text', array(
            'name' => 'printlabel[lastname]',
            'label' => $this->__('Last Name'),
            'required' => TRUE
        ));

        $contactInfo->addField('company', 'text', array(
            'name' => 'printlabel[company]',
            'label' => $this->__('Company')
        ));

        $contactInfo->addField('telephone', 'text', array(
            'name' => 'printlabel[telephone]',
            'label' => $this->__('Telephone'),
            'required' => TRUE
        ));

        $contactInfo->addField('fax', 'text', array(
            'name' => 'printlabel[fax]',
            'label' => $this->__('Fax')
        ));

        $returnAddress = $form->addFieldset('return_address', array(
            'legend' => $this->__('Return Address')
        ));

        $returnAddress->addField('streetaddress', 'multiline', array(
            'name' => 'printlabel[streetaddress]',
            'label' => $this->__('Street Address'),
            'required' => TRUE
        ))->setLineCount(Mage::getStoreConfig('customer/address/street_lines', $this->getStoreId()));

        $returnAddress->addField('city', 'text', array(
            'name' => 'printlabel[city]',
            'label' => $this->__('City'),
            'required' => TRUE
        ));

        $returnAddress->addField('country_id', 'select', array(
            'name' => 'printlabel[country_id]',
            'label' => $this->__('Country'),
            'required' => TRUE
        ))->setValues(Mage::getSingleton('directory/country')->getResourceCollection()->loadByStore()->toOptionArray());

        $returnAddress->addField('region', 'text', array(
            'name' => 'printlabel[stateprovince]',
            'label' => $this->__('State/Province'),
            'required' => TRUE
        ))->setRenderer(
            $this->getLayout()->createBlock('adminhtml/customer_edit_renderer_region')
        );

        $returnAddress->addField('region_id', 'select', array(
            'name' => 'printlabel[stateprovince_id]',
            'label' => $this->__('State/Province'),
            'required' => TRUE
        ))->setNoDisplay(true);

        $returnAddress->addField('postcode', 'text', array(
            'name' => 'printlabel[postcode]',
            'label' => $this->__('Zip/Postal Code'),
            'required' => TRUE
        ));

        $additionalInfo = $form->addFieldset('addinfo', array(
            'legend' => $this->__('Additional Information')
        ));

        $additionalInfo->addField('additionalinfo', 'textarea', array(
            'name' => 'printlabel[additionalinfo]'
        ));
    }

}