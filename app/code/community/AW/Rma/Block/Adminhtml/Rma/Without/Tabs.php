<?php
/**
 * Created by PhpStorm.
 * User: ShutEmDown
 * Date: 15.02.2015
 * Time: 0:19
 */

class AW_Rma_Block_Adminhtml_Rma_Without_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('rmaitems_grid');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('RMA Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('rma_items', array(
            'label' => $this->__('RMA Items'),
            'url' => $this->getUrl('*/*/productswg', array('_current' => true)),
            'class' => 'ajax',
        ));

        $this->addTab('customer_address', array(
            'label' => $this->__('Customer Address'),
            'title' => $this->__('Customer Address'),
            'content' => $this->getLayout()->createBlock('awrma/adminhtml_rma_without_tab_customeraddress')->toHtml()
        ));

        $this->addTab('customer', array(
            'label' => $this->__('Customer'),
            'url' => $this->getUrl('*/*/customerwg', array('_current' => true)),
            'class' => 'ajax'
        ));

        return parent::_beforeToHtml();
    }

}
