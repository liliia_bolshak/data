<?php
/**
 * Created by PhpStorm.
 * User: ShutEmDown
 * Date: 14.02.2015
 * Time: 15:54
 */

class AW_Rma_Block_Adminhtml_Rma_Without extends Mage_Adminhtml_Block_Widget_Form_Container {
    public function __construct()
    {
        $this->_controller = 'adminhtml_rma';
        parent::__construct();

        $this->_blockGroup = 'awrma';
        $this->_objectId = 'id';
        $this->_removeButton('delete');
    }

    public function getHeaderText()
    {
        return $this->__('New RMA Without Order');
    }
}