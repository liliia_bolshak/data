<?php
/**
 * Created by PhpStorm.
 * User: ShutEmDown
 * Date: 15.02.2015
 * Time: 0:32
 */

class AW_Rma_Block_Adminhtml_Rma_Without_Tab_Rmaitems extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('rma_items');
        $this->setDefaultSort('entity_id', 'asc');
        $this->setSkipGenerateContent(true);
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        //die('Im here');
        $this->addColumn('products_ids', array(
            'header_css_class' => 'a-center',
            'type'             => 'checkbox',
            'html_name'        => 'products_ids',
            'disabled'         => false,
            'value'            => array(),
            'align'            => 'center',
            'index'            => 'entity_id',
            'required'         => TRUE
        ));

        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('awrma')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('awrma')->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('type', array(
            'header'    => Mage::helper('awrma')->__('Type'),
            'width'     => 100,
            'index'     => 'type_id',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name', array(
            'header'    => Mage::helper('awrma')->__('Attrib. Set Name'),
            'width'     => 130,
            'index'     => 'attribute_set_id',
            'type'      => 'options',
            'options'   => $sets,
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('awrma')->__('Status'),
            'width'     => 90,
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        $this->addColumn('visibility', array(
            'header'    => Mage::helper('awrma')->__('Visibility'),
            'width'     => 90,
            'index'     => 'visibility',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_visibility')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('awrma')->__('SKU'),
            'width'     => 80,
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'        => Mage::helper('awrma')->__('Price'),
            'type'          => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index'         => 'price'
        ));

        $this->addColumn('qty', array(
            'header'            => Mage::helper('awrma')->__('Qty'),
            'name'              => 'qty',
            'type'              => 'number',
            //'validate_class'    => 'validate-number',
            'editable'          => true,
            'width'             => 60,
            'values'            => '',
            'filter'            => false
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/productswga', array('_current' => true));
    }

    /***/
    public function getRmaItems()
    {
        $rmaItems = array();
        $products = $this->getRequest()->getParam('products');
        if (isset($products['without']) && !empty($products['without'])) {
            //decode products data
            $products = Mage::helper('adminhtml/js')->decodeGridSerializedInput($products['without']);
            foreach ($products as $id => $qty) {
                $rmaItems[] = $id;
            }
        }
        return $rmaItems;
    }
}