<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 09.02.15
 * Time: 11:12
 */

class AW_Rma_Block_Customer_Candidates extends Mage_Core_Block_Template
{

    /**
     * products for candidates.
     */
    protected $_products;

    public function __construct($products)
    {
        parent::__construct();
        $_template = 'aw_rma/customer/candidates.phtml';
        $this->setTemplate($_template);
        $this->setProducts($products);
    }

    /**
     * Set Candidates
     *
     * @param $products
     */
    protected function setProducts($products)
    {
        $this->_products = $products;
    }

    /**
     * Get products for candidates
     *
     * @return mixed
     */
    protected function getProducts()
    {
        return $this->_products;
    }

    /**
     * Get Candidates html
     *
     * @return mixed
     */
    public function getCandidatesHtml()
    {
        return $this->_toHtml();
    }
}