<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Callforprice_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('rulesGrid');
        $this->setDefaultSort('rule_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('awcallforprice/rule')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('awcallforprice');


        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'align' => 'left',
            'index' => 'title'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_ids', array(
                'header' => $helper->__('Store View'),
                'align' => 'left',
                'index' => 'store_ids',
                'type' => 'store',
                'store_view' => true,
                'display_deleted' => false,
                'renderer' => 'Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Store',
                'filter_condition_callback' => array($this, '_filterStoreCondition')
            ));
        }
        $this->addColumn('is_active', array(
            'header' => $helper->__('Is Active'),
            'align' => 'left',
            'index' => 'is_active',
            'type' => 'options',
            'options' => Mage::getModel('awcallforprice/source_status')->getOptionArray()
        ));

        $this->addColumn('active_from', array(
            'header' => $helper->__('Active From'),
            'align' => 'left',
            'index' => 'active_from',
            'type' => 'date',
            'default'   => '--',

        ));

        $this->addColumn('active_to', array(
            'header' => $helper->__('Active To'),
            'align' => 'left',
            'index' => 'active_to',
            'type' => 'date',
            'default'   => '--',
        ));

        $this->addColumn('sort_order', array(
            'header' => $helper->__('Sort Order'),
            'align' => 'left',
            'index' => 'sort_order',
            'type' => 'number'
        ));


        $this->addColumn('action', array(
            'header' => $helper->__('Action'),
            'align' => 'center',
            'width' => '48px',
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => Mage::helper('awcallforprice')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'rule_id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'is_system' => true,
            'index' => 'rule_id'
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('XML'));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('rule_id' => $row->getRuleId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('rule_id');
        $this->getMassactionBlock()->setFormFieldName('rule_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('awcallforprice')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('awcallforprice')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('is_active', array(
            'label' => Mage::helper('awcallforprice')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'is_active',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('awcallforprice')->__('Is Active'),
                    'values' => Mage::getModel('awcallforprice/source_status')->toOptionArray()
                )
            ),
            'confirm' => Mage::helper('awcallforprice')->__('Are you sure?'),
        ));
        return $this;
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!($value = $column->getFilter()->getValue())) return;
        $collection->addStoreFilter($value);
    }


}