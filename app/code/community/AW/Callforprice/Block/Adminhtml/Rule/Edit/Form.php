<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Callforprice_Block_Adminhtml_Rule_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form(array('id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('rule_id' => $this->getRequest()->getParam('rule_id'))),
            'method' => 'post',
            'enctype' => 'multipart/form-data'));

        $helper = Mage::helper('awcallforprice');
        $_fieldset = $form->addFieldset('rule_form', array('legend' => $helper->__('General')));
        $form->setHtmlIdPrefix('rule_');
        $form->setUseContainer(true);
        $_data = Mage::getModel('awcallforprice/rule');
        if (Mage::registry('current_callforprice_rule')) {
            $_data = Mage::registry('current_callforprice_rule');
        }

        $dateOutputFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $inputType = 'textarea';
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $inputType = 'editor';
        }

        $_fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $this->__('Title'),
            'title' => $this->__('Title'),
            'required' => true,
        ));

        $_fieldset->addField('tooltip', 'textarea', array(
            'name' => 'tooltip',
            'label' => $this->__('Tooltip'),
            'title' => $this->__('Tooltip'),
            'required' => false,
            'note' => $this->__('Leave empty to disable tooltip. HTML tags allowed.')
        ));

        $_fieldset->addField('catalog_text', $inputType, array(
            'name' => 'catalog_text',
            'label' => $this->__('Catalog Text'),
            'title' => $this->__('Catalog Text'),
            'required' => false,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg' => true,
        ));

        $_fieldset->addField('product_text', $inputType, array(
            'name' => 'product_text',
            'label' => $this->__('Product Text'),
            'title' => $this->__('Product Text'),
            'required' => false,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg' => true,
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $_fieldset->addField('store_ids', 'multiselect', array(
                'name' => 'store_ids',
                'label' => $this->__('Store View'),
                'title' => $this->__('Store View'),
                'required' => false,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(FALSE, TRUE),
            ));
        } else {
            $_fieldset->addField('store_ids', 'hidden', array(
                'name' => 'store_ids',
            ));
        }

        $_fieldset->addField('customer_group_ids', 'multiselect', array(
            'name' => 'customer_group_ids[]',
            'label' => $this->__('Enable For'),
            'title' => $this->__('Enable For'),
            'required' => true,
            'values' => Mage::getResourceModel('customer/group_collection')->toOptionArray(),
        ));

        $_fieldset->addField('is_active', 'select', array(
            'name' => 'is_active',
            'label' => $this->__('Is Active'),
            'title' => $this->__('Is Active'),
            'required' => false,
            'values' => Mage::getModel('awcallforprice/source_status')->getOptionArray()
        ));

        $_fieldset->addField('active_from', 'date', array(
            'name' => 'active_from',
            'label' => $this->__('Active From'),
            'title' => $this->__('Active From'),
            'required' => false,
            'format' => $dateOutputFormat,
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));

        $_fieldset->addField('active_to', 'date', array(
            'name' => 'active_to',
            'label' => $this->__('Active To'),
            'title' => $this->__('Active To'),
            'required' => false,
            'format' => $dateOutputFormat,
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));

        $_fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => $this->__('Sort Order'),
            'title' => $this->__('Sort Order'),
            'required' => false,
            'class' => "validate-zero-or-greater",
        ));

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('*/promo_catalog/newConditionHtml/form/rule_conditions_fieldset'));

        $fieldset = $form->addFieldset('conditions_fieldset', array(
                'legend' => Mage::helper('catalogrule')->__('Conditions (leave blank for all products)'))
        )->setRenderer($renderer);

        $fieldset->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('catalogrule')->__('Conditions'),
            'title' => Mage::helper('catalogrule')->__('Conditions'),
            'required' => true,
        ))->setRule($_data)->setRenderer(Mage::getBlockSingleton('rule/conditions'));
        $_fieldset->addField('hidden', 'hidden', array(
            'name' => 'hidden',
            'required' => false,
            // CFP-10 - fix
            'after_element_html' => '
<script type="text/javascript">
window.addEventListener("load",function()
    {
        $$(".rule-chooser").each(function(el)
            {
            if(el.getAttribute("url").indexOf("form/rule_conditions_fieldset")<0) {
                el.setAttribute("url",el.getAttribute("url")+"form/rule_conditions_fieldset/")
            }
        }
    )},
false);
</script>
',
        ));

        if ($_data->getData('store_ids') === null)
            $_data->setData('store_ids', 0);
        if ($_data->getData('sort_order') === null)
            $_data->setData('sort_order', 0);

        $form->setValues($_data->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

}