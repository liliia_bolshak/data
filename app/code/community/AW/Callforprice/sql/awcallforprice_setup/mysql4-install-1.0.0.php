<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


$this->startSetup();
try {
    $this->run("

CREATE TABLE IF NOT EXISTS `{$this->getTable('awcallforprice/rule')}` (
`rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`title` varchar(255) NOT NULL,
`tooltip` text NOT NULL,
`catalog_text` text NOT NULL,
`product_text` text NOT NULL,
`store_ids` varchar(255) NOT NULL,
`customer_group_ids` varchar(255) NOT NULL,
`is_active` int(10) NOT NULL,
`active_from` date NULL DEFAULT NULL,
`active_to` date NULL DEFAULT NULL,
`sort_order` int(10) NOT NULL,
`conditions_serialized` text NOT NULL,
PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `{$this->getTable('awcallforprice/indexer')}` (
`indexer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`product_id` int(10) NOT NULL,
`store_ids` varchar(255) NOT NULL,
`rule_id` int(10) NOT NULL,
`index_date` datetime NOT NULL,
PRIMARY KEY (`indexer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

");
} catch (Exception $ex) {
    Mage::logException($ex);
}

// Install attribute ----

/** @var $attribute AW_Callforprice_Model_Attribute_Callforprice_Source */
$attribute = Mage::getModel('awcallforprice/attribute_callforprice_source');

//Remove if installed
$attribute->removeAttribute();

//Install attribute
$attribute->installAttribute();

$this->endSetup();