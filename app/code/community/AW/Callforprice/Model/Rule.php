<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Callforprice_Model_Rule extends Mage_Rule_Model_Rule
{
    private $_rules = null;
    private $_attributes = null;

    public function _construct()
    {
        parent::_construct();
        $this->_init('awcallforprice/rule', 'rule_id');
    }

    /**
     * Getter for rule conditions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('catalogrule/rule_condition_combine');
    }

    /**
     * Getter for rule actions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Action_Collection
     */
    public function getActionsInstance()
    {
        return Mage::getModel('catalogrule/rule_action_collection');
    }

    public function _beforeSave()
    {
        parent::_beforeSave();
        if (is_array($this->getData('store_ids')))
            $this->setData('store_ids', implode(',', $this->getData('store_ids')));
        if (is_array($this->getData('customer_group_ids'))) {
            $this->setData('customer_group_ids', implode(',', $this->getData('customer_group_ids')));
        }
    }

    public function _afterLoad()
    {
        parent::_afterLoad();
        if (!is_array($this->getData('store_ids')))
            $this->setData('store_ids', explode(',', $this->getData('store_ids')));
        if (!is_array($this->getData('customer_group_ids')))
            $this->setData('customer_group_ids', explode(',', $this->getData('customer_group_ids')));
    }

    public function getRules($storeId = null)
    {
        /**
         * IMPORTANT
         * Use only singleton on this step
         */
        if (is_null($this->_rules)) {
            $rule = $this;
            $now = Mage::getModel('core/date')->date('Y-m-d');
            $this->_rules = $rule->getCollection()
                ->addFieldToFilter('main_table.is_active', array('eq' => AW_Callforprice_Model_Source_Status::STATUS_ACTIVE))
                ->addDateFilter($now)
                ->addCustomerGroupFilter()
                ->addStoreFilter($storeId);
            $this->_rules->getSelect()->order(array('sort_order ASC'));
        }
        return $this->_rules;
    }

    public function getAttributesFromRules()
    {
        if (is_null($this->_attributes)) {
            $rules = $this->getRules();
            if (!$rules->getSize()) return false;
            $this->_attributes = array();
            foreach ($rules as $rule) {
                $conditions = $rule->getConditions()->asArray();
                if (array_key_exists('conditions', $conditions) && count($conditions['conditions']) > 0)
                    foreach ($conditions['conditions'] as $condition) {
                        if (!in_array($condition['attribute'], $this->_attributes))
                            array_push($this->_attributes, $condition['attribute']);
                    }
            }
        }
        return $this->_attributes;
    }

    public function validateProductByRules($product, $storeId = null)
    {
        if(is_null($storeId))
            $storeId = Mage::app()->getStore()->getId();
        if (!is_object($product))
            $product = Mage::getModel('catalog/product')->setStore($storeId)->load((int)$product);

        if (!$product->getId()) return false;

        $rules = $this->getRules($storeId);

        if ($rules->getSize()) {

            foreach ($rules as $rule) {
                $result = $rule->getConditions()->validate($product);
                if ($result === true) {
                    return $rule;
                }
            }
        }
        return false;
    }
}