<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Callforprice_Model_Source_Status extends Mage_Core_Model_Abstract
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function toOptionArray()
    {
        $helper = Mage::helper('awcallforprice');
        return array(
            array('label' => $helper->__("Yes"), 'value' => self::STATUS_ACTIVE),
            array('label' => $helper->__("No"), 'value' => self::STATUS_INACTIVE),
        );
    }

    public function getOptionArray()
    {
        $helper = Mage::helper('awcallforprice');
        return array(
            self::STATUS_ACTIVE => $helper->__("Yes"),
            self::STATUS_INACTIVE => $helper->__("No"),
        );
    }
}