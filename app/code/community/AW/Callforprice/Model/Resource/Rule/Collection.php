<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Callforprice_Model_Resource_Rule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('awcallforprice/rule');
    }

    protected function _afterLoad()
    {
        foreach ($this as $item) {
            $item->setData('store_ids', explode(',', $item->getStoreIds()));
        }
        $this->walk('afterLoad');
        parent::_afterLoad();
        return $this;
    }

    // By default use active status
    public function addStatusFilter($status = null)
    {
        if (is_null($status)) {
            $status = AW_Callforprice_Model_Source_Status::STATUS_ACTIVE;
        }
        $this->addFieldToFilter('is_active', array('eq' => $status));
        return $this;
    }


    public function addDateFilter($now = null)
    {
        if(is_null($now))
            $now = Mage::getModel('core/date')->date('Y-m-d');
        $this->getSelect()->where('active_from is null or active_from <= ?', $now)
        ->where('active_to is null or active_to >= ?', $now);
        return $this;
    }

    public function addStoreFilter($id = null)
    {
        if (is_null($id)) {
            $id = Mage::app()->getStore()->getId();
        }
        $this->getSelect()->where('find_in_set(?, store_ids) or find_in_set(0, store_ids)', $id);
        return $this;
    }

    public function addCustomerGroupFilter($id = null)
    {
        if (is_null($id)) {
            $id = 0;
            if(Mage::getSingleton('customer/session')->isLoggedIn())
            $id = (int)Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
        }
        $this->getSelect()->where('find_in_set(?, customer_group_ids)', $id);
        return $this;
    }
}