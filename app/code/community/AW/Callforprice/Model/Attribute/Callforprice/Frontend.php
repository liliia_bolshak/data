<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

/**
 * Callforprice Model
 */
class AW_Callforprice_Model_Attribute_Callforprice_Frontend extends Mage_Eav_Model_Entity_Attribute_Frontend_Abstract
{


    public function getSelectOptions()
    {
        $options = array(
            array('value' => 0, 'label' => Mage::helper('awcallforprice')->__('No')),
            array('value' => 1, 'label' => Mage::helper('awcallforprice')->__('Yes')));
        return $options;
    }
}