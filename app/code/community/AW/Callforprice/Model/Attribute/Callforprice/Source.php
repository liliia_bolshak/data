<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

/**
 * Callforprice Model
 */
class AW_Callforprice_Model_Attribute_Callforprice_Source extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    const ATTRIBUTE_CODE = 'aw_callforprice';

    private $_installer = null;

    /*
     * Retrive all attribute options
     * @return array
     */
    public function getAllOptions()
    {
        $options = array(
            array('value' => 0, 'label' => Mage::helper('awcallforprice')->__('No')),
            array('value' => 1, 'label' => Mage::helper('awcallforprice')->__('Yes')));

        return $options;
    }


    private function _initInstaller()
    {
        if (!$this->_installer) {
            $this->_installer = new Mage_Eav_Model_Entity_Setup('core_setup');
        }
    }

    public function removeAttribute()
    {
        $this->_initInstaller();
        $this->_installer->removeAttribute('catalog_product', self::ATTRIBUTE_CODE);
    }

    public function installAttribute()
    {
        $this->_initInstaller();
        $this->_installer->addAttribute('catalog_product', self::ATTRIBUTE_CODE, array(
            'type' => 'int',
            'backend' => 'awcallforprice/attribute_callforprice_backend',
            'source' => 'awcallforprice/attribute_callforprice_source',
            'frontend' => 'awcallforprice/attribute_callforprice_frontend',
            'label' => 'Disable call for price rules for this product',
            'searchable' => false,
            'filterable' => false,
            'group' => 'Call For Price',
            'input' => 'select',
            'global' => false,
            'visible' => false,
            'required' => false,
            'user_defined' => false,
            'default' => '0',
            'visible_on_front' => false,
            'is_used_for_promo_rules' => true,
            'is_visible_in_advanced_search' => false,
            'is_filterable_in_search' => false,
        ));
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'apply_to', 'simple,configurable,virtual,downloadable');
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'is_filterable', false);
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'is_searchable', false);
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'is_used_for_promo_rules', true);
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'used_in_product_listing', true);
        $this->_installer->updateAttribute('catalog_product', self::ATTRIBUTE_CODE, 'is_visible_in_advanced_search', false);
    }

}