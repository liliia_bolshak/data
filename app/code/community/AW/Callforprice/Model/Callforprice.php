<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Callforprice_Model_Callforprice extends Mage_Core_Model_Abstract
{

    /**
     * Key to aggregate all applied rules mapped for product ids
     * used to remove twise rules validation
     */
    const MAPPED_PRODUCTS = 'mapped_products';

    const ACTIVE_RULES = 'active_rules';
    /**
    @deprecated
    public function dRewrite($observer)
    {
    $node = Mage::getConfig()->getNode('global/blocks/catalog/rewrite');
    $dnodes = Mage::getConfig()->getNode('global/blocks/catalog/drewrite');
    foreach ($dnodes->children() as $dnode) {
    $node->appendChild($dnode);
    }
    }
     */


    public function getMappedProducts()
    {

        if (!is_array($this->getData(self::MAPPED_PRODUCTS))) {
            return array();
        }
        return $this->getData(self::MAPPED_PRODUCTS);
    }


    public function getActiveRules()
    {

        if (!is_array($this->getData(self::ACTIVE_RULES))) {
            return array();
        }
        return $this->getData(self::ACTIVE_RULES);
    }

    /**
     * @param $product
     * @param $rule
     * Mapped products array aggregates on first rules validation when deside to display price
     * So when we should deside to display "add to" block, we just can read this mapped array without additional loads
     */
    public function aggregateMappedProducts($product, $rule)
    {
        $map = $this->getMappedProducts();
        if (!@is_array($map[$rule->getId()])) {
            $map[$rule->getId()] = array();
        }
        if (!in_array($product->getId(), $map[$rule->getId()])) {
            array_push($map[$rule->getId()], $product->getId());
            $this->setData(self::MAPPED_PRODUCTS,$map);
            $this->aggregateActiveRules($rule);
        }

    }


    /**
     * @param $rule
     */
    public function aggregateActiveRules($rule)
    {
        $rules = $this->getActiveRules();
        if (!array_key_exists($rule->getId(),$rules)) {
            $rules[$rule->getId()] = $rule;
            $this->setData(self::ACTIVE_RULES,$rules);
        }
    }

    public function getRules()
    {
        return Mage::getSingleton('awcallforprice/rule')->getRules();
    }

    public function getRuleByProduct($product)
    {
        $this->getRules();

        $map = $this->getMappedProducts();

        if (count($map) === 0) return false;
        foreach ($map as $ruleId => $products) {
            $key = array_search($product->getId(), $products);

            if ($key !== false) {
                $rule = false;
                $activeRules = $this->getActiveRules();
                if(array_key_exists($ruleId,$activeRules)) {
                    $rule = $activeRules[$ruleId];
                }
                return $rule;
            }
        }
        return false;
    }

    public function processPriceBlock($block, $product)
    {

        $class = get_class($block);

        if ($class == 'Mage_Wishlist_Block_Render_Item_Price' ||
            $class == 'Mage_Catalog_Block_Product_Price' ||
            $class == 'Mage_Bundle_Block_Catalog_Product_Price' ||
            $class == 'AW_Sarp_Block_Catalog_Product_Price' ||
            $class == 'AW_Sarp_Block_Product_Price'
        ) {
            $type = $product->getTypeId();

           if($type == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE || $type == Mage_Catalog_Model_Product_Type::TYPE_GROUPED)
            return;
            $rule = $this->getRuleByProduct($product);

            if ((int)$product->getData(AW_Callforprice_Model_Attribute_Callforprice_Source::ATTRIBUTE_CODE) == 1) {
                return false;
            }
            if(!$rule) {
                $rule = Mage::getSingleton('awcallforprice/rule')->validateProductByRules($product, Mage::app()->getStore()->getId());
            }
            if ($rule !== false) {
                $this->aggregateMappedProducts($product, $rule);
                $html = Mage::app()->getLayout()->createBlock('awcallforprice/price')
                    ->setProduct($product)
                    ->setRule($rule)
                    ->toHtml();
                $block->setHtml($html);
                return true;
            }
        }
        return false;
    }

    public function processAddtoBlock($block, $product)
    {
        $name = $block->getNameInLayout();

        if (($name == 'product.info.addtocart' ||
                $name == 'customer.wishlist.item.cart'
            ) && !$block->getCallforpriceRule()
        ) {
            $rule = $this->getRuleByProduct($product);
            if ($rule) {
                $block->setTemplate('aw/callforprice/addtocart.phtml');
                $block->setCallforpriceRule($rule);
                $block->toHtml();
                return true;
                }
        }
       // else{$block->setTemplate('wishlist/item/column/cart.phtml');}
        return false;
    }

    public function processWishlist($block)
    {
        $name = $block->getNameInLayout();
        if (version_compare(Mage::getVersion(), '1.5', '<')) {
            if ($name == 'customer.wishlist' && !$block->getCallforpriceFlag()) {
                $block->setTemplate('aw/callforprice/wishlist/view.phtml');
                $block->setCallforpriceFlag(true);
                $block->toHtml();
                return true;
            }

            if ($name == 'wishlist_sidebar' && !$block->getCallforpriceFlag()) {
                $block->setTemplate('aw/callforprice/wishlist/sidebar14.phtml');
                $block->setCallforpriceFlag(true);
                $block->toHtml();
                return true;
            }

            return false;
        } elseif(version_compare(Mage::getVersion(), '1.7', '<')) {
            if ($name == 'customer.wishlist' && !$block->getCallforpriceFlag()) {
                $block->setTemplate('aw/callforprice/wishlist/view16.phtml');
                $block->setCallforpriceFlag(true);
                $block->toHtml();
                return true;
            }
        }
        if(AW_All_Helper_Versions::getPlatform() == AW_All_Helper_Versions::CE_PLATFORM) {
            if ($name == 'wishlist_sidebar' && !$block->getCallforpriceFlag()) {
                $block->setTemplate('aw/callforprice/wishlist/sidebar.phtml');
                $block->setCallforpriceFlag(true);
                $block->toHtml();
                return true;
            }
        }
    }

    public function blockAbstractToHtmlAfter($event)
    {
        if(!AW_Callforprice_Helper_Data::isEnabled())
            return;
        $block = $event->getData('block');
        $name = $block->getNameInLayout();
        $product = $block->getProduct();

        $this->processWishlist($block);

        if ($name == 'customer.wishlist.item.cart') {
            $product = $block->getItem()->getProduct();
        }

        if ($product) {
            /** @var Mage_Catalog_Model_Product $product */
            if (!$this->processPriceBlock($block, $product)) {
                $this->processAddtoBlock($block, $product);
            }
        }
    }

    /**
     * @param $ids
     * @param $attributes
     * @return Mage_Catalog_Model_Resource_Product_Collection
     * This method used in case catalog product flat enabled, to get product collection with needed attributes
     */
    protected function _getProducts($ids,$attributes)
    {
        $adminstore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $currentStore = Mage::app()->getStore()->getId();
        Mage::app()->getStore()->setId($adminstore);
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect($attributes);
        $collection->addAttributeToSelect(AW_Callforprice_Model_Attribute_Callforprice_Source::ATTRIBUTE_CODE);
        $collection->addFieldToFilter('entity_id',array('in'=>$ids))->load();
        foreach($collection as $product) {
            $type = $product->getTypeId();
            if($type == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE || $type == Mage_Catalog_Model_Product_Type::TYPE_GROUPED) {
                continue;
            }
            if ((int)$product->getData(AW_Callforprice_Model_Attribute_Callforprice_Source::ATTRIBUTE_CODE) == 1) {
                continue;
            }
            $rule = Mage::getSingleton('awcallforprice/rule')->validateProductByRules($product, $currentStore);
            if ($rule !== false) {
                $this->aggregateMappedProducts($product, $rule);
            }
        }
        Mage::app()->getStore()->setId($currentStore);
        return $collection;
    }
    /**
     *
     * This event is for dealing with products in the wishlist
     * Event - abstract_collection_load_before
     * @param Varien_Event_Observer $event
     *
     */
    public function productCollectionBeforeLoad($event)
    {
        if(!AW_Callforprice_Helper_Data::isEnabled())
            return;
        $collection = $event->getData('collection');

        $attributes = Mage::getSingleton('awcallforprice/rule')->getAttributesFromRules();
        if (count($attributes > 0)) {
            if (!$collection->isEnabledFlat()) {
                $collection->addAttributeToSelect($attributes);
            } else {
                //backendCollection
                $ids = $collection->getAllIds();
                $collection = $this->_getProducts($ids,$attributes);
            }
        }
        //add call for price attribute
        $collection->addAttributeToSelect(AW_Callforprice_Model_Attribute_Callforprice_Source::ATTRIBUTE_CODE);
        return $collection;
    }

    public function cartSaveBefore($event)
    {
        if(!AW_Callforprice_Helper_Data::isEnabled())
            return;
        $cart = $event->getData('cart');

        $items = $cart->getItems();
        $recollectFlag = false;

        if (count($cart->getItems())<1) {
            return;
        }
        $this->getRules();
        foreach ($items as $item) {
            $product = $item->getProduct();
            if ((int)$product->getData(AW_Callforprice_Model_Attribute_Callforprice_Source::ATTRIBUTE_CODE) == 1) {
                return false;
            }

            $rule = Mage::getSingleton('awcallforprice/rule')->validateProductByRules($product, Mage::app()->getStore()->getId());
            if ($rule !== false) {
                $this->aggregateMappedProducts($product, $rule);

                $cart->removeItem($item->getId());
                $recollectFlag = true;
                Mage::getSingleton('checkout/session')->getMessages(true);
                Mage::getSingleton('checkout/session')->addNotice(Mage::helper('awcallforprice')->__('You cannot add %s to cart. Please view %s product details %s.',$product->getName(),"<a href='{$product->getProductUrl()}'/>",'</a>'));
            }
        }
        if($recollectFlag)
        $cart->getQuote()->unsTotalsCollectedFlag()->collectTotals()->save();
        return;
    }

}