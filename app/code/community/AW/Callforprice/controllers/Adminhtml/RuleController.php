<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Callforprice
 * @version    1.0.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Callforprice_Adminhtml_RuleController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction($title = 'Manage Rules')
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('promo/awcallforprice')
            ->_addBreadcrumb(
                Mage::helper('awcallforprice')->__('Manage Rules'),
                Mage::helper('awcallforprice')->__('Manage Rules'));
        $this->_title($this->__('Call For Price'))->_title($this->__($title));
        return $this;
    }

    protected function _filterDates($array, $dateFields)
    {
        if (empty($dateFields)) {
            return $array;
        }
        $filterInput = new Zend_Filter_LocalizedToNormalized(array(
            'date_format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
        ));
        $filterInternal = new Zend_Filter_NormalizedToLocalized(array(
            'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
        ));

        foreach ($dateFields as $dateField) {
            if (array_key_exists($dateField, $array) && !empty($dateField)) {
                $array[$dateField] = $filterInput->filter($array[$dateField]);
                $array[$dateField] = $filterInternal->filter($array[$dateField]);
            }
        }
        return $array;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('promo/awcallforprice');
    }

    public function indexAction()
    {
        $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('awcallforprice/adminhtml_rule'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $title = 'New Rule';
        $model = Mage::getModel('awcallforprice/rule');

        if ($id = $this->getRequest()->getParam('rule_id')) {
            $model = Mage::getModel('awcallforprice/rule')->load($id);
            $title = 'Edit Rule';
        }
        Mage::register('current_callforprice_rule', $model);
        $this->_initAction($this->__($title))
            ->_addContent($this->getLayout()->createBlock('awcallforprice/adminhtml_rule_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                /** @var $model Mage_SalesRule_Model_Rule */
                $model = Mage::getModel('awcallforprice/rule');
                $data = $this->getRequest()->getPost();
               // $info = $this->_filterDates($data, array('active_from', 'active_to'));
                if(!array_key_exists('store_ids',$data)) {
                    $data['store_ids'] = array(0);
                }
                $data = $this->_filterDates($data, array('active_from', 'active_to'));
                $id = $this->getRequest()->getParam('rule_id');
                if ($id) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        Mage::throwException(Mage::helper('catalogrule')->__('Wrong rule specified.'));
                    }
                }

                $session = Mage::getSingleton('adminhtml/session');

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach ($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                    $session->setPageData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                if (isset($data['rule']['conditions'])) {
                    $data['conditions'] = $data['rule']['conditions'];
                }

                unset($data['rule']);

                if(!$data['active_from']) {
                    $data['active_from'] = null;
                }
                if(!$data['active_to']) {
                    $data['active_to'] = null;
                }
                $model->loadPost($data);

                $model->save();
                $session->addSuccess(Mage::helper('salesrule')->__('The rule has been saved.'));


                if ($this->getRequest()->getParam('back')) {

                    $this->_redirect('*/*/' . $this->getRequest()->getParam('back'), array('rule_id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $id = (int)$this->getRequest()->getParam('rule_id');
                if (!empty($id)) {
                    $this->_redirect('*/*/edit', array('id' => $id));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;

            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('rule_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('rule_id')) {
            $ruleModel = Mage::getModel('awcallforprice/rule');
            try {
                $ruleModel->load($id)->delete();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('awcallforprice')->__('Rule was successfully deleted'));
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'callforprice_rules.csv';
        $content = $this->getLayout()->createBlock('awcallforprice/adminhtml_rule_grid')
            ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName = 'callforprice_rules.xml';
        $content = $this->getLayout()->createBlock('awcallforprice/adminhtml_rule_grid')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        try {
            $ranksToDelete = $this->getRequest()->getParam('rule_id');
            foreach ($ranksToDelete as $rankToDelete) {
                Mage::getModel('awcallforprice/rule')->load($rankToDelete)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Successfully deleted'));
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('awcallforprice')->__('Unable to find rule to delete'));
        }
        $this->_redirect('*/*/');
    }

    public function massStatusAction()
    {
        try {
            $rules = $this->getRequest()->getParam('rule_id');
            foreach ($rules as $rule) {
                Mage::getModel('awcallforprice/rule')->load($rule)->setIsActive($this->getRequest()->getParam('is_active'))->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Successfully updated'));
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('awcallforprice')->__('Unable to find rule to save'));
        }
        $this->_redirect('*/*/');
    }

}