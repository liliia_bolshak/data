<?php

class AW_Advancedsearch_Block_Attachments_Result extends Mage_Core_Block_Template
{
    protected $_results = null;

    protected function _prepareLayout()
    {
        $helper = Mage::helper('awadvancedsearch');
        $queryText = Mage::app()->getRequest()->getParam('product_value');

        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $title = $this->__("Search results for: '%s'", $queryText);
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('awadvancedsearch')->__('Home'),
                'title'=>Mage::helper('awadvancedsearch')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array('label' => $title,
                        'title' => $title
                ));
        }
        $title = $this->__("Search results for: '%s'", $queryText);
        $this->getLayout()->getBlock('head')->setTitle($title);

        return parent::_prepareLayout();
    }

    public function getNoResultText()
    {
        return $this->__('There is no items matching the query');
    }

    public function getResults()
    {
        if ($this->_results === null) {
            $this->_results = Mage::helper('awadvancedsearch/attachments')->getResults();
        }
        return $this->_results;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}