<?php

class AW_Advancedsearch_Block_Attachments_Form extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb('home', array(
                'label'=>Mage::helper('awadvancedsearch')->__('Home'),
                'title'=>Mage::helper('awadvancedsearch')->__('Go to Home Page'),
                'link'=>Mage::getBaseUrl()
            ))->addCrumb('search', array(
                    'label'=>Mage::helper('awadvancedsearch')->__('Attachments Advanced Search')
                ));
        }
        return parent::_prepareLayout();
    }

    public function getModel()
    {
        return Mage::getSingleton('awadvancedsearch/attachments');
    }

    public function getAttachmentTypes()
    {
        $attachmentTypes = array(
            array('title' => $this->__('Document'), 'type' =>'document'), // doc, pdf, xls, rtf
            array('title' => $this->__('Drivers'), 'type' =>'drivers'),   // zip, exe
            array('title' => $this->__('Media'), 'type' =>'media')        // jpg, png, gif, mp3
        );
        return $attachmentTypes;
    }

    public function getSearchPostUrl()
    {
        return $this->getUrl('*/*/result');
    }
    public function getSearchMiniFormPostUrl()
    {
        return $this->getUrl('advancedsearch/attachments/result');
    }
}