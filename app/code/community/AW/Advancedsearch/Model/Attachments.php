<?php

class AW_Advancedsearch_Model_Attachments extends Mage_Core_Model_Abstract
{
    protected $_attachmentType = null;
    protected $_serialNumber = null;

    public function getAttachmentsByProductValue($productValue)
    {
        $resultCollection = $this->getResultCollection($productValue);
        $productIds = $this->getAllProductIdsFromCollection($resultCollection);

        $attachments = array();
        if(isset($productIds) && !empty($productIds))
        {
            $productIds = explode(',',$productIds);
            $_helper = Mage::helper('fileuploader');
            $this->setAttachmentType();
            foreach($productIds as $productId)
            {
                $data = $this->getAttachmentCollection($productId);
                $totalFiles = $data['totalRecords'];
                if ($totalFiles > 0) {
                    $record = $data['items'];
                    $i=0;
                    foreach ($record as $rec) {
                        $i++;
                        $file = $_helper->getFilesHtml($rec['uploaded_file'], $rec['title'],$i,true,$rec['content_disp'],true);
                        $attachments[] = array('title' => $rec['title'], 'file' => $file, 'content' => $rec['file_content']);
                    }
                }
            }
        }
        return $attachments;
    }

    public function getAttachmentsBySerialNumber($serialNumber)
    {
        $attachments = array();

        $serialNumbersCollection = $this->getArticleBySerialNumber($serialNumber);
        if(is_null($serialNumbersCollection)) return $attachments;
        $productId = $serialNumbersCollection['productId'];

        if(isset($productId) && !empty($productId))
        {
            $_helper = Mage::helper('fileuploader');
            //uncomment if you want to use attachment types to sort by
            //$this->setAttachmentType();

            $data = $this->getAttachmentCollection($productId);
            $totalFiles = $data['totalRecords'];
            if ($totalFiles > 0) {
                $record = $data['items'];
                $i=0;
                foreach ($record as $rec) {
                    $i++;
                    $file = $_helper->getFilesHtml($rec['uploaded_file'], $rec['title'],$i,true,$rec['content_disp'],true);
                    $attachments[] = array('title' => $rec['title'], 'file' => $file, 'content' => $rec['file_content']);
                }
            }
        }
        return $attachments;
    }

    public function getResultCollection($queryText)
    {
        $resultsHelper = Mage::helper('awadvancedsearch/results');
        $results = $resultsHelper->query($queryText);
        if ($results) {
            $helper = Mage::helper('awadvancedsearch/catalogsearch');
            $helper->addCatalogsearchQueryResults($queryText, $results);
            $helper->setResults($results);
            return $helper->getResults();
        } else {
            $collection = $this->getData('result_collection');
            if (is_null($collection)) {
                $collection = Mage::getResourceModel('catalogsearch/search_collection');
                $collection->addSearchFilter($queryText)->addStoreFilter();
                $this->setData('result_collection', $collection);
            }
            return $collection;
        }
    }

    public function getAllProductIdsFromCollection($resultCollection)
    {
        $ids = array();
        foreach ($resultCollection as $item) {
	    if ($item->getData('type') == 1) {
		$sphinxResults = $item->getData('sphinx_results');
		$ids = array_keys($sphinxResults['matches']);
            }
            else if($item->getEntityId())
	    {
	       $productId = $item->getEntityId();
	       if(!empty($productId))
	       {
		   $ids[] = $productId;
	       }
            }
        }
        return (!empty($ids))? implode(',',$ids) : 0;
    }

    public function getAttachmentCollection($productId)
    {
        $data = array();
        $collection = Mage::getResourceModel('fileuploader/fileuploader_collection');
        $collection->addFieldToFilter('product_ids', array('finset' => $productId))
                   ->addFieldToFilter('file_status', 1);
        $attachmentType = $this->getAttachmentType();
        if($attachmentType)
        {
            switch($attachmentType)
            {
                case 'document':
                    $collection->addFieldToFilter(
                        'uploaded_file',
                        array(
                            array('like'=>'%.doc'),
                            array('like'=>'%.docx'),
                            array('like'=>'%.xls'),
                            array('like'=>'%.xlsx'),
                            array('like'=>'%.rtf'),
                            array('like'=>'%.djvu'),
                            array('like'=>'%.djv'),
                            array('like'=>'%.pdf')
                        )
                    );
                    break;
                case 'drivers':
                    $collection->addFieldToFilter(
                        'uploaded_file',
                        array(
                            array('like'=>'%.zip'),
                            array('like'=>'%.rar'),
                            array('like'=>'%.tgz'),
                            array('like'=>'%.tar'),
                            array('like'=>'%.tar.gz'),
                            array('like'=>'%.tar.Z'),
                            array('like'=>'%.tar.xz'),
                            array('like'=>'%.tar.lzma'),
                            array('like'=>'%.exe')
                        )
                    );
                    break;
                case 'media':
                    $collection->addFieldToFilter(
                        'uploaded_file',
                        array(
                            array('like'=>'%.png'),
                            array('like'=>'%.jpg'),
                            array('like'=>'%.jpeg'),
                            array('like'=>'%.gif'),
                            array('like'=>'%.mp3')
                        )
                    );
                    break;
            }
        }
        $collection->getSelect()->order('sort_order');
        return $collection->toArray();
    }

    public function setAttachmentType()
    {
        $params = Mage::app()->getRequest()->getParams();
        $attachmentType = null;
        if(isset($params['attachment_type']) && !empty($params['attachment_type']))
        {
            $attachmentType = strip_tags($params['attachment_type']);
            $attachmentType = htmlspecialchars($attachmentType);
	    $attachmentType = (mysql_real_escape_string($attachmentType)) ? mysql_real_escape_string($attachmentType) : mysql_escape_string($attachmentType);
        }
        $this->_attachmentType = $attachmentType;
    }

    public function getAttachmentType()
    {
        return $this->_attachmentType;
    }

    public function getArticleBySerialNumber($serial)
    {
        $arieteResult = Mage::getModel('ariete/documenti')->getDDTdaSerialNumber($serial);
        if(!count($arieteResult)) return null;
        $ddtAriete = Mage::getModel('ariete/documenti')->getLeggiDDT($arieteResult[0]['_iddocumento'], false);
        /* @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product');
        $product = $product->loadByAttribute('id_ariete', $arieteResult[0]['_idarticolo']);
        return array(
            'productId' => $product->getId()
        );
    }

}
