<?php

class AW_Advancedsearch_AttachmentsController extends Mage_Core_Controller_Front_Action
{
    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function resultAction()
    {
        $queryParameters = $this->getQueryParameters();

        if(!empty($queryParameters['product_value']) || !empty($queryParameters['serial_number']))
        {
            $this->setSearchResults($queryParameters);
        }
        else
        {
            Mage::getSingleton('catalogsearch/session')->addError('Empty Query Parameters');
            $this->_redirectError(
                Mage::getModel('core/url')
                    ->setQueryParams($this->getRequest()->getQuery())
                    ->getUrl('*/*/result/')
            );
        }
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function setSearchResults($queryParameters)
    {
        $result = null;
        if(!empty($queryParameters['product_value']))
        {
            $result = Mage::getModel('awadvancedsearch/attachments')->getAttachmentsByProductValue($queryParameters['product_value']);
            $result = array_unique($result, SORT_REGULAR);
        }
        else
        {
            $result = Mage::getModel('awadvancedsearch/attachments')->getAttachmentsBySerialNumber($queryParameters['serial_number']);
        }
        Mage::helper('awadvancedsearch/attachments')->setResults($result);
    }

    public function getQueryParameters()
    {
        $params = Mage::app()->getRequest()->getParams();

        if(isset($params['serial_number']) && !empty($params['serial_number']))
        {
	        $serialNumber = strip_tags($params['serial_number']);
            $serialNumber = htmlspecialchars($serialNumber);
            $serialNumber = (mysql_real_escape_string($serialNumber)) ? mysql_real_escape_string($serialNumber) : mysql_escape_string($serialNumber);
            $queryParameters = array('serial_number' => $serialNumber);
        }
        else
        {
            $productValue = strip_tags($params['pvalue']);
            $productValue = htmlspecialchars($productValue);
            $productValue = (mysql_real_escape_string($productValue)) ? mysql_real_escape_string($productValue) : mysql_escape_string($productValue);
            $queryParameters = array('product_value' => $productValue);
        }

        return $queryParameters;
    }
}
