<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    My
 * @package     My_Igallery
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base Banner Block
 *
 * @category   My
 * @package    My_Igallery
 * @author     Theodore Doan <theodore.doan@gmail.com>
 */
class My_Igallery_Block_Lastgallery extends Mage_Core_Block_Template {
    const DEFAULT_IMAGE_LIMIT = 12;

    protected $_isDisabled = 0;
    protected $_collection;
    protected $_lastGallery;

    /**
     * @return My_Igallery_Model_Banner
     */
    protected function _getGallery() {
        if ($this->_lastGallery) {
            return $this->_lastGallery;
        }
        
        $this->_lastGallery = Mage::getModel('igallery/banner')
        	->getCollection()->addEnableFilter(1)
        	->getLastItem();
        	
        return $this->_lastGallery;
    }

    /**
     * Load collection of images contained in the last gallery inserted
     * 
     * @return My_Igallery_Model_Mysql4_Banner_Image_Collection
     */
    protected function _getCollection() {
        if ($this->_collection) {
            return $this->_collection;
        }
        
        $lastGalleryId = $this->_getGallery()->getBannerId();
        
        $this->_collection = Mage::getModel('igallery/banner_image')
			->getCollection()->addEnableFilter($this->_isDisabled)
			->addFieldToFilter('banner_id', $lastGalleryId)
			->setOrder('position', Varien_Data_Collection::SORT_ORDER_ASC);
			
        return $this->_collection;
    }

    /**
     * Wrapper for standart strip_tags() function with extra functionality for html entities
     *
     * @param string $data
     * @param string $allowableTags
     * @param bool $escape
     * @return string
     */
    public function stripTags($data, $allowableTags = null, $escape = false)
    {
        $result = strip_tags($data, $allowableTags);
        return $escape ? $this->escapeHtml($result, $allowableTags) : $result;
    }

    /**
     * Escape html entities
     *
     * @param   mixed $data
     * @param   array $allowedTags
     * @return  mixed
     */
    public function escapeHtml($data, $allowedTags = null)
    {
        if (is_array($data)) {
            $result = array();
            foreach ($data as $item) {
                $result[] = $this->escapeHtml($item);
            }
        } else {
            // process single item
            if (strlen($data)) {
                if (is_array($allowedTags) and !empty($allowedTags)) {
                    $allowed = implode('|', $allowedTags);
                    $result = preg_replace('/<([\/\s\r\n]*)(' . $allowed . ')([\/\s\r\n]*)>/si', '##$1$2$3##', $data);
                    $result = htmlspecialchars($result);
                    $result = preg_replace('/##([\/\s\r\n]*)(' . $allowed . ')([\/\s\r\n]*)##/si', '<$1$2$3>', $result);
                } else {
                    $result = htmlspecialchars($data);
                }
            } else {
                $result = $data;
            }
        }
        return $result;
    }
}