<?php 

class IG_FlatShipping5_Model_Source_Getflatrates
{
    public function toOptionArray()
    {
        $returnArray = array();
        $carriers = Mage::getModel('shipping/config')->getAllCarriers();
        unset($carriers["flatrate"]);
        $i = 0;
        foreach ($carriers as $k=>$c){
            if(substr($k,0,8) == "flatrate"){
                $returnArray[$i]["value"] = $k;
                $returnArray[$i]["label"] = $k;
                $i++;
            }
        }
        return $returnArray;
    }
}

