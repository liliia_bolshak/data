<?php

class Web4pro_Feed_Helper_Data extends GoMage_Feed_Helper_Data
{
    const CATEGORY_PREFIX       = 'Category ';
    const CATEGORY_VALUE_PREFIX = 'category_';

    public function setCategoryAdditionalAttribute()
    {
        $categoryMetaDescriptionCollection = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->setEntityTypeFilter(Mage::getResourceModel('catalog/category')->getTypeId())
            ->addFieldToFilter('attribute_code', array('eq' => 'meta_description'));

        $categoryMetaDescriptionAttribute  = $categoryMetaDescriptionCollection->getFirstItem();
        $this->attribute_options[self::CATEGORY_PREFIX.$categoryMetaDescriptionAttribute->getFrontLabel()] = array('code' => self::CATEGORY_VALUE_PREFIX.$categoryMetaDescriptionAttribute->getAttributeCode(),
            'label' => ($categoryMetaDescriptionAttribute->getFrontendLabel() ? self::CATEGORY_PREFIX.$categoryMetaDescriptionAttribute->getFrontendLabel() : self::CATEGORY_PREFIX.$categoryMetaDescriptionAttribute->getAttributeCode()));

        return $this->attribute_options;
    }

    public function getAttributeOptionsArray()
    {
        if (is_null($this->attribute_options)) {
            $this->attribute_options = array();
            $this->attribute_options['Product Id'] = array('code' => "entity_id", 'label' => "Product Id");
            $this->attribute_options['Is In Stock'] = array('code' => "is_in_stock", 'label' => "Is In Stock");
            $this->attribute_options['Qty'] = array('code' => "qty", 'label' => "Qty");
            $this->attribute_options['Image'] = array('code' => "image", 'label' => "Image");
            $this->attribute_options['URL'] = array('code' => "url", 'label' => "URL");
            $this->attribute_options['Category'] = array('code' => "category", 'label' => "Category");
            $this->attribute_options['Final Price'] = array('code' => "final_price", 'label' => "Final Price");
            $this->attribute_options['Store Price'] = array('code' => "store_price", 'label' => "Store Price");
            $this->attribute_options['Image 2'] = array('code' => "image_2", 'label' => "Image 2");
            $this->attribute_options['Image 3'] = array('code' => "image_3", 'label' => "Image 3");
            $this->attribute_options['Image 4'] = array('code' => "image_4", 'label' => "Image 4");
            $this->attribute_options['Image 5'] = array('code' => "image_5", 'label' => "Image 5");
            $this->attribute_options['SKU Amazon'] = array('code' => "sku_amazon", 'label' => "SKU Amazon");
            $this->attribute_options['Category > SubCategory'] = array('code' => "category_subcategory", 'label' => "Category > SubCategory");
            $custom_attributes = Mage::getResourceModel('gomage_feed/custom_attribute_collection');
            foreach ($custom_attributes as $attribute) {
                $label = '* ' . $attribute->getName();
                $this->attribute_options[$label] = array('code' => sprintf('custom:%s', $attribute->getCode()), 'label' => $label);
            }
            foreach ($this->getAttributeCollection() as $attribute) {
                if ($attribute->getFrontendLabel()) {
                    $this->attribute_options[$attribute->getFrontendLabel()] = array('code' => $attribute->getAttributeCode(), 'label' => ($attribute->getFrontendLabel() ? $attribute->getFrontendLabel() : $attribute->getAttributeCode()));
                }
            }

            $this->attribute_options = $this->setCategoryAdditionalAttribute();
            ksort($this->attribute_options);
        }
        return $this->attribute_options;
    }
}