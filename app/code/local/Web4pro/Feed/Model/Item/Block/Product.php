<?php

class Web4pro_Feed_Model_Item_Block_Product extends GoMage_Feed_Model_Item_Block_Product
{
    public function writeTempFile($curr_page = 0, $length = 50, $filename = '') {

        $collection = $this->getFeed()->getProductsCollection('', $curr_page, $length);

        $content = @file_get_contents(Mage::helper('gomage_feed/generator')->getBaseDir() . DS . 'feed-' . $this->getFeed()->getId() . '-xml-product-block-template.tmp');
        $template_attributes = $this->getAllVars($content);

        $codes = array();

        foreach ($template_attributes as $attribute_code) {
            $codes[] = $attribute_code;
        }

        $custom_attributes = Mage::getResourceModel('gomage_feed/custom_attribute_collection');
        $custom_attributes->load();

        foreach ($custom_attributes as $_attribute) {
            $options = Zend_Json::decode($_attribute->getData('data'));
            if ($options && is_array($options)) {
                $_attribute->setOptions($options);
                foreach ($options as $option) {
                    if (isset($option['value_type_attribute']) && $option['value_type_attribute']) {
                        foreach ($option['value_type_attribute'] as $_value_type_attribute) {
                            $codes[] = $_value_type_attribute['attribute'];
                        }
                    }
                    foreach ($option['condition'] as $_condition) {
                        $codes[] = $_condition['attribute_code'];
                    }
                }
            }
            else {
                $_attribute->setOptions(array());
            }
        }

        $attributes = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(Mage::getResourceModel('catalog/product')->getEntityType()->getData('entity_type_id'))->setCodeFilter($codes);
        $root_category = $this->getFeed()->getRootCategory();

        $this->getFeed()->addLog(date("F j, Y, g:i:s a") . ', page:' . $curr_page . ', items selected:' . count($collection) . "\n");

        foreach ($collection as $_product) {

            if ($this->getFeed()->getUseLayer() == GoMage_Feed_Model_Adminhtml_System_Config_Source_Uselayer::NO_WITH_CHILD) {
                if ($_product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                    if (! $_product->isSalable()) {
                        $generate_info = Mage::helper('gomage_feed/generator')->getGenerateInfo($this->getFeed()->getId());
                        $generate_info->addGeneratedRecords(1)->save();
                        continue;
                    }
                }
            }

            $product = new Varien_Object();

            $this->getFeed()->prepareProductCategory($_product, $root_category);

            foreach ($template_attributes as $attribute_code) {

                if (strpos($attribute_code, 'parent:') === 0) {
                    $attribute_code_parent = trim(str_replace('parent:', '', $attribute_code));
                    $parent_product = $this->getFeed()->getParentProduct($_product);
                    if ($parent_product->getId()) {
                        $value = $this->getFeed()->getProductAttributeValue($parent_product, $attribute_code_parent, $attributes, $custom_attributes);
                    }
                    else {
                        $value = $this->getFeed()->getProductAttributeValue($_product, $attribute_code_parent, $attributes, $custom_attributes);
                    }
                } elseif($attribute_code == 'category_meta_description') {
                    $value = $this->getProductCategoryAttributeValue($_product, $attribute_code);
                }
                else {
                    $value = $this->getFeed()->getProductAttributeValue($_product, $attribute_code, $attributes, $custom_attributes);
                }

                if ($value && ! $product->getData($attribute_code)) {
                    $product->setData($attribute_code, $value);
                }

            }



            $fp = fopen($this->getFeed()->getTempFilePath(), 'a');
            fwrite($fp, GoMage_Feed_Model_Item_Block::setVars($content, $product) . "\r\n");
            fclose($fp);

            $generate_info = Mage::helper('gomage_feed/generator')->getGenerateInfo($this->getFeed()->getId());
            if ($generate_info->getData('stopped')){
                return false;
            }
            $generate_info->addGeneratedRecords(1)->save();

        }

    }

    public function getProductCategoryAttributeValue(&$product, $attribute_code)
    {
        $value                  = null;

        switch($attribute_code) {
            case ('category_meta_description'):
                $categoryIds           = $product->getCategoryIds();
                $categoryIds           = array_reverse($categoryIds);

                foreach($categoryIds as $key => $categoryId) {
                    $categoryMetaDescription = null;
                    $categoryMetaDescription = trim($this->_getCategoryMetaDescription($categoryId));
                    if($categoryMetaDescription) {
                        $value = $categoryMetaDescription;
                        break;
                    }
                }

                break;
        }

        return $value;
    }

    protected function _getCategoryMetaDescription($categoryId)
    {
        $value = null;
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $metaDescription = $category->getMetaDescription();
        if($metaDescription) {
            $value = $metaDescription;
        }

        return $value;
    }
}