<?php

class Web4pro_Feed_Model_Item extends GoMage_Feed_Model_Item
{
    const PRODUCTS_ROOT_CATEGORY_ID = 3;

    public function writeTempFile($start = 0, $length = 50, $filename = '') {

        if ($filename) {
            $filePath = Mage::helper('gomage_feed/generator')->getBaseDir() . DS . $filename;
        }
        else {
            $filePath = $this->getTempFilePath();
        }
        $fileDir = Mage::helper('gomage_feed/generator')->getBaseDir();

        if (! file_exists($fileDir)) {
            mkdir($fileDir);
            chmod($fileDir, 0777);
        }

        if (is_dir($fileDir)) {

            if ($this->getType() == 'csv') {

                switch ($this->getDelimiter()) {
                    case ('comma'):
                    default:
                        $delimiter = ",";
                        break;
                    case ('tab'):
                        $delimiter = "\t";
                        break;
                    case ('colon'):
                        $delimiter = ":";
                        break;
                    case ('space'):
                        $delimiter = " ";
                        break;
                    case ('vertical pipe'):
                        $delimiter = "|";
                        break;
                    case ('semi-colon'):
                        $delimiter = ";";
                        break;
                }

                switch ($this->getEnclosure()) {
                    case (1):
                    default:
                        $enclosure = "'";
                        break;
                    case (2):
                        $enclosure = '"';
                        break;
                    case (3):
                        $enclosure = ' ';
                        break;
                    case (4):
                        $enclosure = ' ';
                        break;
                }

                $collection = $this->getProductsCollection('', $start, $length);

                $maping = json_decode($this->getContent());

                $codes = array();

                foreach ($maping as $col) {
                    if ($col->type == 'attribute') {
                        $codes[] = $col->attribute_value;
                    }
                }

                $custom_attributes = Mage::getResourceModel('gomage_feed/custom_attribute_collection');
                $custom_attributes->load();

                foreach ($custom_attributes as $_attribute) {
                    $options = Zend_Json::decode($_attribute->getData('data'));
                    if ($options && is_array($options)) {
                        $_attribute->setOptions($options);
                        foreach ($options as $option) {
                            if (isset($option['value_type_attribute']) && $option['value_type_attribute']) {
                                foreach ($option['value_type_attribute'] as $_value_type_attribute) {
                                    $codes[] = $_value_type_attribute['attribute'];
                                }
                            }
                            foreach ($option['condition'] as $_condition) {
                                $codes[] = $_condition['attribute_code'];
                            }
                        }
                    }
                    else {
                        $_attribute->setOptions(array());
                    }
                }

                $attributes = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(Mage::getResourceModel('catalog/product')->getEntityType()->getData('entity_type_id'))->setCodeFilter($codes);
                $categoryDescriptionCollection = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(Mage::getResourceModel('catalog/category')->getEntityType()->getData('entity_type_id'))->addFieldToFilter('attribute_code', array('eq' => 'meta_description'));
                $root_category = $this->getRootCategory();

                $this->addLog(date("F j, Y, g:i:s a") . ', page:' . $start . ', items selected:' . count($collection) . "\n");

                foreach ($collection as $product) {

                    if ($this->getUseLayer() == GoMage_Feed_Model_Adminhtml_System_Config_Source_Uselayer::NO_WITH_CHILD) {
                        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                            if (! $product->isSalable()) {
                                $generate_info = Mage::helper('gomage_feed/generator')->getGenerateInfo($this->getId());
                                $generate_info->addGeneratedRecords(1)->save();
                                continue;
                            }
                        }
                    }

                    $fields = array();

                    $this->prepareProductCategory($product, $root_category);

                    foreach ($maping as $col) {

                        $value = null;


                        if ($col->type == 'attribute') {

                            if ($col->attribute_value) {
                                if($col->attribute_value == 'category_meta_description') {
                                    $value = $this->getProductCategoryAttributeValue($product, $col->attribute_value, $categoryDescriptionCollection);
                                } else {
                                    $value = $this->getProductAttributeValue($product, $col->attribute_value, $attributes, $custom_attributes);
                                }
                            }
                            else {
                                $value = '';
                            }
                        }
                        elseif ($col->type == 'parent_attribute') {

                            if ($col->attribute_value) {
                                $parent_product = $this->getParentProduct($product);

                                if ($parent_product->getId()) {
                                    if($col->attribute_value == 'url'){
                                        $value = '';
                                    }else{
                                        $value = $parent_product->getAttributeText($col->attribute_value);
                                    }


                                    if(empty($value)){
                                        if ( $col->attribute_value == 'sku_amazon' )
                                        {
                                            $value = $this->getProductAttributeValue($parent_product, 'sku', $attributes, $custom_attributes);
                                        }
                                        else
                                        {

                                            $value = $this->getProductAttributeValue($parent_product, $col->attribute_value, $attributes, $custom_attributes);
                                        }
                                    }
                                }
                                else {
                                    if($col->attribute_value == 'url'){
                                        $value = '';
                                    }else{
                                        $value = $product->getAttributeText($col->attribute_value);
                                    }


                                    if(empty($value)){
                                        if ( $col->attribute_value == 'sku_amazon' )
                                        {
                                            $value = '';
                                        }
                                        else
                                        {
                                            if($col->attribute_value != 'sku'){
                                                $value = $this->getProductAttributeValue($product, $col->attribute_value, $attributes, $custom_attributes);
                                            }

                                        }
                                    }
                                }
                            }
                            else {
                                $value = '';
                            }
                            // if empty
                        }elseif ($col->type == 'if_empty_child_attribute') {
                            if ($col->attribute_value) {
                                $value = $this->getProductAttributeValue($product, $col->attribute_value, $attributes, $custom_attributes);

                                if(empty($value)){
                                    $parent_product = $this->getParentProduct($product);
                                    if($parent_product){
                                        if ($parent_product->getId()) {
                                            $value = $this->getProductAttributeValue($parent_product, $col->attribute_value, $attributes, $custom_attributes);
                                        }
                                    }
                                }
                            }else {
                                $value = '';
                            }

                        }
                        elseif ($col->type == 'if_empty_parent_attribute') {
                            if ($col->attribute_value) {
                                $value = $this->getProductAttributeValue($product, $col->attribute_value, $attributes, $custom_attributes);
                                if(empty($value)){
                                    $child_product = $this->getChildProduct($product);
                                    if($child_product){
                                        if ($child_product->getId()) {
                                            $value = $this->getProductAttributeValue($child_product, $col->attribute_value, $attributes, $custom_attributes);
                                        }
                                    }
                                }
                            }
                            else {
                                $value = '';
                            }


                        }
                        else {
                            $value = $col->static_value;
                        }


                        // prefix value
                        if ($col->prefix_type == 'attribute') {
                            if ($col->attribute_prefix_value) {
                                $value_prefix = $product->getAttributeText($col->attribute_prefix_value);
                                if(empty($value_prefix)){
                                    $value_prefix = $this->getProductAttributeValue($product, $col->attribute_prefix_value, $attributes, $custom_attributes);
                                }
                            }
                            else {
                                $value_prefix = '';
                            }
                        }
                        elseif ($col->prefix_type == 'parent_attribute') {

                            if ($col->attribute_prefix_value) {
                                $parent_product = $this->getParentProduct($product);

                                if ($parent_product->getId()) {

                                    if($col->attribute_prefix_value == 'url'){
                                        $value_prefix = '';
                                    }else{
                                        $value_prefix = $parent_product->getAttributeText($col->attribute_prefix_value);
                                    }

                                    if(empty($value_prefix)){
                                        if ( $col->attribute_prefix_value == 'sku_amazon' )
                                        {
                                            $value_prefix = $this->getProductAttributeValue($parent_product, 'sku', $attributes, $custom_attributes);
                                        }
                                        else
                                        {
                                            $value_prefix = $this->getProductAttributeValue($parent_product, $col->attribute_prefix_value, $attributes, $custom_attributes);
                                        }
                                    }
                                }
                                else {
                                    if($col->attribute_prefix_value == 'url'){
                                        $value_prefix = '';
                                    }else{
                                        $value_prefix = $product->getAttributeText($col->attribute_prefix_value);
                                    }

                                    if(empty($value_prefix)){

                                        if ( $col->attribute_prefix_value == 'sku_amazon' )
                                        {
                                            $value_prefix = '';
                                        }
                                        else
                                        {
                                            if($col->attribute_prefix_value != 'sku'){
                                                $value_prefix = $this->getProductAttributeValue($product, $col->attribute_prefix_value, $attributes, $custom_attributes);
                                            }

                                        }
                                    }
                                }
                            }
                            else {
                                $value_prefix = '';
                            }
                        }
                        else {
                            $value_prefix = $col->prefix_value;
                        }



                        // suffix value
                        if ($col->suffix_type == 'attribute') {
                            if ($col->attribute_suffix_value) {
                                $value_suffix = $product->getAttributeText($col->attribute_prefix_value);
                                if(empty($value_suffix)){
                                    $value_suffix = $this->getProductAttributeValue($product, $col->attribute_suffix_value, $attributes, $custom_attributes);
                                }
                            }
                            else {
                                $value_suffix = '';
                            }
                        }
                        elseif ($col->suffix_type == 'parent_attribute') {

                            if ($col->attribute_suffix_value) {
                                $parent_product = $this->getParentProduct($product);

                                if ($parent_product->getId()) {


                                    if($col->attribute_suffix_value == 'url'){
                                        $value_suffix = '';
                                    }else{
                                        $value_suffix = $parent_product->getAttributeText($col->attribute_suffix_value);
                                    }
                                    if(empty($value_suffix)){
                                        if ( $col->attribute_suffix_value == 'sku_amazon' )
                                        {
                                            $value_suffix = $this->getProductAttributeValue($parent_product, 'sku', $attributes, $custom_attributes);
                                        }
                                        else
                                        {
                                            $value_suffix = $this->getProductAttributeValue($parent_product, $col->attribute_suffix_value, $attributes, $custom_attributes);
                                        }
                                    }
                                }
                                else {
                                    if($col->attribute_suffix_value == 'url'){
                                        $value_suffix = '';
                                    }else{
                                        $value_suffix = $product->getAttributeText($col->attribute_suffix_value);
                                    }

                                    if(empty($value_suffix)){
                                        if ( $col->attribute_suffix_value == 'sku_amazon' )
                                        {
                                            $value_suffix = '';
                                        }
                                        else
                                        {
                                            if($col->attribute_suffix_value != 'sku'){
                                                $value_suffix = $this->getProductAttributeValue($product, $col->attribute_suffix_value, $attributes, $custom_attributes);
                                            }

                                        }
                                    }
                                }
                            }
                            else {
                                $value_suffix = '';
                            }
                        }
                        else {
                            $value_suffix = $col->suffix_value;
                        }


                        if ($output_type = $col->output_type) {
                            $value = $this->applyValueFilter('format', $output_type, $value);
                        }

                        if (intval($this->getRemoveLb())) {
                            $value = str_replace("\n", '', $value);
                            $value = str_replace("\r", '', $value);
                        }

                        $value = $this->applyValueFilter('limit', $col->limit, $value);

                        $fields[] = $value_prefix. $this->getDelimiterPrefix(). $value . $this->getDelimiterSufix() . $value_suffix;

                    }

                    $fp = fopen($filePath, 'a');
                    if ($this->getEnclosure() != 4)
                        fputcsv($fp, $fields, $delimiter, $enclosure);
                    else
                        $this->extended_fputcsv($fp, $fields, $delimiter);
                    fclose($fp);

                    $generate_info = Mage::helper('gomage_feed/generator')->getGenerateInfo($this->getId());
                    if ($generate_info->getData('stopped')) {
                        return false;
                    }
                    $generate_info->addGeneratedRecords(1)->save();

                }
            }
            else {
                //XML FORMAT
                $rootBlock = Mage::getModel('gomage_feed/item_block', array('content' => $this->getContent(), 'feed' => $this));
                @file_put_contents($filePath, $rootBlock->render());
            }
        }
    }

    public function getProductCategoryAttributeValue(&$product, $coll_attribute_value, $attributes)
    {
        $store                  = $this->getStore();
        $value                  = null;

        switch($coll_attribute_value) {
            case ('category_meta_description'):
                $categoryIds           = $product->getCategoryIds();
                $categoryIds           = array_reverse($categoryIds);

                foreach($categoryIds as $key => $categoryId) {
                    $categoryMetaDescription = null;
                    $categoryMetaDescription = trim($this->_getCategoryMetaDescription($categoryId));
                    if($categoryMetaDescription) {
                        $value = $categoryMetaDescription;
                        break;
                    }
                }

            break;
        }

        return $value;
    }

    protected function _getCategoryMetaDescription($categoryId)
    {
        $value = null;
        $category = Mage::getModel('catalog/category')->load($categoryId);
        $metaDescription = $category->getMetaDescription();
        if($metaDescription) {
            $value = $metaDescription;
        }

        return $value;
    }
}