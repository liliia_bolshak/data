<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Followupemail
 * @version    3.6.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class Web4pro_Followupevent_Model_Rule extends AW_Followupemail_Model_Rule
{
    /*
     * Validates rule basing on product properties
     * @param array $params Parameters to inspect
     * @return bool|string Check result
     */
    protected function _validate($params)
    {
        $this->_validated = true;

        if (true !== $res = $this->validateByCustomer($params)) {
            return $res;
        }

        // MSS check
        $mssRuleId = false;
        if (Mage::helper('followupemail')->isMSSInstalled()
            && $mssRuleId = $this->getMssRuleId()
        ) {
            if (isset($params['customer'])) {
                if (!Mage::getModel('marketsuite/api')->checkRule($params['customer'], $mssRuleId)) {
                    return 'MSS rule d=' . $mssRuleId . ' validation failed';
                }
                $mssRuleId = false; // preventing further MSS checks
            }
        }

        // Check is customer is unsubscribed for this rule
        if (isset($params['customer']) && $params['customer']->getId()) {
            if (in_array($params['customer']->getId(), $this->getData('unsubscribed_customers'))) {
                return Mage::helper('followupemail')->__(
                    'Customer with ID %s is unsubscribed from rule %s', $params['customer']->getId(), $this->getId()
                );
            }
        }

        switch ($this->getEventType()) {
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_WISHLIST_SHARED :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_WISHLIST_PRODUCT_ADD :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_REVIEW :
                return $this->validateByProduct($params);
                break;

            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_ABANDONED_CART_NEW :
                if ($mssRuleId
                    && isset($params['quote'])
                    && !Mage::getModel('marketsuite/api')->checkRule($params['quote'], $mssRuleId)
                ) {
                    return 'MSS rule d=' . $mssRuleId . ' validation failed';
                }

                return $this->validateOrderOrCart($params, 'quote');
                break;

            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_NEW :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_LOGGED_IN :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_LAST_ACTIVITY :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_BIRTHDAY :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_NEW_SUBSCRIPTION :
            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_INVOICE_IMPORTED :
                return true;
                break;

            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_CAME_BACK_BY_LINK :
                return true;
                break;

            case AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_GROUP_CHANGED:
                return $this->validateByCustomer($params);
                break;

            default :
                if ($this->_orderStatus) {
                    if ($mssRuleId
                        && isset($params['order'])
                        && !Mage::getModel('marketsuite/api')->checkRule($params['order'], $mssRuleId)
                    ) {
                        return 'MSS rule d=' . $mssRuleId . ' validation failed';
                    }

                    return $this->validateOrderOrCart($params, 'order');
                }
                break;
        }
        return 'Unknown event';
    }
}
