<?php


class Web4pro_Followupevent_Model_Observer {

    public function afterInvoiceImported($observer) {

        $customerId = $observer->getCustomerId();
        $order = $observer->getOrderModel();

        $queue = Mage::getResourceModel('followupemail/queue');
//        $queue->cancelByEvent(
//            $customer->getEmail(),
//            AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_CUSTOMER_GROUP_CHANGED
//        );

        $ruleIds = Mage::getModel('followupemail/mysql4_rule')
            ->getRuleIdsByEventType(AW_Followupemail_Model_Source_Rule_Types::RULE_TYPE_INVOICE_IMPORTED);
        if (count($ruleIds)) {
            $params = array(
                'customer_id'        => $customerId,
                'store_id'           => Mage::app()->getStore()->getId(),
            );
            $objects = array(
                'object_id' => $customerId(),
//                ''          => $
            );

            foreach ($ruleIds as $ruleId) {
                Mage::getModel('followupemail/rule')->load($ruleId)->process($params, $objects);
            }
        }

    }
}