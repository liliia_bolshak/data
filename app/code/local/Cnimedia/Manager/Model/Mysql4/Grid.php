<?php

class Cnimedia_Manager_Model_Mysql4_Grid extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Note that the manager_id refers to the key field in your database table.
		$this->_init('manager/grid', 'id');
	}
	public function addGridPosition($collection,$manager_id){
		$table2 = $this->getMainTable();
		$cond = $this->_getWriteAdapter()->quoteInto('e.entity_id = t2.customer_id','');
		$where = $this->_getWriteAdapter()->quoteInto('t2.manager_id = ? OR ', $manager_id).
		$this->_getWriteAdapter()->quoteInto('isnull(t2.manager_id)','');
		$collection->getSelect()->joinLeft(array('t2'=>$table2), $cond)->where($where);
			
		//echo $collection->getSelect();die;
	}
}