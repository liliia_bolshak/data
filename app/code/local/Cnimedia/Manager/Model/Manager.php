<?php

class Cnimedia_Manager_Model_Manager extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('manager/manager');
    }

    public function createNewManager($managerId, $customerId, $rights, $createViaResource = true)
    {
        $createFirstManager = Mage::getModel('manager/grid');
        $createFirstManager->setManagerId($managerId);
        $createFirstManager->setCustomerId($customerId);
        $createFirstManager->setPosition(0);
        $createFirstManager->save();

        if ($createViaResource) {
            $lastOrderIncrementId = Mage::getModel("manager/grid")->getCollection()->getLastItem()->getId() + 1;

            $data = array(
                'manager_id' => $lastOrderIncrementId,
                'subaccount_position' => $rights['subaccount_position'],
                'enable_orders' => $rights['enable_orders'],
                'enable_aw_hdu' => $rights['enable_aw_hdu'],
                'enable_aw_rma' => $rights['enable_aw_rma'],
                'enable_aw_followup' => $rights['enable_aw_followup'],
                'enable_admin' => $rights['enable_admin'],
            );

            $subAccountData = Mage::getModel('manager/gridvar')->setData($data);

            try {
                $subAccountData->save();
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
        } else {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            try {
                $table = Mage::getSingleton('core/resource')->getTableName('grid_manager_var');
                $query = "INSERT INTO {$table}
                          (manager_id, subaccount_position, enable_orders, enable_aw_hdu, enable_aw_rma, enable_aw_followup, enable_admin)
                   VALUES (:manager_id, :subaccount_position, :enable_orders, :enable_aw_hdu, :enable_aw_rma, :enable_aw_followup, :enable_admin)";

                $lastOrderIncrementId = Mage::getModel("manager/grid")->getCollection()->getLastItem()->getId();
                $data = array(
                    'manager_id' => $lastOrderIncrementId,
                    'subaccount_position' => $rights['subaccount_position'],
                    'enable_orders' => $rights['enable_orders'],
                    'enable_aw_hdu' => $rights['enable_aw_hdu'],
                    'enable_aw_rma' => $rights['enable_aw_rma'],
                    'enable_aw_followup' => $rights['enable_aw_followup'],
                    'enable_admin' => $rights['enable_admin'],
                );

                $write->query($query, $data);
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
        }
    }
}