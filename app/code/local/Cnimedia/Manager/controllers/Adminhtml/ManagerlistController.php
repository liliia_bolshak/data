<?php

class Cnimedia_Manager_Adminhtml_ManagerlistController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('manager/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()
            ->renderLayout();
    }

    public function managerlistAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('managerlist.grid')
            ->setManagers($this->getRequest()->getPost('managers', null));
        $this->renderLayout();
    }

    public function managerlistgridAction(){
        echo $this->getLayout()->createBlock('manager/adminhtml_managerlist_grid')->toHtml();
    }

}