<?php

class Cnimedia_Manager_Adminhtml_CustomerController extends Mage_Adminhtml_Controller_action
{

    public function customerAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('customer.grid')
            ->setCustomers($this->getRequest()->getPost('customers', null));
        $this->renderLayout();
    }

    public function customergridAction(){
        $this->_initAction()
            ->renderLayout();
    }


    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('manager/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()
            ->renderLayout();
    }

    public function editcustomerAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('manager/items');
        $this->renderLayout();
    }

    public function editAction() {
        $this->loadLayout();
        $this->_setActiveMenu('manager/items');

        $cID = ($this->getRequest()->getParam('customer_id')) ? $this->getRequest()->getParam('customer_id') : -1;
        $mID = $this->getRequest()->getParam('manager_id');

        if($cID > 0 && !empty($mID))
        {
            $managerGrid= Mage::getModel('manager/grid')->getCollection()
                ->addFieldToFilter('customer_id', $cID)
                ->addFieldToFilter('manager_id', array("eq" => $mID))
                ->load()
                ->getLastItem();

            $managerGridVar = Mage::getModel('manager/grid')->getCollection()
                ->addFieldToFilter('manager_id', $managerGrid->getId())
                ->load()
                ->getLastItem();

            if(count($managerGridVar) < 1)
            {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to edit customer rights.'));
            }
        }
        elseif($cID == -1 && !empty($mID))
        {

            $managerGrid= Mage::getModel('manager/grid')->getCollection()
                ->addFieldToFilter('manager_id', array("eq" => $mID))
                ->load()
                ->getLastItem();

            $managerGridVar = Mage::getModel('manager/grid')->getCollection()
                ->addFieldToFilter('manager_id', $managerGrid->getId())
                ->load()
                ->getLastItem();

            if(count($managerGridVar) < 1)
            {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to edit customer rights.'));
            }
        }
        else
        {
           Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to edit customer rights.'));
        }

        $this->renderLayout();
    }

    public function saveAction() {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/');
        }
        if ($this->getRequest()->isPost()) {
            $cID = $this->getRequest()->getParam('customer_rights_id');

            $positionType = ($this->getRequest()->getParam('position_type')>0) ? $this->getRequest()->getParam('position_type') : '135';
            $reservedPrices = ($this->getRequest()->getParam('reserved_prices') == '1') ? true : false;
            $helpDesk = ($this->getRequest()->getParam('help_desk') == '1') ? true : false;
            $ordersRMA = ($this->getRequest()->getParam('orders_rma') == '1') ? true : false;
            $followupEmail = ($this->getRequest()->getParam('followup_email') == '1') ? true : false;
            $isAdmin = ($this->getRequest()->getParam('is_admin') == '1') ? true : false;

            if($isAdmin)
            {
                $reservedPrices = true;
                $helpDesk = true;
                $ordersRMA = true;
                $followupEmail = true;
            }

            $data = array(
                'subaccount_position' => $positionType,
                'enable_orders' => $reservedPrices,
                'enable_aw_hdu' => $helpDesk,
                'enable_aw_rma' => $ordersRMA,
                'enable_aw_followup' => $followupEmail,
                'enable_admin' => $isAdmin,
            );

            $customerRights = Mage::getModel('manager/gridvar')->load($cID)->addData($data);
            $customer = Mage::getModel('manager/grid')->getCollection()
                ->addFieldToFilter('id', intval($cID))
                ->getLastItem();

            try {
                $customerRights->setManagerId($cID)->save();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Customer rights saved successfully'));
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/adminhtml_customer/edit', array('manager_id' => $customer->getManagerId(), 'customer_id'=> $customer->getCustomerId()));
                    return;
                }
                $this->_redirect('*/adminhtml_manager/edit', array('manager_id' => $customer->getManagerId()));
                return;
            } catch (Exception $e){
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
                $this->_redirect('*/adminhtml_customer/edit', array('manager_id' => $customer->getManagerId(), 'customer_id'=> $customer->getCustomerId()));
            }
        }
        else
        {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manager')->__('Unable to save customer rights'));
            $this->_redirect('*/*/');
        }

    }

}