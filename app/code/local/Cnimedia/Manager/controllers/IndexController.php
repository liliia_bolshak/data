<?php
class Cnimedia_Manager_IndexController extends Mage_Core_Controller_Front_Action
{
    protected function _initAction($title = 'RMA') {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__($title));
        return $this;
    }

    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/hourly?id=15 
    	 *  or
    	 * http://site.com/hourly/id/15 	
    	 */
    	/* 
		$hourly_id = $this->getRequest()->getParam('id');

  		if($hourly_id != null && $hourly_id != '')	{
			$hourly = Mage::getModel('hourly/hourly')->load($hourly_id)->getData();
		} else {
			$hourly = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($hourly == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$hourlyTable = $resource->getTableName('hourly');
			
			$select = $read->select()
			   ->from($hourlyTable,array('hourly_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$hourly = $read->fetchRow($select);
		}
		Mage::register('hourly', $hourly);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
	
	public function customerordersAction() {
		
			
		$this->loadLayout();     
		$this->renderLayout();
	}
	
	public function locationAction() {
		
			
		$this->loadLayout();     
		$this->renderLayout();
	}
	public function customercreateAction() {
        $this->loadLayout();

        $session=Mage::getSingleton('customer/session', array('name'=>'frontend') );

        if (!$session->isLoggedIn()) {
            Mage::getSingleton('customer/session')->addError('You cannot create the sub account. Please contact the Customer support to connect the sub account.');
            $this->_redirect('customer/account/login');
        }

		$this->renderLayout();
	}
	public function viewAction() {
		
			
		$this->loadLayout();     
		$this->renderLayout();
	}
	
	public function createpostAction() {
			$manager 	= Mage::getSingleton('customer/session')->getCustomer();
			$customer 	= Mage::getModel('customer/customer');
			//$customer = new Mage_Customer_Model_Customer();

			$password 	= $this->getRequest()->getParam('password');
			$email 		= $this->getRequest()->getParam('email');
			$firstname 	= $this->getRequest()->getParam('firstname');
			$lastname 	= $this->getRequest()->getParam('lastname');
            $telephone  = $this->getRequest()->getParam('telephone');
            $position = $this->getRequest()->getParam('position_type');

            //rights
            $reservedPrices = ($this->getRequest()->getParam('reserved_prices') == 'on') ? true : false;
            $helpDesk = ($this->getRequest()->getParam('help_desk') == 'on') ? true : false;
            $ordersRMA = ($this->getRequest()->getParam('orders_rma') == 'on') ? true : false;
            $followupEmail = ($this->getRequest()->getParam('followup_email') == 'on') ? true : false;
            $isAdmin = ($this->getRequest()->getParam('is_admin') == 'on') ? true : false;

            $isSubscribed = ($this->getRequest()->getParam('is_subscribed') == 'on') ? true : false;
			 
			$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
			$customer->loadByEmail($email);

            //Add Sub-account rights information
            if($isAdmin)
            {
                $reservedPrices = true;
                $helpDesk = true;
                $ordersRMA = true;
                $followupEmail = true;
            }

            $rights = array(
                "subaccount_position" => $position,
                "enable_orders" => $reservedPrices,
                "enable_aw_hdu" => $helpDesk,
                "enable_aw_rma" => $ordersRMA,
                "enable_aw_followup" => $followupEmail,
                "enable_admin" => $isAdmin
            );

			
			$manName 	= ($manager->getFirstname()) ? $manager->getFirstname() : $firstname.' '.$lastname;
			$manager 	= $manager->getId();

			//Zend_Debug::dump($customer->debug()); exit;
			if(!$customer->getId()) {
 
			    $customer->setEmail($email);
			    $customer->setFirstname($firstname);
			    $customer->setLastname($lastname);
			    $customer->setPassword($password);
                if(isset($telephone) && !empty($telephone))
                {
                    $customer->setTelephone($telephone);
                }
                if($reservedPrices)
                {
                    $customer->setData('group_id', 4);
                }

                try {
                    $customer->save();
                    $customer->setConfirmation(null);
                    $customer->save();

                    Mage::getSingleton('customer/session')->addSuccess(Mage::helper('manager')->__('Customer Created'));
                    //$this->_redirect('*/*/customercreate');

                    $cId = $customer->getId();

                    $collection1 = Mage::getModel('manager/manager')->getCollection();
                    $collection1->addFieldToFilter('manager_id',$manager);

                    $model = Mage::getModel('manager/manager');
                    $model->setManagerId($manager);
                    $model->setTitle($manName);
                    $model->save();

                    // fetch write database connection that is used in Mage_Core module
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    //echo "insert into customer_manager values ($manager,$manName)"; exit();
                    // now $write is an instance of Zend_Db_Adapter_Abstract
                    //$write->query("insert into customer_manager values ($manager,$manName)");

                    $collection = Mage::getModel('manager/grid')->getCollection();
                    $collection->addFieldToFilter('manager_id',$manager);

                    //both users set manager of each other
                    //first manager
                    Mage::getModel('manager/manager')->createNewManager($manager,$cId,$rights,false);
                    //second manager
                    Mage::getModel('manager/manager')->createNewManager($cId,$manager,$rights,false);

                    //Make a "login" of new customer
                    //  Mage::getSingleton('customer/session')->loginById($customer->getId());


                    $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();

                    if (isset($customerAddressId) && $customerAddressId !== ""){
                        $address = Mage::getModel('customer/address')->load($customerAddressId);


                        $_custom_address = array (
                            'firstname' => $firstname,
                            'lastname' => $lastname,
                            'street' => $address->getStreet(),

                            'city' => $address->getCity(),
                            'region_id' =>  $address->getRegionId(),
                            'region' =>  $address->getRegion(),
                            'postcode' =>  $address->getPostcode(),
                            'country_id' =>  $address->getCountryId(),
                            'telephone' =>  $address->getTelephone(),
                        );

                        $customAddress = Mage::getModel('customer/address');

                        //$customAddress = new Mage_Customer_Model_Address();
                        $customAddress->setData($_custom_address)
                            ->setCustomerId($cId)
                            ->setIsDefaultBilling('1')
                            ->setIsDefaultShipping('0')
                            ->setSaveInAddressBook('1');

                        try {
                            $customAddress->save();
                        }
                        catch (Exception $ex) {
                            Zend_Debug::dump($ex->getMessage());
                        }

                        Mage::getSingleton('checkout/session')->getQuote()->setBillingAddress(Mage::getSingleton('sales/quote_address')->importCustomerAddress($customAddress));
                    }

                    $this->_redirect('*/*/customercreate');
                }
                catch (Exception $ex) {

                    Mage::getSingleton('customer/session')->addError($ex->getMessage());
                    $this->_redirect('*/*/customercreate');
                }
			}
            else
            {
                Mage::getSingleton('customer/session')->addError('You cannot create the sub account, email address already exists. Please contact the Customer support to connect the sub account.');
                $this->_redirect('*/*/customercreate');
            }
	}
	
	public function mycustomersAction() {
		/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/hourly?id=15 
    	 *  or
    	 * http://site.com/hourly/id/15 	
    	 */
    	
		$hourly_id = $this->getRequest()->getParam('id');
		echo $hourly_id;
  		if($hourly_id != null && $hourly_id != '')	{
			$hourly = Mage::getModel('manager/grid')->load($hourly_id)->getData();
		} else {
			$hourly = null;
		}	
		
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($hourly == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$hourlyTable = $resource->getTableName('hourly');
			
			$select = $read->select()
			   ->from($hourlyTable,array('hourly_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$hourly = $read->fetchRow($select);
		}
		Mage::register('hourly', $hourly);*/
		
		
			
		$this->loadLayout();     
		$this->renderLayout();
	}

    public function subcustomerordersAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function subcustomerhduAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function subcustomerrmaAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function scrmaviewAction() {
        $this->_initAction();
        $_rmaRequest = $this->_getRmaRequest();
        if ($_rmaRequest) {
            Mage::unregister('awrma-request');
            Mage::register('awrma-request', $_rmaRequest);
        }
        $this->renderLayout();
    }

    protected function _getRmaRequest() {
        if ($this->getRequest()->getParam('id') && $this->getRequest()->getParam('account')) {
            $_rmaRequest = Mage::getModel('awrma/entity')->load($this->getRequest()->getParam('id'));
            if (is_object($_rmaRequest) && $_rmaRequest->getData() != array()) {
                if ($_rmaRequest->getData('customer_id') == $this->getRequest()->getParam('account')) {
                    return $_rmaRequest;
                } else {
                    $this->_getSession()->addError($this->__('Wrong request ID'));
                }
            } else {
                $this->_getSession()->addError($this->__('Can\'t load RMA request'));
            }
        } else {
            $this->_getSession()->addError($this->__('External RMA ID isn\'t specified'));
        }

        $this->_redirect('manager/index/subcustomerrma');
        return null;
    }

    public function schduviewAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    private function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function subcustomeraccountAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAccountAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/index/customerorders/');
        }
        $customerId = null;

        if ($this->getRequest()->isPost()) {
            $cID = intval($this->getRequest()->getParam('id'));
            $firstName = $this->getRequest()->getParam('firstname');
            $lastName = $this->getRequest()->getParam('lastname');
            $email = $this->getRequest()->getParam('email');
            $isActive = $this->getRequest()->getParam('is_active');
            $positionType = $this->getRequest()->getParam('position_type');

            $reservedPrices = ($this->getRequest()->getParam('reserved_prices') == 'on') ? true : false;
            $helpDesk = ($this->getRequest()->getParam('help_desk') == 'on') ? true : false;
            $ordersRMA = ($this->getRequest()->getParam('orders_rma') == 'on') ? true : false;
            $followupEmail = ($this->getRequest()->getParam('followup_email') == 'on') ? true : false;
            $isAdmin = ($this->getRequest()->getParam('is_admin') == 'on') ? true : false;

            if($isAdmin)
            {
                $reservedPrices = true;
                $helpDesk = true;
                $ordersRMA = true;
                $followupEmail = true;
            }
            //grid
            $gridCustomer = Mage::getModel('manager/grid')->getCollection()
                                                           ->addFieldToFilter('id', array("eq" => $cID))
                                                           ->getLastItem();

            //GridVar Customer Rules
            $data = array(
                'subaccount_position' => $positionType,
                'enable_orders' => $reservedPrices,
                'enable_aw_hdu' => $helpDesk,
                'enable_aw_rma' => $ordersRMA,
                'enable_aw_followup' => $followupEmail,
                'enable_admin' => $isAdmin,
            );
            $model = Mage::getModel('manager/gridvar')->load($cID)->addData($data);
            try {
                $model->setManagerId($cID)->save();

            } catch (Exception $e){
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customerId = intval($gridCustomer->getManagerId());
            $customer = Mage::getModel('customer/customer')->load(intval($gridCustomer->getManagerId()));

            $resourceWrite = Mage::getSingleton('core/resource')->getConnection('core_write');
            $resourceTableName = $resourceWrite->getTableName('customer_entity');
            $resourceWrite->query("UPDATE $resourceTableName SET is_active = '$isActive' WHERE entity_id = {$gridCustomer->getManagerId()};");

            if(isset($firstName) && !empty($firstName))
            {
                $customer->addData(array(
                    'firstname'=>$firstName
                ));
            }
            if(isset($lastName) && !empty($lastName))
            {
                $customer->addData(array(
                    'lastname'=>$lastName
                ));
            }
            if(isset($email) && !empty($email))
            {
                $customer->addData(array(
                    'email'=>$email
                ));
            }
                // If password change was requested then add it to common validation scheme
                if ($this->getRequest()->getParam('change_password')) {
                    $currPass   = $this->getRequest()->getPost('current_password');
                    $newPass    = $this->getRequest()->getPost('password');
                    $confPass   = $this->getRequest()->getPost('confirmation');

                    $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
                    if (Mage::helper('core/string')->strpos($oldPass, ':')) {
                        list($_salt, $salt) = explode(':', $oldPass);
                    } else {
                        $salt = false;
                    }

                    if ($customer->hashPassword($currPass, $salt) == $oldPass) {
                        if (strlen($newPass)) {
                            /**
                             * Set entered password and its confirmation - they
                             * will be validated later to match each other and be of right length
                             */
                            $customer->setPassword($newPass);
                            $customer->setConfirmation($confPass);
                        } else {
                            Mage::getSingleton('core/session')->addError($this->__('New password field cannot be empty.'));
                        }
                    } else {
                        Mage::getSingleton('core/session')->addError($this->__('Invalid current password'));
                    }
                }

            try {
                $customer->setConfirmation(null);
                $customer->save();

                Mage::getSingleton('core/session')->addSuccess($this->__('Customer information succesfully updated'));
                session_write_close();
                $this->_redirect('*/index/subcustomeraccount/',array('id'=>$customerId));

            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($this->__('Cannot save the customer.'));
            }
        }

        $this->_redirect('*/index/customerorders/');
    }
}