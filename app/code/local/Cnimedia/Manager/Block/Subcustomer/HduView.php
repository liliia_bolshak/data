<?php

class Cnimedia_Manager_Block_Subcustomer_HduView extends Mage_Core_Block_Template {

    const TEMPLATE_PATH = "manager/hdu/view.phtml";

    public function __construct()
    {
        $this->setTemplate(self::TEMPLATE_PATH);
        $ticketModel = Mage::getModel('helpdeskultimate/ticket');
        if ($this->getRequest()->getParam('id', false)) {
            $ticketModel->load($this->getRequest()->getParam('id', false));
        }
        $this->setTicket($ticketModel);
        $this->_customer = Mage::getModel('customer/customer')->load($this->getRequest()->getParam('account', false));
    }

    public function dateFormat($date)
    {
        $_dateFormat = ""
            . $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            . " "
            . $this->formatTime($date, Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        return $_dateFormat;
    }

    public function getOrder()
    {
        if (!is_null($this->getTicket()->getOrder()->getId())) {
            return $this->getTicket()->getOrder();
        }
        return null;
    }

    /*getting data from ticket as block. Ex: $block->getOrder() ~ $this->getTicket()->getOrder()*/
    public function getData($key = '', $index = null)
    {
        $_indexes = array(
            'title', 'uid',
            'status_text',
            'department',
            'created_time',
            'filename',
            'file_url',
            'customer_name',
            'order',
        );
        if (in_array($key, $_indexes)) {
            $method = 'get' . $this->_camelize($key, '');
            return call_user_func(array($this->getTicket(), $method));
        }
        return parent::getData($key, $index);
    }

    public function getContent()
    {
        $cnt = $this->getTicket()->getContent();
        $cnt = Mage::getModel('helpdeskultimate/data_parser')->setText($cnt)->prepareToDisplay()->getText();
        return $cnt;
    }

    public function getCustomer()
    {
        return $this->_customer;
    }

    public function getTicketId()
    {
        return $this->getTicket()->getHelpdeskMessageId();
    }

    public function getTicketTitle()
    {
        return $this->getTicket()->getTitle();
    }

    public function getBackUrl()
    {
        return Mage::getUrl('manager/index/customerorders/');
    }
}