<?php

class Cnimedia_Manager_Block_Subcustomer_HduList extends Mage_Core_Block_Template {
    protected $_collection;

    public function count()
    {
        return $this->_collection->getSize();
    }

    protected function _construct()
    {
        $subaccount = $this->getRequest()->getParam('id');
        $this->_collection = Mage::getModel('helpdeskultimate/ticket')->getCollection();
        $this->_collection
            ->addCustomerFilter($subaccount);
    }

    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    protected function _prepareLayout()
    {
        $toolbar = $this->getLayout()->createBlock('page/html_pager', 'customer_review_list.toolbar')
            ->setCollection($this->_getCollection());

        $this->setChild('toolbar', $toolbar);
        return parent::_prepareLayout();
    }

    protected function _getCollection()
    {
        return $this->_collection;
    }

    public function getCollection()
    {
        return $this->_getCollection();
    }

    public function getTicketLink($subaccount, $id = null)
    {
        $args = is_null($id) ? array() : array('account' => $subaccount,'id' => $id);
        return Mage::getUrl('manager/index/schduview', $args);
    }

    public function dateFormat($date)
    {
        return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    protected function _beforeToHtml()
    {
        $this->_getCollection()
            ->orderBy('created_time DESC')
            ->load();
        return parent::_beforeToHtml();
    }
}