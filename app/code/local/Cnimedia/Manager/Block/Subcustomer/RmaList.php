<?php

class Cnimedia_Manager_Block_Subcustomer_RmaList extends Mage_Core_Block_Template {

    public function __construct() {
        parent::__construct();
        $_template = 'manager/subcustomerrma.phtml';
        $this->setTemplate($_template);
        return $this;
    }

    /**
     * Collection of RMA entities
     * @var AW_Rma_Model_Mysql4_Entity_Collection
     */
    private $_rmaEntitiesCollection = null;

    /**
     * Returns RMA entities collection with some filters. Filtered by current
     * user id or email, for sample.
     * @return AW_Rma_Model_Mysql4_Entity_Collection
     */
    public function getRmaEntitiesCollection() {
        $subCustomerId = $this->getRequest()->getParam('id');

        if ($this->_rmaEntitiesCollection instanceof AW_Rma_Model_Mysql4_Entity_Collection)
            return $this->_rmaEntitiesCollection;

        $this->_rmaEntitiesCollection = Mage::getModel('awrma/entity')->getCollection()
            ->setCustomerFilter($subCustomerId)
            ->joinStatusNames()
            ->setOrder('created_at', 'DESC');

        return $this->_rmaEntitiesCollection;
    }

    public function setRmaEntitiesCollection(AW_Rma_Model_Mysql4_Entity_Collection $collection) {
        $this->_rmaEntitiesCollection = $collection;

        return $this;
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'awrma.entity.list.pager')
            ->setCollection($this->getRmaEntitiesCollection());
        $this->setChild('pager', $pager);
        $this->getRmaEntitiesCollection()->load();

        return $this;
    }

}