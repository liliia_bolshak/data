<?php

class Cnimedia_Manager_Block_Adminhtml_Manager_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('manager_form', array('legend' => Mage::helper('manager')->__('Manager information')));

        $collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*')->setOrder('id');

        $result = array();
        $vCount = 0;

        $id = $this->getRequest()->getParam('manager_id');

        foreach ($collection as $customer) {
            $result[] = $customer->toArray();
            $values[$vCount]['label'] = ($result[$vCount]['lastname']) ? $result[$vCount]['firstname'] . " " . $result[$vCount]['lastname'] : $result[$vCount]['email'];
            $values[$vCount]['value'] = $customer->getId();
            if ($id == $customer->getId()) {
                if ($vCount == 0) {
                    $selMe = 0;
                } else {
                    $selMe = $vCount;
                }
                ?>

                <script language="javascript" type="text/javascript">
                    function getParameterByName(name) {
                        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                        var regexS = "[\\?&]" + name + "=([^&#]*)";
                        var regex = new RegExp(regexS);
                        var results = regex.exec(window.location.href);
                        if (results == null)
                            return "";
                        else
                            return decodeURIComponent(results[1].replace(/\+/g, " "));
                    }
                    window.onload = function (e) {


                        var eform = document.getElementById('edit_form');
                        var esel = document.getElementById('mid');
                        esel.options[<?php echo $selMe; ?>].selected = true;


                    }


                </script>

            <?php

            }

            $vCount++;

        }

        $fieldset->addField('mid', 'select', array(
            'label' => Mage::helper('manager')->__('Manager'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'mid',
            'values' => $values
        ));

        $fieldset->addField('title', 'hidden', array(
            'label' => 'title',
            'name' => 'title',
            'value' => 'Manager',
        ));


        if (Mage::getSingleton('adminhtml/session')->getManagerData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getManagerData());
            Mage::getSingleton('adminhtml/session')->setManagerData(null);
        } elseif (Mage::registry('manager_data')) {
            $form->setValues(Mage::registry('manager_data')->getData());
        }
        return parent::_prepareForm();
    }
}