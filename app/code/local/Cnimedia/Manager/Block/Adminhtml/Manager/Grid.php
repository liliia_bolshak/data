<?php

class Cnimedia_Manager_Block_Adminhtml_Manager_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('managerGrid');
      $this->setDefaultSort('manager_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getResourceModel('customer/customer_collection');
      $resource = Mage::getSingleton('core/resource');
      $collection->getSelect()
          ->joinRight(
              array('mg'=>$resource->getTableName('manager/grid')),
              'mg.manager_id = e.entity_id'
          )
          ->group('mg.manager_id');
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('manager_id', array(
          'header'    => Mage::helper('manager')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'manager_id',
      ));

      $this->addColumn('email', array(
          'header'    => Mage::helper('manager')->__('Manager'),
          'align'     =>'left',
          'index'     => 'email',
      ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('manager')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('manager')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'manager_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('manager_id');
        $this->getMassactionBlock()->setFormFieldName('manager');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('manager')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('manager')->__('Are you sure?')
        ));

        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('manager_id' => $row->getManagerId()));
  }

}