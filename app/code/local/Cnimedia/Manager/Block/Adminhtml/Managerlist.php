<?php
class Cnimedia_Manager_Block_Adminhtml_Managerlist extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_managerlist';
        $this->_blockGroup = 'manager';
        $this->_headerText = Mage::helper('manager')->__('Customer Manager List');
        $this->_addButtonLabel = Mage::helper('manager')->__('Add Customer');
        parent::__construct();
    }


}