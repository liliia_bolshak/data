<?php

class Cnimedia_Manager_Block_Adminhtml_Customer_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $managerID = $this->getRequest()->getParam('manager_id');
        $customerID = $this->getRequest()->getParam('customer_id');
        $customerRightsIDCollection = Mage::getModel('manager/grid')->getCollection()
            ->addFieldToFilter('customer_id', $customerID)
            ->addFieldToFilter('manager_id', array("eq" => $managerID))
            ->load()->getFirstItem();
        $customerRightsID = $customerRightsIDCollection->getId();

        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('customerrights_id' => $customerRightsID)),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        $customerRights = Mage::getModel('manager/gridvar')->getCollection()
            ->addFieldToFilter('manager_id', intval($customerRightsID))
            ->getFirstItem();

        $isEnableOrders = ($customerRights->getEnableOrders()) ? true : false;
        $isEnableAwHdu = ($customerRights->getEnableAwHdu()) ? true : false;
        $isEnableAwRma = ($customerRights->getEnableAwRma()) ? true : false;
        $isEnableAwFollowUp = ($customerRights->getEnableAwFollowup()) ? true : false;
        $isEnableAdmin = ($customerRights->getEnableAdmin()) ? true : false;
        $subaccountPosition = $customerRights->getSubaccountPosition();

        if($isEnableAdmin)
        {
            $isEnableOrders = $isEnableAwHdu = $isEnableAwRma = $isEnableAwFollowUp = true;
        }

        # add a fieldset, this returns a Varien_Data_Form_Element_Fieldset object
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => Mage::helper('manager')->__('General Information'),
            )
        );
        # now add fields on to the fieldset object, for more detailed info
        # see https://makandracards.com/magento/12737-admin-form-field-types

        $positionTypes = Mage::getModel('eav/entity_attribute')->getCollection()->addFieldToFilter('attribute_code', array('like' => 'ref_%'));
        $positionsArray = array();
        $positionsArray['-1'] = "Please Select..";
        foreach($positionTypes as $key => $attribute)
        {
            $positionsArray[$attribute->getAttributeId()] = $attribute->getFrontendLabel();
        }
        $fieldset->addField('customer_rights_id', 'hidden', array(
            'label' => Mage::helper('manager')->__('Customer Rights'),
            'required' => false,
            'name' => 'customer_rights_id',
            'value' => $customerRightsID
        ));

        $fieldset->addField('position_type', 'select', array(
            'label'     => Mage::helper('manager')->__('Position'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'position_type',
            'onclick' => "",
            'onchange' => "",
            "value" => $subaccountPosition,
            'values' => $positionsArray,
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));

        $fieldset->addField('reserved_prices', 'checkbox', array(
            'label'     => Mage::helper('manager')->__('Orders'),
            'name'      => 'reserved_prices',
            'class'     => 'rulles',
            'checked' => $isEnableOrders,
            'onclick' => "",
            'onchange' => "",
            'value'  => '1',
            'disabled' => false,
            'after_element_html' => '<small>(Check it to see reserved prices)</small>',
            'tabindex' => 1
        ));
        $fieldset->addField('help_desk', 'checkbox', array(
            'label'     => Mage::helper('manager')->__('Help Desk'),
            'name'      => 'help_desk',
            'class'     => 'rulles',
            'checked' => $isEnableAwHdu,
            'onclick' => "",
            'onchange' => "",
            'disabled' => false,
            'value'  => '1',
            'after_element_html' => '<small>(Check it to see/manage Help Desk)</small>',
            'tabindex' => 1
        ));
        $fieldset->addField('orders_rma', 'checkbox', array(
            'label'     => Mage::helper('manager')->__('Orders RMA'),
            'name'      => 'orders_rma',
            'class'     => 'rulles',
            'checked' => $isEnableAwRma,
            'onclick' => "",
            'onchange' => "",
            'value'  => '1',
            'disabled' => false,
            'after_element_html' => '<small>(Check it to see/manage Orders RMA)</small>',
            'tabindex' => 1
        ));
        $fieldset->addField('followup_email', 'checkbox', array(
            'label'     => Mage::helper('manager')->__('FollowUp emails'),
            'name'      => 'followup_email',
            'class'     => 'rulles',
            'checked' => $isEnableAwFollowUp,
            'onclick' => "",
            'onchange' => "",
            'value'  => '1',
            'disabled' => false,
            'after_element_html' => '<small>(Check it for FollowUp emails)</small>',
            'tabindex' => 1
        ));

        $fieldset->addField('is_admin', 'checkbox', array(
            'label'     => Mage::helper('manager')->__('Is Admin?'),
            'name'      => 'is_admin',
            'checked' => $isEnableAwFollowUp,
            'onclick' => "checkAll(this)",
            'onchange' => "",
            'value'  => '1',
            'disabled' => false,
            'after_element_html' => '<small>(Check it for Admin rights)</small>',
            'tabindex' => 1
        ));

        return parent::_prepareForm();
    }
}