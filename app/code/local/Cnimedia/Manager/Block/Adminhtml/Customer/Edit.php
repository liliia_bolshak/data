<?php

class Cnimedia_Manager_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'manager_id';
        $this->_blockGroup = 'manager';
        $this->_controller = 'adminhtml_customer';

        $this->_updateButton('save', 'label', Mage::helper('manager')->__('Save Customer'));
        $this->_updateButton('delete', 'label', Mage::helper('manager')->__('Delete Customer'));

        $this->_removeButton('back');
        $this->_removeButton('reset');
        $this->_addButton('custom_back', array(
            'label'     => Mage::helper('adminhtml')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/adminhtml_manager/edit', array('manager_id' => $this->getRequest()->getParam('manager_id'))) . '\')',
            'class'     => 'back',
        ),-1);

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }

            function checkAll(button) {
                var form = $(button.form);
                var inputs = form.getInputs('checkbox');
                if(document.getElementById('is_admin').checked) {
                    inputs.each(function (elem) {
                        elem.checked = true;
                    });
                }
                else
                {
                    inputs.each(function (elem) {
                        elem.checked = false;
                    });
                }
    	    }
        ";
    }

    public function getHeaderText()
    {
        return Mage::helper('manager')->__('Edit Customer Rights');
    }
}