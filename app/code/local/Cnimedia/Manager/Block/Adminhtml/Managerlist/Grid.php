<?php

class Cnimedia_Manager_Block_Adminhtml_Managerlist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('managerlistGrid');
        $this->setUseAjax(true); // Using ajax grid is important
        $this->setDefaultSort('entity_id');
        $this->setDefaultFilter(array('in_managerlist'=>1)); // By default we have added a filter for the rows, that in_managerlist value to be 1
        $this->setSaveParametersInSession(false);  //Dont save paramters in session or else it creates problems
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('customer/customer_collection');

       /* $tm_id = $this->getRequest()->getParam('manager_id');
        if(!isset($tm_id)) {
            $tm_id = 0;
        }
        Mage::getResourceModel('manager/grid')->addGridPosition($collection,$tm_id);*/

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_managerlist', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'customer',
            'values'            => $this->_getSelectedCustomers(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('customer')->__('ID'),
            'width'     => '50px',
            'index'     => 'entity_id',
            'type'  => 'number',
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('customer')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('email', array(
            'header'    => Mage::helper('customer')->__('Email'),
            'width'     => '150',
            'index'     => 'email'
        ));

        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  Mage::helper('customer')->__('Group'),
            'width'     =>  '100',
            'index'     =>  'group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));

        $this->addColumn('Telephone', array(
            'header'    => Mage::helper('customer')->__('Telephone'),
            'width'     => '100',
            'index'     => 'billing_telephone'
        ));

        $this->addColumn('billing_country_id', array(
            'header'    => Mage::helper('customer')->__('Country'),
            'width'     => '100',
            'type'      => 'country',
            'index'     => 'billing_country_id',
        ));

        $this->addColumn('billing_region', array(
            'header'    => Mage::helper('customer')->__('State/Province'),
            'width'     => '100',
            'index'     => 'billing_region',
        ));

        $this->addColumn('position', array(
            'header'            => Mage::helper('catalog')->__('Position'),
            'name'              => 'position',
            'width'             => 60,
            'type'              => 'number',
            'validate_class'    => 'validate-number',
            'index'             => 'position',
            'editable'          => true,
            'edit_only'         => true
        ));

        return parent::_prepareColumns();
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_managerlist') {
            $ids = $this->_getSelectedCustomers();
            if (empty($ids)) {
                $ids = 0;
            }
            if ($column->getFilter()->getValue() && $ids!=0) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$ids));
            } else {
                if($ids) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$ids));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/adminhtml_managerlist/managerlistgrid', array('_current'=>true));
    }

    protected function _getSelectedCustomers()
    {
        $customers = array_keys($this->getSelectedCustomers());
        return $customers;
    }

    public function getSelectedCustomers()
    {
        // Customer Data
        $tm_id = $this->getRequest()->getParam('manager_id');
        if(!isset($tm_id)) {
            $tm_id = 0;
        }
        $collection = Mage::getModel('manager/grid')->getCollection()->addFieldToFilter('manager_id',$tm_id);
        $custIds = array();
        foreach($collection as $obj){
            $custIds[$tm_id] = array('position'=>$obj->getPosition());
        }
        return $custIds;
    }
}