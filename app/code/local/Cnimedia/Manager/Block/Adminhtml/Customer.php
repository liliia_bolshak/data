<?php
class Cnimedia_Manager_Block_Adminhtml_Customer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_customer';
        $this->_blockGroup = 'manager';
        $this->_headerText = Mage::helper('manager')->__('Customer Rights');
        $this->_addButtonLabel = Mage::helper('manager')->__('Customer Rights');

        parent::__construct();
    }
}