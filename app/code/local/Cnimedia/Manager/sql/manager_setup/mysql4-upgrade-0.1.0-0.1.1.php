<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('grid_manager_var')};
CREATE TABLE {$this->getTable('grid_manager_var')} (
  `manager_id` int(11) unsigned NOT NULL,
  `subaccount_position` int(11) unsigned NOT NULL DEFAULT 0,
  `enable_orders` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `enable_aw_hdu` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `enable_aw_rma` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `enable_aw_followup` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `enable_admin` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`manager_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
");

$installer->endSetup();