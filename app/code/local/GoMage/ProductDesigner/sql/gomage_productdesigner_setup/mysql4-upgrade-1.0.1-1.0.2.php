<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

$installer = $this;
/* @var $installer GoMage_ProductDesigner_Model_Resource_Setup */

$installer->startSetup();

try {

    $table = $installer->getConnection()->newTable($installer->getTable('gomage_designer/uploadedFile'))
        ->addColumn('file_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 12, array(
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true,
            ), "File Id"
        )
        ->addColumn('file', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'File')
        ->addColumn('session_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(), 'Session Id');
    $installer->getConnection()->createTable($table);

    $installer->addAutoIncrement($installer->getTable('gomage_designer/uploadedFile'), 'file_id');

    $installer->endSetup();
} catch (Exception $e) {
    Mage::logException($e);
} catch (Mage_Core_Exception $e) {
    Mage::logException($e);
}