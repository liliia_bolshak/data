<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options_Scratch extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = '0';

    protected $prices = array(
        '100'   => 0.024,
        '250'   => 0.0098,
        '500'   => 0.0098,
        '1000'  => 0.0098,
        '2000'  => 0.0098,
        '3000'  => 0.0098,
        '4000'  => 0.0098,
        '5000'  => 0.0098,
        '8000'  => 0.0098,
        '10000' => 0.0098,
        '20000' => 0.0098,
        '50000' => 0.0098,
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No'),
            '1'                 => $helper->__('Yes'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Scratch off pannel');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'scratch';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                '1' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/scratch.png',
            )
        );
    }


}