<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Numbering extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        'black' => array(
            '100'   => 0.049,
            '250'   => 0.020,
            '500'   => 0.010,
            '1000'  => 0.005,
            '2000'  => 0.005,
            '3000'  => 0.005,
            '4000'  => 0.005,
            '5000'  => 0.005,
            '8000'  => 0.005,
            '10000' => 0.005,
            '20000' => 0.005,
            '50000' => 0.005,
        ),
        'other' => array(
            '100'   => 0.110,
            '250'   => 0.044,
            '500'   => 0.022,
            '1000'  => 0.011,
            '2000'  => 0.011,
            '3000'  => 0.011,
            '4000'  => 0.011,
            '5000'  => 0.011,
            '8000'  => 0.011,
            '10000' => 0.011,
            '20000' => 0.011,
            '50000' => 0.011,
        )
    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No Numering'),
            'black'             => 'Black Color',
            'other'             => 'Other Colors',
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Numering or data from database');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[$this->getValue()][GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'numbering';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'text',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                'black' => 'Numering',
                'other' => 'Numering',
            )
        );
    }

}