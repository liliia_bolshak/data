<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Barcode extends GoMage_ProductDesigner_Model_Options_Abstract
{
    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        '100'   => 0.061,
        '250'   => 0.024,
        '500'   => 0.012,
        '1000'  => 0.006,
        '2000'  => 0.006,
        '3000'  => 0.006,
        '4000'  => 0.006,
        '5000'  => 0.006,
        '8000'  => 0.006,
        '10000' => 0.006,
        '20000' => 0.006,
        '50000' => 0.006,

    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No barcode'),
            'codabar'           => $helper->__('codabar'),
            'code11'            => $helper->__('code 11'),
            'code39'            => $helper->__('code 39'),
            'code93'            => $helper->__('code 93'),
            'code128'           => $helper->__('code 128'),
            'ean8'              => $helper->__('ean 8'),
            'ean13'             => $helper->__('ean 13'),
            'std25'             => $helper->__('standard 2 of 5 - industrial 2 of 5'),
            'int25'             => $helper->__('interleaved 2 of 5'),
            'msi'               => $helper->__('msi'),
            'datamatrix'        => $helper->__('ASCII + extended'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Barcode');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'barcode';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'image',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                'codabar'    => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'code11'     => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'code39'     => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'code93'     => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'code128'    => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'ean8'       => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'ean13'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'std25'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'int25'      => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'msi'        => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
                'datamatrix' => Mage::getBaseUrl('js') . 'custom/gomage/productdesigner/images/barcode.jpg',
            )
        );
    }

}