<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

abstract class GoMage_ProductDesigner_Model_Options_Silkbackground extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'no';

    protected $prices = array(
        '100'   => 0.037,
        '250'   => 0.015,
        '500'   => 0.007,
        '1000'  => 0.004,
        '2000'  => 0.004,
        '3000'  => 0.004,
        '4000'  => 0.004,
        '5000'  => 0.004,
        '8000'  => 0.004,
        '10000' => 0.004,
        '20000' => 0.004,
        '50000' => 0.004,

    );

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('No Silk Background'),
            '#999999'           => 'Gray',
            '#9C5C2F'           => 'Chocolate',
            '#E6E6E6'           => 'LightGray',
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Silk background');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        if ($this->getValue() == self::DEFAULT_VALUE) {
            return 0;
        }
        return $this->prices[GoMage_ProductDesigner_Model_Options::getQtyKey($qty)];
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'silkbackground';
    }

    public function getDesignConfig()
    {
        return array(
            'key'           => $this->getKey(),
            'type'          => 'background',
            'default_value' => self::DEFAULT_VALUE,
            'values'        => array(
                self::DEFAULT_VALUE => '#FFF',
                '#999999'           => '#999999',
                '#9C5C2F'           => '#9C5C2F',
                '#E6E6E6'           => '#E6E6E6',
            ),
            'position'      => array(
                'left' => 450,
                'top'  => 180,
            )
        );
    }

}