<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options_Lamination extends GoMage_ProductDesigner_Model_Options_Abstract
{

    CONST DEFAULT_VALUE = 'glossy';

    protected function _construct()
    {
        $this->setValue(self::DEFAULT_VALUE);
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        $helper = Mage::helper('gomage_designer');
        return array(
            self::DEFAULT_VALUE => $helper->__('Glossy'),
            'matt'              => $helper->__('Matt'),
        );
    }

    public function getLabel()
    {
        $helper = Mage::helper('gomage_designer');
        return $helper->__('Lamination (finishing)');
    }

    /**
     * @param  int $qty
     * @return float
     */
    public function getPrice($qty = 1)
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'lamination';
    }
}