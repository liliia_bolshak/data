<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Options extends Varien_Object
{

    protected $base_price = array(
        '100'   => 0.122,
        '250'   => 0.122,
        '500'   => 0.098,
        '1000'  => 0.085,
        '2000'  => 0.085,
        '3000'  => 0.073,
        '4000'  => 0.073,
        '5000'  => 0.067,
        '8000'  => 0.067,
        '10000' => 0.067,
        '20000' => 0.067,
        '50000' => 0.061,
    );

    protected $base_price_two_side = array(
        '100'   => 1.183,
        '250'   => 0.473,
        '500'   => 0.237,
        '1000'  => 0.118,
        '2000'  => 0.088,
        '3000'  => 0.074,
        '4000'  => 0.067,
        '5000'  => 0.057,
        '8000'  => 0.050,
        '10000' => 0.041,
        '20000' => 0.040,
        '50000' => 0.038,
    );

    protected $customer_partner_groups = array(3, 4);

    public function getOption($key)
    {
        return Mage::getModel('gomage_designer/options_' . $key);
    }

    public function getPrice($design, $qty = 1)
    {
        $options = json_decode($design->getOptions());
        return $this->getOptionsPrice($options, $qty);
    }

    public function getOptionsPrice($options, $qty = 1)
    {
        $price = $this->base_price[$this->getQtyKey($qty)] +
            $this->base_price_two_side[$this->getQtyKey($qty)];
        foreach ($options as $key => $value) {
            $option = $this->getOption($key)->setValue($value);
            $price += $option->getPrice($qty);
        }
        if (in_array($this->getCustomerGroup(), $this->customer_partner_groups)) {
            $price = $price * 1.4;
        } else {
            $price = $price * 1.6;
        }

        return $price;
    }

    protected function getCustomerGroup()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getSingleton('customer/session')->getCustomerGroupId();
        }

        return 0;
    }

    public static function getQtyKey($qty)
    {
        $qty = intval($qty);
        if ($qty <= 100) {
            return '100';
        } elseif ($qty <= 250) {
            return '250';
        } elseif ($qty <= 500) {
            return '500';
        } elseif ($qty <= 1000) {
            return '1000';
        } elseif ($qty <= 2000) {
            return '2000';
        } elseif ($qty <= 3000) {
            return '3000';
        } elseif ($qty <= 4000) {
            return '4000';
        } elseif ($qty <= 5000) {
            return '5000';
        } elseif ($qty <= 8000) {
            return '8000';
        } elseif ($qty <= 10000) {
            return '10000';
        } elseif ($qty <= 20000) {
            return '20000';
        } else {
            return '50000';
        }
    }
}