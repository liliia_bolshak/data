<?php
/**
 * GoMage Product Designer Extension
 *
 * @category     Extension
 * @copyright    Copyright (c) 2013 GoMage (http://www.gomage.com)
 * @author       GoMage
 * @license      http://www.gomage.com/license-agreement/  Single domain license
 * @terms of use http://www.gomage.com/terms-of-use/
 * @version      Release: 1.0.0
 * @since        Available since Release 1.0.0
 */

class GoMage_ProductDesigner_Model_Mysql4_UploadedFile extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('gomage_designer/uploadedFile', 'file_id');
    }

    public function removeFilesByIds($ids = array())
    {
        $ids = implode(', ', $ids);
        $this->_getWriteAdapter()->delete($this->getMainTable(), "file_id IN ({$ids})");
        return $this;
    }
}

