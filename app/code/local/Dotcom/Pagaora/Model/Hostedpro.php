<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Website Payments Pro Hosted Solution payment gateway model
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Dotcom_Pagaora_Model_Hostedpro extends Mage_Paypal_Model_Hostedpro
{

    /**
     * Instantiate state and set it to state object
     *
     * @param string $paymentActiosn
     * @param Varien_Object $stateObject
     * @param Varien_Object $stateObject
     * @param Varien_Object $order
     */
    public function initialize2($order)
    {
    	$request = $this->_buildBasicRequest()
            ->setOrder($order)
            ->setPaymentMethod($this)
    	    ->setReturn($this->getReturnUrl());
        /* @var $response string Contiene l'URL da usare come proprietà src dell'Iframe di PayPal Pro */
    	Mage::getSingleton("checkout/session")->setIsPagaOra(true);
        $response = $this->_sendFormUrlRequest($request);
        //echo $response;
        return $response;
    }
}
