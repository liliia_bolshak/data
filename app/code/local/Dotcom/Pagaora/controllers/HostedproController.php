<?php
require_once(Mage::getModuleDir('controllers','Mage_Paypal').DS.'HostedproController.php');

class Dotcom_Pagaora_HostedproController extends Mage_Paypal_HostedproController
{
    public function cancelAction()
    {
        if(Mage::getSingleton("checkout/session")->getIsPagaOra()){
            Mage::getSingleton("checkout/session")->unsIsPagaOra();
            return;
        }
        else {
            parent::cancelAction();
        }
    }

}
