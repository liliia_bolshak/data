<?php
/**
 * @category   ASPerience
 * @package    Asperience_DeleteAllOrders
 * @author     ASPerience - www.asperience.fr
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Dotcom_DeleteOrders_Model_Order extends Mage_Sales_Model_Order
{
    public function canDelete()
    {
        if (Mage::getStoreConfig('admin/delete_orders/use_state_filters'))
        {
            if ($this->getState() === self::STATE_CANCELED || $this->getState() === self::STATE_COMPLETE || $this->getState() === self::STATE_CLOSED) {
            	return true;
            }
            return false;
        }
        else
        {
            return true;
        }
    }
}
