<?php
class Dotcom_DeleteOrders_Model_Observer
{
    public function addDeleteOrdersMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction' && $block->getRequest()->getControllerName() == 'sales_order')
        {
            $block->addItem('dotcom_deleteorders', array(
                'label' => 'Delete orders',
                'url' => Mage::app()->getStore()->getUrl('deleteorders'),
                'confirm' => Mage::helper('dotcom_deleteorders')->__('Are you sure?')
            ));
        }
    }
}