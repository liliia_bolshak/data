<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer account controller
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once(Mage::getModuleDir('controllers','Mage_Customer').DS.'AccountController.php');

class Dotcom_Customer_AccountController extends Mage_Customer_AccountController
{

    /**
     * Forgot customer password action
     */
    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        $piva = (string) $this->getRequest()->getPost('piva');
        
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                    $this->_getSession()
                    ->addSuccess(Mage::helper('customer')->__('Abbiamo inviato una mail a %s per reimpostare la password.', Mage::helper('customer')->htmlEscape($email)));
                    
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            else {
                $this->_getSession()
                ->addNotice(Mage::helper('customer')->__('Nei nostri archivi non è presente la mail %s, verifica se ti sei registrato con un altra email oppure contattaci tramite il <a href="http://www.infordatadealers.com/contacts/">form</a> o telefona allo 040-367189.', Mage::helper('customer')->htmlEscape($email)));
            }
            
            $this->_redirect('*/*/');
            return;
        }
        
        if ($piva) {
            //print_r(Mage::getModel('customer/customer')->getCollection()->addAttributeToFilter("taxvat",array("eq"=>$piva))->getSelect()->__toString()); die();
            $customer = Mage::getModel('customer/customer')->getCollection()->addAttributeToFilter("taxvat",array("eq"=>$piva));
            if(count($customer) == 1){
                $registeredAzienda = $customer->getFirstItem();
                try {
                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                    $registeredAzienda->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $registeredAzienda->sendPasswordResetConfirmationEmail();
                    $aziendaMail = preg_replace("/(.*)@(.*)\.(.*)/", "$1@xxxxxx.$3", Mage::helper('customer')->htmlEscape($registeredAzienda->getEmail()));
                    $this->_getSession()
                    ->addSuccess(Mage::helper('customer')->__('Abbiamo inviato una mail a %s per reimpostare la password.', $aziendaMail));
                
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            else {
                $this->_getSession()
                ->addNotice(Mage::helper('customer')->__('Siamo spiacenti, la P.IVA indicata non risulta nei nostri archivi, nel caso hai bisogno di aiuto contattaci tramite il <a href="http://www.infordatadealers.com/contacts/">form</a> o telefona allo 040-367189.', Mage::helper('customer')->htmlEscape($email)));
            }
            $this->_redirect('*/*/');
            return;
        }
    }
}
