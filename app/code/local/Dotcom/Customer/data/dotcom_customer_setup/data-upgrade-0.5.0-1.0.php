<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

/**
 * Fill table ariete_codpag
 */
$data = array(
    array("3BON", "B", "Bonifico bancario 30/60/90gg d.f. f.m."),
    array("A30", "N", "Assegno postale 30gg data fatt."),
    array("A60", "N", "assegno 60gg f.m. d.f."),
    array("AB30", "H", "30% anticipato, saldo 30gg d.f.f.m."),
    array("ADD", "D", "RID - Addebito S.B.F. su c/c"),
    array("ADV", "R", "Anticipato-In advance"),
    array("ANDD", "R", "Anticipato 30% - Saldo RiBa 60gg d.f.f.m."),
    array("ANT", "N", "30% in advance. Saldo before delivery."),
    array("ANT0", "H", "anticipato"),
    array("ANT1", "H", "In anticipo - In advance"),
    array("ANT2", "H", "anticipo - avans"),
    array("ANT3", "H", "anticipo 77%, saldo a 30gg data fattura"),
    array("ANT4", "N", "50% bon. ant. + saldo alla consegna"),
    array("ANT5", "R", "50% bon. anticipato, saldo RiBa 30gg d.f. f.m."),
    array("ANT6", "N", "50% alla consegna + 50% a 30gg d.f."),
    array("ANT7", "N", "30% in advance - saldo 30 days from invoice date"),
    array("ANT8", "B", "50% bonifico anticipato e 50% 30gg d.f.f.m."),
    array("ANT9", "B", "50% bonifico ant. 50% avviso merce pronta"),
    array("ANTA", "R", "Anticipo 30% - saldo 30gg d.f.f.m."),
    array("ANTB", "R", "50% bonifico anticipato - 50% 30gg d.f."),
    array("ANTC", "N", "50% alla consegna + 50% a 30gg d.f. f.m."),
    array("ANTD", "R", "20% anticipo, 40% riba 30gg fm, 40% riba 60gg fm"),
    array("ANTG", "R", "30% ordine, 40% consegna, saldo 30gg dffm"),
    array("ANTI", "R", "35% in anticipo, saldo prima della consegna"),
    array("ANTO", "R", "50% bonifico anticipato 50% 60 gg. d.f.f.m."),
    array("ANTX", "R", "50% anticipo-50%c/assegno"),
    array("ANTZ", "R", "Anticipato 10% - Saldo alla consegna"),
    array("AP60", "N", "Assegno postale 60gg d.f."),
    array("ASC", "R", "Assegno alla consegna"),
    array("ASCI", "N", "Assegno circ. alla consegna"),
    array("B301", "B", "Bonifico bancario 30gg d.f. f.m. + 10gg"),
    array("B30P", "B", "ELIMINATO - Bonifico 30 GG FM"),
    array("BAN", "D", "bancomat aziendale"),
    array("BFM", "B", "Bonifico f.m."),
    array("BO00", "B", "Bonifico 30gg fine mese"),
    array("BO01", "B", "Bonifico 30 gg. d.f."),
    array("BO02", "B", "Bonifico 60 gg. d.f."),
    array("BO03", "B", "Bonifico 90 gg. d.f."),
    array("BO04", "B", "Bonifico 30gg d.f.f.m."),
    array("BO05", "B", "Bonifico ban. anticipato"),
    array("BO06", "N", "45 gg data fattura - 45 dni od datuma fakture"),
    array("BO07", "B", "30gg data fattura"),
    array("BO09", "B", "Bonifico 60 gg d.f.f.m."),
    array("BO10", "N", "30gg data fattura - 30 dana od datuma racuna"),
    array("BO11", "B", "30 gg d.f. f.m."),
    array("BO12", "B", "Bonifico bancario a 15gg d.f."),
    array("BO13", "B", "Bonifico a 30gg f.m. d.f. e 60gg f.m. d.f."),
    array("BO14", "B", "Imponibile bonifico antic., saldo contrassegno"),
    array("BO15", "B", "Bonifico 60gg f.m. d.f. - non revocabile"),
    array("BO16", "N", "RD 50% importo 60gg/RD saldo 90gg f.m. d.f."),
    array("BO17", "N", "30gg data fattura- 30 days from invoice date"),
    array("BO18", "N", "30% ob narocilu, 40% ob obvestilu posiljke, 30% df"),
    array("BO19", "B", "ELIMINATO - Bonifico banc. 90gg d.f.f.m."),
    array("BO20", "B", "Bonifico 60gg d.f.-Bank transf. 60 days inv.date"),
    array("BO21", "B", "Bonifico bancario 60gg d.f. f.m. + 10gg"),
    array("BO22", "B", "30% in anticipo - 70% alla consegna"),
    array("BO30", "R", "30% anticipato - 30% 30ggdffm - 40% 60ggdffm"),
    array("BO36", "B", "Bonifico a 30/60gg f.m. d.f. + 10 gg"),
    array("BO45", "B", "45gg data fattura"),
    array("BO60", "B", "Bank transfer 60 days after invoice date"),
    array("BON1", "B", "Bonifico 30gg data ric. fatt."),
    array("BON2", "B", "Bonifico bancario vista fatt."),
    array("BON9", "B", "Bonifico 90gg data fattura"),
    array("BONB", "R", "Bonifico bancario vista fattura"),
    array("BONC", "B", "70% anticipato, saldo (30%) 30gg ric. merce"),
    array("BONE", "B", "30gg data fattura- 30 days from invoice date"),
    array("BONV", "B", "Bonifico bancario vista fatt."),
    array("BOS1", "B", "Bonifico 30 gg d.f. - 30 dni od datuma racuna"),
    array("BOS2", "B", "Vista fattura - ob prejemu racuna"),
    array("BOS3", "B", "30 gg data fattura - 30 dni od datuma fakture"),
    array("BPA3", "B", "ELIMINATO - Bonifico Bancario 30gg dffm"),
    array("BPA6", "B", "ELIMINATO - Bonifico Bancario 60gg dffm"),
    array("BRD", "B", "Rimessa diretta vista fattura"),
    array("BRD0", "B", "R.D. vista fattura"),
    array("BRD1", "B", "R.D. a 60gg f.m. d.f."),
    array("BRD2", "B", "RD 30gg f.m. d.f."),
    array("BRD3", "B", "RD d.f. f.m."),
    array("BRD4", "B", "Rimessa diretta 30gg d.f. +15"),
    array("CC", "N", "Carta di credito"),
    array("CL", "N", "Irrevocable Letter of Credit at sight"),
    array("CO31", "R", "30% in advance, saldo at sight"),
    array("COM", "R", "Compensazione - Counterbalance"),
    array("CON", "S", "contrassegno"),
    array("CON0", "N", "Contanti - Cash"),
    array("CON1", "R", "anticipo 20% in contrassegno, saldo RB 30gg fm df"),
    array("CON2", "R", "50% contrassegno, 50% RB 30gg df fm"),
    array("CON3", "R", "30% bonifico anticipato, saldo contrassegno"),
    array("CONT", "N", "Contanti alla cons.-Cash on delivery"),
    array("DFFM", "N", "data fattura f.m."),
    array("FM", "R", "Fine mese d.f.-At the end of month Inv. date"),
    array("IMP", "S", "imponibile anticipato, IVA contrassegno"),
    array("INC", "P", "Già incassato - Already paid"),
    array("INC1", "P", "Già incassato - ze placano"),
    array("INC2", "P", "già incassato - unaprijed platano"),
    array("INC3", "P", "Già incassato"),
    array("IVA", "R", "IVA all'ordine, imponibile R.B. 30gg d.f."),
    array("MIX", "R", "Bonifico Ant. o Contrassegno o Carta di Credito"),
    array("P", "N", "PAGATO"),
    array("PAG", "P", "Già incassato"),
    array("PAG1", "N", "Pagamento non standard"),
    array("PAG2", "N", "30gg dall'accettazione - saldo 30 dni od prevzema"),
    array("PAG3", "P", "Già incassato"),
    array("PAG4", "N", "saldo 45gg data fatt.-saldo 45 dni od datuma fakt."),
    array("PAG6", "N", "45 days after receipt of the payment"),
    array("PAG7", "N", "60 gg data ric. fattura"),
    array("PAY", "N", "Cash at the pick-up of the goods"),
    array("RATE", "R", "Rateale 24 mesi (su bolletta)"),
    array("RB01", "R", "R.B. 30 gg. d.f."),
    array("RB02", "R", "R.B. 60 gg. d.f."),
    array("RB03", "R", "R.B. 90 gg. d.f."),
    array("RB04", "R", "R.B. 120 gg. d.f."),
    array("RB05", "R", "R.B. 30/60 gg. d.f."),
    array("RB06", "R", "R.B. 60/90 gg. d.f."),
    array("RB07", "R", "R.B. 90/120 gg. d.f."),
    array("RB08", "R", "R.B. 30/60/90 gg. d.f."),
    array("RB09", "R", "R.B. 60/90/120 gg. d.f."),
    array("RB10", "R", "R.B. 90/120/150 gg. d.f."),
    array("RB11", "R", "R.B. f.m. d.f. e 30gg f.m. d.f."),
    array("RB21", "R", "R.B. 30gg f.m. d.f."),
    array("RB22", "R", "R.B. 60 gg. f.m. d.f."),
    array("RB23", "R", "R.B. 90 gg. f.m. d.f."),
    array("RB24", "R", "R.B. 120 gg. f.m. d.f."),
    array("RB25", "R", "R.B. 50% a 30gg dffm e 50% a 60 gg.dffm."),
    array("RB26", "R", "R.B. 60/90 gg. f.m. d.f."),
    array("RB27", "R", "R.B. 90/120 gg. f.m. d.f."),
    array("RB28", "R", "R.B. 30/60/90 gg. f.m. d.f."),
    array("RB29", "R", "R.B. 60/90/120 gg. f.m. d.f."),
    array("RB30", "R", "R.B. 90/120/150 gg. f.m. d.f."),
    array("RB31", "R", "RI.BA. 60gg f.m.+10 giorni del mese successivo"),
    array("RB32", "R", "50% contrassegno + 50% R.B. 60gg d.f."),
    array("RB33", "R", "Ri.Ba. 30 dffm + 10gg"),
    array("RB34", "R", "15% ant. - 45% R.B. 60gg dffm - 40% R.B. 90gg dffm"),
    array("RB35", "R", "R.B. 60 gg. f.m. d.f. + 10gg"),
    array("RB40", "R", "Bonifico Anticipo 50%, saldo RiBa a 60gg dffm"),
    array("RB45", "R", "R.B. 45gg d.f."),
    array("RB50", "R", "50% RB a 60gg f.m. d.f.e 50% RB a 90gg f.m. d.f."),
    array("RD01", "D", "Rimessa diretta v.f. - At receipt of the invoice"),
    array("RD02", "D", "R.D. d.f.f.m."),
    array("RD03", "D", "vista fattura - ob prejemu racuna"),
    array("RD05", "D", "vista fattura - uz primitak racuna"),
    array("RD06", "D", "RD 120gg data ric. fatt."),
    array("RD07", "D", "Rimessa diretta 90gg data fattura fine mese"),
    array("RD09", "D", "R.D. a 30gg d.f."),
    array("RD12", "D", "30gg data ric. fatt.  "),
    array("RD14", "D", "30gg dal ricevimento merce -30dni od prejema blaga"),
    array("RD15", "D", "90gg data fattura"),
    array("RD16", "D", "RD 60gg data fattura"),
    array("RD17", "D", "RD 50%a 30gg dffm e 50% a 60gg dffm"),
    array("RD1S", "B", "30gg data ric. fatt.  - 30 dni od prejema racuna"),
    array("RD90", "D", "90gg data fattura fine mese"),
    array("RDPA", "R", "ELIMINATO - Rimessa diretta per Pubblica Amm."),
    array("RID", "R", "Rimessa Diretta vista fattura"),
    array("SAL", "N", "Saldo 30gg data fatt. -saldo 30 dni  od datuma f."),
    array("VIS0", "B", "vista fattura"),
    array("VIST", "N", "vista fattura - at sight")
);

$columns = array('codpag_id', 'tippag', 'descri');
$installer->getConnection()->insertArray($installer->getTable('dotcom_customer/codpag'), $columns, $data);
