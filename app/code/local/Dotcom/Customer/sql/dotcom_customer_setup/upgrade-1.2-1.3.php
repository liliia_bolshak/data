<?php
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'codspe', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Codice spedizione Ariete',
        'sort_order'    => 200,
        'position'      => 200,
        'visible'       => true,
        'required'      => false
));

$installer->addAttribute('customer', 'codpor', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Codice porto Ariete',
        'sort_order'    => 201,
        'position'      => 201,
        'visible'       => true,
        'required'      => false
));

$installer->addAttribute('customer', 'iban', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Iban',
        'sort_order'    => 202,
        'position'      => 202,
        'visible'       => true,
        'required'      => false
));

$attribute = Mage::getSingleton('eav/config');
$attribute->getAttribute('customer', 'codspe')->setData('used_in_forms', array('adminhtml_customer'))->save();
$attribute->getAttribute('customer', 'codpor')->setData('used_in_forms', array('adminhtml_customer'))->save();
$attribute->getAttribute('customer', 'iban')->setData('used_in_forms', array('adminhtml_customer', 'customer_account_create','customer_account_edit'))->save();


$installer->endSetup();
