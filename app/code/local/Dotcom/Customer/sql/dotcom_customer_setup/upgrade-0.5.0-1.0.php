<?php
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'ariete_codpag'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('dotcom_customer/codpag'))
    ->addColumn('codpag_id', Varien_Db_Ddl_Table::TYPE_TEXT, 4, array(
        'nullable'  => false,
        'primary'   => true,
        'default'   => '',
    ), 'Codice pagamento in Ariete')
    ->addColumn('tippag', Varien_Db_Ddl_Table::TYPE_TEXT, 1, array(
        'nullable'  => false,
        'default'   => '',
    ), 'Tipo pagamento')
    ->addColumn('descri', Varien_Db_Ddl_Table::TYPE_TEXT, 256, array(
        'nullable'  => false,
        'default'   => '',
    ), 'Descrizione condizioni di pagamento')
    ->setComment('Elenco condizioni di pagamento in Ariete');
$installer->getConnection()->createTable($table);

$installer->addAttribute('customer', 'codpag_ariete', array(
        'input'         => 'select',
        'type'          => 'varchar',
        'label'         => 'Codice pagamento Ariete',
        'source'        => 'dotcom_customer/resource_attribute_source_codpag',
        'sort_order'    => 190,
        'position'      => 190,
        'visible'       => true,
        'required'      => false
));

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'codpag_ariete');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();

$installer->endSetup();
