<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'company');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));
$attribute->save();

$setup->endSetup();
