<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();
/*
$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);
*/
$setup->addAttribute('customer', 'cf', array(
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Codice fiscale',
    'visible'       => true,
    'required'      => false
));
/*
$setup->addAttribute('customer', 'c_refcomm', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Referente commerciale',
        'visible'       => true,
        'required'      => false
));

$setup->addAttribute('customer', 'c_refamm', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Referente amministrativo',
        'visible'       => true,
        'required'      => false
));

$setup->addAttribute('customer', 'c_reftecn', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Referente tecnico',
        'visible'       => true,
        'required'      => false
));
*/

$eavConfig = Mage::getSingleton('eav/config');
$attribute = $eavConfig->getAttribute('customer', 'cf');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
/*
$attribute = $eavConfig->getAttribute('customer', 'c_refcomm');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();


$attribute = $eavConfig->getAttribute('customer', 'c_refamm');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();

$attribute = $eavConfig->getAttribute('customer', 'c_reftecn');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
*/

$setup->endSetup();
