<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->run("

    DROP TABLE IF EXISTS `dotcom_sync_regioni`;

    CREATE TABLE `dotcom_sync_regioni` (
      `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `address_id` int(11) NOT NULL,
      `isdone` smallint(5) NOT NULL default '0',
      CONSTRAINT `FK_DOTCOMSYNCREGIONI_ADDRESS_ID_ADDRESSENTITY_ENTITY_ID` FOREIGN KEY (`address_id`) REFERENCES `customer_address_entity` (`entity_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  ");

$installer->endSetup();
