<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute('customer', 'privacy1', array(
    'input'         => 'text',
    'type'          => 'int',
    'label'         => 'Privacy 1',
    'visible'       => true,
    'required'      => true
));

$setup->addAttribute('customer', 'privacy2', array(
		'input'         => 'text',
		'type'          => 'int',
		'label'         => 'Privacy 2',
		'visible'       => true,
		'required'      => false
));

$setup->addAttribute('customer', 'privacy3', array(
		'input'         => 'text',
		'type'          => 'int',
		'label'         => 'Privacy 3',
		'visible'       => true,
		'required'      => false
));

$eavConfig = Mage::getSingleton('eav/config');
$attribute = $eavConfig->getAttribute('customer', 'privacy1');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create')); 
$attribute->save();
$attribute = $eavConfig->getAttribute('customer', 'privacy2');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
$attribute = $eavConfig->getAttribute('customer', 'privacy3');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();


$setup->endSetup();
