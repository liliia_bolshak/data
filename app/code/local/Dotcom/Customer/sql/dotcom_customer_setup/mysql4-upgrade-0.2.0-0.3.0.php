<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->updateAttribute('customer', 'company', 'required', 0);
$setup->updateAttribute('customer', 'PI_codice', 'required', 0);
$setup->updateAttribute('customer', 'keyrif', 'required', 0);
$setup->updateAttribute('customer', 'emailpec', 'required', 0);
$setup->updateAttribute('customer', 'emailinvioft', 'required', 0);

$setup->endSetup();
