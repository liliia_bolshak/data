<?php
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'codage', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Codice Agente Ariete',
        'sort_order'    => 199,
        'position'      => 199,
        'visible'       => true,
        'required'      => false
));

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'codage');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();

$installer->endSetup();
