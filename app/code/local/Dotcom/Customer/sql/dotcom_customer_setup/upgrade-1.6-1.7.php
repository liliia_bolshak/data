<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();
$setup->addAttribute('customer_address', 'provincia', array(
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Provincia',
    'sort_order'    => 81,
    'visible'       => true,
    'required'      => false
));

$eavConfig = Mage::getSingleton('eav/config');
$attribute = $eavConfig->getAttribute('customer_address', 'provincia');
$attribute->setData('used_in_forms', array('adminhtml_customer_address', 'customer_address_edit', 'customer_register_address'));  //enable all action
$attribute->save();

$setup->endSetup();
