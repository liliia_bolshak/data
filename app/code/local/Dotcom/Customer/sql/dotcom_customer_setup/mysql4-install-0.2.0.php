<?php 
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute('customer', 'ref_contab', array(
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Ref. contabilità',
    'visible'       => true,
    'required'      => false
));

$setup->addAttribute('customer', 'ref_acquisti', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Ref. acquisti',
		'visible'       => true,
		'required'      => false
));

$setup->addAttribute('customer', 'ref_tecnico', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Ref. tecnico',
		'visible'       => true,
		'required'      => false
));

$setup->addAttribute('customer', 'company', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Company',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => true,
));

$setup->addAttribute('customer', 'PI_codice', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Ariete code',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => true,
));

$setup->addAttribute('customer', 'keyrif', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Infordata code',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => true,
));


$setup->addAttribute('customer', 'emailpec', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Email PEC',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => true,
));


$setup->addAttribute('customer', 'emailinvioft', array(
		'input'         => 'text',
		'type'          => 'text',
		'label'         => 'Email invio fatture',
		'visible'       => true,
		'required'      => true,
		'user_defined'  => true,
));


$eavConfig = Mage::getSingleton('eav/config');
$attribute = $eavConfig->getAttribute('customer', 'ref_contab');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
$attribute = $eavConfig->getAttribute('customer', 'ref_acquisti');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
$attribute = $eavConfig->getAttribute('customer', 'ref_tecnico');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();

$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'company');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'PI_codice');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'keyrif');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'emailpec');
$attribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));  //enable all action
$attribute->save();
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'emailinvioft');
$attribute->setData('used_in_forms', array('adminhtml_customer'));
$attribute->save();

$setup->endSetup();
