// <?php
/* @var $this Mage_Catalog_Model_Resource_Setup */
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->startSetup();

$setup->updateAttribute('customer_address','firstname','is_required','false');
$setup->updateAttribute('customer_address','lastname','is_required','false');
$setup->updateAttribute('customer_address','telephone','is_required','false');
$setup->endSetup();
