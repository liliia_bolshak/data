<?
class Dotcom_Customer_Helper_Data extends Mage_Core_Helper_Abstract 
{
    const GRUPPO_PRIVATI = "11";
    const GRUPPO_AZIENDE = "1";
    const GRUPPO_AZIENDE_APPROVARE = "12";
    const GRUPPO_RIVENDITORI_APPROVARE = "10";
    const GRUPPO_RIVENDITORI = "4";
    
    protected function _getDefaultCountryId($customer){
        $billingAddress = $customer->getPrimaryBillingAddress();
        if($billingAddress){
            return $billingAddress->getCountryId();;
        }
        return false;        
    }
    
    public function needVatCharge($customer, $checkItaly = true){
       $countryId = $this->_getDefaultCountryId($customer);
       
       if(in_array($countryId, $this->getEuCountries($checkItaly))){
           return $this->isCustomerGroupAzienda($customer->getGroupId());
       }
       return false; 
    }
    
    public function needInsertCf($customer){
        $countryId = $this->_getDefaultCountryId($customer);
        return $countryId == "IT" && $this->isCustomerGroupCliente($customer->getGroupId());
    }
    
    public function getEuCountries($includeIt = false){
        if($includeIt){
            return array("AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","IT","LV","LT","LU","MT","NL","PL","PT","SK","SI","ES","SE","GB");
        }
        else{
            return array("AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GR","HU","LV","LT","LU","MT","NL","PL","PT","SK","SI","ES","SE","GB");
        }
    }
    
    public function getWorldCountries(){
        return array("AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AZ","BS","BH","BD","BB","BY","BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","BN","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","CU","CW","DJ","DM","DO","EC","EG","SV","GQ","ER","ET","FK","FO","FJ","GF","PF","TF","GA","GM","GE","GH","GI","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","IS","IN","ID","IR","IQ","IE","IM","IL","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LB","LS","LR","LY","LI","MO","MK","MG","MW","MY","MV","ML","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PR","QA","RE","RO","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SX","SB","SO","ZA","GS","SS","LK","SD","SR","SJ","SZ","CH","SY","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","US","UM","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW");
    }

    public function isCustomerGroupAzienda($group){
        return in_array($group, array(self::GRUPPO_AZIENDE, self::GRUPPO_AZIENDE_APPROVARE, self::GRUPPO_RIVENDITORI, self::GRUPPO_RIVENDITORI_APPROVARE));
    }
    
    public function isCustomerGroupCliente($group){
        return $group == self::GRUPPO_PRIVATI;
    }
    
}
