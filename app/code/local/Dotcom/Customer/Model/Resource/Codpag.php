<?php
class Dotcom_Customer_Model_Resource_Codpag extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     *
     */
    protected function _construct()
    {
        $this->_init('dotcom_customer/codpag', 'codpag_id');
    }
}
