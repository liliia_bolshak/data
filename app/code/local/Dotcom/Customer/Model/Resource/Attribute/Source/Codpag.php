<?php
class Dotcom_Customer_Model_Resource_Attribute_Source_Codpag extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * Retreive all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('dotcom_customer/codpag_collection')->toOptionArray();
        }
        return $this->_options;
    }
}
