<?php
class Dotcom_Customer_Model_Resource_Codpag_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('dotcom_customer/codpag');
    }

    /**
     * Loads Item By Id
     *
     * @param string $codpagId
     * @return Dotcom_Customer_Model_Resource_Codpag
     */
    public function getItemById($codpagId)
    {
        foreach ($this->_items as $item) {
            if ($item->getCodpagId() == $codpagId) {
                return $item;
            }
        }
        return Mage::getResourceModel('dotcom_customer/codpag');
    }

    /**
     * Convert collection items to select options array
     *
     * @param string $emptyLabel
     * @return array
     */
    public function toOptionArray($emptyLabel = ' ')
    {
        $options = $this->_toOptionArray('codpag_id', 'descri', array('tipo'=>'tippag'));

        $sort = array();
        foreach ($options as $data) {
            $name = $data['value'];
            if (!empty($name)) {
                $sort[$name] = $data['label'];
            }
        }

        Mage::helper('core/string')->ksortMultibyte($sort);
        $options = array();
        foreach ($sort as $value=>$label) {
            $options[] = array(
               'value' => $value,
               'label' => $label
            );
        }

        if (count($options) > 0 && $emptyLabel !== false) {
            array_unshift($options, array('value' => '', 'label' => $emptyLabel));
        }

        return $options;
    }
}
