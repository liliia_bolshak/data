<?php
class Dotcom_Customer_Model_Codpag extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('dotcom_customer/codpag');
    }

    public function getName()
    {
        if(!$this->getData('name')) {
            $this->setData(
                'name',
                $this->getDescri()
            );
        }
        return $this->getData('name');
    }

}
