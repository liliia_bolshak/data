<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Payment
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Dotcom_Bonifico_Block_Info_Bonifico extends Mage_Payment_Block_Info
{
	protected $_notes;
	
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('dotcom/bonifico/info/bonifico.phtml');
    }

    public function getNotes()
    {
        if (is_null($this->_notes)) {
            $this->_convertAdditionalData();
        }
        return $this->_notes;
    }

    protected function _convertAdditionalData()
    {
        $details = @unserialize($this->getInfo()->getAdditionalData());
        if (is_array($details)) {
            $this->_notes = isset($details['notes']) ? (string) $details['notes'] : '';
        } else {
            $this->_notes = '';
        }
        return $this;
    }
    
    public function toPdf()
    {
        $this->setTemplate('dotcom/bonifico/info/pdf/checkmo.phtml');
        return $this->toHtml();
    }

}
