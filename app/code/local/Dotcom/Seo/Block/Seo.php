<?php

class Dotcom_Seo_Block_Seo extends Mage_Core_Block_Template
{
    /**
     * Add meta information from product to head block
     *
     */
    public function addProductCanonicalUrl()
    {
        $product = Mage::registry('product');
        if($product)
        {
            $headBlock = $this->getLayout()->getBlock('head');
            $headBlock->addLinkRel('canonical', Mage::helper("dotcom_catalog")->getUrlProdotto($product));
            parent::_prepareLayout();
        }
    }
    
    public function addProductMetaTitle(){
        $product = Mage::registry('product');
        if($product)
        {
            $headBlock = $this->getLayout()->getBlock('head');
            $headBlock->setTitle($product->getSku()." | ".$product->getName());
            $headBlock->setData("flagtitle", 1);
            parent::_prepareLayout();
        } 
    }

}
