<?php 


class Dotcom_Checkoutobserver_Model_Observer
{
    const VAT_VALIDATION_WSDL_URL = 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService?wsdl';
    public function saveCategoriaUtente($observer)
    {
        $customer = $observer->getCustomer();
        $post = Mage::app()->getFrontController()->getRequest()->getParam('group_id');
        if($post){
            $customer->setGroupId($post);
        }

        try {
            $paese = Mage::app()->getFrontController()->getRequest()->getParam('country_id');
            if($customer->getDefaultBilling()){
                $paese = Mage::getModel('customer/address')->load($customer->getDefaultBilling())->getCountryId();
            }
            if (!$customer->getTaxvat() && !$paese || $customer->getTaxvat() == $customer->getOrigData("taxvat"))
            {
                return $this;
            } else {
                $result = $this->_checkVatNumber($paese, $customer->getTaxvat());
                if (!Mage::app()->getStore()->isAdmin()) {
                    if ($result->getIsValid()) {
                        if($customer->getGroupId() == Dotcom_Customer_Helper_Data::GRUPPO_AZIENDE_APPROVARE){
                            $customer->setGroupId(Dotcom_Customer_Helper_Data::GRUPPO_AZIENDE);
                        }
                        Mage::getSingleton('customer/session')->addSuccess(Mage::helper("customer")->__("L'IVA inserita è valida."));
                    } else {
                        Mage::getSingleton('customer/session')->addError(Mage::helper("customer")->__("L'IVA inserita non sembra essere valida, ma comunque il tuo account sarà attivato, prima di fare ordini devi correggere la P.IVA o Codice fiscale"));
                    }
                }
            }
        } catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError($e->getMessage());
        }
        
    }
    
    private function _checkVatNumber($countryCode, $vatNumber){
        // Default response
        $gatewayResponse = new Varien_Object(array(
                'is_valid' => false,
                'request_date' => '',
                'request_identifier' => '',
                'request_success' => false
        ));
    
        if (!extension_loaded('soap')) {
            Mage::logException(Mage::exception('Mage_Core',
            Mage::helper('core')->__('PHP SOAP extension is required.')));
            return $gatewayResponse;
        }
        try{
            $soapClient = new SoapClient(self::VAT_VALIDATION_WSDL_URL, array('trace' => false));
    
            $requestParams = array();
            $requestParams['countryCode'] = $countryCode;
            $requestParams['vatNumber'] = str_replace(array(' ', '-'), array('', ''), $vatNumber);
            $requestParams['requesterCountryCode'] = '';
            $requestParams['requesterVatNumber'] = '';
    
            // Send request to service
            $result = $soapClient->checkVatApprox($requestParams);
    
            $gatewayResponse->setIsValid((boolean) $result->valid);
            $gatewayResponse->setRequestDate((string) $result->requestDate);
            $gatewayResponse->setRequestIdentifier((string) $result->requestIdentifier);
            $gatewayResponse->setRequestSuccess(true);
        } catch (Exception $exception) {
            $gatewayResponse->setIsValid(false);
            $gatewayResponse->setRequestDate('');
            $gatewayResponse->setRequestIdentifier('');
        }
    
        return $gatewayResponse;
    }
    
	public function checkPrice($observer)
	{
		if(Mage::getModel('checkout/cart')->getQuote()->getSubtotal() == 0){
			 Mage::getSingleton('core/session')->addError("Impossibile effettuare il checkout, il totale del carrello è zero.");
			 session_write_close();
   			 Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
		}
	}
	
	public function afterAddProduct($observer){
	    if(Mage::registry('addspesemovimentazione')){
	        return $this;
	    }
	     
	    /* @var $cart Mage_Checkout_Model_Cart */
	    $cart = Mage::getSingleton('checkout/cart');
	    $quote = $observer->getQuote();
	    if(count($quote->getAllItems()) == 0) return $this;
	    
	    $spese = Mage::getSingleton('catalog/product')->loadByAttribute('sku', 'spese-movimentazione');
	    $quoteItem = $quote->getItemByProduct($spese);
	    $totals = $quote->collectTotals()->getSubtotal();
	    if ($quote->hasProductId($spese->getId())){
	        $totals = $totals - $spese->getPrice();
	    }
	    if($totals <= 50 && !$quote->hasProductId($spese->getId())){
	        Mage::register('addspesemovimentazione', 1);
	        $cart->addProduct($spese->getId(),array('qty' => 1))->save();
	    }
	    else if($totals > 50 && $quote->hasProductId($spese->getId())){
	        Mage::register('addspesemovimentazione', 1);
	        $quote->removeItem($quoteItem->getId());
	    }
	}
	
	public function checkZeroPriceQuote ($observer)
	{
	    if(!$observer->getProduct()->getFinalPrice())
	    {
	        Mage::getSingleton('core/session')->addError("Impossibile aggiungere un prodotto al carrello se il suo prezzo è zero.");
	        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('/'));
	        Mage::app()->getResponse()->sendResponse();
	        exit;
	    }
	}
	
	
	public function addHandles($observer)
	{
	    $category = Mage::registry('current_category');
	    if ($category instanceof Mage_Catalog_Model_Category && $category->getLevel() > 2) {
	        $update = Mage::getSingleton('core/layout')->getUpdate();
	        foreach ($category->getPathIds() as $pathId) {
	            $update->addHandle('CATEGORY_PRODOTTI');
	        }
	    }
	    
	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "prodotti") {
	        $update = Mage::getSingleton('core/layout')->getUpdate();
	        foreach ($category->getPathIds() as $pathId) {
	            $update->addHandle('CATEGORIA_ROOT_PRODOTTI');
	        }
	    }

	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "offerte") {
	    	$update = Mage::getSingleton('core/layout')->getUpdate();
	    	foreach ($category->getPathIds() as $pathId) {
	    		$update->addHandle('CATEGORIA_ROOT_OFFERTE');
	    	}
	    }

	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "promotions") {
	    	$update = Mage::getSingleton('core/layout')->getUpdate();
	    	foreach ($category->getPathIds() as $pathId) {
	    		$update->addHandle('CATEGORIA_ROOT_OFFERTE');
	    	}
	    }

	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "promociones") {
	    	$update = Mage::getSingleton('core/layout')->getUpdate();
	    	foreach ($category->getPathIds() as $pathId) {
	    		$update->addHandle('CATEGORIA_ROOT_OFFERTE');
	    	}
	    }
	     
	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "novita") {
	        $update = Mage::getSingleton('core/layout')->getUpdate();
	        foreach ($category->getPathIds() as $pathId) {
	            $update->addHandle('CATEGORIA_ROOT_NOVITA');
	        }
	    }

	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "new-products") {
	    	$update = Mage::getSingleton('core/layout')->getUpdate();
	    	foreach ($category->getPathIds() as $pathId) {
	    		$update->addHandle('CATEGORIA_ROOT_NOVITA');
	    	}
	    }

	    if ($category instanceof Mage_Catalog_Model_Category && $category->getUrlKey() == "novedades") {
	    	$update = Mage::getSingleton('core/layout')->getUpdate();
	    	foreach ($category->getPathIds() as $pathId) {
	    		$update->addHandle('CATEGORIA_ROOT_NOVITA');
	    	}
	    }
	    
	    return $this;
	}
	
	public function setFinalPriceInProductView($observer)
	{
	    $customer = Mage::getSingleton('customer/session')->getCustomer();
	    if(!$customer->getId()) return false;
	    
	    $product = $observer->getProduct();
	    $tierprices = $product->getTierPrice();
	    $specialPrice = $product->getSpecialPrice();
	    foreach ($tierprices as $tp){
	        if ((int)$tp["price_qty"] == 1){
	            $prezzoFinale = $tp["price"];
	            break; //un solo tierprice per gruppo utente può avere quantità 1
	        }
	    }
	    /*
	    $prodotto = Mage::getModel("catalog/product")->getCollection()
	    ->addAttributeToFilter("entity_id", $product->getId())
	    ->setPage(1, 1)
	    ->addMinimalPrice()
	    ->addFinalPrice()
	    ->addTaxPercents()
	    ->load()
	    ->getFirstItem();
	    $product->setFinalPrice($prodotto->getMinimalPrice());
	    $product->setIsInProductView(true);
	    $product->setMinimalPrice($prodotto->getMinimalPrice());
	    */
	    if(isset($prezzoFinale) && $prezzoFinale <= $product->getPrice() && ($prezzoFinale <= $specialPrice || !isset($specialPrice)) ){
	        $product->setFinalPrice($prezzoFinale);
	        $product->setIsInProductView(true);
	    } 
	    return $this;
	}
	
	public function saveAdditionalInfoBefore($observer){
	    $post = Mage::app()->getRequest()->getPost("numabbonamento");
	    if($post) {
	        $quote = Mage::getSingleton('checkout/session')->getQuote();
	        $quote->setNumabbonamento($post);
	    }
	}
	
}