<?php 

class Dotcom_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_List {
	
	protected function _getProductCollection()
	{
		if (is_null($this->_productCollection)) {
			$catrules = Mage::getModel('catalogrule/rule')->getCollection();
			$prodids = array();
			$rulemodel = Mage::getResourceModel('catalogrule/rule');
			$todayDate = date('Y-m-d');
			foreach ($catrules as $cr) {
				if ($cr->getIsActive() && $todayDate <= $cr->getToDate() && $todayDate >= $cr->getFromDate()) {
					$prodids = array_merge($prodids, $rulemodel->getRuleProductIds($cr->getId()));
				}
			}
			$catalog = Mage::getModel('catalog/product');
			$_productCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())->addAttributeToFilter('entity_id', array('in' => $prodids));

			Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($_productCollection);
			Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($_productCollection);
		
			$this->_productCollection = $_productCollection;
		}
		
		return $this->_productCollection;
	}
	
}
?>