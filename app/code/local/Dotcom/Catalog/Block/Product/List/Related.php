<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog product related items block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dotcom_Catalog_Block_Product_List_Related extends Mage_Catalog_Block_Product_Abstract
{
    protected $_itemCollection;
	
    protected $_acItemCollection;
    protected $_prItemCollection;
    protected $_mcItemCollection;
    
    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getRelatedProductCollection()
            ->addAttributeToSelect('required_options')
            ->addAttributeToSelect('type_product_ariete')
            ->addAttributeToSort('position', 'asc')
            ->addFieldToFilter('type_product_ariete', 3)
            ->addStoreFilter();
        
        $this->_acItemCollection = $product->getRelatedProductCollection()
        ->addAttributeToSelect('required_options')
        ->addAttributeToSelect('type_product_ariete')
        ->addAttributeToSort('position', 'asc')
        ->addFieldToFilter('type_product_ariete', 4)
        ->addStoreFilter();
        
        $this->_mcItemCollection = $product->getRelatedProductCollection()
        ->addAttributeToSelect('required_options')
        ->addAttributeToSelect('type_product_ariete')
        ->addAttributeToSort('position', 'asc')
        ->addFieldToFilter('type_product_ariete', 5)
        ->addStoreFilter()
        ;
        
        $this->_prItemCollection = $product->getRelatedProductCollection()
        ->addAttributeToSelect('required_options')
        ->addAttributeToSelect('type_product_ariete')
        ->addAttributeToSort('position', 'asc')
        ->addFieldToFilter('type_product_ariete', 6)
        ->addStoreFilter();
        Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
            Mage::getSingleton('checkout/session')->getQuoteId()
        );

        
        $this->_addProductAttributesAndPrices($this->_itemCollection);
        $this->_addProductAttributesAndPrices($this->_prItemCollection);
        $this->_addProductAttributesAndPrices($this->_mcItemCollection);
        $this->_addProductAttributesAndPrices($this->_acItemCollection);
        
//        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
      //  Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        $this->_itemCollection->load();
        $this->_prItemCollection->load();
        $this->_mcItemCollection->load();
        $this->_acItemCollection->load();

        
        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }
        foreach ($this->_mcItemCollection as $product) {
        	$product->setDoNotUseCategoryId(true);
        }
        foreach ($this->_prItemCollection as $product) {
        	$product->setDoNotUseCategoryId(true);
        }
        foreach ($this->_acItemCollection as $product) {
        	$product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    public function getItems()
    {
        return $this->_itemCollection;
    }
    
    public function getAcItems()
    {
    	return $this->_acItemCollection;
    }
    
    public function getMcItems()
    {
    	return $this->_mcItemCollection;
    }
    
    public function getPrItems()
    {
    	return $this->_prItemCollection;
    }
}
