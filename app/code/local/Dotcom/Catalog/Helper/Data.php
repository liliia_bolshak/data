<?PHP

class Dotcom_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
	function getCategories($categories=null, $level=1) {
	    if(is_null($categories))
	    {
		    $categories = Mage::getModel('catalog/category')->getCategories(Mage::getStoreConfig('ariete_section/arieteconfig_group/rootcategory_field'));
	    }
        $return = '<ul>';
        foreach ($categories as $category) {
        	if(!$category->getIsActive()) continue;
            /*@var $cat Mage_Catalog_Model_Category*/
        	$cat = Mage::getModel('catalog/category')->load($category->getId());
            $return .= '<li class="level' . $category->getLevel() . '">' .
                     '<a href="' . Mage::getUrl($cat->getUrlPath()) . '">' .
                     $category->getName() . "(" . $this->getProductCount($cat) . ")</a>\n";
            if ($category->hasChildren()) {
                $children = Mage::getModel('catalog/category')->getCategories($category->getId());
                $return .= $this->getCategories($children, $level += 1);
            }
            
            $return .= '</li>';
        }
        return $return . '</ul>';
	}
	
	function getProductCount($category)
	{
		$prodCollection = Mage::getResourceModel('catalog/product_collection')
		->addCategoryFilter($category);
		//->addAttributeToSelect('type_product_ariete')
		//->addFieldToFilter('type_product_ariete', 3);
		
		return $prodCollection->count();
	}
	
	public function getUrlProdotto(Mage_Catalog_Model_Product $_product = null){
	    if(!$_product) return '';
	
	    // force display deepest child category as request path
	    $categories = $_product->getCategoryCollection();
	    $deepCatId = 0;
	    $path = '';
	    $product_path = false;
	    foreach ($categories as $category) {
	        $id = $category->getId();
	        /*
	        if(in_array($id, array('15','146'))){
	            // skip home(15) and special(146) categories
	            continue;
	        }
	        */
	        // look for the deepest path and save
	        if(substr_count($category->getData('path'), '/') > substr_count($path, '/')){
	            $path = $category->getData('path');
	            $deepCatId = $id;
	        }
	    }
	    // load category
	    /*
	    $category = Mage::getModel('catalog/category')->load($deepCatId);
	
	    // remove .html from category url_path
	    $category_path = str_replace('.html', '',  $category->getData('url_path'));
	
	    // get product url path if set
	    $product_url_path = $_product->getData('url_path');
	
	    // get product request path if set
	    $product_request_path = $_product->getData('request_path');
	
	    // now grab only the product path including suffix (if any)
	    if($product_url_path){
	        $path = explode('/', $product_url_path);
	        $product_path = array_pop($path);
	    } elseif($product_request_path){
	        $path = explode('/', $product_request_path);
	        $product_path = array_pop($path);
	    }
	
	    // now set product request path to be our full product url including deepest category url path
	    if($product_path){
	        $_product->setData('request_path', $category_path . $product_path);
	    }
	    // END: force display deepest child category as request path
	
	*/
	    $idpath = "product/".$_product->getId()."/".$deepCatId;
	    $resource = Mage::getSingleton('core/resource');
	    $readConnection = $resource->getConnection('core_read');
	    $query = "SELECT request_path FROM core_url_rewrite WHERE id_path ='".$idpath."' AND store_id =".Mage::app()->getStore()->getId();
	    $reqPathProdotto = $readConnection->fetchOne($query);
	    if($reqPathProdotto){
	        $_product->setData('request_path', $reqPathProdotto);
	    }
	    return $_product->getProductUrl();
	}
	
	public function switchBlockIf(){
	    $store = Mage::app()->getStore()->getId();
	    switch ($store){
	        case 5: //en
	            $block = "elenco_categorie_en";
	            break;
            case 6: //es
                $block = "elenco_categorie_es";
                break;
            default: //it
                $block = "elenco_categorie";
	    }
	    
	    return $block; 
	    
	}
	
}