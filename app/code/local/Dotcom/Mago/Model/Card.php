<?php

/**
 * model per il calcolo del costo card
 */

class Dotcom_Mago_Model_Card
{
		private $_quantity = array(
			'100'	=> '0400',
			'250'	=> '0220',
			'500'	=> '0150',
			'1000'   => '0000',
			'2000'   => '0000',
			'3000'   => '0000',
			'5000'   => '0000',
			'10000'  => '0000',
			'15000'  => '0000',
			'20000'  => '0000'
		);
	private $_spessore = array(
		'76' => '0000',
		'40' => '0000'
	);
		private $_fronte = array(
			'quad'  => array(1 => '0350', 2 => '0350', 3 => '0350', 4 => '0101', 5 => '0090', 6 => '0082', 7 => '0069', 8 => '0054', 9 => '0062', 10 => '0050'),
			'mono'  => array(1 => '0200', 2 => '0150', 3 => '0150', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000'),//DAI '1000' pezzi in su vedi i prezzi di quad fronte
			'blnk'  => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_retro = array(
			'quad'  => array(1 => '0350', 2 => '0350', 3 => '0350', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000'),//DAI '1000' pezzi in su vedi i prezzi di quad fronte
			'mono'  => array(1 => '0200', 2 => '0150', 3 => '0150', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000'),//DAI '1000' pezzi in su vedi i prezzi di quad fronte
			'blnk'  => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_tipostampa = array(
			'ind'   => array(1 => '0430', 2 => '0450', 3 => '0480', 4 => '0540', 5 => '0540', 6 => '0540', 7 => '0540', 8 => '0540', 9 => '0540', 10 => '0540'),
			'dir'   => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000'),
			'off'   => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_stampaacaldo = array(
			'true'  => array(1 => null, '2' => null, '3' => null, '4' => '0085', 5 => '0080', 6 => '0075', 7 => '0070', 8 => '0065', 9 => '0060', 10 => '0060'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_serigrafico = array(
			'true'  => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0110', 5 => '0100', 6 => '0090', 7 => '0080', 8 => '0070', 9 => '0060', 10 => '0050'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_banda = array(
			'hico'  => array(1 => '0050', 2 => '0050', 3 => '0050', 4 => '0050', 5 => '0045', 6 => '0040', 7 => '0035', 8 => '0035', 9 => '0035', 10 => '0035'),
			'loco'  => array(1 => '0050', 2 => '0050', 3 => '0050', 4 => '0050', 5 => '0045', 6 => '0040', 7 => '0035', 8 => '0035', 9 => '0035', 10 => '0035'),
			'no'	=> array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_firma = array(
			'true'  => array(1 => '0150', 2 => '0150', 3 => '0150', 4 => '0110', 5 => '0100', 6 => '0090', 7 => '0080', 8 => '0070', 9 => '0060', 10 => '0050'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_chipacontatto = array(
		   'FM4442' => array(1 => '0614', 2 => '0614', 3 => '0614', 4 => '0553', 5 => '0503', 6 => '0460', 7 => '0410', 8 => '0390', 9 => '0366', 10 => '0365'),
		  'SLE5542' => array(1 => '0770', 2 => '0770', 3 => '0770', 4 => '0693', 5 => '0631', 6 => '0579', 7 => '0518', 8 => '0493', 9 => '0468', 10 => '0466'),
		   'FM4428' => array(1 => '0729', 2 => '0729', 3 => '0729', 4 => '0656', 5 => '0596', 6 => '0544', 7 => '0486', 8 => '0469', 9 => '0444', 10 => '0442'),
		  'SLE5528' => array(1 => '0975', 2 => '0975', 3 => '0975', 4 => '0877', 5 => '0809', 6 => '0754', 7 => '0677', 8 => '0651', 9 => '0624', 10 => '0622'),
		'ISSI24C02' => array(1 => '0778', 2 => '0778', 3 => '0778', 4 => '0700', 5 => '0638', 6 => '0586', 7 => '0525', 8 => '0505', 9 => '0480', 10 => '0478'),
		'ISSI24C16' => array(1 => '0893', 2 => '0893', 3 => '0893', 4 => '0804', 5 => '0738', 6 => '0684', 7 => '0614', 8 => '0590', 9 => '0564', 10 => '0562'),
		'ISSI24C64' => array(1 => '1139', 2 => '1139', 3 => '1139', 4 => '1025', 5 => '0952', 6 => '0894', 7 => '0804', 8 => '0773', 9 => '0743', 10 => '0742'),
			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_chiprfid = array(
		   'EM4100' => array(1 => '0549', 2 => '0549', 3 => '0549', 4 => '0494', 5 => '0439', 6 => '0411', 7 => '0359', 8 => '0335', 9 => '0312', 10 => '0311'),
			'T5567' => array(1 => '0942', 2 => '0942', 3 => '0942', 4 => '0848', 5 => '0781', 6 => '0733', 7 => '0664', 8 => '0627', 9 => '0600', 10 => '0586'),
	   'PhilipsS50' => array(1 => '1040', 2 => '1040', 3 => '1040', 4 => '0936', 5 => '0866', 6 => '0810', 7 => '0728', 8 => '0688', 9 => '0659', 10 => '0646'),
		 'FudanF08' => array(1 => '0614', 2 => '0614', 3 => '0614', 4 => '0553', 5 => '0496', 6 => '0446', 7 => '0397', 8 => '0372', 9 => '0348', 10 => '0335'),
	   'PhilipsS70' => array(1 => '1434', 2 => '1434', 3 => '1434', 4 => '1291', 5 => '1208', 6 => '1146', 7 => '1033', 8 => '0979', 9 => '0947', 10 => '0934'),
		 'ICODESLI' => array(1 => '0795', 2 => '0795', 3 => '0795', 4 => '0715', 5 => '0653', 6 => '0600', 7 => '0537', 8 => '0505', 9 => '0480', 10 => '0466'),
		  'UHFGEN2' => array(1 => '1015', 2 => '1015', 3 => '1015', 4 => '0913', 5 => '0824', 6 => '0773', 7 => '0664', 8 => '0633', 9 => '0606', 10 => '0598'),
			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_pvc = array(
			  'pvc' => array(1 => '0142', 2 => '0142', 3 => '0142', 4 => '0128', 5 => '0114', 6 => '0081', 7 => '0077', 8 => '0071', 9 => '0058', 10 => '0057'),
			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_persdativariabili = array(
		   'fronte' => array(1 => '0150', 2 => '0150', 3 => '0120', 4 => '0080', 5 => '0075', 6 => '0070', 7 => '0065', 8 => '0060', 9 => '0055', 10 => '0050'),
			'retro' => array(1 => '0150', 2 => '0150', 3 => '0120', 4 => '0080', 5 => '0075', 6 => '0070', 7 => '0065', 8 => '0060', 9 => '0055', 10 => '0050'),
			 'ambo' => array(1 => '0300', 2 => '0300', 3 => '0240', 4 => '0160', 5 => '0150', 6 => '0140', 7 => '0130', 8 => '0120', 9 => '0110', 10 => '0100'),
			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_persstampafoto = array(
		   'fronte' => array(1 => '0150', 2 => '0150', 3 => '0120', 4 => '0080', 5 => '0075', 6 => '0070', 7 => '0065', 8 => '0060', 9 => '0055', 10 => '0050'),
			'retro' => array(1 => '0150', 2 => '0150', 3 => '0120', 4 => '0080', 5 => '0075', 6 => '0070', 7 => '0065', 8 => '0060', 9 => '0055', 10 => '0050'),
			 'ambo' => array(1 => '0300', 2 => '0300', 3 => '0240', 4 => '0160', 5 => '0150', 6 => '0140', 7 => '0130', 8 => '0120', 9 => '0110', 10 => '0100'),
			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
// 		private $_foto = array(
// 		   'fronte' => array(1 => '0500', 2 => '0500', 3 => '0500', 4 => '0500', 5 => '0500', 6 => '0500', 7 => '0500', 8 => '0500', 9 => '0500', 10 => '0500'),
// 			'retro' => array(1 => '0500', 2 => '0500', 3 => '0500', 4 => '0500', 5 => '0500', 6 => '0500', 7 => '0500', 8 => '0500', 9 => '0500', 10 => '0500'),
// 			 'ambo' => array(1 => '1000', 2 => '1000', 3 => '1000', 4 => '1000', 5 => '1000', 6 => '1000', 7 => '1000', 8 => '1000', 9 => '1000', 10 => '1000'),
// 			   'no' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
// 		);
		private $_codificamagnetica = array(
			'true'  => array(1 => '0120', 2 => '0120', 3 => '0090', 4 => '0060', 5 => '0050', 6 => '0045', 7 => '0040', 8 => '0030', 9 => '0020', 10 => '0015'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_codificadatichip = array(
			'true'  => array(1 => '0120', 2 => '0120', 3 => '0120', 4 => '0050', 5 => '0050', 6 => '0040', 7 => '0040', 8 => '0030', 9 => '0025', 10 => '0025'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_punzonatura = array(
			'true'  => array(1 => '0150', 2 => '0150', 3 => '0120', 4 => '0100', 5 => '0090', 6 => '0080', 7 => '0080', 8 => '0070', 9 => '0070', 10 => '0070'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);
		private $_asolatura = array(
			'true'  => array(1 => '0110', 2 => '0100', 3 => '0090', 4 => '0070', 5 => '0065', 6 => '0060', 7 => '0055', 8 => '0050', 9 => '0050', 10 => '0050'),
			'false' => array(1 => '0000', 2 => '0000', 3 => '0000', 4 => '0000', 5 => '0000', 6 => '0000', 7 => '0000', 8 => '0000', 9 => '0000', 10 => '0000')
		);

	public function calcola($params, $customerGroupCode = null)
	{
		$costo = 0;
		$indice = 0;
		$qtyWildCard = $params['costoCardQuantita'];

		if($params['costoCardQuantita'] <= 100 )        {$params['costoCardQuantita'] = '100'; $indice = 1;}
		else if ($params['costoCardQuantita'] <= 250)   {$params['costoCardQuantita'] = '250'; $indice = 2;}
		else if ($params['costoCardQuantita'] <= 500)   {$params['costoCardQuantita'] = '500'; $indice = 3;}
		else if ($params['costoCardQuantita'] <= 1000)  {$params['costoCardQuantita'] = '1000'; $indice = 4;}
		else if ($params['costoCardQuantita'] <= 2000)  {$params['costoCardQuantita'] = '2000'; $indice = 5;;}
		else if ($params['costoCardQuantita'] <= 3000)  {$params['costoCardQuantita'] = '3000'; $indice = 6;}
		else if ($params['costoCardQuantita'] <= 5000)  {$params['costoCardQuantita'] = '5000'; $indice = 7;}
		else if ($params['costoCardQuantita'] <= 10000) {$params['costoCardQuantita'] = '10000'; $indice = 8;}
		else if ($params['costoCardQuantita'] <= 15000) {$params['costoCardQuantita'] = '15000'; $indice = 9;}
		else											{$params['costoCardQuantita'] = '20000'; $indice = 10;}

		$costo += $this->_quantity[$params['costoCardQuantita']];
		if ($indice < 5)
		{
			$costo += $this->_fronte[$params['colorifronte']][$indice];
			$costo += $this->_retro[$params['coloriretro']][$indice];
		}
		else
		{
			$costo += $this->_fronte['quad'][$indice];
		}
		if (($params['tipostampa'] != 'off') && ($indice > 4)) return 'selezionato termo x più di 1000pz';

		if ($params['colorifronte'] != 'blnk') $costo += $this->_tipostampa[$params['tipostampa']][$indice];
		if ($params['coloriretro'] != 'blnk')  $costo += $this->_tipostampa[$params['tipostampa']][$indice];
		$costo += $this->_spessore[$params['spessoreCard']];
		$costo += $this->_banda[$params['bandamagnetica']][$indice];
		$costo += $this->_serigrafico[$params['coloreserigrafico']][$indice];
		$costo += $this->_stampaacaldo[$params['stampaacaldo']][$indice];

		/**
		 * Richiesta di semplificare, il costo del pvc va aggiunto anche per .40mm
		 */
		//if ($params['spessoreCard'] == '76') {
			if ($params['chipcontatto'] == 'no' && $params['chipcrfid'] == 'no')
				$costo += $this->_pvc['pvc'][$indice];

			$costo += $this->_chipacontatto[$params['chipcontatto']][$indice];
			$costo += $this->_chiprfid[$params['chipcrfid']][$indice];
		// }// if

		$costo += $this->_firma[$params['firma']][$indice];
		$costo += $this->_punzonatura[$params['punzonatura']][$indice];
		$costo += $this->_persdativariabili[$params['persdativariabili']][$indice];
		$costo += $this->_persstampafoto[$params['persstampafoto']][$indice];
		//$costo += $this->_foto[$params['foto']][$indice];
		$costo += $this->_codificamagnetica[$params['codificamagnetica']][$indice];
		$costo += $this->_codificadatichip[$params['codificadatichip']][$indice];
		$costo += $this->_asolatura[$params['asolaturacard']][$indice];


		//Se è un rivenditore non gli applico l'iva
		if ($customerGroupCode != null)
		{
			if($customerGroupCode == 'Rivenditori')
			{
				return $costo;
			}
		}

		return (int)($costo * 1.21);
	}// function
}// class





