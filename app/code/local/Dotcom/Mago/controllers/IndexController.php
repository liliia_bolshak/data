<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dotcom_Mago_IndexController extends Mage_Core_Controller_Front_Action
{
	const XML_PATH_ENABLED          = 'mago/mago/enabled';

	const XML_PATH_INFORDATA_EMAIL_TEMPLATE   	= 'mago/email/infordata_template';
	const XML_PATH_CUSTOMER_EMAIL_TEMPLATE   	= 'mago/email/customer_template';
	const XML_PATH_EMAIL_RECIPIENT  	= 'mago/email/recipient_email';
	const XML_PATH_EMAIL_SENDER_EMAIL  	= 'mago/email/noreply_email';
	const XML_PATH_EMAIL_SENDER_NAME	= 'mago/email/sender_name';

    

    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
            $this->norouteAction();
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('dotcom.mago')
            ->setFormAction( Mage::getUrl('*/*/post') );

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }
    
    /*
     * TODO:
     *  - Implemntare la validazione
     */
    public function postAction()
    {
    	/* @var $modelCard Dotcom_Mago_Model_Card */
    	$modelCard = Mage::getModel("dotcom_mago/card");
    	
    	
    	//normalizzo i dati
    	$params = $this->getRequest()->getParams();
    	unset($params['captcha']);
    	unset($params['no_csrf_baz']);
    	$params['costoCardQuantita'] = (!empty($params['costoCardQuantita'])) ? $params['costoCardQuantita'] : 0;
    	
    	//Verifico i parametri
    	if (/*!$form->isValid($this->_getAllParams()) ||*/ ($params['costoCardQuantita'] < 1)){
    		$response['error'] = 'I dati per il calcolo sono errati, riprova.';
    	}
    	else
    	{
    		$costo = $modelCard->calcola($params);
    	    	
	    	if (!is_int($costo))
	    	{
	    		if (is_string($response['costo']))
	    		{
	    			$response['error'] = 'Errore: '.$response['costo'];
	    		}
	    		else 
	    		{
	    			$response['error'] = 'C\'è un\'incoerenza nel calcolo, riprova.';
	    		}
	    	}
	    	else
	    	{
	    		$response['costoCardQuantita'] = $params['costoCardQuantita'];
	    		$response['costo'] = $costo;
	    	}
    	}
    	$this->getResponse()->setBody(Zend_Json::encode($response));
    }
    
    
    public function cqsAction()
    {
    	$params = $this->getRequest()->getParams();

    	if ($params['security_code'] != $params['captacha_code'])
    	{
    		throw new Exception("Codice di Sicurezza errato!");
    	}
    	
    	$params['costoCardQuantita'] = !empty($params['costoCardQuantita']) ? $params['costoCardQuantita'] : 0; 

    	//customer
    	/* @var $customerSession Mage_Customer_Model_Session */
    	$customerSession = Mage::getSingleton('customer/session');
    	
    	/* @var $customerGroup Mage_Customer_Model_Group */
    	$customerGroup = Mage::getModel('customer/group');
    	
    	$customerGroup->load($customerSession->getCustomer()->getGroupId());
    	$customerGroupCode = $customerGroup->getCustomerGroupCode();  	
	   	//fine customer
    	
		/* @var $modelCard Dotcom_Mago_Model_Card */
		$modelCard = Mage::getModel("dotcom_mago/card");
		    	
		$costo = $modelCard->calcola($params,$customerGroupCode);
		$params['costounitario'] = $costo / 1000 . '€';
		$params['costototale'] = $costo * $params['costoCardQuantita'] / 1000 . '€';
    


		$templateData = $this->prepareTemplateData($params);  		


		//print_r($paramsVO->getData());die;
		/* @var $mailTemplate Mage_Core_Model_Email_Template */
		$mailTemplate = Mage::getModel('core/email_template');
   			
   		//invio la mail ad infordata
   		$mailTemplate->setDesignConfig(array('area' => 'frontend'))
			->setReplyTo($params['addresseeEmail'])
            ->sendTransactional(
        Mage::getStoreConfig(self::XML_PATH_INFORDATA_EMAIL_TEMPLATE),
   			array(
   				'name' => $params['name'] . $params['surname']. " - " . $params['company'], 
   				'email' => $params['addresseeEmail']
   			),
            Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
            null,
            array('data' => $templateData)
 		);

   		if (!$mailTemplate->getSentSuccess()) throw new Exception("Errore nell'invio email ad infordata");
   			
   		//invio la mail al cliente
   		$mailTemplate->setDesignConfig(array('area' => 'frontend'))
   			->setReplyTo(Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER_EMAIL))
   			->sendTransactional(
	   			Mage::getStoreConfig(self::XML_PATH_CUSTOMER_EMAIL_TEMPLATE),
	   			array(
	   				'name' => Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER_NAME), 
	   				'email' => Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER_EMAIL)
	   			),
	   			$params['addresseeEmail'],
	   			null,
	   			array('data' => $templateData)
   			);
   		if (!$mailTemplate->getSentSuccess()) throw new Exception("Errore nell'invio email al cliente");
   			
		$this->loadLayout();
		$this->getLayout()->getBlock('dotcom.cqs')->setPostedParams($params);
		$this->renderLayout();

    }
    
    /*
     *  Preparo i dati postati per presentarli nel template
     */
    private function prepareTemplateData($params)
    {
    	$data = new Varien_Object();
    	$data->setData($params);
    	$data->setData('costocardquantita',$params['costoCardQuantita']);
    	$data->setData('spessorecard',$params['spessoreCard']);
    	$data->setData('addresseeemail',$params['addresseeEmail']);
    	foreach (array("colorifronte", "coloriretro") as $key)
    	{
    		switch($params[$key])
    		{
    			case 'blnk': $data->setData($key,'No'); break;
    			case 'mono': $data->setData($key,'Monocromia'); break;
    			default: $data->setData($key,'Quadricromia'); break;
    		}
    		 
    	}
    	foreach (array("firma", "coloreserigrafico", "stampaacaldo", "asolaturacard", "punzonatura", "codificamagnetica", "codificadatichip") as $key)
    	{
    		$data->setData($key,($params[$key] == "true" ? "Si":"No"));
    	}
    	switch ($params["tipostampa"])
    	{
    		case 'off': $data->setData("tipostampa",'Offset');; break;
    		case 'dir': $data->setData("tipostampa",'Termografica Diretta'); break;
    		case 'ind': $data->setData("tipostampa",'Termografica Indiretta'); ;break;
    	}
    	$data->setData('today',date("j/m/Y"));
    	return $data;
    }   

}
