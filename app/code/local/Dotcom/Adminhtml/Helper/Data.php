<?php
class Dotcom_Adminhtml_Helper_Data
{
    private $_categories = array();
    
	public function getTypeArieteProductArray()
	{
		return array(
				'3' => 'Prodotti',
				'4' => 'Accessori',
				'5' => 'Consumabili',
		        '6' => 'Ricambi'
		);
	}
	
	public function getCategories() {
	    if (count($this->_categories)) {
	        return $this->_categories;
	    }
	    foreach (Mage::app()->getWebsites() as $website) {
	        $rootId = $website->getDefaultStore()->getRootCategoryId();
	        $rootCat = Mage::getModel('catalog/category')->load($rootId);
	        $this->_categories[$rootId] = $rootCat->getName();
	        $this->getChildCats($rootCat, 0);
	    }
	    return $this->_categories;
	}
	
	public function getChildCats($cat, $level) {
	    if ($children = $cat->getChildren()) {
	        $level++;
	        $children = explode(',', $children);
	        foreach ($children as $childId) {
	            $childCat = Mage::getModel('catalog/category')->load($childId);
	            $this->_categories[$childId] = str_repeat('-', $level) . $childCat->getName();
	            if ($childCat->getChildren()) {
	                $this->getChildCats($childCat, $level);
	            }
	        }
	    }
	}
}
