<?php
class Dotcom_Adminhtml_Block_Sales_Shipment_Grid extends Mage_Adminhtml_Block_Sales_Shipment_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->getColumn("increment_id")->setIndex("numero_ariete");
        return $this;
    }
}
