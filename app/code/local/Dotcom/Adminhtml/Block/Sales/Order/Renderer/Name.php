<?php

class Dotcom_Adminhtml_Block_Sales_Order_Renderer_Name extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    protected $_addressCollection;
    
    public function render(Varien_Object $row)
    {
        $columnId = $this->getColumn()->getId();
        $columnIndex = $this->getColumn()->getIndex() == "billing_name" ? "billing" : "shipping";
        
        if(!isset($this->_addressCollection[$row->getId()])){
            $collection = Mage::getModel("sales/order_address")->getCollection()->addFieldToFilter("parent_id",$row->getId());
            foreach ($collection as $item){
                $this->_addressCollection[$item->getParentId()][$item->getAddressType()] = $item;
                
            }  
        }
        
        $address = $this->_addressCollection[$row->getId()];
        
        if($address[$columnIndex]->getCompany()){
            return $address[$columnIndex]->getCompany();
        }
        else {
            return $address[$columnIndex]->getFirstname()." ".$address[$columnIndex]->getLastname();
        }
        
    }
}