<?php
class Dotcom_Adminhtml_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        $this->getColumn("billing_name")
            ->setData("renderer", "Dotcom_Adminhtml_Block_Sales_Order_Renderer_Name")
            ->setData("filter_condition_callback", array($this, "_filterCallbackBilling"));

        $this->getColumn("shipping_name")
            ->setData("renderer", "Dotcom_Adminhtml_Block_Sales_Order_Renderer_Name")
            ->setData("filter_condition_callback", array($this, "_filterCallbackShipping"));
        
        return $this;
    }
    
    protected function _filterCallbackShipping($collection, $column){
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->join(array('shipping' => 'sales/order_address'), 'main_table.entity_id = shipping.parent_id AND shipping.address_type = "shipping"', array('shipping.company', 'shipping.firstname', 'shipping.lastname'))
        ->getSelect()->where("shipping.company LIKE '%$value%'")->orWhere("shipping.firstname LIKE '%$value%'")->orWhere("shipping.lastname LIKE '%$value%'");
        return $this;
    }
    
    protected function _filterCallbackBilling($collection, $column){
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->join(array('billing' => 'sales/order_address'), 'main_table.entity_id = billing.parent_id AND billing.address_type = "billing"', array('billing.company', 'billing.firstname', 'billing.lastname'))
        ->getSelect()->where("billing.company LIKE '%$value%'")->orWhere("billing.firstname LIKE '%$value%'")->orWhere("billing.lastname LIKE '%$value%'");
        return $this;
    }
}
