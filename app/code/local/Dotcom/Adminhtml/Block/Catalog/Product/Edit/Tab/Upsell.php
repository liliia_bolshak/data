<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Upsell products admin grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dotcom_Adminhtml_Block_Catalog_Product_Edit_Tab_Upsell  extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Upsell
{
    public function setCollection($collection)
    {
        $collection->calculateSizeWithoutGroupClause = true;
        $collection->addAttributeToSelect(array('type_product_ariete','codcat'));
        $collection->getSelect()
        ->joinLeft(array('category' => $collection->getTable('catalog/category_product')),
                'e.entity_id = category.product_id',
                array('cat_ids' => 'GROUP_CONCAT(category.category_id)'))
                ->group('e.entity_id');
        parent::setCollection($collection);
    }
    
    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        $this->removeColumn("set_name");
        
        $this->addColumnAfter('type_product_ariete', array(
                'header'=> Mage::helper('catalog')->__('Type Product Ariete'),
                'index' => 'type_product_ariete',
                'type'  => 'options',
                'options' => Mage::helper('dotcom_adminhtml')->getTypeArieteProductArray(),
                'width' => '50px',
        ),'type');
         
        $this->addColumnAfter('codcat', array(
                'header'=> Mage::helper('sales')->__('Marca'),
                'index' => 'codcat',
                'width' => '50px',
        ),'type');
         
        $this->addColumnAfter('category', array(
                'header' => 'Category',
                'index' => 'cats',
                'type' => 'options',
                'options' => Mage::helper('dotcom_adminhtml')->getCategories(),
                'renderer' => 'mageworx/customoptions_options_edit_tab_renderer_prodcat',
                'filter_condition_callback' => array($this, 'category_filter')
        ), 'type');
         
        return parent::_prepareColumns();
    }
    
    public function category_filter($collection, $column) {
        $cond = $column->getFilter()->getCondition();
        if (empty($cond['eq'])) {
            return true;
        }
        $where = 'category.category_id = ' . $cond['eq'];
        $collection->getSelect()->where($where);
    }

}
