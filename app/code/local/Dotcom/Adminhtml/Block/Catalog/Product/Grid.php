<?php

class Dotcom_Adminhtml_Block_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid
{
	public function setCollection($collection)
	{
	    $collection->calculateSizeWithoutGroupClause = true; //fix paginazione prodotti. See local/Mage/Catalog/Model/Product/Collection function getSelectCountSql()
		$collection->addAttributeToSelect(array('type_product_ariete','codcat'));
		$collection->getSelect()
		->joinLeft(array('category' => $collection->getTable('catalog/category_product')),
		        'e.entity_id = category.product_id',
		        array('cat_ids' => 'GROUP_CONCAT(category.category_id)'))
		        ->group('e.entity_id');
		parent::setCollection($collection);
	}

	protected function _prepareColumns()
	{
		parent::_prepareColumns();

		$this->removeColumn("set_name");
		
		$this->addColumn('type_product_ariete', array(
				'header'=> Mage::helper('sales')->__('Ariete Product Type'),
				'index' => 'type_product_ariete',
				'type'  => 'options',
				'options' => Mage::helper('dotcom_adminhtml')->getTypeArieteProductArray(),
				'width' => '50px',
		));
		
		$this->addColumn('category', array(
		        'header' => 'Category',
		        'index' => 'cats',
		        'type' => 'options',
		        'options' => Mage::helper('dotcom_adminhtml')->getCategories(),
		        'renderer' => 'mageworx/customoptions_options_edit_tab_renderer_prodcat',
		        'filter_condition_callback' => array($this, 'category_filter')
		));
		
		
		$this->addColumn('codcat', array(
		        'header'=> Mage::helper('sales')->__('Marca'),
		        'index' => 'codcat',
		        'width' => '50px',
		));
		
		//return parent::_prepareColumns();
	}
	
	public function category_filter($collection, $column) {
	    $cond = $column->getFilter()->getCondition();
	    if (empty($cond['eq'])) {
	        return true;
	    }
	    $where = 'category.category_id = ' . $cond['eq'];
	    $collection->getSelect()->where($where);
	}
}
