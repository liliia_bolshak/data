<?php

class Dotcom_Adminhtml_Block_Catalog_Product_Edit_Tab_Related extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Related
{
    
	public function setCollection($collection)
	{
	    $collection->calculateSizeWithoutGroupClause = true;
		$collection->addAttributeToSelect(array('type_product_ariete','codcat'));
		$collection->getSelect()
		->joinLeft(array('category' => $collection->getTable('catalog/category_product')),
		        'e.entity_id = category.product_id',
		        array('cat_ids' => 'GROUP_CONCAT(category.category_id)'))
		        ->group('e.entity_id');
		parent::setCollection($collection);
	}

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        
        $this->removeColumn("set_name");
        
    	$this->addColumnAfter('type_product_ariete', array(
    			'header'=> Mage::helper('catalog')->__('Type Product Ariete'),
    			'index' => 'type_product_ariete',
    			'type'  => 'options',
				'options' => Mage::helper('dotcom_adminhtml')->getTypeArieteProductArray(),
    			'width' => '50px',
    	),'type');
    	
    	$this->addColumnAfter('codcat', array(
    	        'header'=> Mage::helper('sales')->__('Marca'),
    	        'index' => 'codcat',
    	        'width' => '50px',
    	),'type');
    	
    	$this->addColumnAfter('category', array(
    	        'header' => 'Category',
    	        'index' => 'cats',
    	        'type' => 'options',
    	        'options' => Mage::helper('dotcom_adminhtml')->getCategories(),
    	        'renderer' => 'mageworx/customoptions_options_edit_tab_renderer_prodcat',
    	        'filter_condition_callback' => array($this, 'category_filter')
    	), 'type');
    	
    }
    
    public function category_filter($collection, $column) {
        $cond = $column->getFilter()->getCondition();
        if (empty($cond['eq'])) {
            return true;
        }
        $where = 'category.category_id = ' . $cond['eq'];
        $collection->getSelect()->where($where);
    }
}
