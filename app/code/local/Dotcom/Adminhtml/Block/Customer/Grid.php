<?php
class Dotcom_Adminhtml_Block_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid
{
	public function setCollection($collection)
	{
		$collection->addAttributeToSelect('company')
		->addAttributeToSelect('taxvat');
		parent::setCollection($collection);
	}

	protected function _prepareColumns()
	{
		parent::_prepareColumns();

		$this->addColumn('company', array(
				'header'=> Mage::helper('sales')->__('Azienda'),
				'index' => 'company',
				'type'  => 'text',
				'width' => '100px',
		));
		
		$this->addColumn('taxvat', array(
				'header'=> Mage::helper('sales')->__('P.IVA'),
				'index' => 'taxvat',
				'type'  => 'text',
				'width' => '150px',
		));
		
		return parent::_prepareColumns();
	}
}