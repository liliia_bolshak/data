<?php

class Dotcom_Sitemap_Model_Sitemap extends Mage_Sitemap_Model_Sitemap
{
	
protected function _beforeSave()
    {
    	$dati = $this->getData();
	    $io = new Varien_Io_File();
	         	
	    $realPath = $io->getCleanPath(Mage::getBaseDir("media") . '/' . ltrim($this->getSitemapPath(), '/'));
	    
        /**
         * Check exists and writeable path
         */
        if (!$io->fileExists($realPath, false)) {
            Mage::throwException(Mage::helper('sitemap')->__('Please create the specified folder "%s" before saving the sitemap.', Mage::helper('core')->htmlEscape($this->getSitemapPath())));
        }

        if (!$io->isWriteable($realPath)) {
            Mage::throwException(Mage::helper('sitemap')->__('Please make sure that "%s" is writable by web-server.', $this->getSitemapPath()));
        }
        /**
         * Check allow filename
         */
        if (!preg_match('#^[a-zA-Z0-9_\.]+$#', $this->getSitemapFilename())) {
            Mage::throwException(Mage::helper('sitemap')->__('Please use only letters (a-z or A-Z), numbers (0-9) or underscore (_) in the filename. No spaces or other characters are allowed.'));
        }
        if (!preg_match('#\.xml$#', $this->getSitemapFilename())) {
            $this->setSitemapFilename($this->getSitemapFilename() . '.xml');
        }

        $this->setSitemapPath(trim($this->getSitemapPath(), '/') . '/');

        return $this;
    }
	/**
     * Return real file path
     *
     * @return string
     */
    protected function getPath()
    {
        if (is_null($this->_filePath)) {
            $this->_filePath = str_replace('//', '/', Mage::getBaseDir('media') .
                $this->getSitemapPath());
        }
        return $this->_filePath;
    }
}
