<?php

class Dotcom_Pagamentostandard_Model_Standard extends Mage_Payment_Model_Method_Abstract
{

    protected $_code  = 'pagamentostandard';
    protected $_formBlockType = 'pagamentostandard/form_standard';

    public function isAvailable($quote = null)
    {
        $codpag = Mage::getSingleton('customer/session')->getCustomer()->getCodpagAriete();
        return parent::isAvailable($quote) && isset($codpag);
    }
}
