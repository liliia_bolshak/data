<?php 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("

    DROP TABLE IF EXISTS `".$installer->getTable("arieteinvoices/invoice")."`;

    CREATE TABLE `".$installer->getTable("arieteinvoices/invoice")."` (
      `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `ddt_id` varchar(255) NOT NULL,
      `annreg` varchar(255) NOT NULL,
      `nregis` varchar(255) NOT NULL,
      `data_ddt` datetime,
      `totmerce` decimal(12,4) NOT NULL,
      `id_fatt_ariete` varchar(255) NOT NULL,
      `data_fattura` datetime,
      `annreg_fattura` varchar(255),
      `nregis_fattura` varchar(255)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    
    DROP TABLE IF EXISTS `ariete_orders_ddt`;
    
    CREATE TABLE IF NOT EXISTS `ariete_orders_ddt` (
        `order_id` int(11) NOT NULL,
        `ddt_id` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  ");


$installer->endSetup();
