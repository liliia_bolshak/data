<?php 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `".$installer->getTable("arieteinvoices/invoice")."` ADD `lvettura` VARCHAR(255) AFTER `data_ddt`;
  ");


$installer->endSetup();
