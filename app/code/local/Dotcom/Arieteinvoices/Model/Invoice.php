<?php

class Dotcom_Arieteinvoices_Model_Invoice extends Mage_Core_Model_Abstract
{
	protected function _construct() {
		$this->_init ( 'arieteinvoices/invoice' );
	}
    
	/**
	 * Carica un certo DDT legato a un certo ordine
	 * 
	 * @param integer $orderId
	 * @param integer $ddtId
	 */
    public function loadByOrderAndDDTid($orderId, $ddtId)
    {
        /* @var $collection Dotcom_Arieteinvoices_Model_Resource_Invoice_Collection */
        $collection = Mage::getResourceModel('arieteinvoices/invoice_collection');
        $collection->getSelect()->join("ariete_orders_ddt", "ariete_orders_ddt.ddt_id = main_table.ddt_id", 'order_id')
                                ->where('order_id = ?', $orderId)
                                ->where('main_table.ddt_id = ?', $ddtId);
        $item = $collection->getFirstItem();
        if($item->getId())
        {
            $this->setData($item->getData());
        }
        return $this;
    }
}