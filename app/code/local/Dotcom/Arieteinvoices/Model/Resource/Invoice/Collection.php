<?php

class Dotcom_Arieteinvoices_Model_Resource_Invoice_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('arieteinvoices/invoice');
    }
    
    public function filterByOrderId($orderId)
    {
        $this->getSelect()->join("ariete_orders_ddt", "ariete_orders_ddt.ddt_id = main_table.ddt_id")->where("order_id = ?", $orderId);
        return $this;
    }
}
