<?php

class Dotcom_Arieteinvoices_Model_Resource_Invoice extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('arieteinvoices/invoice', 'id');
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        if($object->getDdtId() && $object->getOrderId())
        {
            $select = $this->_getReadAdapter()->select()
                                            ->from('ariete_orders_ddt')
                                            ->where('order_id = ?', $object->getOrderId())
                                            ->where('ddt_id = ?', $object->getDdtId());
            $data = $this->_getReadAdapter()->fetchRow($select);
            
            if (!$data) {
                $this->_getWriteAdapter()->insert('ariete_orders_ddt', array('order_id' => $object->getOrderId(), 'ddt_id' => $object->getDdtId()));
            }
        }
        parent::_afterSave($object);
        return $this;
    }
}
