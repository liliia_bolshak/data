<?php 
class Dotcom_Arieteinvoices_Block_Invoice extends Mage_Core_Block_Template
{
    public function getDdtCollection($orderId){
         $collection = Mage::getModel('arieteinvoices/invoice')->getCollection()->filterByOrderId($orderId);
         return  $collection;
    }

        public function checkValidTrack($url)
    {
        $xml = file_get_contents($url);
        try {
            $doc = @simplexml_load_string($xml);
            $valid = true;
        } catch (Exception $e) {
            $valid = false;
        }

        return $valid;
    }
}