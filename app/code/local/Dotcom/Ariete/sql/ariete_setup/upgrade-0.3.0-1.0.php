<?php
//la tabella la uso come log delle sincronizzazioni
//il type export si riferisce al tipo di sincronizzazione (magazzino, segreteria, ...)


$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS `sync`;
    DROP TABLE IF EXISTS `{$installer->getTable('ariete/sync')}`;

    CREATE TABLE `{$installer->getTable('ariete/sync')}` (
      `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `date` datetime NOT NULL,
      `type` varchar(255) NOT NULL,
      `direction` varchar(255) NOT NULL,
      `last_entity_id` int(11) UNSIGNED NOT NULL,
      `log` varchar(255) NOT NULL default ''
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();
