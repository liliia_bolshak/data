<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

//modifiche a certi attributi
$setup->updateAttribute('catalog_product', 'type_product_ariete',  array(
		'frontend_input'         => 'select',
));

$installer->endSetup();
