<?php
/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = new Mage_Customer_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute('customer_address', 'ariete_id', array(
        'label'         => 'Ariete ID'
));

$installer->endSetup();
