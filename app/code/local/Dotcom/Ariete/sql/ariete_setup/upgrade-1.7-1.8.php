<?php
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'ariete_codcat'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('ariete/codcat'))
    ->addColumn('codice', Varien_Db_Ddl_Table::TYPE_TEXT, 5, array(
        'nullable'  => false,
        'primary'   => true,
        'default'   => '',
    ))
    ->addColumn('descri', Varien_Db_Ddl_Table::TYPE_TEXT, 256, array(
        'nullable'  => false,
        'default'   => '',
    ))
    ->addColumn('aggsta', Varien_Db_Ddl_Table::TYPE_TEXT, 256, array(
        'nullable'  => false,
        'default'   => '',
    ));
$installer->getConnection()->createTable($table);


$setup = new Mage_Sales_Model_Resource_Setup('core_setup');
$setup->addAttribute('catalog_product', 'codcat', array(
        'group'         => 'Ariete attributes',
        'input'         => 'select',
        'type'          => 'varchar',
        'label'         => 'Ariete codcat',
        'source'        => 'ariete/resource_attribute_source_codcat',
        'visible'       => 1,
		'required'      => 0,
		'user_defined' => 1,
		'searchable' => 0,
		'filterable' => 0,
		'comparable'    => 0,
		'visible_on_front' => 0,
		'visible_in_advanced_search'  => 0,
		'is_html_allowed_on_front' => 0,
		'used_in_product_listing' => 1,
		'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
));

$installer->endSetup();
