<?php
/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = new Mage_Customer_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->addAttribute('customer_address', 'ariete_codice', array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Ariete Codice',
        'visible'       => true,
        'required'      => false
));

$installer->addAttribute('customer', 'ariete_last_import', array(
        'input'         => 'date',
        'type'          => 'datetime',
        'label'         => 'Data ultimo import da Ariete',
        'backend'       => 'eav/entity_attribute_backend_datetime',
        'validate_rules'    => 'a:1:{s:16:"input_validation";s:4:"date";}',
        'input_filter'      => 'date',
        'visible'       => true,
        'required'      => false
));

$installer->updateAttribute('customer_address', 'ariete_id', 'is_required', 0);
$attribute = Mage::getSingleton('eav/config');
$attribute->getAttribute('customer_address', 'ariete_codice')->setData('used_in_forms', array('adminhtml_customer_address'))->save();
$attribute->getAttribute('customer_address', 'ariete_id')->setData('used_in_forms', array('adminhtml_customer_address'))->save();
$attribute->getAttribute('customer', 'ariete_last_import')->setData('used_in_forms', array('adminhtml_customer'))->save();

$installer->endSetup();