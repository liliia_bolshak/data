<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

//attributo per l'associazione del prodotto con i clienti per creare i "prodotti personali"
$installer->addAttribute('catalog_product', 'ariete_allowed_customers', array(
    'group'         => 'Ariete attributes',
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Clienti collegati',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'searchable' => 0,
    'filterable' => 0,
    'comparable'    => 0,
    'visible_on_front' => 0,
    'visible_in_advanced_search'  => 0,
    'is_html_allowed_on_front' => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
$installer->endSetup();