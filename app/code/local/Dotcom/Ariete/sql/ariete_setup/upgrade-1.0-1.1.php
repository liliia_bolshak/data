<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Add status column to 'ariete_sync' table
 */
$installer->getConnection()->addColumn($installer->getTable('ariete/sync'), 'status', array(
    'comment'   => "Indica se c'è stato un errore nella procedura: 1=chiamata senza errori, 0=la chiamata ha generato errori",
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'default'   => 1
));    

$installer->endSetup();
