<?php
//la tabella la uso come log delle sincronizzazioni
//il type export si riferisce al tipo di sincronizzazione (magazzino, segreteria, ...)


$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$installer->run("

    DROP TABLE IF EXISTS `{$this->getTable('tblariete')}`;

    CREATE TABLE `{$this->getTable('tblariete')}` (
      `import_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `status` smallint(6) NOT NULL default '0',
      `created_time` datetime NULL,
      `type_import` varchar(255) NOT NULL default '',
      `log` varchar(255) NOT NULL default ''
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  ");



$setup->addAttributeGroup('catalog_product', 'Default', 'Ariete attributes', 1000);

//attributo per l'associazione del prodotto con il prodotto in Ariete
$setup->addAttribute('catalog_product', 'ariete_id_product_attribute', array(
    'group'         => 'Ariete attributes',
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Ariete id product',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'searchable' => 1,
    'filterable' => 0,
    'comparable'    => 1,
    'visible_on_front' => 1,
    'visible_in_advanced_search'  => 0,
    'is_html_allowed_on_front' => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

//categoria prodotto Prodottti, consumabili, accessori ...
// $setup->addAttribute('catalog_product', 'type_product_ariete', array(
// 		'group'         => 'Ariete attributes',
// 		'input'         => 'text',
// 		'type'          => 'text',
// 		'label'         => 'Ariete product type',
// 		'backend'       => '',
// 		'visible'       => 1,
// 		'required'      => 0,
// 		'user_defined' => 1,
// 		'searchable' => 1,
// 		'filterable' => 1,
// 		'comparable'    => 1,
// 		'visible_on_front' => 1,
// 		'visible_in_advanced_search'  => 1,
// 		'is_html_allowed_on_front' => 0,
// 		'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
// ));

//aggiungo un nuovo attributo alla categoria
$setup->addAttribute('catalog_category', 'id_cat_ariete',  array(
    'type'     => 'int',
    'label'    => 'Id categoria Ariete',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
    'default'           => 0
));

$installer->endSetup();
