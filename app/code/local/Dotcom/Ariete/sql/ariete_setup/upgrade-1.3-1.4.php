<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

//modifiche a certi attributi
$setup->updateAttribute('customer', 'taxvat', 'is_required', 1);
$setup->updateAttribute('customer', 'ref_contab', 'frontend_label', 'Email ref. contabilità');
$setup->updateAttribute('customer', 'ref_acquisti', 'frontend_label', 'Email ref. acquisti');
$setup->updateAttribute('customer', 'ref_tecnico', 'frontend_label', 'Email ref. tecnico');

$installer->endSetup();
