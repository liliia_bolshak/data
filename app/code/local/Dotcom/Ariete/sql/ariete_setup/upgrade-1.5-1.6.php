<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/* @var $setup Mage_Sales_Model_Resource_Setup */
$setup = new Mage_Sales_Model_Resource_Setup('core_setup');
$setup->addAttribute(
    'shipment', 
    'id_ariete', 
    array(
    	'label'				=> 'Id DDT Ariete',
    	'input'				=> 'text',
        'type' => 'varchar', /* varchar, text, decimal, datetime */
        'grid' => true, /* or true if you wan't use this attribute on orders grid page */
    	//'default'      		 => '',
    	'visible'			=> true,
    	'visible_on_front'	=> false,
    	'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    	'required'			=> false,
    	'is_user_defined'	=> true
    )
);
$setup->addAttribute(
    'shipment', 
    'numero_ariete', 
    array(
    	'label'				=> 'Numero DDT Ariete',
    	'input'				=> 'text',
        'type' => 'varchar', /* varchar, text, decimal, datetime */
        'grid' => true, /* or true if you wan't use this attribute on orders grid page */
    	//'default'      		 => '',
    	'visible'			=> true,
    	'visible_on_front'	=> false,
    	'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    	'required'			=> false,
    	'is_user_defined'	=> true
    )
);
$setup->addAttribute(
    'invoice', 
    'id_ariete', 
    array(
    	'label'				=> 'Id fattura Ariete',
    	'input'				=> 'text',
        'type' => 'varchar', /* varchar, text, decimal, datetime */
        'grid' => true, /* or true if you wan't use this attribute on orders grid page */
    	//'default'      		 => '',
    	'visible'			=> true,
    	'visible_on_front'	=> false,
    	'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    	'required'			=> false,
    	'is_user_defined'	=> true
    )
);
$setup->addAttribute(
    'invoice', 
    'numero_ariete', 
    array(
    	'label'				=> 'Numero fattura Ariete',
    	'input'				=> 'text',
        'type' => 'varchar', /* varchar, text, decimal, datetime */
        'grid' => true, /* or true if you wan't use this attribute on orders grid page */
    	//'default'      		 => '',
    	'visible'			=> true,
    	'visible_on_front'	=> false,
    	'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    	'required'			=> false,
    	'is_user_defined'	=> true
    )
);

$installer->endSetup();
