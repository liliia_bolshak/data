<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

//eliminare l'attributo id_ariete dalle categorie
$setup->removeAttribute('catalog_category', 'id_ariete');

//modifico il tipo dell'attributo id_cat_ariete da int a varchar perché gli Id Ariete sono alfanumerici.
$setup->updateAttribute('catalog_category', 'id_cat_ariete', 'backend_type', 'varchar')
      ->updateAttribute('catalog_category', 'id_cat_ariete', 'is_unique', 1)
      ->updateAttribute('catalog_category', 'id_cat_ariete', 'is_user_defined', 1)
      ->updateAttribute('catalog_category', 'id_cat_ariete', 'default_value', null)
      ->updateAttribute('catalog_category', 'id_cat_ariete', 'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL)
      ->updateAttribute('catalog_category', 'id_cat_ariete', 'note', 'ID categoria in Ariete (P_codice se si tratta di un raggruppamento oppure _codgru se si tratta di un gruppo)');

//rinominare l'attributo prodotto ariete_id_product_attribute in id_ariete
$setup->updateAttribute('catalog_product', 'ariete_id_product_attribute', 'attribute_code', 'id_ariete')
      ->updateAttribute('catalog_product', 'id_ariete', 'frontend_label', 'ID prodotto Ariete')
      ->updateAttribute('catalog_product', 'id_ariete', 'is_unique', 1)
      ->updateAttribute('catalog_product', 'id_ariete', 'note', 'ID prodotto in Ariete (PI_codice)');


//rendo non obbligatorio il campo weight
$setup->updateAttribute('catalog_product', 'weight', 'is_required', 0);

$installer->endSetup();
