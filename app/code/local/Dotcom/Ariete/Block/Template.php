<?php

class Dotcom_Ariete_Block_Template extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('ariete/grid.phtml');
    }


    protected function _prepareLayout()
    {
        $this->setChild('grid', $this->getLayout()->createBlock('ariete/grid'), 'grid');
    	 
    	return parent::_prepareLayout();
    }

}
