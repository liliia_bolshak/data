<?php

class Dotcom_Ariete_Block_Account_InvoiceList extends Mage_Core_Block_Template
{    
    public function getInvoices()
    {
        try
        {
            return Mage::getModel('ariete/documenti')->loadInvoices(Mage::getSingleton('customer/session')->getCustomer()->getId());
        }
        catch(Exception $e)
        {
            Mage::getSingleton('core/session')->addError($this->__('Errore nella richiesta Soap') . ': ' . $e->getMessage());
        }
    }

    public function getPdfUrl($invoiceId)
    {
        return $this->getUrl('*/*/download', array('id' => $invoiceId, 'type' => 'invoice'));
    }
}