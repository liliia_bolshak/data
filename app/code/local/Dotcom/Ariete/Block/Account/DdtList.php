<?php

class Dotcom_Ariete_Block_Account_DdtList extends Mage_Core_Block_Template
{
    
    public function getDDTs()
    {
        try
        {
            return Mage::getModel('ariete/documenti')->loadDDTs(Mage::getSingleton('customer/session')->getCustomer()->getId());
        }
        catch(Exception $e)
        {
            Mage::getSingleton('core/session')->addError($this->__('Errore nella richiesta Soap') . ': ' . $e->getMessage());
        }
    }

    public function getPdfUrl($ddtId)
    {
        return $this->getUrl('*/*/download', array('id' => $ddtId, 'type' => 'ddt'));
    }
}