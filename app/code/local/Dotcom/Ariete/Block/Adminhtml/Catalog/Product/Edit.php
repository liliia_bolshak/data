<?php

class Dotcom_Ariete_Block_Adminhtml_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
	private $parent;

	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('ariete/product/edit.phtml');
		$this->setId('product_edit');
	}
	
	protected function _prepareLayout()
	{

		$this->parent = parent::_prepareLayout();
		
		$this->setChild('ariete_button',
		$this->getLayout()->createBlock('adminhtml/widget_button')
		->setData(array(
		                    'label'     => Mage::helper('catalog')->__('Ariete'),
		                   // 'onclick'   => 'window.open(\''. $this->getUrl('ariete/adminhtml_ariete/importproduct', array('_current'=>true, 'back'=>null)) .'\')'
							'onclick'   => 'updateAriete(\''.$this->updateAriete().'\')'
		))
		);

 		return $this->parent;
	
	}//_prepareLayout
	
 	public function getArieteButtonHtml()
    {
        return $this->getChildHtml('ariete_button');
    }
    
    public function updateAriete()
    {
    	return $this->getUrl('*/*/arieteupdate', array(
                '_current'   => true,
                'back'       => 'edit',
                'tab'        => '{{tab_id}}',
                'active_tab' => null
    	));
    }

}