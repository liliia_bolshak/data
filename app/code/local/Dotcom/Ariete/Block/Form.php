<?php
class Dotcom_Ariete_Block_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
                                            'id' => 'edit_form',
                                            'action' => $this->getUrl('*/*/import', array('id' => $this->getRequest()->getParam('id'))),
                                            'method' => 'post',
		)
		);
		 
		 $fieldset = $form->addFieldset('import_form', array(
             'legend' =>Mage::helper('ariete')->__('Import data')
        ));
		 
		 $fieldset->addField('type_import', 'select', array(
		           'label'     => Mage::helper('ariete')->__('Type import'),
		           'class'     => 'required-entry',
		           'required'  => true,
		           'name'      => 'type_import',
		           'onclick' => "",
		           'onchange' => "",
		           'value'  => '1',
		           'values' => array('-1'=>'Please Select..','1' => 'Magazzino','2' => 'Segeteria', '3' => 'Documenti', '4' => 'Import Xml', '5' => 'Segreteria Export'),
		           'disabled' => false,
		           'readonly' => false,		           
		           'tabindex' => 1
		 ));
 
        $fieldset->addField('starting_row', 'text', array(
             'label'     => Mage::helper('ariete')->__('Starting row'),
             'class'     => 'required-entry',
             'required'  => true,
             'name'      => 'starting_row',
        	 'after_element_html' => '<small><small>',            
        ));
        
        $fieldset->addField('row_count', 'text', array(
                     'label'     => Mage::helper('ariete')->__('Row count'),
                     'class'     => 'required-entry',
                     'required'  => true,
                     'name'      => 'row_count',
                     'after_element_html' => '<small><small>',
        ));
 
       
		$form->setUseContainer(true);
		$this->setForm($form);
		return parent::_prepareForm();
	}
}