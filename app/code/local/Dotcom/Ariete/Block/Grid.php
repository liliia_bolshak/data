<?php

class Dotcom_Ariete_Block_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('grid');
		$this->setDefaultSort('import_id');
		$this->setSaveParametersInSession(true);

	}


	protected function _prepareCollection()
	{
		$collection = Mage::getModel('ariete/tblAriete')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
			
		$this->addColumn('import_id', array(
            'header'    => Mage::helper('ariete')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'import_id',
		));


		$this->addColumn('created_time', array(
		            'header'    => Mage::helper('ariete')->__('Created time'),
		            'align'     =>'center',
		            'index'     => 'created_time',
		));



		$this->addColumn('status', array(
						            'header'    => Mage::helper('ariete')->__('Status'),
						            'align'     =>'left',
						            'index'     => 'status',
		));

		$this->addColumn('type_export', array(
								            'header'    => Mage::helper('ariete')->__('Type import'),
								            'align'     =>'left',
								            'index'     => 'type_import',
		));


		$this->addColumn('log', array(
						            'header'    => Mage::helper('ariete')->__('Log'),
						            'align'     =>'left',

						            'index'     => 'log',
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/', array('import_id' => $row->getId()));
	}


}

