<?php

class Dotcom_Ariete_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }
    
    public function ddtAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My DDTs'));

        $this->renderLayout();
    }
    
    public function invoiceAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Invoices'));

        $this->renderLayout();
    }

    public function downloadAction()
    {
        try {
            $id = (int) $this->getRequest()->getParam('id');
            $type = $this->getRequest()->getParam('type');
            if (!$id || !$type) {
                throw new Exception("Parametri mancanti");
            }
            switch(strtolower($type))
            {
                case 'ddt':
                    /* @var $document Varien_Object */
                    $document = Mage::getModel('ariete/documenti')->getPdfDDT($id);
                    break;
                case 'invoice':
                    /* @var $document Varien_Object */
                    $document = Mage::getModel('ariete/documenti')->getPdfInvoice($id);
                    break;
                default:
                    throw new Exception("Parametro errato");
            }
            
            return $this->_prepareDownloadResponse($document->getFileName(), base64_decode($document->getFileContent()));
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($this->__('An error occurred while getting requested content.') . ' ' . $e->getMessage());
        }
        return $this->_redirectReferer();
    }    
}
