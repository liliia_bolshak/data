<?php

class Dotcom_Ariete_EmailController extends Mage_Core_Controller_Front_Action
{
    public function downloadAction()
    {
        try {
            $id = (int) $this->getRequest()->getParam('id');
            $type = $this->getRequest()->getParam('type');
            if (!$id || !$type) {
                throw new Exception("Parametri mancanti");
            }
            switch(strtolower($type))
            {
                case 'ddt':
                    /* @var $document Varien_Object */
                    $document = Mage::getModel('ariete/documenti')->getPdfDDT($id);
                    break;
                case 'invoice':
                    /* @var $document Varien_Object */
                    $document = Mage::getModel('ariete/documenti')->getPdfInvoice($id);
                    break;
                case 'order':
                    $document = Mage::getModel('ariete/documenti')->getPdfOrder($id);
                    break;
                default:
                    throw new Exception("Parametro errato");
            }
            
            return $this->_prepareDownloadResponse($document->getFileName(), base64_decode($document->getFileContent()));
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($this->__('An error occurred while getting requested content.') . ' ' . $e->getMessage());
        }
        return $this->_redirectReferer();
    }    
}
