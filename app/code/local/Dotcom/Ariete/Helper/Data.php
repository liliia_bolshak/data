<?

class Dotcom_Ariete_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $_map;
    
    public function mappaturaRegioni(){
        if(is_null($this->_map)){
            $this->_map = $this->getArrayMappatura();
        }
        return $this->_map;
    }
    
    private function getArrayMappatura(){
        return array(
                //Abruzzo
                "CH" => "1000", 
                "AQ" => "1000", 
                "PE" => "1000", 
                "TE" => "1000", 
                //Abruzzo
                
                //Basilicata
                "MT" => "1001", 
                "PZ" => "1001", 
                //Basilicata
        
                //Calabria
                "CZ" => "1002", 
                "CS" => "1002", 
                "KR" => "1002", 
                "RC" => "1002", 
                "VV" => "1002",
                //Calabria
        
                //Campania
                "AV" => "1003",
                "BN" => "1003", 
                "CE" => "1003", 
                "NA" => "1003", 
                "SA" => "1003",
                //Campania
        
                //Emilia Romagna
                "BO" => "1004", 
                "FE" => "1004", 
                "FC" => "1004", 
                "MO" => "1004", 
                "PR" => "1004", 
                "PC" => "1004", 
                "RA" => "1004", 
                "RE" => "1004", 
                "RN" => "1004", 
                //Emilia Romagna
        
                //Friuli Venezia Giulia
                "GO" => "1005", 
                "PN" => "1005", 
                "TS" => "1005", 
                "UD" => "1005", 
                //Friuli Venezia Giulia
        
                //Lazio
                "FR" => "1006", 
                "LT" => "1006", 
                "RI" => "1006", 
                "RM" => "1006", 
                "VT" => "1006", 
                //Lazio
        
                //Liguria
                "GE" => "1007", 
                "IM" => "1007", 
                "SP" => "1007", 
                "SV" => "1007", 
                //Liguria
        
                //Lombardia
                "BG" => "1008", 
                "BS" => "1008", 
                "CO" => "1008", 
                "CR" => "1008", 
                "LC" => "1008", 
                "LO" => "1008", 
                "MN" => "1008", 
                "MI" => "1008", 
                "MB" => "1008", 
                "PV" => "1008", 
                "SO" => "1008", 
                "VA" => "1008", 
                //Lombardia
        
                //Marche
                "AN" => "1009", 
                "AP" => "1009", 
                "FM" => "1009", 
                "MC" => "1009", 
                "PU" => "1009", 
                //Marche
        
                //Molise
                "CB" => "1010", 
                "IS" => "1010", 
                //Molise
        
                //Piemonte
                "AL" => "1011", 
                "AT" => "1011", 
                "BI" => "1011", 
                "CN" => "1011", 
                "NO" => "1011", 
                "TO" => "1011", 
                "VB" => "1011", 
                "VC" => "1011", 
                //Piemonte
        
                //Puglia
                "BA" => "1012", 
                "BT" => "1012", 
                "BR" => "1012", 
                "FG" => "1012", 
                "LE" => "1012", 
                "TA" => "1012", 
                //Puglia
        
                //Sardegna
                "CA" => "1013", 
                "CI" => "1013", 
                "VS" => "1013", 
                "NU" => "1013", 
                "OG" => "1013", 
                "OT" => "1013", 
                "OR" => "1013", 
                "SS" => "1013", 
                //Sardegna
        
                //Sicilia
                "AG" => "1014", 
                "CL" => "1014", 
                "CT" => "1014", 
                "EN" => "1014", 
                "ME" => "1014", 
                "PA" => "1014", 
                "RG" => "1014", 
                "SR" => "1014", 
                "TP" => "1014", 
                //Sicilia
        
                //Toscana
                "AR" => "1015", 
                "FI" => "1015", 
                "GR" => "1015", 
                "LI" => "1015", 
                "LU" => "1015", 
                "MS" => "1015", 
                "PI" => "1015", 
                "PT" => "1015", 
                "PO" => "1015", 
                "SI" => "1015", 
                //Toscana
        
                //Trentino Alto Adige
                "BZ" => "1016", 
                "TN" => "1016", 
                //Trentino Alto Adige
        
                //Umbria
                "PG" => "1017", 
                "TR" => "1017", 
                //Umbria
        
                //Valle d'Aosta
                "AO" => "1018", 
                //Valle d'Aosta
        
                //Veneto//Veneto
                "BL" => "1019", 
                "PD" => "1019", 
                "RO" => "1019", 
                "TV" => "1019", 
                "VE" => "1019", 
                "VR" => "1019", 
                "VI" => "1019" 
                //Veneto
        );
    }
    
    public function getProvinceJson(){
        return Mage::helper('core')->jsonEncode(array("1000","1001","1002","1003","1004","1005","1006","1007","1008","1009","1010","1011","1012","1013","1014","1015","1016","1017","1018","1019"));        
    }
    
    public function mappaIdRegioni(){
        return array("1000","1001","1002","1003","1004","1005","1006","1007","1008","1009","1010","1011","1012","1013","1014","1015","1016","1017","1018","1019");
    }
    
    public function getRegionIdFromAriete($provincia){
        $mappa = $this->mappaturaRegioni();
        if($provincia){
            if(array_key_exists($provincia, $mappa)){
                return (int)$mappa[$provincia];
            }
        }
        return false;
    }
    
}
