<?php
class Dotcom_Ariete_Model_Resource_Attribute_Source_Codcat extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * Retreive all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('ariete/codcat_collection')->toOptionArray();
        }
        return $this->_options;
    }
}
