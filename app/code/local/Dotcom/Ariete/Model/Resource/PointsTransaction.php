<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Points
 * @version    1.6.3
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class Dotcom_Ariete_Model_Resource_PointsTransaction extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('ariete/pointsTransaction', 'id');
    }

    public function loadByOrderIncrementId($transaction, $orderIncrementId)
    {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('transaction_orderspend'))
                ->where('order_increment_id = ?', $orderIncrementId);
        $data = $this->_getReadAdapter()->fetchRow($select);
        if (isset($data['transaction_id'])) {
            $transaction->load($data['transaction_id']);
            $transaction->setPointsToMoney($data['points_to_money']);
        }
        return $this;
    }
    
    public function updateSpendOrderInfo($transaction, $orderNumber)
    {
        $data = array(
            'order_increment_id' => $orderNumber
        );
        $this->_getWriteAdapter()->update($this->getTable('transaction_orderspend'), $data, "transaction_id=" . $transaction->getId());
        return $this;
    }

}
