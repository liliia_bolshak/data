<?php

class Dotcom_Ariete_Model_Resource_Sync_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ariete/sync');
    }
    
}
