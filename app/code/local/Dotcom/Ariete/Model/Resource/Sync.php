<?php

class Dotcom_Ariete_Model_Resource_Sync extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Init Resource model and connection
     *
     */
    protected function _construct()
    {
        $this->_init('ariete/sync', 'id');
    }

}
