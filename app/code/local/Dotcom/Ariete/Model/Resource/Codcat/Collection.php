<?php

class Dotcom_Ariete_Model_Resource_Codcat_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ariete/codcat');
    }
    
    public function getItemById($codice)
    {
        foreach ($this->_items as $item) {
            if ($item->getCodice() == $codice) {
                return $item;
            }
        }
        return Mage::getResourceModel('ariete/codcat');
    }
    
    /**
     * Convert collection items to select options array
     *
     * @param string $emptyLabel
     * @return array
     */
    public function toOptionArray($emptyLabel = ' ')
    {
        $options = $this->_toOptionArray('codice', 'descri', array('tipo'=>'aggsta'));
    
        $sort = array();
        foreach ($options as $data) {
            $name = $data['value'];
            if (!empty($name)) {
                $sort[$name] = $data['label'];
            }
        }
    
        Mage::helper('core/string')->ksortMultibyte($sort);
        $options = array();
        foreach ($sort as $value=>$label) {
            $options[] = array(
                    'value' => $value,
                    'label' => $label
            );
        }
    
        if (count($options) > 0 && $emptyLabel !== false) {
            array_unshift($options, array('value' => '', 'label' => $emptyLabel));
        }
    
        return $options;
    }
}
