<?php 
class Dotcom_Ariete_Model_DataDb extends Mage_Core_Model_Abstract
{
	protected function _construct()
	{
		$this->_init('ariete/DataDb');
	}
	
	
	public function saveData($data){
		$log = 'Importa da ' . $data['starting_row'] . ' ,n. righe:  ' . $data['row_count'];
 		$model = Mage::getModel('ariete/tblAriete');
 		$type_import='Magazzino';
 		switch ($data['type_import']) {
 			case 1:
 				$type_import='Magazzino';
 				break;
 			case 2:
 				$type_import='Segreteria';
 				break;
 			case 3:
 				$type_import='Documenti';
 				break;
 			case 4:
				$type_import = 'Import Xml';
				break;
			case 5:
				$type_import = 'Segreteria Export';
				break;
 		}
 		$model->setTypeImport($type_import);
 		$model->setLog($log);
 		$model->setCreatedTime(now());
 		$model->setStatus(1);
 		$model->save();
	}
	

}