<?php

class Dotcom_Ariete_Model_Sync extends Mage_Core_Model_Abstract
{
    /**
    * @method string getId()
    * @method Dotcom_Ariete_Model_Import setId(string $value)
    * @method string getLastEntityId()
    * @method Dotcom_Ariete_Model_Import setLastEntityId(string $value)
    * @method string getDirection()
    * @method Dotcom_Ariete_Model_Import setDirection(string $value)
    * @method string getDate()
    * @method Dotcom_Ariete_Model_Import setDate(string $value)
    * @method string getType()
    * @method Dotcom_Ariete_Model_Import setType(string $value)
    * @method string getLog()
    * @method Dotcom_Ariete_Model_Import setLog(string $value)
    */
    
    const DIRECTION_IMPORT = "import";
    const DIRECTION_EXPORT = "export";
    
	protected function _construct() {
		$this->_init ( 'ariete/sync' );
	}
	
	/**
	 * Restituisce l'ultima referenza dell'operazione di import su un certo oggetto (ordine, cliente, ecc.)
	 * 
	 * @param string $type Tipo di sincronizzazione (Orders, Clients, ecc.). Valore che comapre nella colonna type della tabella ariete_sync.
	 * @return string
	 */
	public function getLastImportedEntityId($type)
	{
		return $this->getLastSynchronizedEntityId($type, self::DIRECTION_IMPORT);
	}
	
	/**
	 * Restituisce l'ultima referenza dell'operazione di export su un certo oggetto (ordine, cliente, ecc.)
	 * 
	 * @param string $type Tipo di sincronizzazione (Orders, Clients, ecc.). Valore che comapre nella colonna type della tabella ariete_sync.
	 * @return string
	 */
	public function getLastExportedEntityId($type)
	{
		return $this->getLastSynchronizedEntityId($type, self::DIRECTION_EXPORT);
	}
	
	/**
	 * Restituisce l'ultima referenza dell'operazione di sincronizzazione su un certo oggetto (ordine, cliente, ecc.)
	 * 
	 * @param string $type Tipo di sincronizzazione (Orders, Clients, ecc.). Valore che comapre nella colonna type della tabella ariete_sync.
	 * @param string $direction Direzione di sincronizzazione, import oppure export.
	 * @return string
	 */
	public function getLastSynchronizedEntityId($type, $direction)
	{
	    /* @var $collection Dotcom_Ariete_Model_Resource_Sync_Collection */
	    $collection = $this->getCollection();
		return $collection->addFieldToFilter('type', array('eq' => $type))
                          ->addFieldToFilter('direction', array('eq' => $direction))
                          ->setOrder('date', 'DESC')
                          ->setPageSize(1)
		                  ->getFirstItem()
		                  ->getLastEntityId();
	}
	
	/**
	 * Restituisce la collection filtrate per tipo di oggetto (ordini, clienti, ecc.)
	 * 
	 * @param string $type Tipo di sincronizzazione (Orders, Clients, ecc.). Valore che comapre nella colonna type della tabella ariete_sync.
	 * @return Dotcom_Ariete_Model_Resource_Sync_Collection
	 */
	private function _getFilteredCollection($type=null)
	{
	    if(!$this->_collection)
	    {
	        $this->_collection = $this->getCollection();
	    }
	    return $this->_collection;
	}
	
	protected function _beforeSave()
	{
	    if(!$this->getDate())
	    {
	        $this->setDate(date('Y-m-d H:i:s'));
	    }
	    return parent::_beforeSave();
	}
}