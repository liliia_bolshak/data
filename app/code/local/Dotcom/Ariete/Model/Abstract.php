<?php
$extlib = Mage::getModuleDir ( 'lib', 'Dotcom_Ariete' ) . '/lib';
require_once ($extlib . '/nusoap/nusoap.php');
require_once ($extlib . '/nusoap/class.nusoapclient.php');

abstract class Dotcom_Ariete_Model_Abstract
{
	protected $client;
	protected $login;
	protected $namespace = "http://www.erriquez.com/";
	protected $wsdl;
	
	const SYNC_TYPE_ORDERS = "Orders"; 
	const SYNC_TYPE_INVOICES = "Invoices"; 
	const SYNC_TYPE_DDTS = "DDTs"; 
	const SYNC_TYPE_CLIENTS = "Clients"; 
	const SYNC_TYPE_PRODUCTS = "Products"; 
	const SYNC_TYPE_CATEGORIES = "Categories";
	const SYNC_TYPE_PRICES = "Prices"; 
	const SYNC_TYPE_QUANTITIES = "Quantities"; 
	
	/**
	 * Model per il salvattaggio dei log di import/export da e verso Ariete
	 * 
	 * @var Dotcom_Ariete_Model_Sync
	 */
	protected $_syncModel;
	
	public function __construct() {
		$this->init();
		if(!$this->wsdl) throw new Exception("Non è stato specificato il WSDL in fase di inizializzione del servizio");	
		$this->client = new nusoap_client($this->wsdl, true, false, false, false, false, 0, 180 );
		$this->client->decodeUTF8(false);
		$this->login = array (
			'PortaComune'  => Mage::getStoreConfig ( 'ariete_section/arietelogin_group/loginportacomune_field' ),
			'HostComune'   => Mage::getStoreConfig ( 'ariete_section/arietelogin_group/loginhostcomune_field' ),
			'UserName'     => Mage::getStoreConfig ( 'ariete_section/arietelogin_group/loginusername_field' ),
			'Password'     => Mage::getStoreConfig ( 'ariete_section/arietelogin_group/loginpassword_field' ),
			'Azienda'      => Mage::getStoreConfig ( 'ariete_section/arietelogin_group/loginazienda_field' ) 
		);
		$this->_syncModel = Mage::getModel('ariete/sync');
	}
	
	abstract protected function init();
	
	protected function logError($msg, $printTimer=false) {
		$this->log($msg, Zend_Log::ERR, $printTimer);
	}
	
	protected function logSuccess($msg, $printTimer=false) {
		$this->log($msg, Zend_Log::INFO, $printTimer);
	}
	
	protected function log($msg, $level=Zend_Log::INFO, $printTimer=false) {
		Mage::log($msg, $level);
		
		if($printTimer)
		{
		    $startTime = new DateTime('@'.$_SERVER['REQUEST_TIME']);
		    $now = new DateTime();
		    $timeInterval = $now->diff($startTime);
		    Mage::log("Tempo trascorso dall'inizio dello script: " . $timeInterval->format('%H:%I:%S'), Zend_Log::INFO);
		}
	}
}
