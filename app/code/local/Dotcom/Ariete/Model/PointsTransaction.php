<?php

class Dotcom_Ariete_Model_PointsTransaction extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ariete/pointsTransaction');
    }

    public function loadByOrder($order)
    {
        $this->getResource()->loadByOrderIncrementId($this, $order->getIncrementId());
        return $this;
    }

    public function updateSpendOrderInfo($orderNumber)
    {
        if($this->getTransactionId() != null)
        {
            $this->getResource()->updateSpendOrderInfo($this, $orderNumber);
        }
        return $this;
    }
}
