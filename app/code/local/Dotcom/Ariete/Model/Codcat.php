<?php
class Dotcom_Ariete_Model_Codcat extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('ariete/codcat');
    }
    
    public function getName()
    {
        if(!$this->getData('name')) {
            $this->setData(
                    'name',
                    $this->getDescri()
            );
        }
        return $this->getData('name');
    }

}
