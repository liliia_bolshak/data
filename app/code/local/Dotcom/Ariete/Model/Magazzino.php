<?php

class Dotcom_Ariete_Model_Magazzino extends Dotcom_Ariete_Model_Abstract
{
	protected $idRootCategory;
	protected $id_website_ita;
	protected $id_website_eng;
	protected $id_website_esp;
	
	private $_productModel;
	
	
	protected function init()
	{
		$this->wsdl = Mage::getStoreConfig ( 'ariete_section/arietewsdl_group/magazzinowsdl_field' );
		$this->idRootCategory = Mage::getStoreConfig ( 'ariete_section/arieteconfig_group/rootcategory_field' );
		$this->id_website_ita = Mage::getStoreConfig ( 'ariete_section/arieteconfig_group/idwebsiteita_field' );
		$this->id_website_eng = Mage::getStoreConfig ( 'ariete_section/arieteconfig_group/idwebsiteeng_field' );
		$this->id_website_esp = Mage::getStoreConfig ( 'ariete_section/arieteconfig_group/idwebsitespa_field' );
	}
	
	/**
	 * Importa le categorie da Ariete verso Magento.
	 * Le categorie presenti in Magento e non presenti in Ariete non vengono eliminate o modificate.
	 * Se c'è la necessità di eliminarle si procederà a mano dal back-office di Magento.
     * 
	 * NOTA: in Ariete i raggruppamenti sono le categorie che stanno al primo livello.
	 * I gruppi invece sono le categorie di secondo livello. Non ci sono ulteriori
	 * livelli di categorie perché Ariete non li gestisce.
	 */
	public function importCategorie()
	{
    	$status = 1; //Indichiamo che non ci sono problemi con la procedura
    	$categoriesQueried = 0;
    	$categoriesUpdated = 0;
    	$categoriesInserted = 0;
    	
		try
		{
    		foreach($this->getElencoRaggruppamenti() as $categoriaAriete)
    		{
    		    $categoriesQueried++;
        		try
        		{
        		    $category = $this->getMagentoCategory($categoriaAriete);
			        if($category)
			        {
			            $updating = true; //è stata trovata una corrispondenza della ctegoria Ariete in Magento quindi faccio l'update dei dati
			        }
			        else
			        {
			            $updating = false; //la categoria non esiste in Magento allora la devo inserire
			        }
			        $category = $this->saveCategory($category, $categoriaAriete);
    				if($updating)
    				{
    				    $categoriesUpdated++;
    				}
    				else
    				{
    				    $categoriesInserted++;
    				}
        		    	
        		    //passo tutte le categorie figlie
        		    foreach($this->getElencoGruppi($categoriaAriete['P_codice']) as $catf)
        		    {
        		        $categoriesQueried++;
                		try
                		{
            		        $subCategory = $this->getMagentoCategory($catf);
        			        if($subCategory)
        			        {
        			            $updating = true; //è stata trovata una corrispondenza della ctegoria Ariete in Magento quindi faccio l'update dei dati
        			        }
        			        else
        			        {
        			            $updating = false; //la categoria non esiste in Magento allora la devo inserire
        			        }
        			        $this->saveCategory($subCategory, $catf, $category);
            				if($updating)
            				{
            				    $categoriesUpdated++;
            				}
            				else
            				{
            				    $categoriesInserted++;
            				}
            			}
            			catch(Exception $e)
            			{
						 //error mail sender
						                            $emailTemplate = Mage::getModel('core/email_template')
						                                ->loadDefault('ariete_syncronization_error');
						                            $emailTemplateVariables = array();
						                            $emailTemplateVariables['process'] = 'Categories Import';
						                            $emailTemplateVariables['message'] = $e->getMessage();
						                            $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
						                            $mail = Mage::getModel('core/email')
						                                ->setToName('Marko Petelin')
						                                ->setToEmail('marko@infordata.it')
						                                ->setBody($processedTemplate)
						                                ->setSubject('magento syncronization error with scriptname')
						                                ->setFromEmail('magento@infordata.it')
						                                ->setFromName('Ariete import error')
						                                ->setType('html');
						                            $mail->send();
        		            $status = 0;
            				$this->logError("Errore nell'import categoria da Ariete (" . $categoriaAriete['_descri'] . '/' . $catf['_descri'] . "): " . $e->getMessage());
            			}
        		    }//foreach
    			}
    			catch(Exception $e)
    			{
		            $status = 0;
    				$this->logError("Errore nell'import categoria da Ariete (" . $categoriaAriete['_descri'] . "): " . $e->getMessage());
    			}
    		}
    		
    		$logMessage = "Categorie interrogate: $categoriesQueried; categorie aggiornate: $categoriesUpdated; nuove categorie inserite: $categoriesInserted";
    		$this->logSuccess("Import categorie da Ariete: $logMessage", true);
		}
		catch(Exception $e)
		{
		    $status = 0;
		    $logMessage = "Errore nella lettura delle categorie";
		    $this->logError("Errore nell'import categorie da Ariete: " . $logMessage . "; " . $e->getMessage(), true);
		}
		$this->_syncModel->setStatus($status)
		                ->setType(self::SYNC_TYPE_CATEGORIES)
		                ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
		                ->setLastEntityId(0)
		                ->setLog($logMessage)
		                ->save();
		
        $this->reindexProcess('catalog_category_flat');
	}
	
	/**
	 * Legge le categoriedi primo livello di Ariete
	 *
	 * @throws Exception
	 * @return array
	 */
	public function getElencoRaggruppamenti()
	{
		$q = array (
			'Login' => $this->login 
		);
		$result = $this->client->call('ElencoRaggruppamenti', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['ElencoRaggruppamentiResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['ElencoRaggruppamentiResult']['List']['anyType']);
		}
		elseif ($result['ElencoRaggruppamentiResult']['Count'] > 1) {
			$return = $result['ElencoRaggruppamentiResult']['List']['anyType'];
		}
		return $return;
	}

	/**
	 * Legge le categorie di secondo livello di Ariete
	 *
	 * @param integer $id_raggruppamento ID del raggruppamento padre (categoria padre)
	 * @throws Exception
	 * @return array
	 */
	public function getElencoGruppi($id_raggruppamento) {
		$q = array (
			'Login' => $this->login,
			'Raggruppamento' => $id_raggruppamento 
		);
		$result = $this->client->call('ElencoGruppi', $q, $this->namespace);
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['ElencoGruppiResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['ElencoGruppiResult']['List']['anyType']);
		}
		elseif ($result['ElencoGruppiResult']['Count'] > 1) {
			$return = $result['ElencoGruppiResult']['List']['anyType'];
		}
		return $return;
	}
	
	/**
	 * Verifica se la categoria già esiste in Magento confrontando
	 * il codice identificativo estratto da Ariete oppure il nome
	 * della categoria
	 * 
	 * @param array $categoriaAriete Dati della categoria estratti da Ariete
	 * @return Mage_Catalog_Model_Category
	 */
	public function getMagentoCategory($categoriaAriete)
	{
	    /* @var $category Mage_Catalog_Model_Category */
		$category = Mage::getModel('catalog/category');
		
		$id_ariete = isset($categoriaAriete['P_codice']) ? $categoriaAriete['P_codice'] : $categoriaAriete['_codgru'];
		$resultCategory = $category->loadByAttribute('id_cat_ariete', $id_ariete);
		if(!$resultCategory)
		{
		    $resultCategory = $category->loadByAttribute('name', $categoriaAriete['_descri']);
		    if($resultCategory && $resultCategory->getIdCatAriete())
		    {
		        $resultCategory = null;
		    }
		}
		return $resultCategory;
	}
	
	/**
	 * Inserisce in Magento una nuova categoria
	 * 
	 * @param Mage_Catalog_Model_Category $category Categoria da inserire/modificare in Magento
	 * @param array $categoriaAriete Dati della categoria in Ariete
	 * @param Mage_Catalog_Model_Category $parentCategory Categoria padre
	 * @return Mage_Catalog_Model_Category
	 */
	public function saveCategory($category, $categoriaAriete, $parentCategory=null)
	{	    
	    if(!$category)
	    {
    	    if(!$parentCategory)
    	    {
        	    /* @var $parentCategory Mage_Catalog_Model_Category */
        		$parentCategory = Mage::getModel('catalog/category');
        		$parentCategory->load($this->idRootCategory);
        		if(!$parentCategory->getId())
        		{
        		    throw new Exception('Categoria Root non esistente (ID ' . $this->idRootCategory . ')');
        		}
    		    $id_cat_ariete = $categoriaAriete['P_codice'];
    	    }
    	    else
    	    {
        		$id_cat_ariete = $categoriaAriete['_codgru'];
    	    }
	    
            $category = Mage::getModel('catalog/category');
	        $category->setPath($parentCategory->getPath())
	                 ->setIdCatAriete($id_cat_ariete)
	                 ->setIsActive(true)
	                 ->setDisplayMode('PRODUCTS')
	                 ->setIsAnchor(0)
	                 ->setIncludeInMenu(1)
	                 ->setAttributeSetId($category->getDefaultAttributeSetId());
	        //non server settare parent_id e level perché vengono calcolati dal path
	    }
	    
	    //questo mi serve per non far uscire dei messaggi di errore nel log perché
	    //al salvataggio della categoria l'attributo image si aspetta la variabile $_FILES settata
	    $_FILES['dummy'] = 'dummy';
	    
	    //salvo la categoria nello store-view di default e italiano
	    $category->setIsMassupdate(true)
	             ->setName($categoriaAriete['_descri'])
	             ->setStoreId(0)
	             ->save();

		if($categoriaAriete['_desing'])
		{
		    //salvo la categoria nello store-view inglese
    	    $category->unsetData('is_active')
    	             ->unsetData('include_in_menu')
    	             ->setName($categoriaAriete['_desing'])
    	             ->setStoreId($this->id_website_eng)
    	             ->save();
		}
		return $category;
	}//saveCategory
	
	/**
	 * Importa i prodotti da Ariete verso Magento
	 */
	public function importProdotti()
	{
    	/**
         * Leggiamo dalla tabella ariete_sync per capire dove siamo rimasti con l'ultima sincronizzazione 
    	 */
	    $startRow = 0;
    	$rowCount = 300;
    	$lastImportedData = $this->_syncModel->getLastImportedEntityId(self::SYNC_TYPE_PRODUCTS);
    	if(preg_match('/^(\d\d\d\d-\d\d-\d\d)( (\d+))?$/', $lastImportedData, $matches))
    	{
    	    if(isset($matches[3]))
    	    {
    	        $startRow = $matches[3];
    	    }
    	    $lastImportedData = $matches[1];
    	}
		
    	$status = 1; //Indichiamo che non ci sono problemi con la procedura
    	$productsQueried = 0;
    	$productsUpdated = 0;
    	$productsInserted = 0;
		
    	$startDate = new DateTime($lastImportedData);
    	
		try
		{
    		while($startDate <= new DateTime() && $productsQueried < 300)
    		{
		        $products = $this->getElencoArticoliDalAl($startDate->format('Y-m-d'), $startDate->format('Y-m-d'), $startRow, $rowCount);
        		
        		foreach($products as $productAriete)
        		{
        		    $productsQueried++;
        			try
        			{
        			    /* @var $product Mage_Catalog_Model_Product */
        			    $product = Mage::getSingleton('catalog/product')->unsetData();
    			        $product = $product->loadByAttribute('id_ariete', $productAriete['PI_codice']);
    			        if($product)
    			        {
    			            $product->load($product->getId());
//     			            if(new DateTime(substr($product->getUpdatedAt(), 0, 10)) > $startDate)
//     			            {
//     			                //Il prodotto è stato aggiornato in una data più futura rispetto a quella usata per l'interrogazione
//     			                //quindi non ha senso aggiornarlo
//     			                continue;
//     			            }
    			            $updating = true; //è stata trovata una corrispondenza del prodotto Ariete in Magento quindi faccio l'update dei dati
    			        }
    			        else
    			        {
    			            $updating = false; //il prodotto non esiste in Magento allora lo devo inserire
                            $product = Mage::getSingleton('catalog/product')->unsetData();
                            $product->setTypeId ( Mage_Catalog_Model_Product_Type::TYPE_SIMPLE )
                        	        ->setWebsiteIDs(array(1))
                        	        ->setAttributeSetId ( Mage::getStoreConfig ( 'ariete_section/arieteconfig_group/attributeset_field' ) )
                                    ->setPrice(0);
    			        }
    			        
    			        try
    			        {
        			        $listinoArieteUtentiFinali = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinoutentifinali_field');
        			        $listinoArieteRivenditori = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinorivenditori_field');
        			        $listinoArietePartners = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinopartners_field');
        			        $customerGroupUtentiFinali = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinoutentifinali_field');
        			        $customerGroupRivenditori = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinorivenditori_field');
        			        $customerGroupPartners = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinopartners_field');
    			            
        			        $price_uf = $this->getPrezzoArticolo($productAriete['PI_codice'], $listinoArieteUtentiFinali);
        			        $price_riv = $this->getPrezzoArticolo($productAriete['PI_codice'], $listinoArieteRivenditori);
        			        $price_par = $this->getPrezzoArticolo($productAriete['PI_codice'], $listinoArietePartners);

        			        $tierPriceData = array();
    			            $hasPrezzoUtF = false;
    			            $hasPrezzoRiv = false;
    			            $hasPrezzoPar = false;
    			            
        			        if($product->getData('tier_price'))
        			        {
        			            $tierPriceData = $product->getData('tier_price');
        			            $tiersToDelete = array();
        			            foreach($tierPriceData AS $key => $tier)
        			            {
        			                if($tier['cust_group'] == $customerGroupUtentiFinali)
        			                {
            			                $hasPrezzoUtF = true;
        			                    if($price_uf > 0)
        			                    {
            			                    $tier['price'] = $price_uf;
            			                    $tier['website_price'] = $price_uf;
        			                    }
        			                    else
        			                    {
        			                        $tiersToDelete[] = $key;
        			                    }
        			                }
        			                elseif($tier['cust_group'] == $customerGroupRivenditori)
        			                {
            			                $hasPrezzoRiv = true;
        			                    if($price_riv > 0)
        			                    {
            			                    $tier['price'] = $price_riv;
            			                    $tier['website_price'] = $price_riv;
        			                    }
        			                    else
        			                    {
        			                        $tiersToDelete[] = $key;
        			                    }
        			                }
        			                elseif($tier['cust_group'] == $customerGroupPartners)
        			                {
            			                $hasPrezzoPar = true;
        			                    if($price_par > 0)
        			                    {
            			                    $tier['price'] = $price_par;
            			                    $tier['website_price'] = $price_par;
        			                    }
        			                    else
        			                    {
        			                        $tiersToDelete[] = $key;
        			                    }
        			                }
        			            }//foreach
        			            
        			            foreach($tiersToDelete AS $key)
        			            {
        			                unset($tierPriceData[$key]);
        			            }
        			        }
        			        
        			        if($hasPrezzoUtF == false && $price_uf > 0)
        			        {
        			            $tierPriceData[] = array(
        			                'website_id' => 0,
        			                'cust_group' => $customerGroupUtentiFinali,
        			                'price_qty' => 1,
        			                'price' => $price_uf,
        			                'website_price' => $price_uf
        			            );
        			        }
        			        if($hasPrezzoUtF == false && $price_riv > 0)
        			        {
        			            $tierPriceData[] = array(
        			                'website_id' => 0,
        			                'cust_group' => $customerGroupRivenditori,
        			                'price_qty' => 1,
        			                'price' => $price_riv,
        			                'website_price' => $price_riv
        			            );
        			        }   
        			        if($hasPrezzoUtF == false && $price_par > 0)
        			        {
        			            $tierPriceData[] = array(
        			                'website_id' => 0,
        			                'cust_group' => $customerGroupPartners,
        			                'price_qty' => 1,
        			                'price' => $price_par,
        			                'website_price' => $price_par
        			            );
        			        }   		          
        			        $product->setPrice($price_uf)
        			                ->setTierPrice($tierPriceData);
    			        }
    			        catch(Exception $e)
    			        {
    			            throw new Exception("Errore nella sincronizzazione listini ($listinoArieteUtentiFinali, $listinoArieteRivenditori, $listinoArietePartners). " . $e->getMessage());
    			        }

    			        try
    			        {
    			            $qty = $this->getDisponibilitaStock($productAriete['PI_codice']);
        			        if($updating == true && $product->getStockItem())
        			        {
        			            /* @var $stockItemModel Mage_CatalogInventory_Model_Stock_Item */
        			            $stockItemModel = $product->getStockItem();
        			            $stockItemModel->setQty($qty)
        			                           ->setIsInStock(1);
        			        }
        			        else
        			        {
            			        $stockData = array(
            			            'qty' => $qty,
            			            'use_config_manage_stock' => 1,
            			            'use_config_min_sale_qty' => 1,
            			            'use_config_max_sale_qty' => 1,
            			            'is_in_stock' => 1 //abilitiamo sempre i prodotti anche se non hanno una quantità disponibile (richiesta del 17/7/2013)
            			        );
            			        $product->setStockData($stockData);
        			        }
    			        }
    			        catch(Exception $e)
    			        {
    			            throw new Exception("Errore nella sincronizzazione delle disponibilita'. " . $e->getMessage());
    			        }
			       
    			        $this->modifyProduct($product, $productAriete, $updating);
        			    
        				if($updating)
        				{
        				    $productsUpdated++;
        				}
        				else
        				{
        				    $productsInserted++;
        				}
        			}
        			catch(Exception $e)
        			{
    		            $status = 0;
        				$this->logError("Errore nell'import prodotti da Ariete (Keyrif " . $productAriete['_keyrif'] . ", ID " . $productAriete['PI_codice'] . "): " . $e->getMessage());
        			}
        		}//foreach

        		if(count($products) == $rowCount)
        		{
        		    $startRow += $rowCount;
        		}
        		else
        		{
        		    $startDate->add(new DateInterval('P1D')); //aggiunto 1 giorno
        		    $startRow = 0;
        		}
    		}//while
    		
    		$logMessage = "Prodotti interrogati: $productsQueried; prodotti aggiornati: $productsUpdated; nuovi prodotti inseriti: $productsInserted";
    		$this->logSuccess("Import prodotti da Ariete: $logMessage", true);
		}
		catch(Exception $e)
		{
		    $status = 0;
		    $logMessage = "Errore nella lettura degli articoli (Data=" . $startDate->format('Y-m-d') . ", StartRow=$startRow, RowCount=$rowCount)";
		    $this->logError("Errore nell'import articoli da Ariete: " . $logMessage . "; " . $e->getMessage(), true);
		}
		
		if($startDate->diff(new DateTime())->format("%r%a") <= 0)
		{
		    $startDate = new DateTime();
		}
		$this->_syncModel->setStatus($status)
		                ->setType(self::SYNC_TYPE_PRODUCTS)
		                ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
		                ->setLastEntityId($startDate->format('Y-m-d') . " $startRow")
		                ->setLog($logMessage)
		                ->save();
		
		if($productsUpdated > 0 || $productsInserted > 0)
		{
    		$this->reindexProcess('catalog_product_attribute');
     		$this->reindexProcess('catalog_url');
     		$this->reindexProcess('catalog_product_flat');
//     		$this->reindexProcess('catalog_category_product');
//     		$this->reindexProcess('catalog_product_price');
//     		$this->reindexProcess('cataloginventory_stock');
		}
	}//importProdotti	

	
	public function getElencoArticoliDalAl($startDate, $endDate, $startRow, $rowCount) {
	    $q = array (
	            'Login' => $this->login,
	            'ddata' => $startDate,
	            'adata' => $endDate,
	            'pubblicaweb' => 0,
	            'listino' => 1,
	            'StartRow' => $startRow,
	            'RowCount' => $rowCount
	    );
	    $result = $this->client->call('ElencoArticoliModificatiMovimentatiDalAl', $q, $this->namespace);
	    $err = $this->client->getError();
	    if ($err) {
	        throw new Exception($err);
	    }
	    $return = array();
	    if ($result['ElencoArticoliModificatiMovimentatiDalAlResult']['Count'] == 1) {
	        // quando ho solo un risultato ariete non mi ritorna un array
	        $return = array($result['ElencoArticoliModificatiMovimentatiDalAlResult']['List']['anyType']);
	    }
	    elseif ($result['ElencoArticoliModificatiMovimentatiDalAlResult']['Count'] > 1) {
	        $return = $result['ElencoArticoliModificatiMovimentatiDalAlResult']['List']['anyType'];
	    }
	    return $return;
	}
	
	public function getElencoDescrizioniLingueArticoloPerKeyRif($keyrif){
	    $q = array (
	            'Login' => $this->login,
	            'KeyRif' => $keyrif,
	            'StartRow' => 0,
	            'RowCount' => 100
	    );
	    $result = $this->client->call('ElencoDescrizioniLingueArticoloPerKeyRif', $q, $this->namespace);
	    $err = $this->client->getError();
	    if ($err) {
	        throw new Exception($err);
	    }
	    
	    $return = array();
	    if ($result['ElencoDescrizioniLingueArticoloPerKeyRifResult']['Count'] == 1) {
	        // quando ho solo un risultato ariete non mi ritorna un array
	        $return = array($result['ElencoDescrizioniLingueArticoloPerKeyRifResult']['List']['anyType']);
	    }
	    elseif ($result['ElencoDescrizioniLingueArticoloPerKeyRifResult']['Count'] > 1) {
	        $return = $result['ElencoDescrizioniLingueArticoloPerKeyRifResult']['List']['anyType'];
	    }
	    return $return;
	}
	
	/**
	 * Salva in Magento il prodotto da Ariete
	 *
	 * @param Mage_Catalog_Model_Product $product Prodotto da salvare in Magento
	 * @param array $productAriete Dati del prodotto estratto da Ariete
	 * @throws Exception
	 */
	public function modifyProduct($product, $productAriete, $updating)
	{	    
	    //Estraiamo le categorie alle quali agganciare il prodotto
	    $categoryIds = array();
	    /* @var $category Mage_Catalog_Model_Category */
		$category = Mage::getModel('catalog/category')->loadByAttribute('id_cat_ariete', $productAriete['_codgru']);
	    if($category)
	    {
	        /* 4-02-2014 non importiamo più i prodotti nelle categorie padre
	        $categoryIds[] = $this->idRootCategory;
            $categoryIds[] = $category->getParentId();
            */
            $categoryIds[] = $category->getId();
	    }
	    else
	    {
	        throw new Exception('Categoria prodotto non trovata (CODGRU ' . $productAriete['_codgru'] . ')');
	    }
		if($productAriete['_codcat'])
		{
		    $categoriaBrand = Mage::getModel('catalog/category')->loadByAttribute('id_cat_ariete', $productAriete['_codcat']);
		    if($categoriaBrand)
		    {
    		    $categoryIds[] = $categoriaBrand->getParentId();
    		    $categoryIds[] = $categoriaBrand->getId();
		    }
		}
	    
		/* aggiungo name2 name3 */
	    $product->setIsMassupdate(true)
	            ->setExcludeUrlRewrite(true)
        	    ->setStoreId(0) //store ID di default e italiano
        	    ->setSku($productAriete['_keyrif'])
        	    ->setIdAriete ( $productAriete ['PI_codice'] )
        	    ->setName ($productAriete ['_desart']) //utf8_encode ( $productAriete ['_desart'] ) )
        	    ->setCategoryIds($categoryIds)
        	    ->setTypeProductAriete($this->_decodeArieteProductType($productAriete ['_codtip']))
        	    ->setName2 ( $productAriete ['_desar2'])//utf8_encode ( $productAriete ['_desar2'] . '<br>' . $productAriete ['_desar3'] ) )
        	    ->setName3 ( $productAriete ['_desar3'] )
        	    ->setWeight ($productAriete ['_kgpeso'])
        	    ->setCodcat ($productAriete['_codcat'])
        	    ->unsetData('url_key')
	            ->setTaxClassId(Mage::getStoreConfig('ariete_section/arieteconfig_group/taxclassid_field'));
	    
	    if(!$updating){
	        //se inserisco un nuovo prodotto imposto gli attributi news_from_date, news_to_date
	        $dataNewFrom = new DateTime();
	        $dataNewTo = clone $dataNewFrom;
	        $dataNewTo->add(new DateInterval('P60D'));
	        $product->setNewsFromDate($dataNewFrom->format('Y-m-d'))
	                ->setNewsToDate($dataNewTo->format('Y-m-d'));
	    }
	    
	    if(!$product->getDescription())
	    {
	        $product->setDescription($product->getName());
	    }
	    if(!$product->getShortDescription()){
	        $product->setShortDescription( $productAriete ['_desar2'] . '<br>' . $productAriete ['_desar3'] );
	    }
	    

	    if($productAriete['_pubblicaweb'] == 'true' || $productAriete['_pubblicaweb'] == 1)
	    {
	        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
	    }
	    else
	    {
	        if($productAriete['_codrag'] == 6)
	        {
	            $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
	            $product->setVisibility ( Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE );
	        }
	        else
	        {
	            $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
	        }
	    }
	    
	    if($productAriete['_codrag'] != 6)
	    {
    	    if($productAriete['_codtip'] == "PD")
    	    {
    	        $product->setVisibility ( Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH );
    	    }
    	    else
    	    {
    	        //Gli accessori e i consumabili li rendiamo visibili solamente nella ricerca e non nel catalogo
    	        $product->setVisibility ( Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH ); //modifica 23-10-2013 tutti sempre visibili
    	    }
	    }
	    
	    // lo salvo nello store di default e in italiano
	    $product->save();
	    
	    if(!$updating){
    	    //per ogni nuovo prodotto che inserisco salvo la sua posizione a 9999 nelle categorie di appartenenza
	        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
	        $write->query("UPDATE catalog_category_product SET position = 9999 WHERE `category_id` IN (".implode(',', $categoryIds).") AND `product_id`=".$product->getId());
	    }
	    
	    // lo salvo in inglese e spagnolo
	    $traduzioni = $this->getElencoDescrizioniLingueArticoloPerKeyRif($productAriete['_keyrif']);//"LM520-SC");
	    foreach ($traduzioni as $traduzione){
	        if($traduzione["_codlng"] == 'ES'){
	            $product->unsetData('visibility')
	            ->unsetData('condition')
	            ->unsetData('tax_class_id')
	            ->unsetData('status')
	            ->unsetData('url_key')
	            ->unsetData('name2')
	            ->unsetData('name3')
	            ->setStoreId($this->id_website_esp)
	            ->setName($traduzione['_desart'])//utf8_encode($traduzione['_desart']))
	            ->setName2 ( $traduzione ['_desar2'] ) //utf8_encode ( $traduzione ['_desar2'] . '<br>' . $traduzione ['_desar3'] ) );
	            ->setName3 ( $traduzione ['_desar3'] );
	            
	            $loadOtherLanguage = $this->_getProductModel()->setStoreId($this->id_website_esp)->load($product->getId());;
	            if(!$loadOtherLanguage->getDescription())
	            {
	                $product->unsetData('description');
	                $product->setDescription ($traduzione['_desart']);
	            }
	            if(!$loadOtherLanguage->getShortDescription()){
	                $product->unsetData('short_description');
	                $product->setShortDescription ($traduzione ['_desar2'] . '<br>' . $traduzione ['_desar3']);
	            }
	            
	            $product->save();
	        }
	        else if($traduzione["_codlng"] == 'GB'){
	            $product->unsetData('visibility')
	            ->unsetData('condition')
	            ->unsetData('tax_class_id')
	            ->unsetData('status')
	            ->unsetData('url_key')
	            ->unsetData('name2')
	            ->unsetData('name3')
	            ->setStoreId($this->id_website_eng)
	            ->setName($traduzione['_desart'])//utf8_encode($traduzione['_desart']))
	            ->setName2 ( $traduzione ['_desar2'] ) //utf8_encode ( $traduzione ['_desar2'] . '<br>' . $traduzione ['_desar3'] ) );
	            ->setName3 ( $traduzione ['_desar3'] );
	            
	            $loadOtherLanguage = $this->_getProductModel()->setStoreId($this->id_website_eng)->load($product->getId());;
	            if(!$loadOtherLanguage->getDescription())
	            {
	                $product->unsetData('description');
	                $product->setDescription ($traduzione['_desart']);
	            }
	            if(!$loadOtherLanguage->getShortDescription()){
	                $product->unsetData('short_description');
	                $product->setShortDescription ($traduzione ['_desar2'] . '<br>' . $traduzione ['_desar3']);
	            }
	            
	            $product->save();
	        }
	    }
	    
	    
	}//modifyProduct
	
	/**
	 * Recupera da Ariete il prezzo di un certo prodotto per un certo listino
	 * Se non trova il listino o il prodotto restituisce il prezzo 0.
	 * 
	 * @param integer $idArticolo
	 * @param integer $listino
	 * @throws Exception
	 * @return float
	 */
	public function getPrezzoArticolo($idArticolo, $listino)
	{
		$q = array (
			'Login' => $this->login,
			'IdArticolo' => $idArticolo,
			'Listino' => $listino 
		);
		$result = $this->client->call('PrezzoArticolo', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		return $result['PrezzoArticoloResult'];
	}
	
	/**
	 * Recupera la disponibilità a magazzino di un certo prodotto in Ariete
	 * 
	 * @param integer $idArticolo
	 * @throws Exception
	 * @return float
	 */
	public function getDisponibilitaStock($idArticolo)
	{
		$q = array (
    		'Login' => $this->login,
    		'IdArticolo' => $idArticolo 
		);
		$result = $this->client->call('GiacenzaArticolo', $q, $this->namespace);
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		return $result['GiacenzaArticoloResult'];
	}

	/**
	 * Viene richiamato dal dettaglio prodotto per aggiornare tutti i dati di un singolo 
	 * prodotto con i dati di Ariete (compresi i listini prezzi e quantità a magazzino).
	 * 
	 * @param integer $id
	 */
	public function updateProduct($id)
	{
	    $p = Mage::getModel ( 'catalog/product' );
	    $product = $p->load ( $id );
	    $idAriete = $p->load ( $id )->getIdAriete ();
	    $productAriete = $this->getLeggiArticolo ( $idAriete );
	    $this->modifyProduct($product, $productAriete);
	    //TODO: bisogna anche aggiornare i prezzi e el quantità
	}//updateProduct
		
	public function getLeggiArticolo($idArticolo)
	{
		$q = array (
			'Login' => $this->login,
			'IdArticolo' => $idArticolo 
		);
		$result = $this->client->call('LeggiArticolo', $q, $this->namespace);
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if(!isset($result['LeggiArticoloResult']['_keyrif']) || empty($result['LeggiArticoloResult']['_keyrif'])){
			return null;
		}
		return $result['LeggiArticoloResult'];
	}
	
	/**
	 * Reindicizza un processo e tutti quelli dipendenti
	 * 
	 * @param string $process Nome del processo da reindicizzare.
	 * 
	 * Possibili valori de parametro $process:
	 *     - catalog_product_attribute
	 *     - catalog_product_price
	 *     - catalog_url
	 *     - catalog_product_flat
	 *     - catalog_category_flat
	 *     - catalog_category_product
	 *     - catalogsearch_fulltext
	 *     - cataloginventory_stock
	 *     - tag_summary
	 */
	private function reindexProcess($process)
	{
        $process = Mage::getModel('index/process')->load($process, 'indexer_code');
        if ($process->getId()) {
            $process->reindexEverything();
        }
	}
	
	private function _decodeArieteProductType($arieteProductType)
	{
	    switch($arieteProductType)
	    {
	        case 'PD':
	            return 3;
	        case 'AC':
	            return 4;
	        case 'MC';
	            return 5;
	        case 'PR':
	            return 6;
	        default:
	            return null;
	    }
	} 
	
	private function _getProductModel(){
	    if (is_null($this->_productModel)) {
	        $this->_productModel = Mage::getModel('catalog/product');
	    }
	    return $this->_productModel;
	}
}
