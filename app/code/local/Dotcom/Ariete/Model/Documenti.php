<?php

class Dotcom_Ariete_Model_Documenti extends Dotcom_Ariete_Model_Abstract
{		
	protected function init()
	{
		$this->wsdl = Mage::getStoreConfig ( 'ariete_section/arietewsdl_group/documentiwsdl_field' );
	}
	
	/**
	 * Importa gli ordini da Ariete verso Magento
	 *
	 */
	public function importOrders()
	{
	    $_SERVER['HTTP_HOST'] = 'infordatadealers.com';
	    /* @var $billingAddress Mage_Sales_Model_Order_Address */
	    $billingAddress = Mage::getModel( 'sales/order_address' );
	    Mage::register("billingAddress", $billingAddress);
	    
	    /* @var $shippingAddress Mage_Sales_Model_Order_Address */
	    $shippingAddress = Mage::getModel( 'sales/order_address' );
	    Mage::register("shippingAddress", $shippingAddress);
	    
	    $startDate = new DateTime($this->_syncModel->getLastImportedEntityId(self::SYNC_TYPE_ORDERS));
	    $endDate = clone $startDate;
	    $endDate->add(new DateInterval('P1D')); //aggiunto 1 giorno
	    
	    $status = 1; //Indichiamo che non ci sono problemi con la procedura
	    $recordsInserted = 0;

	    $result = $this->getElencoOrdiniPerData($startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
	    foreach ( $result as $order ) {
	        /* @var $orderModel Mage_Sales_Model_Order */
	        $orderModel = Mage::getSingleton('sales/order')->reset();
	        $customerModel = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->addAttributeToFilter("PI_codice",$order["_codana"])->getFirstItem();
	        
            try
            {
    	        if($orderModel->loadByAttribute('id_ordine_ariete', $order['PI_id'])->getId() === null && $orderModel->loadByIncrementId($this->_formatNumOrdineAriete($order['_numord']))->getId() === null)
    	        {
        	        $righeOrdine = $this->getLeggiOrdine($order['PI_id']);
                    $this->saveOrder ( $orderModel, $customerModel, $order, $righeOrdine);
                    Mage::log("Ordine importato da Ariete " . $this->_formatNumOrdineAriete($order['_numord']) . " (ID " . $order['PI_id'] . ")", Zend_Log::INFO);
                    $recordsInserted++;
    	        }
            }
            catch(Exception $e)
            {
				//error mail sender
				                $emailTemplate = Mage::getModel('core/email_template')
				                    ->loadDefault('ariete_syncronization_error');
				                $emailTemplateVariables = array();
				                $emailTemplateVariables['process'] = 'Orders Import';
				                $emailTemplateVariables['message'] = $e->getMessage();
				                $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
				                $mail = Mage::getModel('core/email')
				                    ->setToName('Marko Petelin')
				                    ->setToEmail('marko@infordata.it')
				                    ->setBody($processedTemplate)
				                    ->setSubject('magento syncronization error with scriptname')
				                    ->setFromEmail('magento@infordata.it')
				                    ->setFromName('Ariete import error')
				                    ->setType('html');
				                    $mail->send();
                $this->logError ($e->getMessage() );
                $status = 0;
            }
	    }//foreach

	    $result = $this->getElencoOrdiniPerDataModifica($startDate->format('Y-m-d'), $endDate->format('Y-m-d'));

	    foreach ( $result as $order ) {
	        /* @var $orderModel Mage_Sales_Model_Order */
	        $orderModel = Mage::getSingleton('sales/order')->reset();
	        $customerModel = Mage::getModel("customer/customer")->getCollection()->addAttributeToSelect("*")->addAttributeToFilter("PI_codice",$order["_codana"])->getFirstItem();
	        
            try
            {                
    	        if($orderModel->loadByAttribute('id_ordine_ariete', $order['PI_id'])->getId() !== null || $orderModel->loadByIncrementId($this->_formatNumOrdineAriete($order['_numord']))->getId() !== null)
    	        {
	                //cancello l'ordine per reinserirlo aggiornato
	                $vecchioOrdine = clone($orderModel);
	                
	                $registryVecchioOrdine = Mage::registry("vecchio_ordine");
	                if(!isset($registryVecchioOrdine)){
	                    Mage::register("vecchio_ordine", $vecchioOrdine);
	                }

                    Mage::register('isSecureArea', true);
	                $orderModel->delete();
	                $orderModel->reset();
                    Mage::log("Ordine cancellato da Magento " . $this->_formatNumOrdineAriete($order['_numord']) . " (ID " . $order['PI_id'] . ")", Zend_Log::INFO);
                    Mage::unregister('isSecureArea');
    	        }

    	        $righeOrdine = $this->getLeggiOrdine($order['PI_id']);
                $this->saveOrder ($orderModel, $customerModel, $order, $righeOrdine);
                Mage::log("Ordine importato da Ariete " . $this->_formatNumOrdineAriete($order['_numord']) . " (ID " . $order['PI_id'] . ")", Zend_Log::INFO);
                $recordsInserted++;
            }
            catch(Exception $e)
            {
                $this->logError ($e->getMessage() );
                $status = 0;
            }
	    }//foreach
	    $logMessage = "Ordini importati: $recordsInserted";
	    $this->logSuccess("Import Ordini da Ariete: $logMessage", true);
	    $lastImportedDate = time() < strtotime($endDate->format('Y-m-d')) ? date('Y-m-d') : $endDate->format('Y-m-d');
	    $this->_syncModel->setStatus($status)
	    ->setType(self::SYNC_TYPE_ORDERS)
	    ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
	    ->setLastEntityId($lastImportedDate)
	    ->setLog($logMessage)
	    ->save();
	}
	
	
	/**
	 * Legge gli ultimi ordini in Ariete per un certo cliente
	 * 
	 * @param integer $idCliente Identificativo del cliente in Ariete
	 * @return array
	 */
	public function getElencoUltimiOrdiniCliente($idCliente) {
		$q = array (
    		'Login' => $this->login,
    		'idCliente' => $idCliente,
    		'StartRow' => 0,
    		'RowCount' => 100 
		);
		$result = $this->client->call ( 'ElencoUltimiOrdiniCliente', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['ElencoUltimiOrdiniClienteResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['ElencoUltimiOrdiniClienteResult']['List']['anyType']);
		}
		elseif ($result['ElencoUltimiOrdiniClienteResult']['Count'] > 1) {
			$return = $result['ElencoUltimiOrdiniClienteResult']['List']['anyType'];
		}
		return $return;
	}
	

	/**
	 * Legge ordini inseriti dalla data alla data
	 *
	 * @param  string $da filtro ordini dalla data
	 * @param  string $a  filtro ordini alla data
	 * @return array
	 */
	public function getElencoOrdiniPerData($da, $a) {
	    $q = array (
	            'Login' => $this->login,
	            'ddata' => $da,
	            'adata' => $a,
	            'StartRow' => 0,
	            'RowCount' => 1000
	    );
	    $result = $this->client->call ( 'ElencoOrdiniPerData', $q, $this->namespace );
	    $err = $this->client->getError ();
	    if ($err) {
	        throw new Exception($err);
	    }
	    $return = array();
	    if ($result['ElencoOrdiniPerDataResult']['Count'] == 1) {
	        // quando ho solo un risultato ariete non mi ritorna un array
	        $return = array($result['ElencoOrdiniPerDataResult']['List']['anyType']);
	    }
	    elseif ($result['ElencoOrdiniPerDataResult']['Count'] > 1) {
	        $return = $result['ElencoOrdiniPerDataResult']['List']['anyType'];
	    }
	    return $return;
	}
	

	/**
	 * Legge ordini inseriti dalla data alla data
	 *
	 * @param  string $da filtro ordini dalla data
	 * @param  string $a  filtro ordini alla data
	 * @return array
	 */
	public function getElencoOrdiniPerDataModifica($da, $a) {
	    $q = array (
	            'Login' => $this->login,
	            'ddata' => $da,
	            'adata' => $a,
	            'StartRow' => 0,
	            'RowCount' => 1000
	    );
	    $result = $this->client->call ( 'ElencoOrdiniPerDataModifica', $q, $this->namespace );
	    $err = $this->client->getError ();
	    if ($err) {
	        throw new Exception($err);
	    }
	    $return = array();
	    if ($result['ElencoOrdiniPerDataModificaResult']['Count'] == 1) {
	        // quando ho solo un risultato ariete non mi ritorna un array
	        $return = array($result['ElencoOrdiniPerDataModificaResult']['List']['anyType']);
	    }
	    elseif ($result['ElencoOrdiniPerDataModificaResult']['Count'] > 1) {
	        $return = $result['ElencoOrdiniPerDataModificaResult']['List']['anyType'];
	    }
	    return $return;
	}

	/**
	 * Legge l'ordine da Ariete
	 *
	 * @param int $id ID dell'ordine Ariete
	 * @param boolean $righe Indica se il metodo deve restituire solamente le righe o tutto l'ordine
	 * @throws Exception
	 * @return array
	 */
	public function getLeggiOrdine($idOrdine, $righe = true) {
		$q = array (
				'Login' => $this->login,
				'IdOrdine' => $idOrdine 
		);
		$result = $this->client->call ( 'LeggiOrdine', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if (!isset ( $result ['LeggiOrdineResult'] ['Righe']['TRigaOrdine'] )) {
		    throw new Exception("Errore nel recuperare i dati dell'ordine Ariete ID $idOrdine");
		}

		if($righe){
    		if(!isset($result['LeggiOrdineResult']['Righe']) || empty($result['LeggiOrdineResult']['Righe']) || count($result['LeggiOrdineResult']['Righe']) == 0){
    		    return array();
    		}
    		elseif(isset($result['LeggiOrdineResult'] ['Righe']['TRigaOrdine']['PI_numeva']))
    		{
    		    return array($result['LeggiOrdineResult'] ['Righe']['TRigaOrdine']);
    		}
    		else
    		{
    		    return $result['LeggiOrdineResult'] ['Righe']['TRigaOrdine'];
    		}
		}
		else {
		    return $result['LeggiOrdineResult'];
		}
	}
	
	/**
	 * Salva in Magento l'ordine proveniente da Ariete
	 * 
	 * @param Mage_Sales_Model_Order $order
	 * @param Mage_Customer_Model_Customer $customer
	 * @param array $ordineAriete
	 * @param array $righeOrdine
	 * @throws Exception
	 */
	public function saveOrder($order, $customer, $ordineAriete, $righeOrdine)
	{
		$storeId = $customer->getStoreId ();
		$storeId = $storeId != 0 ? $storeId : 4; //dovrei estrarre dal DB l'ID dello store Infordata
		$order->setIncrementId ($this->_formatNumOrdineAriete($ordineAriete['_numord']))
		    //->setState('processing')
		    //->setStatus('processing')
			->setGlobalCurrencyCode ( 'EUR' )
			->setBaseCurrencyCode ( 'EUR' )
			->setStoreCurrencyCode ( 'EUR' )
			->setOrderCurrencyCode ( 'EUR' )
		    ->setStoreId($storeId)
		    ->setCreatedAt($ordineAriete['_datord'])
		    ->setIdOrdineAriete($ordineAriete['PI_id']);
		
		// set Customer data
		$order->setCustomerEmail ( $customer->getEmail () )
			->setCustomerFirstname ( $customer->getFirstname () )
			->setCustomerLastname ( $customer->getLastname () )
			->setCustomerTaxvat ( $customer->getTaxvat () )
			->setCustomerGroupId ( $customer->getGroupId () )
			->setCustomerIsGuest ( 0 )
			->setCustomer ( $customer );
		
		// set Billing Address
		$billing = $customer->getDefaultBillingAddress ();
		
		if($billing == null) {
		    $this->_reimportCustomerAddress($customer);//try to reimport customer addresses
		    $billing = Mage::getModel("customer/customer")->getCollection()
		                ->addAttributeToSelect("*")->addAttributeToFilter("entity_id",$customer->getId())
		                ->getFirstItem()->getDefaultBilling();
		    if($billing == null){
		        throw new Exception("Il cliente ID " . $customer->getId() . " non ha settato un indirizzo di fatturazione.");
		    }
		}
        /* @var $billingAddress Mage_Sales_Model_Order_Address */
	    $billingAddress = Mage::registry("billingAddress");
		$billingAddress->unsetData()
            		->setStoreId ( $storeId )
            		->setAddressType ( Mage_Sales_Model_Quote_Address::TYPE_BILLING )
            		->setCustomerId ( $billing->getCustomerId () )
            		->setCustomerAddressId ( $billing->getEntityId () )
            		->setPrefix ( $billing->getPrefix () )
            		->setFirstname ( $billing->getFirstname () )
            		->setMiddlename ( $billing->getMiddlename () )
            		->setLastname ( $billing->getLastname () )
            		->setSuffix ( $billing->getSuffix () )
            		->setCompany ( $billing->getCompany () )
            		->setStreet ( $billing->getStreet () )
            		->setCity ( $billing->getCity () )
            		->setCountryId ( $billing->getCountryId () )
            		->setRegion ( $billing->getRegion () )
            		->setRegion_id ( $billing->getRegionId () )
            		->setPostcode ( $billing->getPostcode () )
            		->setTelephone ( $billing->getTelephone () )
            		->setFax ( $billing->getFax () );
		$order->setBillingAddress ( $billingAddress );
		
		//se trovo nell'ordine il codice destinazione cerco di caricare l'indirizzo cliente altrimenti prendo lo shipping address di default
		if($ordineAriete["_coddes"]){
		    $orderShippingAddress = $customer->getAddressCollection()->setCustomerFilter($customer)->addAttributeToFilter('ariete_codice', $ordineAriete["_coddes"])->getFirstItem();
		}
		if(isset($orderShippingAddress) && $orderShippingAddress->getId()){
		    $shipping = $orderShippingAddress;
		}
		else {
		    $shipping = $customer->getDefaultShippingAddress ();
		}
		if($shipping)
		{
	        /* @var $shippingAddress Mage_Sales_Model_Order_Address */
		    $shippingAddress = Mage::registry("shippingAddress");
		    $shippingAddress->unsetData()
                    		->setStoreId ( $storeId )
                    		->setAddressType ( Mage_Sales_Model_Quote_Address::TYPE_SHIPPING )
                			->setCustomerId ( $shipping->getCustomerId () )
                			->setCustomerAddressId ( $shipping->getEntityId () )
                			->setPrefix ( $shipping->getPrefix () )
                			->setFirstname ( $shipping->getFirstname () ? $shipping->getFirstname () : $billingAddress->getFirstname () )
                			->setMiddlename ( $shipping->getMiddlename () )
                			->setLastname ( $shipping->getLastname() ? $shipping->getLastname() : $billingAddress->getLastname() )
                			->setSuffix ( $shipping->getSuffix () )
                			->setCompany ( $shipping->getCompany () )
                			->setStreet ( $shipping->getStreet () )
                			->setCity ( $shipping->getCity () )
                			->setCountryId ( $shipping->getCountryId() ? $shipping->getCountryId() : $billingAddress->getCountryId() )
                			->setRegion ( $shipping->getRegion () )
                			->setRegionId ( $shipping->getRegionId () )
                			->setPostcode ( $shipping->getPostcode() ? $shipping->getPostcode() : $billingAddress->getPostcode() )
                			->setTelephone ( $shipping->getTelephone() ? $shipping->getTelephone() : '-/-' )
                			->setFax ( $shipping->getFax () );
		    $order->setShippingAddress ( $shippingAddress );
		}
		else
		{
		    $order->setShippingAddress ( clone $billingAddress );
		}

		/**
		 * Setto il metodo di spedizione
		 */
		$carriers = Mage::getModel('shipping/config')->getAllCarriers();
		foreach ($carriers As $code => $carrier)
		{
		    /* @var $carrier Mage_Shipping_Model_Carrier_Abstract */
		    $name = trim($carrier->getConfigData('name'));
		    $title = trim($carrier->getConfigData('title'));
		    $codtra = trim($ordineAriete['_codtra']);
		    switch($codtra)
            {
                case 'OVDV':
                    $codtra = 'Door to Door';
                    break;
                case '':
                    $codtra = 'standard';
                    break;
            }
		    if(empty($name) || empty($title) || empty($codtra)) continue;
		    if(strpos(strtolower($name), strtolower($codtra)) !== false)
		    {
        		if($ordineAriete['_codpor'] == '001' && strtolower($title) == 'porto franco')
    		    {
    		        $order->setShippingMethod($code . '_' . $name);
	                $order->setShippingDescription($name . ' - ' . $title);
    		        break;
    		    }
        		elseif($ordineAriete['_codpor'] == '004' && (strtolower($title) == 'porto assegnato' || strpos(strtolower($title), 'destinatario') !== false))
    		    {
    		        $order->setShippingMethod($code . '_' . $name);
	                $order->setShippingDescription($name . ' - ' . $title);
    		        break;
    		    }
		    }
		}
		
		if(!$order->getShippingMethod())
		{
		    $order->setShippingMethod('flatrate_Spedizione standard');
		    $order->setShippingDescription('Spedizione standard');
		    //throw new Exception("L'ordine (ID " . $ordineAriete['PI_id'] . ") non ha settato un metodo di spedizione conosciuto (" . $ordineAriete['_codtra'] . ' - ' . $ordineAriete['_codpor'] . ").");
		}
		    
		/**
		 * Setto il metodo di pagamento
		 */
		/* @var $orderPayment Mage_Sales_Model_Order_Payment */
		$orderPayment = Mage::getSingleton( 'sales/order_payment' );
		$orderPayment->unsetData()
		             ->setStoreId ( $storeId );
		if($customer->getCodpagAriete())
		{
		    $orderPayment->setMethod('pagamentostandard');
		}
		elseif(strpos($ordineAriete['_codpag'], 'B') === 0)
		{
		    $orderPayment->setMethod('bonifico');
		}
		elseif(strpos($ordineAriete['_codpag'], 'CC') === 0)
		{
		    $orderPayment->setMethod('hosted_pro');
		}
		elseif(strpos($ordineAriete['_codpag'], 'CON') === 0)
		{
		    $orderPayment->setMethod('contrassegno');
		}
		else
		{
		    $orderPayment->setMethod('pagamentostandard');
		}
		$order->setPayment($orderPayment);
		
		$subTotal = 0;
		$percentIva = 0.22;
		$totSconto = 0;
		foreach ( $righeOrdine as $riga ) {
		    //saltiamo le righe di tipo "note"
		    if($riga['_tiprig'] == 3) {
                //add comment for order
                if(isset($riga['_descri'])) {
                    Mage::log($customer->getEmail(), null, 'johnnysk_comments_entered.log', true);
                    $order->addStatusHistoryComment($riga['_descri']);
                }

                continue;
            }
		    
		    //saltiamo le righe con le descrizioni aggiuntive 
		    if($riga['_tiprig'] == 2) continue;
		    
			if($riga['_codiva'] == '20')
			{
			    $percentIva = 0.2;
			}
			elseif($riga['_codiva'] == '21')
			{
			    $percentIva = 0.21;
			}
			elseif($riga['_codiva'] == '22')
			{
			    $percentIva = 0.22;
			}
			else
			{
			    $percentIva = 0;
			}

			/* @var $product Mage_Catalog_Model_Product */
			$product = Mage::getModel('catalog/product');
			$product = $product->loadByAttribute('id_ariete', $riga['_codart']);
			if(!$product)
			{
			    throw new Exception("Prodotto ID Ariete " . $riga['_codart'] . " non trovato in Magento (ordine Ariete ID " . $ordineAriete['PI_id'] . ")");
			}
			
			if($riga['_unitam'] != 'PZ')
			{
			    $isQtyDecimal = 1;
			}
			else
		    {
		        $intero = floor($riga['_quaord']);
		        if(($riga['_quaord'] - $intero) > 0)
		        {
		            $isQtyDecimal = 1;
		        }
		        else
		        {
		            $isQtyDecimal = 0;
		        }
		    }
			
			$rowTotal = $riga['_preuni'] * $riga ['_quaord'];	
			$sconto = ($riga['_preuni'] - $riga['_prenet']) * $riga['_quaord'];		
		    /* @var $orderItem Mage_Sales_Model_Order_Item */
			$orderItem = Mage::getModel( 'sales/order_item' );
			$orderItem->setStoreId ( $storeId )
        			->setCreatedAt ( $riga['_datord'] )
        			->setProductId ( $product->getId() )
        			->setProductType ( 'simple' )
        			->setIsQtyDecimal($isQtyDecimal)
        			->setTotalQtyOrdered ( $riga ['_quaord'] )
        			->setQtyOrdered ( $riga ['_quaord'] )
        			->setName($riga['_descri'])
        			->setSku(isset($riga['_keyrif']) ? $riga['_keyrif'] : $riga['_codart'])
        			->setPrice ( $riga['_preuni'] )
        			->setBasePrice ( $riga['_preuni'] )
        			->setOriginalPrice ( $riga['_preuni'] )
        			->setBaseOriginalPrice ( $riga['_preuni'] )
        			->setRowTotal ( $rowTotal )
        			->setBaseRowTotal ( $rowTotal )
        			->setTaxPercent ( $percentIva * 100 )
        			->setTaxAmount ( $riga['_totmer'] * $percentIva )
        			->setBaseTaxAmount ( $riga['_totmer'] * $percentIva )
        			->setDiscountAmount ( $sconto )
        			->setDiscountPercent ( $riga['_persco'] )
        			->setBaseDiscountAmount ( $sconto )
        			->setPriceInclTax ( $riga['_preuni'] * (1 + $percentIva) )
        			->setBasePriceInclTax ( $riga['_preuni'] * (1 + $percentIva) )
        			->setRowTotalInclTax ( $rowTotal * (1 + $percentIva) )
        			->setBaseRowTotalInclTax ( $rowTotal * (1 + $percentIva) )
			        //data consegna stimata 9-01-2014
			        ->setDataConsegna( $riga['_datsca'] );
			//_codiva 20=20%, 21=21%, tutti gli altri 0%
			//_preuni e _totmer sono IVA esclusa
			
			$subTotal += $rowTotal;
			$totSconto += $sconto;
			$order->addItem ( $orderItem );
			
			//Verifichiamo se il prodotto è del tipo "personale", cioè visibile solamente per il cliente
			/* @var $serviziCategoryId Mage_Catalog_Model_Category */
			$serviziCategoryId = Mage::getSingleton('catalog/category')->loadByAttribute('url_key', 'servizi')->getId();
			//se il prodotto ha idariete 0 non lo associo a nessun cliente
			if($product->getIdAriete() != 0 && $product->getVisibility() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE && $product->canBeShowInCategory($serviziCategoryId))
			{
			    $clientiCollegati = $product->getArieteAllowedCustomers();
			    //associo il prodotto al cliente per poterlo visualizzare nel tab "i miei prodotti"
			    //concateniamo gli id dei clienti con la virgola
			    if($clientiCollegati)
			    {
			        $collegamenti = $clientiCollegati . "," . $customer->getId();
			    }
			    else
			    {
			        $collegamenti = $customer->getId();
			    } 
			    $product->setArieteAllowedCustomers($collegamenti)
			            ->save();
			}
		}//foreach

		//setto le spese di trasporto
		$shippingAmount = $ordineAriete['_spetra'];
		$order->setShippingAmount($shippingAmount)
		      ->setBaseShippingAmount($shippingAmount)
              ->setShippingInclTax($shippingAmount * (1 + $percentIva))
              ->setBaseShippingInclTax($shippingAmount * (1 + $percentIva))
        	  ->setShippingTaxAmount($shippingAmount * $percentIva)
        	  ->setBaseShippingTaxAmount($shippingAmount * $percentIva);
		
		$order->setSubtotal( $subTotal )
		      ->setBaseSubtotal ( $subTotal )
		      ->setSubtotalInclTax ( $subTotal * (1 + $percentIva) )
		      ->setBaseSubtotalInclTax ( $subTotal * (1 + $percentIva) )
		      ->setGrandTotal ( $ordineAriete['_totord'] * (1 + $percentIva) )
		      ->setBaseGrandTotal ( $ordineAriete['_totord'] * (1 + $percentIva) )
		      ->setTaxAmount ( $ordineAriete['_totord'] * $percentIva )
		      ->setBaseTaxAmount ( $ordineAriete['_totord'] * $percentIva )
		      ->setDiscountAmount($totSconto)
		      ->setBaseDiscountAmount($totSconto)
		      ->setTotalItemCount(count($righeOrdine));

		/* @var $transaction Mage_Core_Model_Resource_Transaction */
		$transaction = Mage::getModel ( 'core/resource_transaction' );
		$transaction->addObject ( $order );
		$transaction->addCommitCallback ( array (
				$order,
				'place' 
		) );
		$transaction->addCommitCallback ( array (
				$order,
				'save' 
		) );
		$transaction->save ();
		
		//se ho cancellato l'ordine per reimportarlo mi reimporto lo state lo status e la status history del vecchio ordine
		$vecchioOrdine =  Mage::registry("vecchio_ordine");
		if(isset($vecchioOrdine) && $vecchioOrdine->getId()){
		    $statusHistoryCollection = $vecchioOrdine->getAllStatusHistory();
		    $state = $vecchioOrdine->getState();
		    $status = $vecchioOrdine->getStatus();
		    foreach($statusHistoryCollection as $statusHistory){
		        $order->addStatusHistory($statusHistory);
		    }
		    
		    $order->setData('state', $state);
		    $order->setData('status', $status);
	        $order->save();
	        Mage::unregister("vecchio_ordine");
		}
	}
	
	/**
	 * Importa i DDT da Ariete verso Magento
	 */
	public function importDDTs()
	{		
	    /* @var $allOrders Mage_Sales_Model_Resource_Order_Collection */
		$allOrders = Mage::getModel('sales/order')->getCollection();
		$allOrders->addAttributeToFilter('state', array('nin' => array(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CLOSED, Mage_Sales_Model_Order::STATE_COMPLETE)))
		          ->addAttributeToFilter('id_ordine_ariete', array('notnull' => true))
		          ->addAttributeToFilter('id_ordine_ariete', array('neq' => ''))
		          ->setOrder('entity_id', 'ASC');
		
		$startOrderId = $this->_syncModel->getLastImportedEntityId(self::SYNC_TYPE_DDTS);
		if($startOrderId)
		{
		    $allOrders->addFieldToFilter('entity_id', array('gt' => $startOrderId));
		}
		if($allOrders->count() == 0)
		{
    		$allOrders = Mage::getModel('sales/order')->getCollection();
    		$allOrders->addAttributeToFilter('state', array('nin' => array(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_CLOSED, Mage_Sales_Model_Order::STATE_COMPLETE)))
    		          ->addAttributeToFilter('id_ordine_ariete', array('notnull' => true))
    		          ->addAttributeToFilter('id_ordine_ariete', array('neq' => ''))
    		          ->setOrder('entity_id', 'ASC');
		}

		$status = 1; //Indichiamo che non ci sono problemi con la procedura
		$limit = 100;
		$recordsInserted = 0;
		$ordersQueried = 0;
		$lastImportedId = 0;
		
		foreach ( $allOrders as $order )
		{
		    if($limit <= 0) break;
		    $decrement = 1;
		    
    		/* @var $orderModel Mage_Sales_Model_Order */
    		$orderModel = Mage::getSingleton('sales/order');
    	    $orderModel->reset()->load($order->getId());
    	    
    	    //se non è un ordine Ariete lo salto
    	    if(!$orderModel->getIdOrdineAriete()) continue;
    	    
			try
			{
                //get Shipments for current order from Ariete ERT
    			$result = $this->getElencoDDTEvasioneOrdine($orderModel->getIdOrdineAriete());
    			$decrement = count($result) > 1 ? count($result) : 1;
    			foreach($result as $ddt)
                {
                    $dataDDT = new DateTime($ddt['_datbol']);
                    
                    /* tolta lettera vettura era inutile - 26-03-2014 adesso importiamo sempre tutti i ddt
                    if(!$ddt['_lvettura'] && new DateTime() < $dataDDT->add(new DateInterval('P7D')))
                    {
                        continue; //salto l'aggiornamento dei DDT recenti che ancora non hanno la lettera di vettura
                    }
                    */

        			/* @var $elencoDDTMagento Mage_Sales_Model_Resource_Order_Shipment_Collection */
        			$elencoDDTMagento = Mage::getResourceModel('sales/order_shipment_collection');
    			    $elencoDDTMagento->addAttributeToFilter('id_ariete', array('eq' => $ddt['PI_id']))
    			                     ->addAttributeToFilter('order_id', $orderModel->getId());
    			    if($elencoDDTMagento->count())
    			    {
    			        //Vuol dire che il DDT importato da Ariete è già presente in Magento dunque non serve registrarlo
    			        continue;
    			    }

    			    $itemsQty = array();
    			    $righeDDT = $this->getLeggiDDT($ddt['PI_id']);
    			    if(!$righeDDT)
    			    {
    			        $this->logError('Il DDT (ID ' . $ddt['PI_id'] . ') non ha righe associate');
    			        continue;
    			    }
    			    foreach ($righeDDT AS $riga)
    			    {
    			        //TODO: controllare cosa mi restituisce Ariete nel campo _numord
    			        if(ltrim(substr($riga['_numord'], 4), '0') != substr($orderModel->getIncrementId(), 0, strpos($orderModel->getIncrementId(), '/'))) continue;
    			        $item = $this->_getOrderItem($riga, $orderModel);
    			        if($item && $item->getQtyToShip() > 0)
    			        {
    			            $itemsQty[$item->getId()] = $riga['_quanti'];
    			        }
    			    }
                    //get order entity with new data
                    $orderModel->load($orderModel->getEntityId());

    			    if(!count($itemsQty))
    			    {
    			        //Non abbiamo trovato nemmeno una riga del DDT Ariete in Magento 
    			        continue;
    			    }
    			    
            		/* @var $shipmentApi Mage_Sales_Model_Order_Shipment_Api */
            		$shipmentApi = Mage::getSingleton('sales/order_shipment_api');
    			    $shipmentId = $shipmentApi->create($orderModel->getIncrementId(), $itemsQty);
    			    
    			    if($shipmentId)
    			    {
    			        $recordsInserted++;
            			try
            			{
    			            $numeroDDT = $ddt['_nregis'] . '/' . $ddt['_annreg'];
            			    /* @var $shipmentModel Mage_Sales_Model_Order_Shipment */
            				$shipmentModel = Mage::getModel('sales/order_shipment');
            				$shipmentModel->loadByIncrementId($shipmentId)
            				              ->setIdAriete($ddt['PI_id'])
            				              ->setNumeroAriete($numeroDDT)
            				              ->setCreatedAt($ddt['_datbol'])
            				              //->setLvettura($ddt['_lvettura'])
            				              ->save();
                    		/* @var $arieteInvoice Dotcom_Arieteinvoices_Model_Invoice */
                    		$arieteInvoice = Mage::getModel('arieteinvoices/invoice');
                    		if(!$arieteInvoice->loadByOrderAndDDTid($orderModel->getId(), $ddt['PI_id'])->getId())
                    		{
                    		    $arieteInvoice->setDdtId($ddt['PI_id'])
                    		                  ->setAnnreg($ddt['_annreg'])
                    		                  ->setNregis($ddt['_nregis'])
                    		                  ->setDataDdt($ddt['_datbol'])
            				                  //->setLvettura($ddt['_lvettura'])
                    		                  ->setOrderId($orderModel->getId())
                    		                  ->save();
                    		}
                    		$orderModel->load($orderModel->getId());//ricarico l'ordine dopo aver creato la spedizione altrimenti perdo i dati modificati nell'order item
                    		if($orderModel->getData("state") != Mage_Sales_Model_Order::STATE_PROCESSING){
                    		    $orderModel ->setData("state", Mage_Sales_Model_Order::STATE_PROCESSING)
                    		                ->setStatus($orderModel->getConfig()->getStateDefaultStatus(Mage_Sales_Model_Order::STATE_PROCESSING))
                    		                ->save();
                    		}
            			}
            			catch(Exception $e)
            			{
            			    $this->logError ($e->getMessage() );
            			}
            			
            			
    			    }
    			    else
    			    {
		                $status = 0;
			            $this->logError("Il DDT Ariete (ID DDT " . $ddt['PI_id'] . ", ID ordine " . $orderModel->getIdOrdineAriete() . ")  non è stato salvato in Magento");
    			    }
    			}//foreach
			}
			catch(Exception $e)
			{
            	if($e instanceof Mage_Api_Exception)
            	{
            		$message = $e->getCustomMessage() ? $e->getCustomMessage() : $e->getMessage();
            	}
            	else
            	{
            	    $message = $e->getMessage();
            	}
		        $status = 0;
			    $this->logError ( "Errore nell'import DDT da Ariete (ID DDT " . @$ddt['PI_id'] . ", ID ordine " . $orderModel->getIdOrdineAriete() . "): " . $message );
			}
			$limit = $limit - $decrement;
			$ordersQueried++;
			$lastImportedId = $orderModel->getId();
		}//foreach

		$logMessage = "Ordini interrogati: $ordersQueried; DDT inseriti: $recordsInserted";
		$this->logSuccess("Import DDT da Ariete: $logMessage", true);
		
		$this->_syncModel->setStatus($status)
		                ->setType(self::SYNC_TYPE_DDTS)
		                ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
		                ->setLastEntityId($lastImportedId)
		                ->setLog($logMessage)
		                ->save();
	}
	
	/**
	 * Legge i DDT da Ariete per l'ordine selezionato
	 * 
	 * @param int $orderId ID ordine Ariete
	 * @throws Exception
	 * @return array
	 */
	public function getElencoDDTEvasioneOrdine($orderId)
	{
		$q = array (
			'Login' => $this->login,
			'idOrdine' => $orderId
		);
		$result = $this->client->call ( 'ElencoDDTEvasioneOrdine', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if ($result['ElencoDDTEvasioneOrdineResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			return array($result['ElencoDDTEvasioneOrdineResult']['List']['anyType']);
		}
		elseif ($result['ElencoDDTEvasioneOrdineResult']['Count'] > 1) {
			return $result['ElencoDDTEvasioneOrdineResult']['List']['anyType'];
		} else {
			return array();
		}
	}
	
	/**
	 * Legge il DDT da Ariete e restituisce l'elenco degli articoli
	 * 
	 * @param int $id ID del DDT Ariete
	 * @param boolean $righe Indica se il metodo deve restituire solamente le righe o tutto il DDT 
	 * @throws Exception
	 * @return array
	 */
	public function getLeggiDDT($id, $righe = true)
	{
		$q = array (
			'Login' => $this->login,
			'IdDDT' => $id
		);
		$result = $this->client->call('LeggiDDT', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}

		if($righe){
    		
    		if(!isset($result['LeggiDDTResult']['Righe']) || empty($result['LeggiDDTResult']['Righe']) || count($result['LeggiDDTResult']['Righe']) == 0){
    		    return null;
    		}
    		if(isset($result['LeggiDDTResult']['Righe']['TRigaDDTCliente']['_codart']))
    		{
    		    return array($result['LeggiDDTResult']['Righe']['TRigaDDTCliente']);
    		}
    		else
    		{
    		    return $result['LeggiDDTResult']['Righe']['TRigaDDTCliente'];
    		    
    		}
		}
		else {
		    return $result['LeggiDDTResult'];
		}
	}
	
	/**
	 * Legge la fattura da Ariete
	 * 
	 * @param int $id ID della fattura Ariete
	 * @throws Exception
	 * @return array
	 */
	public function getLeggiFattura($id)
	{
		$q = array (
			'Login' => $this->login,
			'idFattura' => $id
		);
		$result = $this->client->call('LeggiFattura', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if(!isset($result['LeggiFatturaResult']['Righe']) || empty($result['LeggiFatturaResult']['Righe']) || count($result['LeggiFatturaResult']['Righe']) == 0){
		    return null;
		}
		else
		{
            return $result['LeggiFatturaResult'];
        }
	}
	
    /**
     * Ricerca un certo codice articolo Ariete tra le righe dell'ordine
     * 
     * @param array $riga Riga del DDT Ariete
     * @param Mage_Sales_Model_Order $orderModel Ordine Magento
     * @return Mage_Sales_Model_Order_Item
     */
	private function _getOrderItem($riga, $orderModel)
	{
	    $articleId = $riga['_codart'];
	    $qta = $riga['_quanti'];
	    /* @var $product Mage_Catalog_Model_Product */
	    $product = Mage::getModel('catalog/product');
	    $product = $product->loadByAttribute('id_ariete', $articleId);
	    if($product)
	    {
    	    foreach($orderModel->getAllItems() As $item)
    	    {
    	        /* @var $item Mage_Sales_Model_Order_Item */
    	        if($product->getId() == $item->getProductId())
    	        {
    	            //if($item->getQtyToShip() < $qta) continue;
                    /**
                     * We have qty higher than present in Magento order
                     */
    	            if($item->getQtyToInvoice() < $qta) {
                        try{
                            $rowTotal = $product->getPrice() * $qta;
                            $item->setTotalQtyOrdered($qta)
                                ->setQtyOrdered($qta)
                                ->setRowTotal($rowTotal)
                                ->setBaseRowTotal($rowTotal);
                            $item->save();
                        } catch (Exception $e) {
                            $this->logError($e->getMessage());
                        }
                    }
    	            return $item;
    	        }
    	    }
            /**
             * We have new item from ERT Ariete
             * code below added them to order
             */
            try {
                $rowTotal = $product->getPrice() * $qta;
                $orderItem = Mage::getModel('sales/order_item')
                    ->setStoreId($orderModel->getStoreId())
                    ->setQuoteItemId(NULL)
                    ->setQuoteParentItemId(NULL)
                    ->setProductId($product->getId())
                    ->setProductType($product->getTypeId())
                    ->setQtyBackordered(NULL)
                    ->setTotalQtyOrdered($qta)
                    ->setQtyOrdered($qta)
                    ->setName($product->getName())
                    ->setSku($product->getSku())
                    ->setPrice($product->getPrice())
                    ->setBasePrice($product->getPrice())
                    ->setOriginalPrice($product->getPrice())
                    ->setRowTotal($rowTotal)
                    ->setBaseRowTotal($rowTotal)
                    ->setOrder($orderModel);
                $orderItem->save();
                if($orderItem) {
                    return $orderItem;
                }
            } catch (Exception $e) {
                $this->logError($e->getMessage());
            }
	    }
	    return null;
	}
	
	/**
	 * Esporta tutti gli ordini di Magento verso Ariete
	 */
	public function exportOrders()
	{
	    /* @var $orderCollection Mage_Sales_Model_Resource_Order_Collection */
		$orderCollection = Mage::getModel('sales/order')->getCollection();
		$orderCollection->addAttributeToFilter('id_ordine_ariete', array('null'=>true))
		                ->addAttributeToFilter('customer_id', array('notnull'=>true))
		                ->addOrder('created_at', 'asc');
		
		$status = 1; //Indichiamo che non ci sono problemi con la procedura
		$ordersQueried = $orderCollection->count();
    	$ordersInserted = 0;
		
		foreach ($orderCollection as $order)
		{
			/* @var $order Mage_Sales_Model_Order */
			try 
			{
			    //if($order->getId() != 16490) continue;//for debug
    		    /* @var $customer Mage_Customer_Model_Customer */
    			$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
    			$contaRighe = 1;
    			$righeOrdineAriete = array('TRigaOrdine' => array());
    			foreach ($order->getAllItems() as $riga)
    			{
    				/* @var $riga Mage_Sales_Model_Order_Item */
    				/* @var $product Mage_Catalog_Model_Product */
    				$product = Mage::getModel('catalog/product')->load($riga->getProductId());
    				$billingAddressId = $order->getBillingAddress()->getId();
    				$salesAddressModel = Mage::getModel('sales/order_address')->load($billingAddressId);
    				$salesCountryId = $salesAddressModel->getCountryId();
    				
    				
    				if(preg_match('/^([0-9]+)\./', $riga->getTaxPercent(), $matches) && $matches[1] > 0)
    				{
    				    $codIva = $matches[1];
    				}
    				else if(in_array($salesCountryId, Mage::helper("dotcom_customer")->getEuCountries(false)))//if paese destinazione europa
    				{
    				    $codIva = 41; //Codice IVA Ariete per indicare IVA a zero per rivenditori EU
    				}
    				else {
    				    $codIva = "81a";
    				}
    				
    				
    				if($product->getTypeId() == 'virtual' || $product->getSku() == 'SPESE-MOVIMENTAZIONE' || $product->getVisibility() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE)
    				{
    				    $_codcve = 'V01';
    				    $_tiprig = 1;
    				}
    				else 
    				{
    				    $_codcve = '';
    				    $_tiprig = 0;
    				}
    				    
    				$righeOrdineAriete['TRigaOrdine'][] = array(
    					'PI_numeva' => '0',
    					'_annreg' => $order->getCreatedAtStoreDate()->toString('Y'),
    					'_nregis' => '0',
    					'_numrig' => $contaRighe++,
    					'_datord' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
    					'_datsca' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
    					'_codana' => $customer->getData('PI_codice'),
    					'_codart' => $product->getIdAriete(),
    					'_descri' => $product->getName(),
    					'_unitam' => 'PZ',
    					'_tiprig' => "$_tiprig",
    					'_preuni' => $riga->getPrice(),
    					'_persco' => $riga->getDiscountPercent(),
    					'_prenet' => $riga->getPrice() * (1 - $riga->getDiscountPercent() / 100),
    					'_quaord' => $riga->getQtyOrdered(),
    					'_totmer' => $riga->getRowTotal(),
    					'_codiva' => $codIva,
    					'_codcve' => $_codcve,
    					'_numord' => $order->getIncrementId()
    				);

    				/**
    				 * Aggiungiamo all'ordine Ariete le custom options del prodotto
    				 */
				    foreach ($this->getItemOptions($riga) as $option) {
				        if ($option['value']) {
				            $label = $option['label'];
				            $printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
				            /* @var $optionInstance Mage_Catalog_Model_Product_Option */
				            $optionInstance = $product->getOptionById($option['option_id']);

				            //if($optionInstance->getPrice(true) > 0)
				            //{
    				            $righeOrdineAriete['TRigaOrdine'][] = array(
    				                'PI_numeva' => '0',
    				                '_annreg' => $order->getCreatedAtStoreDate()->toString('Y'),
    				                '_nregis' => '0',
    				                '_numrig' => $contaRighe++,
    				                '_datord' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
    		                        '_datsca' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
                                    '_codana' => $customer->getData('PI_codice'),
    		                        '_codart' => 0,
                                    '_descri' => $label . ": " . $printValue,
                                    '_unitam' => '',
                                    '_tiprig' => '2',
                                    '_preuni' => 0,
                                    '_persco' => 0,
                                    '_prenet' => 0,
    		                        '_quaord' => 0,
    		                        '_totmer' => 0,
    		                        '_codiva' => $codIva,
    		                        '_codcve' => '',
                					'_numord' => $order->getIncrementId()
                				);
				            //}
				        }//if
				    }//foreach
    			}//foreach
    			
    			if(strpos(strtolower($order->getShippingDescription()), 'porto franco') !== false)
    			{
    			    $codpor = '001';
    			}
    			else
    			{
    			    $codpor = '004';
    			}

    			$method = $order->getShippingMethod();
    			if(preg_match('/^([^_]+)_([^_]+)$/', $method, $matches))
    			{
    			    $method = $matches[2];
    			}
    			
    			if($order->getShippingMethod() == "tablerate_bestway"){
    			    $codtra = 'TNT';
    			}
    			else {
    			    /* @var $carrier Mage_Shipping_Model_Carrier_Abstract */
    			    $carrier = Mage::getModel('shipping/config')->getCarrierInstance($method);
    			    $codtra = $carrier->getConfigData('name');
    			}
    			
    			if(strpos(strtolower($codtra), 'gls') !== false)
    			{
    			    $codtra = 'EXEC';
    			}

    			//Definisco il metodo di pagamento
    			switch($order->getPayment()->getMethod())
    			{
    			    case 'contrassegno':
    			        $codpag = 'CON';
    			        break;
    			    case 'bonifico':
    			        $codpag = 'BO05';
    			        break;
    			    case 'hosted_pro':
    			    case 'paypal_standard':
			        case 'setefi_cc':
    			        $codpag = 'CC';
    			        break;
    			    case 'pagamentostandard':
    			        $codpag = ($customer->getCodpagAriete() ? $customer->getCodpagAriete() : 'BO05'); //di default Bonifico bancario anticipato
    			        break;
    			    default:
    			        $codpag = 'BO05'; //di default Bonifico bancario anticipato
    			        break;
    			}
    			
    			$orderconf = 'Ordine Web ';
    			if ($order->getStoreId()==5) {
    				$orderconf = 'Order confirmation ';
    			}
    			
    			$testataOrdine = array(
					'PI_id' => '0',
					'_annreg' => $order->getCreatedAtStoreDate()->toString('Y'),
					'_nregis' => '0',
					'_datcof' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
					'_rifcof' => $orderconf . $order->getIncrementId(),
					'_datsca' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
					'_datord' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
					'_codana' => $customer->getData('PI_codice'),
					'_coddes' => $order->getShippingAddress()->getCustomerAddressId(),
					'_codpor' => $codpor,
					'_codtra' => $codtra,
					'_spetra' => $order->getShippingAmount(),
					'_codimb' => '',
					'_speimb' => '0',
					'_codpag' => $codpag,
					'_rifper' => $customer->getName(),
					'_totmer' => $order->getSubtotal(),
					'_totord' => $order->getGrandTotal(),
					'_tippagonl' => '0',
					'_codpagonl' => '',
					'_noteweb' => '',
					'_numord' => $order->getIncrementId(),
					'Righe' => $righeOrdineAriete
    			);

            	/* @var $awPointsModel Dotcom_Ariete_Model_PointsTransaction */
            	$awPointsModel = Mage::getModel('ariete/pointsTransaction');
            	$awPointsModel->loadByOrder($order);
            	$puntiUtilizzati = $awPointsModel->getPointsToMoney();
            	
            	
            	if($puntiUtilizzati)
            	{
            	    $testataOrdine['Righe']['TRigaOrdine'][] = array(
            	        'PI_numeva' => '0',
            	        '_annreg' => $order->getCreatedAtStoreDate()->toString('Y'),
            	        '_nregis' => '0',
            	        '_numrig' => $contaRighe++,
            	        '_datord' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
    	                '_datsca' => $order->getCreatedAtStoreDate()->toString('Y-M-d'),
                        '_codana' => $customer->getData('PI_codice'),
    	                '_codart' => 0,
                        '_descri' => 'Crediti utilizzati',
                        '_unitam' => 'PZ',
                        '_tiprig' => '1',
                        '_preuni' => $puntiUtilizzati,
                        '_persco' => 0,
                        '_prenet' => $puntiUtilizzati,
    	                '_quaord' => 1,
    	                '_totmer' => $puntiUtilizzati,
    	                '_codiva' => $codIva,
    	                '_codcve' => 'V01',
    					'_numord' => $order->getIncrementId()
    				);
            	}
    			
    			$idOrdineAriete = $this->inserisciOrdineAriete($testataOrdine);
            	$ordineAriete = $this->getLeggiOrdine($idOrdineAriete, false);
            	$newOrderNumber = $ordineAriete['_nregis'] . '/' . $ordineAriete['_annreg'];
            	
            	$commento = $awPointsModel->getComment();
            	if($commento = preg_replace('/#' . $order->getIncrementId() . '/', $newOrderNumber, $commento))
            	{
            	    $awPointsModel->setComment($commento);
            	    $awPointsModel->save();
            	}
            	
				$order->setIdOrdineAriete($idOrdineAriete)
				      ->setIncrementId($newOrderNumber)
				      ->save();
				
				$awPointsModel->updateSpendOrderInfo($newOrderNumber);
				
    			$ordersInserted++;
			}
			catch(Exception $e)
			{
	            $status = 0;
				$this->logError("Errore nell'export ordini verso Ariete (N.ord. " . $order->getIncrementId() . "): " . $e->getMessage());
			}
		}//foreach
		
		$logMessage = "Ordini interrogati: $ordersQueried; ordini inseriti: $ordersInserted;";
		$this->logSuccess("Esportati gli ordini verso Ariete: $logMessage", true);

		$this->_syncModel->setStatus($status)
                		->setType(self::SYNC_TYPE_ORDERS)
                		->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_EXPORT)
                		->setLastEntityId('')
                		->setLog($logMessage)
                		->save();
	}
	
	/**
	 * Chiamata al Web-service di Ariete per l'inserimento di un nuovo ordine
	 * 
	 * @param array $datiTestataOrdine
	 * @throws Exception
	 * @return array
	 */
	public function inserisciOrdineAriete($datiTestataOrdine){
		$q = array (
				'Login' => $this->login,
				'Ordine' => $datiTestataOrdine
		);
		
		$result = $this->client->call ( 'NuovoOrdine', $q, $this->namespace );
		
		$err = $this->client->getError ();
		if ($err) {
		    $this->logError($datiTestataOrdine);
			throw new Exception( $err );
		}
		
		if (!isset($result['NuovoOrdineResult']['PI_id'])) {
		    throw new Exception("Errore sconosciuto nell'inserimento ordine in Ariete");
		}
		
		return $result['NuovoOrdineResult']['PI_id'];
	}
	
	/**
	 * Importa le fatture da Ariete verso Magento
	 */
	public function importInvoices()
	{		
		/* @var $allOrders Mage_Sales_Model_Resource_Order_Collection */
		$allOrders = Mage::getModel('sales/order')->getCollection();
		$allOrders->addAttributeToFilter('state', array(
												'nin' => array(Mage_Sales_Model_Order::STATE_CANCELED,
													Mage_Sales_Model_Order::STATE_CLOSED,
													Mage_Sales_Model_Order::STATE_COMPLETE)))
												->addAttributeToFilter('id_ordine_ariete', array('notnull' => true))
												->addAttributeToFilter('id_ordine_ariete', array('neq' => ''))
												->setOrder('entity_id', 'ASC');

		//$startOrderId = $this->_syncModel->getLastImportedEntityId(self::SYNC_TYPE_INVOICES);
        $startOrderId = '219983';
		if($startOrderId)
		{
		    $allOrders->addFieldToFilter('entity_id', array('gt' => $startOrderId));
		}
		if($allOrders->count() == 0)
		{
			$allOrders = Mage::getModel('sales/order')->getCollection();
			$allOrders->addAttributeToFilter('state', array(
				'nin' => array(Mage_Sales_Model_Order::STATE_CANCELED,
					Mage_Sales_Model_Order::STATE_CLOSED,
					Mage_Sales_Model_Order::STATE_COMPLETE)))
				->addAttributeToFilter('id_ordine_ariete', array('notnull' => true))
				->addAttributeToFilter('id_ordine_ariete', array('neq' => ''))
				->setOrder('entity_id', 'ASC');
		}

		$status = 1; //Indichiamo che non ci sono problemi con la procedura
		$limit = 100; //limite massimo di fatture da sincronizzare
		$recordsInserted = 0;
		$ordersQueried = 0;
		$lastImportedId = 0;
		
		foreach ( $allOrders as $order )
		{
		    if($limit <= 0) break;
		    $decrement = 0;

		    try
		    {
        		/* @var $orderModel Mage_Sales_Model_Order */
        		$orderModel = Mage::getSingleton('sales/order');
        	    $orderModel->reset()->load($order->getId());
                $customerId = $orderModel->getCustomerId();
                $idOrdineAriete = $orderModel->getIdOrdineAriete();

        	    //se non è un ordine Ariete lo salto
        	    if(!$orderModel->getIdOrdineAriete()) continue;

    			$result = $this->getElencoDDTEvasioneOrdine($idOrdineAriete);
                if(!empty($result)) {
                    foreach ($result as $ddt) {
                        try {
                            if ($ddt['IdFattura'] == '0' || !$ddt['IdFattura']) continue;

                            /* @var $elencoFattureMagento Mage_Sales_Model_Resource_Order_Invoice_Collection */
                            $elencoFattureMagento = Mage::getResourceModel('sales/order_invoice_collection');
                            $elencoFattureMagento->addAttributeToFilter('id_ariete', array('eq' => $ddt['IdFattura']))
                                ->addAttributeToFilter('order_id', $orderModel->getId());
                            if ($elencoFattureMagento->count()) {
                                //Vuol dire che la fattura importata da Ariete è già presente in Magento dunque non serve registrarla
                                continue;
                            }

                            //Per estrerre le righe della fattura bisogna interrogare un altro WS di Ariete
                            $fatturaAriete = $this->getLeggiFattura($ddt['IdFattura']);

                            /* @var $arieteInvoice Dotcom_Arieteinvoices_Model_Invoice */
                            $arieteInvoice = Mage::getModel('arieteinvoices/invoice');
                            if ($arieteInvoice->loadByOrderAndDDTid($orderModel->getId(), $ddt['PI_id'])->getId()) {
                                $arieteInvoice->setIdFattAriete($fatturaAriete['PI_nregis'])
                                    ->setAnnregFattura($fatturaAriete['_annreg'])
                                    ->setNregisFattura($fatturaAriete['_numpro'])
                                    ->setDataFattura($fatturaAriete['_datreg'])
                                    ->setTotmerce($fatturaAriete['_totfat'])
                                    ->save();
                            } else {
                                //Prima di inserire le fatture devo avere inseriti i DDT
                                continue 2;
                            }

                            if (isset($fatturaAriete['Righe']['TRigaFattura']['_codart'])) {
                                $righeFatturaAriete = array($fatturaAriete['Righe']['TRigaFattura']);
                            } else {
                                $righeFatturaAriete = $fatturaAriete['Righe']['TRigaFattura'];
                            }
                            if (!$righeFatturaAriete) {
                                throw new Exception('La fattura non ha righe associate');
                            }

                            $itemsQty = array();
                            foreach ($righeFatturaAriete AS $riga) {
                                if (ltrim(substr($riga['_numord'], 4), '0') != substr($orderModel->getIncrementId(), 0, strpos($orderModel->getIncrementId(), '/'))) continue;
                                $item = $this->_getOrderItem($riga, $orderModel);
                                if ($item && $item->getQtyToInvoice() > 0) {
                                    if ($item->getQtyToInvoice() > $riga['_quanti'])
                                        $q = $riga['_quanti'];
                                    else
                                        $q = $item->getQtyToInvoice();
                                    $itemsQty[$item->getId()] = $q;
                                }
                            }
                            if (!count($itemsQty)) {
                                throw new Exception('Non abbiamo trovato nemmeno una riga della fattura Ariete in Magento');
                            }

                            //get order entity with new data
                            $orderModel->load($orderModel->getEntityId());

                            /**
                             * Check invoice create availability
                             */
                            if (!$orderModel->canInvoice()) {
                                throw new Exception("L'ordine non puo' essere fatturato");
                            }


                            /* @var $invoice Mage_Sales_Model_Order_Invoice */
                            $invoice = $orderModel->prepareInvoice($itemsQty);
                            $invoice->register();
                            $invoice->getOrder()->setIsInProcess(true);

                            $invoice->setIdAriete($fatturaAriete['PI_nregis'])
                                ->setNumeroAriete($fatturaAriete['_numpro'] . '/' . $fatturaAriete['_annreg'])
                                ->setCreatedAt($fatturaAriete['_datreg']);

                            $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder())
                                ->save();

                                			//firedUp custom event
			 Mage::dispatchEvent('ariete_invoice_imported_in_magento',
			     array('customerid' => $customerId, 'order' => $orderModel)
			 );

                            $recordsInserted++;
                        } catch (Exception $e) {
                            $status = 0;
                            $this->logError("La fattura Ariete (ID fattura " . $ddt['IdFattura'] . ", ID DDT " . $ddt['PI_id'] . ", ID ordine " . $orderModel->getIdOrdineAriete() . ") non è stata salvata in Magento: " . $e->getMessage());
                        }
                        $decrement++;
                    }//foreach
                } else {
                    //get all customer's invoices from Ariete
                    $customersInvoices = $this->loadInvoices($customerId);
                    //get order info from Ariete
                    $arieteOrder = $this->getLeggiOrdine($idOrdineAriete);
                    if(!empty($customersInvoices)) {
                        foreach($customersInvoices as $arieteInvoiceData) {
                            //get all invoice data from Ariete
                            $arieteInvoice = $this->getLeggiFattura($arieteInvoiceData['PI_nregis']);

							//if(!array_key_exists('_numord',$arieteInvoice['Righe']['TRigaFattura'])) continue;
                            //find invoice for current order
                            //strange check
                            /** TODO: need additional check */
                            //if($arieteOrder[0]['_numord'] == $arieteInvoice['Righe']['TRigaFattura']['_numord']) {
                                try{
                                    //just reduced nesting for more comfortable coding
                                    if (isset($arieteInvoice['Righe']['TRigaFattura']['_codart'])) {
                                        $righeInvoiceAriete = array($arieteInvoice['Righe']['TRigaFattura']);
                                    } else {
                                        $righeInvoiceAriete = $arieteInvoice['Righe']['TRigaFattura'];
                                    }
                                    if (!$righeInvoiceAriete) {
                                        throw new Exception('La fattura non ha righe associate');
                                    }
                                    //prepare items qty
                                    $itemsQty = array();
                                    foreach ($righeInvoiceAriete as $riga) {
                                        $item = $this->_getOrderItem($riga, $orderModel);
                                        if ($item && $item->getQtyToInvoice() > 0) {
                                            if ($item->getQtyToInvoice() > $riga['_quanti'])
                                                $q = $riga['_quanti'];
                                            else
                                                $q = $item->getQtyToInvoice();
                                            $itemsQty[$item->getId()] = $q;
                                        }
                                    }

                                    if (!count($itemsQty)) {
                                        throw new Exception('Non abbiamo trovato nemmeno una riga della fattura Ariete in Magento');
                                    }

                                    /**
                                     * Check invoice create availability
                                     */
                                    if (!$orderModel->canInvoice()) {
                                        throw new Exception("L'ordine non puo' essere fatturato");
                                    }

                                    /* @var $invoice Mage_Sales_Model_Order_Invoice */
                                    $invoice = $orderModel->prepareInvoice($itemsQty);
                                    $invoice->register();
                                    $invoice->getOrder()->setIsInProcess(true);

                                    $invoice->setIdAriete($arieteInvoice['PI_nregis'])
                                        ->setNumeroAriete($arieteInvoice['_numpro'] . '/' . $arieteInvoice['_annreg'])
                                        ->setCreatedAt($arieteInvoice['_datreg']);

                                    $transactionSave = Mage::getModel('core/resource_transaction')
                                        ->addObject($invoice)
                                        ->addObject($invoice->getOrder())
                                        ->save();

                                        			//firedUp custom event
			 Mage::dispatchEvent('ariete_invoice_imported_in_magento',
			     array('customerid' => $customerId, 'order' => $orderModel)
			 );

                                    //START Handle Shipment
                                    $shipment = $orderModel->prepareShipment();
                                    $shipment->register();
                                    $orderModel->setIsInProcess(true);

                                    $transactionSave = Mage::getModel('core/resource_transaction')
                                        ->addObject($shipment)
                                        ->addObject($shipment->getOrder())
                                        ->save();
								 
                                    $recordsInserted++;
                                    //stop foreach, when we found current invoice
                                    break;
                                } catch (Exception $e) {
                                    $status = 0;
                                    $this->logError($e->getMessage());
                                }
                            //}
                        }
                    }

                }
    			
    			//Dopo aver importato tutte le fatture di un certo ordine se ancora non è stato
    			//settato come completato e l'ordine è più vecchio di 90 giorni allora lo chiudo
                $dataOrdine = new DateTime($orderModel->getCreatedAt());
                if(($dataOrdine->diff(new DateTime())->format("%a") > 90))
                {
                    //ordine che non sarà mai fatturato quindi va chiuso
                    $orderModel->setData('state', Mage_Sales_Model_Order::STATE_CLOSED)
                          ->setStatus($orderModel->getConfig()->getStateDefaultStatus(Mage_Sales_Model_Order::STATE_CLOSED))
                          ->save();
                    $orderModel->addStatusHistoryComment("Ordine chiuso manualmente perché non ha altre fatture ed è più vecchio di 90 giorni", false)
                          ->save();
                    $this->logSuccess("Import fatture da Ariete: ordine piu' vecchio di 90 giorni chiuso perche' non ci sono altre fatture da importare (ID Ariete " . $orderModel->getIdOrdineAriete() . ", ID Magento " . $orderModel->getId() . ")");
                }
			}
			catch(Exception $e)
			{
		        $status = 0;
		        $message = $e->getMessage();
			    $this->logError ( "Errore nell'import fatture da Ariete (ID ordine " . $orderModel->getIdOrdineAriete() . "): " . $message );
			}
			
			Mage::log('JohnnyImport:customerId = '.$customerId.'; orderId = '.$orderModel->getId(), null, 'johnnyInvoiceImport.log');
			
			if($decrement == 0) $decrement = 1;
			$limit = $limit - $decrement;
			$ordersQueried++;
			$lastImportedId = $orderModel->getId();
		}//foreach

		$logMessage = "Ordini interrogati: $ordersQueried; fatture inserite: $recordsInserted";
		$this->logSuccess("Import fatture da Ariete: $logMessage", true);
		
		$this->_syncModel->setStatus($status)
		                ->setType(self::SYNC_TYPE_INVOICES)
		                ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
		                ->setLastEntityId($lastImportedId)
		                ->setLog($logMessage)
		                ->save();
	}

	/**
	 * Legge le fatture da Ariete per il cliente selezionato
	 *
	 * @param int $customerId
	 * @throws Exception
	 * @return array
	 */
	public function loadInvoices($customerId)
	{
	    $customer = Mage::getModel('customer/customer')->load($customerId);
		$q = array (
				'Login' => $this->login,
				'idCliente' => $customer->getData('PI_codice'),
				'StartRow' => 0,
				'RowCount' => 100 
		);
		$result = $this->client->call ( 'ElencoFattureCliente', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if ($result['ElencoFattureClienteResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			return array($result['ElencoFattureClienteResult']['List']['anyType']);
		}
		elseif ($result['ElencoFattureClienteResult']['Count'] > 1) {
			return $result['ElencoFattureClienteResult']['List']['anyType'];
		} else {
			return array();
		}
	}
	
	/**
	 * Legge i DDT da Ariete per il cliente selezionato
	 * 
	 * @param int $customerId
	 * @throws Exception
	 * @return array
	 */
	public function loadDDTs($customerId)
	{
	    $customer = Mage::getModel('customer/customer')->load($customerId);
		$q = array (
				'Login' => $this->login,
				'idCliente' => $customer->getData('PI_codice'),
				'StartRow' => 0,
				'RowCount' => 100 
		);
		$result = $this->client->call ( 'ElencoDDTCliente', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if ($result['ElencoDDTClienteResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			return array($result['ElencoDDTClienteResult']['List']['anyType']);
		}
		elseif ($result['ElencoDDTClienteResult']['Count'] > 1) {
			return $result['ElencoDDTClienteResult']['List']['anyType'];
		} else {
			return array();
		}
	}

	/**
	 * Legge il PDF del DDT selezionato da Ariete 
	 *
	 * @param int $id ID DDT Ariete
	 * @throws Exception
	 * @return Varien_Object
	 */
	public function getPdfDDT($id)
	{
		$q = array (
				'Login' => $this->login,
				'idDdt' => $id
		);
		$result = $this->client->call ( 'PDFDdt', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if (isset($result['PDFDdtResult'])) {
			$response = new Varien_Object();
			$response->setFileContent($result['PDFDdtResult']);
			$response->setFileName("ddt_$id.pdf");
			return $response;
		} else {
			throw new Exception("File PDF non trovato");
		}	    
	}
	
	/**
	 * Legge il PDF del ordine selezionato da Ariete
	 *
	 * @param int $id ID DDT Ariete
	 * @throws Exception
	 * @return Varien_Object
	 */
	public function getPdfOrder($id)
	{
	    $q = array (
	            'Login' => $this->login,
	            'IdOrdine' => $id
	    );
	    $result = $this->client->call ( 'PDFOrdine', $q, $this->namespace );
	    $err = $this->client->getError ();
	    if ($err) {
	        throw new Exception($err);
	    }
	    if (isset($result['PDFOrdineResult'])) {
	        $response = new Varien_Object();
	        $response->setFileContent($result['PDFOrdineResult']);
	        $response->setFileName("ordine_$id.pdf");
	        return $response;
	    } else {
	        throw new Exception("File PDF non trovato");
	    }
	}
	

	/**
	 * Legge il PDF della fattura selezionata da Ariete 
	 *
	 * @param int $id ID fattura Ariete
	 * @throws Exception
	 * @return Varien_Object
	 */
	public function getPdfInvoice($id)
	{
		$q = array (
				'Login' => $this->login,
				'idFattura' => $id
		);
		$result = $this->client->call ( 'PDFFattura', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		if (isset($result['PDFFatturaResult'])) {
			$response = new Varien_Object();
			$response->setFileContent($result['PDFFatturaResult']);
			$response->setFileName("fattura_$id.pdf");
			return $response;
		} else {
			throw new Exception("File PDF non trovato");
		}	    
	}
	
	/**
	 * Restituisce un array con indicato il numero d'ordine e
	 * l'ID prodotto associati al serial number cercato
	 * 
	 * @param string $serial Serial number del prodotto registrato in Ariete
	 * @return array
	 */
	public function getArticleBySerialNumber($serial)
	{
	    $arieteResult = $this->getDDTdaSerialNumber($serial);
	    if(!count($arieteResult)) throw new Exception(Mage::helper("ariete")->__("Serial number non trovato"));
	    $ddtAriete = $this->getLeggiDDT($arieteResult[0]['_iddocumento'], false);
	    /* @var Mage_Sales_Model_Order_Shipment */
	    $shipmentModel = Mage::getModel('sales/order_shipment')->load($ddtAriete['_nregis'] . '/' . $ddtAriete['_annreg'], 'numero_ariete');
	    /* @var $product Mage_Catalog_Model_Product */
	    $product = Mage::getModel('catalog/product');
	    $product = $product->loadByAttribute('id_ariete', $arieteResult[0]['_idarticolo']);
	    return array(
	        'incrementId' => $shipmentModel->getOrder()->getIncrementId(), 
	        'productId' => $product->getId()
	    );
	}
	
	/**
	 * Cerca un serial number in Ariete e restituisce il DDT e il codice articolo corrispondenti.
	 * 
	 * @param string $serial Serial number del prodotto registrato in Ariete
	 * @throws Exception
	 * @return array
	 */
	public function getDDTdaSerialNumber($serial)
	{
		$q = array (
			'Login' => $this->login,
			'SerialNumber' => $serial,
			'StartRow' => 0,
			'RowCount' => 100 
		);
		$result = $this->client->call ( 'SerialNumber', $q, $this->namespace );
		$err = $this->client->getError ();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['SerialNumberResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['SerialNumberResult']['List']['anyType']);
		}
		elseif ($result['SerialNumberResult']['Count'] > 1) {
			$return = $result['SerialNumberResult']['List']['anyType'];
		}
		
		if(count($return) > 1 )
		{
		    //TODO: devo prendere l'ultimo DDT
		}
		return $return;
	}
	
	/**
	 * Retrive product custom options for specified order item
	 * 
	 * @param Mage_Sales_Model_Order_Item $item
	 * @return array
	 */
	private function getItemOptions($item) {
		$result = array();
		if ($options = $item->getProductOptions()) {
			if (isset($options['options'])) {
				$result = array_merge($result, $options['options']);
			}
			if (isset($options['additional_options'])) {
				$result = array_merge($result, $options['additional_options']);
			}
			if (isset($options['attributes_info'])) {
				$result = array_merge($result, $options['attributes_info']);
			}
		}
		return $result;
	}
	
	/**
	 * Formatta il numero ordine ricevuto dal WS di Ariete secondo il
	 * formato che Infordata usa nella stampa degli ordini
	 * 
	 * @param string $nOrdine
	 * @return string
	 */
	private function _formatNumOrdineAriete($nOrdine)
	{
	    return ltrim(substr($nOrdine, 4), '0') . '/' . substr($nOrdine, 0, 4);
	}
	
	private function _reimportCustomerAddress($customer){
	    $i = 0;
	    $arieteId = $customer->getData("PI_codice");
	    
	    if($arieteId){
	        $segreteriaModel = Mage::getModel("ariete/segreteria");
	        $customerAriete = $segreteriaModel->getLeggiCliente($arieteId);
	        if($customerAriete){
	            $i += $segreteriaModel->saveCustomer($customer, $customerAriete, true);
	        }
	        $sa = $segreteriaModel->getElencoDestinazioneMerce($arieteId);
	        $i += $segreteriaModel->saveShippingAddress($sa, $customer);
	        
	    }
	    return $i;
	}
}