<?php
class Dotcom_Ariete_Model_ListCustomerGroup {
	public function toOptionArray() {
		
		$collection = Mage::getModel('customer/group') -> getCollection();

		$result = array();
		$r = array();
		$f = array();
		foreach ($collection as $group) {
			$result = $group -> toArray(array('customer_group_id', 'customer_group_code'));
			$r['value'] = $result['customer_group_id'];
			$r['label'] = $result['customer_group_code'];
			array_push($f,$r);
		}
		
		return  $f;
	}

}
