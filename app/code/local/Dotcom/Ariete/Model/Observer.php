<?php 
class Dotcom_Ariete_Model_Observer
{
    public function importCustomer()
    {
        /* @var $segreteria Dotcom_Ariete_Model_Segreteria */
    	$segreteria = Mage::getModel('ariete/segreteria');
    	$segreteria->import();
    }
    
    public function exportCustomer()
    {
        /* @var $segreteria Dotcom_Ariete_Model_Segreteria */
    	$segreteria = Mage::getModel('ariete/segreteria');
    	$segreteria->export();
    }
    
    public function importRaggruppamenti()
    {
        /* @var $magazzino Dotcom_Ariete_Model_Magazzino */
    	$magazzino = Mage::getModel('ariete/magazzino');
    	$magazzino->importCategorie();
    }
    
    public function importArticoli()
    {
        /* @var $magazzino Dotcom_Ariete_Model_Magazzino */
    	$magazzino = Mage::getModel('ariete/magazzino');
    	$magazzino->importProdotti();
    }
    
    public function importListini()
    {
        /* @var $magazzino Dotcom_Ariete_Model_Magazzino */
    	$magazzino = Mage::getModel('ariete/magazzino');
    	$magazzino->importListiniPrezzi();
    }
    
    public function importQuantita()
    {
        /* @var $magazzino Dotcom_Ariete_Model_Magazzino */
    	$magazzino = Mage::getModel('ariete/magazzino');
    	$magazzino->importQuantitaInventario();
    }
    
    public function exportOrders(){
        /* @var $documenti Dotcom_Ariete_Model_Documenti */
    	$documenti = Mage::getModel('ariete/documenti');
    	$documenti->exportOrders();
    }
    
    public function importOrders(){
        /* @var $documenti Dotcom_Ariete_Model_Documenti */
    	$documenti = Mage::getModel('ariete/documenti');
    	$documenti->importOrders();
    }
    
    public function importDDTs(){
        /* @var $documenti Dotcom_Ariete_Model_Documenti */
    	$documenti = Mage::getModel('ariete/documenti');
    	$documenti->importDDTs();
    }
    
    public function importInvoices(){
        /* @var $documenti Dotcom_Ariete_Model_Documenti */
    	$documenti = Mage::getModel('ariete/documenti');
    	$documenti->importInvoices();
    }
    
    public function runReindexAll(){
        $processes = Mage::getModel('index/process')->getCollection();
        foreach ($processes as $process){
            if ($process->getId()) {
                $process->reindexEverything();
            }
        }
    }
    
    public function verifyCustomerExists($observer)
    {
        /* @var $customer Mage_Customer_Model_Customer */
        $customer = $observer['customer'];
        
        //Se il cliente esiste già in Magento non faccio niente
        if($customer->getId()) return;
        
        /* @var $segreteria Dotcom_Ariete_Model_Segreteria */
        $segreteria = Mage::getModel('ariete/segreteria');
        try
        {
            if($id = $segreteria->esisteClienteInMagento($customer->getEmail(), $customer->getTaxvat()))
            {
			    /* @var $customer Mage_Customer_Model_Customer */
		        $customer = Mage::getModel('customer/customer');
		        $customer->load($id);
		        if($customer->getCompany())
		        {
		            $msg = Mage::helper("customer")->__("L'azienda <i>%s</i> risulta già registrata nel nostro database.<br />Per recuperare le credenziali d’accesso contattare %s oppure il help desk Infordata.", $customer->getCompany(), $customer->getEmail());
		        }
		        else
		        {
		            $msg = Mage::helper("customer")->__("Cliente o Azienda già presente nella base di dati.<br />Per recuperare le credenziali d'accesso contattare il help desk Infordata.");
		        }
                throw new Mage_Core_Exception($msg);
            }
        }
        catch (Mage_Core_Exception $e)
        {
		    Mage::getSingleton('customer/session')->setEscapeMessages(false);
            throw $e;
        }
        catch(Exception $e)
        {
            Mage::log($e->getMessage());
		    Mage::getSingleton('customer/session')->setEscapeMessages(false);
            //Mage::app()->getRequest()->setParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_ERROR_URL, Mage::app()->getStore()->getBaseUrl() . 'nuovo/url');
            throw new Mage_Core_Exception($e->getMessage() . "<br />Per recuperare le credenziali d'accesso contattare il help desk Infordata.");
        }
    }
    
    public function setCustomerUpdatedAtOnAddressSave($observer){
        //return; //for debug;
        $resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('customer/entity');
        $writeConnection = $resource->getConnection('core_write');
        
        $customerId = $observer->getCustomerAddress()->getCustomerId();
        $updatedAt = $observer->getCustomerAddress()->getUpdatedAt();
        $query = "UPDATE {$table} SET updated_at = '{$updatedAt}' WHERE entity_id = ".$customerId;
        $writeConnection->query($query);
    }

    public function salvaRegioniItaliane(){
        $mappa = Mage::helper("ariete")->mappaturaRegioni();
        $limit = 10000;
        
        $connection = Mage::getSingleton('core/resource');
        $read = $connection->getConnection('core_read');
        $write = $connection->getConnection('core_write');
        
        $select = $read->select()->from("dotcom_sync_regioni")->where('isdone = 0')->limit($limit);
        $results = $read->fetchAll($select);
        
        foreach ($results as $res){
            $address = Mage::getModel("customer/address")->load($res["address_id"]);
            $provincia = strtoupper($address->getRegion());
            $write->update("dotcom_sync_regioni", array('isdone' => '1'), array('id = ?' => $res["id"]) );
            if($provincia){
                if(array_key_exists($provincia, $mappa)){
                    $address->setRegion((int)$mappa[$provincia]);
                    $address->setProvincia($provincia);
                    $address->save();
                }
            }
        }
    }
    
    public function salvaRegionId(){
        $connection = Mage::getSingleton('core/resource');
        $read = $connection->getConnection('core_read');
        $select = $read->select()->from("dotcom_sync_regioni AS a")->join("customer_address_entity_varchar AS b", "a.address_id = b.entity_id AND b.attribute_id = 28");
        $results = $read->fetchAll($select);
        foreach ($results as $res){
            $address = Mage::getModel("customer/address")->load($res["address_id"]);
            $region = $address->getRegion();
            if(in_array($region, Mage::helper("ariete")->mappaIdRegioni()) ){
                $address->setRegionId($region)->save();
            }
        }
            
    }
    
}