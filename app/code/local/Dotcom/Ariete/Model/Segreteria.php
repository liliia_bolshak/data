<?php

class Dotcom_Ariete_Model_Segreteria extends Dotcom_Ariete_Model_Abstract
{
	protected $listinoArieteGen;
	protected $listinoArieteRiv;
	protected $listinoArietePar;
	protected $gruppoMagentoGen;
	protected $gruppoMagentoRiv;
	protected $gruppoMagentoPar;
	protected $listinoArieteDapprov;
	protected $gruppoMagentoDapprov;
	protected $listinoArieteAziendaDapprov;
	protected $gruppoMagentoAziendaDapprov;
	protected $listinoArietePrivati;
	protected $gruppoMagentoPrivati;
	
	protected function init()
	{
		$this->wsdl = Mage::getStoreConfig('ariete_section/arietewsdl_group/segreteriawsdl_field');		
		$this->listinoArieteGen =  Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinoutentifinali_field');
		$this->listinoArieteRiv = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinorivenditori_field');
		$this->listinoArietePar = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietelistinopartners_field');
		$this->gruppoMagentoGen = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinoutentifinali_field');
		$this->gruppoMagentoRiv = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinorivenditori_field');
		$this->gruppoMagentoPar = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinopartners_field');
		//aggiunto il listino da approvare
		$this->listinoArieteDapprov = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarietedaapprovare_field');
		$this->gruppoMagentoDapprov = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinodaapprovare_field');
		//aggiunta azienda
		$this->listinoArieteAziendaDapprov = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarieteaziendadaapprovare_field');
		$this->gruppoMagentoAziendaDapprov = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinoarieteaziendadaapprovare_field');
		//aggiunta privati
		$this->listinoArietePrivati = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/idarieteprivati_field');
		$this->gruppoMagentoPrivati = Mage::getStoreConfig('ariete_section/arietecustomergroup_group/listinoarieteprivati_field');
	}

	/**
	 * Importa i clienti da Ariete verso Magento
	 */
	public function import()
	{    	
    	/**
         * Leggiamo dalla tabella ariete_sync per capire dove siamo rimasti con l'ultima sincronizzazione 
    	 */
		$lastImportedDate = $this->_syncModel->getLastImportedEntityId(self::SYNC_TYPE_CLIENTS);
		if(empty($lastImportedDate) || ctype_digit((string) $lastImportedDate))
        {
            $lastImportedDate = '-7 days'; // sincronizzo i clienti modificati sette giorni prima fino ad oggi
        }
        $queryDate = new DateTime($lastImportedDate);
        $lastImportedDate = $queryDate->format('Y-m-d');
		
    	$status = 1; //Indichiamo che non ci sono problemi con la procedura
    	$customersQueried = 0;
    	$customersUpdated = 0;
    	$customersInserted = 0;
    	$addressesInserted = 0;
    	$rowCount = 200; //numero massimo di clienti da sincronizzare
		
		try
		{
		    while($queryDate <= new DateTime() && $rowCount > 0)
		    {
        	    $customers = $this->getElencoClientiModificati($queryDate->format('Y-m-d'));
                $lastImportedDate = $queryDate->format('Y-m-d');
                $rowCount--;
                
        		foreach($customers as $customerAriete){
        		    //if($customerAriete['_email']!= "giuseppe@vigliar.it") continue; //for debug
        		    $customersQueried++;
        			try
        			{
        			    $this->_verificaIndirizzoEmail($customerAriete['_email']);
        			    
    		            if(!isset($customerAriete['_email']) || empty($customerAriete['_email']))
    		            {
    		                throw new Exception('cliente non caricato perché in Ariete il campo email è vuoto');
    		            }
    			            
        			    /* @var $customer Mage_Customer_Model_Resource_Customer */
        			    /* @var $customerCollection Mage_Customer_Model_Resource_Customer_Collection */
    			        $customerCollection = Mage::getModel('customer/customer')->getCollection();
    			        $customer = $customerCollection->addAttributeToFilter('PI_codice', array('eq' => $customerAriete['PI_codice']))->getFirstItem();
    			        /* @var $customer Mage_Customer_Model_Customer */
    			        $customerModel = Mage::getModel('customer/customer');
    			        if(!$customer->getId())
    			        {
    			            if($id = $this->esisteClienteInMagento($customerAriete['_email'], $customerAriete['_pariva']))
    			            {
    			                //ho trovato un cliente in Magento con la stessa email o partita IVA
    			                $customerModel->load($id);
    			                //se il cliente ha già un ID Ariete diverso verifico se posso aggiornarlo
    			                if($customerModel->getData('PI_codice'))
    			                {
    			                    if($c = $this->getLeggiCliente($customerModel->getData('PI_codice')))
    			                    {
    			                        //il vecchio ID Cliente Ariete è ancora presente in Ariete quindi non posso sovrascrivere i dati
    			                        throw new Exception("L'indirizzo email " . $customerAriete['_email'] . " o la partita IVA " . $customerAriete['_pariva'] . " sono già associati ad un altro utente (Keyrif " . $customerModel->getKeyrif() . ", PI_codice " . $customerModel->getData('PI_codice') . ")");
    			                    }
    			                    else
    			                    {
    			                        //il vecchio ID Cliente Ariete è stato eliminato da Ariete quindi procedo con l'aggiornamento del cliente
    			                        $customerModel->setData('PI_codice', $customerAriete['PI_codice']);
    			                    }
    			                }
    			                else 
    			                {
    			                    //Se il cliente non è un cliente Ariete lo notifico e non faccio niente
    			                    throw new Exception("L'indirizzo email " . $customerAriete['_email'] . " o la partita IVA " . $customerAriete['_pariva'] . " sono già associati ad un altro utente (ID " . $customerModel->getId() . ", " . $customerModel->getName() . ")");
    			                }
    			            }
    			        }
    			        else 
    			        {
    			            $customerModel->load($customer->getId());
    			        }
    			        
    			        if($customerModel->getId())
    			        {
    			            $updatingCustomer = true; //è stata trovata una corrispondenza del cliente Ariete in Magento quindi faccio l'update dei dati
    			        }
    			        else
    			        {
    			            Mage::register('utenteinserito', true, true);
    			            $updatingCustomer = false; //il cliente non esiste in Magento allora lo devo inserire
    			        }
        				
    			        //Verifico la data di ultima modifica del cliente in Magento e la confronto con la data di ultimo import da Ariete
    			        //Se il prodotto è stato modificato in Magento dall'ultimo import da Ariete allora non lo sovrascrivo per non cancellare
    			        //le modifiche apportate ai propri dati da parte del cliente in Magento
    			        $lastImportDate = new DateTime($customerModel->getArieteLastImport());
    			        $updatedAt = new DateTime($customerModel->getUpdatedAt());
    			        if($customerModel->getArieteLastImport() && $customerModel->getUpdatedAt() && $lastImportDate < $updatedAt) continue;
    			        
    				    $i = $this->saveCustomer($customerModel, $customerAriete, $updatingCustomer);
    				    $addressesInserted += $i;
        				if($updatingCustomer)
        				{
        				    $customersUpdated++;
        				}
        				else
        				{
        				    $customersInserted++;
        				}
        				$sa = $this->getElencoDestinazioneMerce($customer->getData('PI_codice'));
        				$i = $this->saveShippingAddress($sa, $customerModel);
        				$addressesInserted += $i;
        			}
        			catch(Exception $e)
        			{
						//error mail sender
						                        $emailTemplate = Mage::getModel('core/email_template')
						                            ->loadDefault('ariete_syncronization_error');
						                        $emailTemplateVariables = array();
						                        $emailTemplateVariables['process'] = 'Customers Import';
						                        $emailTemplateVariables['message'] = $e->getMessage();
						                        $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
						                        $mail = Mage::getModel('core/email')
						                            ->setToName('Marko Petelin')
						                            ->setToEmail('marko@infordata.it')
						                            ->setBody($processedTemplate)
						                            ->setSubject('magento syncronization error with scriptname')
						                            ->setFromEmail('magento@infordata.it')
						                            ->setFromName('Ariete import error')
						                            ->setType('html');
						                        $mail->send();
    		            $status = 0;
        				$this->logError("Errore nell'import cliente da Ariete (PI_codice " . $customerAriete['PI_codice'] . "): " . $e->getMessage());
        			}
        		}//foreach
        	    $queryDate->add(new DateInterval('P1D'));
		    }//while
    		
    		$logMessage = "Clienti interrogati: $customersQueried; clienti aggiornati: $customersUpdated; nuovi clienti inseriti: $customersInserted; indirizzi importati: $addressesInserted";
    		$this->logSuccess("Import clienti da Ariete: $logMessage", true);
		}
		catch(Exception $e)
		{
		    $status = 0;
		    $logMessage = "Errore nella lettura dei clienti (Data ultima modifica = " . $queryDate->format('Y-m-d') . ")";
		    $this->logError("Errore nell'import clienti da Ariete: " . $logMessage . "; " . $e->getMessage());
		}
		$this->_syncModel->setStatus($status)
		                ->setType(self::SYNC_TYPE_CLIENTS)
		                ->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_IMPORT)
		                ->setLastEntityId($lastImportedDate)
		                ->setLog($logMessage)
		                ->save();
	}

	/**
	 * Legge i clienti da Ariete
	 * 
	 * @param string $data Data ultima modifica clientin in Ariete
	 * @throws Exception
	 * @return array
	 */
	public function getElencoClientiModificati($data)
	{
		$q = array (
			'Login' => $this->login,
			'data' => $data,
		    'FiltroEmail' => 1,
		    'StartRow' => 0,
            'RowCount' => 1000
		);
		$result =  $this->client->call('ElencoClientiModificati', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['ElencoClientiModificatiResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['ElencoClientiModificatiResult']['List']['anyType']);
		}
		elseif ($result['ElencoClientiModificatiResult']['Count'] > 1) {
			$return = $result['ElencoClientiModificatiResult']['List']['anyType'];
		}
		return $return;
	}
	
	/**
	 * Salva in Magento il cliente da Ariete
	 * 
	 * @param Mage_Customer_Model_Customer $customer
	 * @param array $customerAriete
	 * @throws Exception
	 * @return integer Restituisce il numero di indirizzi inseriti/aggiornati per il cliente salvato
	 */
	public function saveCustomer($customer, $customerAriete, $updatingCustomer){
	    if($customerAriete['_ragso1'] || $customerAriete['_ragso2'])
	    {
		    $company = trim($customerAriete['_ragso1'] . ' ' . $customerAriete['_ragso2']);
	    }
	    else
	    {
	        $company = '';
	    }
		$data = array(
	 		'company' => $company,
	        'firstname' => $customerAriete['rifper'],
	        'lastname' => '',
	        'email' => $customerAriete['_email'],
	        'emailinvioft' => $customerAriete['_emailinvioft'],		
			'emailpec' => $customerAriete['_emailpec'],
	        'PI_codice' =>$customerAriete['PI_codice'],
	        'keyrif' =>$customerAriete['_keyrif'],
	        'taxvat'=>$customerAriete['_pariva'],
	        'group_id' =>$this->_decodificaListino($customerAriete['_numlis']),
	        'codpag_ariete'	=> (isset($customerAriete['_codpag']) ? $customerAriete['_codpag'] : 'BO05'), //di default Bonifico bancario anticipato
	        'privacy1' => 1,
	        //nuovi attributi 12 settembre 2013
	        'codage'=>$customerAriete['_codage'],
	        'codpor'=>$customerAriete['_codpor'],
	        'codspe'=>$customerAriete['_codspe'],
	        'iban'=>$customerAriete['iban'],
		    'ariete_last_import' => date('Y-m-d H:i:s', strtotime('+60 seconds')),
		);

        //set store_id by CustomerAriete state data
        if(isset($customerAriete['_codlng']) && !empty($customerAriete['_codlng'])) {
            $storeId = Mage::getConfig()
                ->getNode('default/storeRatioLanguage/'.$customerAriete['_codlng'])->asArray();
            if($storeId) {
                $data['store_id'] = $storeId;
            }
        }
        //set default store id to 5 value (English)
        if(!isset($data['store_id']) || empty($data['store_id'])) {
            if($storeId = Mage::getConfig()->getNode('default/storeRatioLanguage/GB')->asArray()) {
                $data['store_id'] = $storeId;
            }
        }
		//Si tratta di un nuovo cliente quindi impostiamo una password di default e settiamo che è stato creato in Ariete
		if(!$customer->getId())
		{
		    $data['password'] = $customerAriete['_pariva'] ? $customerAriete['_pariva'] : 'infordata2013';
		    $data['created_in'] = 'Ariete';
	        $data['website_id'] = 1;
		}
		$customer->addData($data);
		Mage::register('utente_da_ariete', true, true); /* per caricare template mail diverso see AW_Followupemail_Model_Rule */
		$customer->save();
		
		//salvo i punti di aw points solo se l'utente è da inserire
	    if(!$updatingCustomer){
    	    $isSubscribedByDefault = Mage::helper('points/config')->getIsSubscribedByDefault();
    	    if ($isSubscribedByDefault) {
    	        $summary = Mage::getModel('points/summary')->loadByCustomer($customer);
    	        $summary
    	        ->setBalanceUpdateNotification(1)
    	        ->setPointsExpirationNotification(1)
    	        ->setUpdateDate(true)
    	        ->save();
    	
    	
    	        $pointsForRegistration = Mage::helper('points/config')
    	        ->getPointsForRegistration();
    	
    	        Mage::getModel('points/api')->addTransaction(
    	        $pointsForRegistration, 'customer_register', $customer, $customer
    	        );
    	    }
		}//fine salvatagio punti
		$customer->setConfirmation(null); //perché un utente sia confermato e possa fare login il campo deve essere vuoto
		//$customer->setOrigData('confirmation', true);
		Mage::register('ariete_skip_changegroup', true); 
		/* faccio in modo che al cambio di gruppo a rivenditore durante un update di ariete non generi mail duplicate in followup @see AW_Followupemail_Model_Events */
		$customer->save();
		
		/* @var $addresses Mage_Customer_Model_Resource_Address_Collection */
		$addresses = $customer->getAddressCollection();
		$addresses->setCustomerFilter($customer)
		          ->addAttributeToSelect('*')
		          ->addAttributeToFilter('ariete_id', $customerAriete['PI_codice']);

		/* @var $customAddress Mage_Customer_Model_Address */
		$customAddress = Mage::getSingleton('customer/address')->unsetData();
		$customAddress->setIsDefaultBilling('1');
		$customAddress->setIsDefaultShipping('1');
		
		if($addresses->count() === 1)
		{
		    $customAddress->load($addresses->getFirstItem()->getId());
		}
		elseif($addresses->count() === 0)
		{
			$customAddress->setIsDefaultBilling('1');
		}
		else
		{
		    throw new Exception("ID Ariete associata a più indirizzi in Magento");
		}
		
		$i = 0;
		$street0 = !empty($customerAriete['_indir1']) ? $customerAriete['_indir1'] : $customerAriete['_indir2'];
		$street1 = empty($customerAriete['_indir1']) ? '' : $customerAriete['_indir2'];
		if(!empty($street0))
		{
			$data = array(
			    'parent_id' => $customer->getId(),
			    'save_in_address_book' => 1,
				'firstname' => $customerAriete['rifper'],
				'lastname' => '',
				'company' => $company,
				'street' => array (
			        '0' => $street0
				),
				'country_id' => $customerAriete['_staest'],
				'postcode' => $customerAriete['_codcap'],
				'city' => $customerAriete['_citta'],
				'provincia' => $customerAriete['_provin'],
				'telephone' => $customerAriete['_numtel'] ? $customerAriete['_numtel'] : $customerAriete['numcel'],
			    'ariete_id' => $customerAriete['PI_codice']
			);
			if(!empty($street1))
			{
			    $data['street']['1'] = $street1;
			}
			if($id_regione = Mage::helper("ariete")->getRegionIdFromAriete( strtoupper($customerAriete['_provin']) )){
			    $data['region_id'] = $id_regione;
			}
			else{
			    $data['region'] = $customerAriete['_provin'];
			}
			
			$customAddress->addData($data)
			              ->save();
			$i++;
		}
		
		
		Mage::unregister('checked_customer');
		Mage::unregister('ariete_skip_changegroup');
		return $i;
	}
	
	/**
	 * 
	 * @param integer $customerId ID del cliente in Ariete
	 * @throws Exception
	 * @return array
	 */
	public function getElencoDestinazioneMerce($customerId)
	{
		$q = array(
		    'Login' => $this->login,
		    'IdCliente' => $customerId
		);
		$result =  $this->client->call('ElencoDestinazioneMerceCliente', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		$return = array();
		if ($result['ElencoDestinazioneMerceClienteResult']['Count'] == 1) {
			// quando ho solo un risultato ariete non mi ritorna un array
			$return = array($result['ElencoDestinazioneMerceClienteResult']['List']['anyType']);
		}
		elseif ($result['ElencoDestinazioneMerceClienteResult']['Count'] > 1) {
			$return = $result['ElencoDestinazioneMerceClienteResult']['List']['anyType'];
		}
		return $return;
	}
	
	/**
	 * Salvo l'indirizzo di spedizione in Magento e lo associo al cliente
	 * 
	 * @param array $addresses Elenco di indirizzi del cliente letti da Ariete  
	 * @param Mage_Customer_Model_Customer $customer
	 * @return integer Restituisce il numero di indirizzi inseriti per il cliente salvato
	 */
	public function saveShippingAddress($addresses, $customer)
	{
	    $i = 0;
		foreach($addresses as $address)
		{
    		/* @var $addresses Mage_Customer_Model_Resource_Address_Collection */
    		$addresses = $customer->getAddressCollection();
    		$addresses->setCustomerFilter($customer)
    		          ->addAttributeToSelect('*')
    		          ->addAttributeToFilter('ariete_id', $address['PI_Id']);
    
    		/* @var $customAddress Mage_Customer_Model_Address */
    		$customAddress = Mage::getSingleton('customer/address')->unsetData();
    		
    		if($addresses->count() === 1)
    		{
    		    $customAddress->load($addresses->getFirstItem()->getId());
    		}
    		elseif($addresses->count() !== 0)
    		{
    		    throw new Exception("ID Ariete associata a più indirizzi in Magento");
    		}
		
			//if($address['_codana'] != $customer->getData('PI_codice')) continue;	
			$street0 = !empty($address['_desag1']) ? $address['_desag1'] : (!empty($address['_desag2']) ? $address['_desag2'] : $address['_desag3']);
			$street1 = empty($address['_desag1']) && !empty($address['_desag2']) ? $address['_desag3'] : $address['_desag2'];
			$street2 = !empty($street0) && !empty($street1) ? $address['_desag3'] : '';
			
			$data = array(
		        'parent_id' => $customer->getId(),
		        'save_in_address_book' => 1,
				'firstname' => '',
		        'lastname' => '',
				'company' => $address['_descri'],
				'street' => array (
			        '0' => $street0
				),
				'postcode' => $address['_codcap'],
				'city' => $address['_locali'],
			    'ariete_id' => $address['PI_Id'],
			    'ariete_codice' => $address['_codice']
			);
			if(!empty($street1))
			{
			    $data['street']['1'] = $street1;
			}
			if(!empty($street2))
			{
			    $data['street']['2'] = $street2;
			}
			
			if($id_regione = Mage::helper("ariete")->getRegionIdFromAriete($address['_provin'])){
			    $data['region_id'] = $id_regione;
			}
			else{
			    $data['region'] = $address['_provin'];
			}

			$customAddress->addData($data);

			if($countryId = $customer->getAddressesCollection()->getFirstItem()->getCountryId())
			{
			    $customAddress->setCountryId($countryId);
			}
			if(!$customer->getDefaultBillingAddress())
			{
			    //$customAddress->setIsDefaultBilling('1');
			}
			if(!$customer->getDefaultShippingAddress())
			{
			    //$customAddress->setIsDefaultShipping('1');
			}	
			
			$customAddress->save();
			$i++;
		}//foreach
		return $i;
	}

	/**
	 * Esporta tutti i clienti di Magento verso Ariete
	 * Estraggo solamente i clienti modificati negli ultimi due giorni. Visto che il cron girerà ogni giorno,
	 * non ha senso sincronizzare ogni volta tutti i clienti nel DB.
	 */
	public function export()
	{
    	/**
         * Leggiamo dalla tabella ariete_sync per capire dove siamo rimasti con l'ultima sincronizzazione 
    	 */
    	$rowCount = 100;
    	$lastExportData = @unserialize($this->_syncModel->getLastExportedEntityId(self::SYNC_TYPE_CLIENTS));
    	if(!is_array($lastExportData))
    	{
    	    $lastExportData = array('lastPage' => null, 'lastExecutionTime' => time());
    	}
		$lastPage = $lastExportData['lastPage'];
		$lastExecutionTime = $lastExportData['lastExecutionTime'];
		if($lastPage === null)
		{
		    $lastPage = 2;
		    $startPage = 1;
		}
		else 
		{
		    $startPage = $lastPage;
		    $lastPage += 1; 
		}
		
	    /* @var $customerCollection Mage_Customer_Model_Resource_Customer_Collection */
		$customerCollection = Mage::getModel('customer/customer')->getCollection()
		                                                         ->addAttributeToSelect('PI_codice')
                                                        		 //->addAttributeToFilter('PI_codice', array('null'=>true, 'eq'=>''), 'left')
                                                        		 //->addAttributeToFilter('keyrif', array('null'=>true, 'eq'=>''), 'left')
																 //->addAttributeToFilter('PI_codice', array('eq'=>23161), 'left') //for debug
                                                        		 ->addAttributeToFilter('updated_at', array('gt' => date('Y-m-d H:i:s', $lastExecutionTime)))
                                                        		 ->setPageSize($rowCount)
                                                        		 ->setCurPage($startPage)
		                                                         ->setOrder('created_at', Mage_Customer_Model_Resource_Customer_Collection::SORT_ORDER_DESC);		
																
	    if($customerCollection->getLastPageNumber() < $startPage)
	    {
	        $lastPage = 2;
	        $startPage = 1;
    		$customerCollection->setCurPage($startPage)->load();
	    }
	    
	    if($customerCollection->getLastPageNumber() == $startPage)
	    {
	        $lastExecutionTime = time();
	    }

		$status = 1; //Indichiamo che non ci sono problemi con la procedura
		$customersQueried = $customerCollection->count();
    	$customersUpdated = 0;
    	$customersInserted = 0;
    	$addressesUpdated = 0;
    	$addressesInserted = 0;
		
		foreach ($customerCollection as $item)
		{	
		    //if($item->getEmail() != "dotcom_fabio@yopmail.com") continue; //for debug
		    /* @var $customer Mage_Customer_Model_Customer */
    		$customer = Mage::getModel('customer/customer')->load($item->getId());
    		$customerAddresses = $customer->getPrimaryAddresses();
    		if(count($customerAddresses) == 0)
    		{
    		    $customerAddress = $customer->getAddressesCollection()->getFirstItem();
    		}
    		else
    		{
    		    $customerAddress = $customerAddresses[0];
    		}
    		
    		//uso questa varibile per segnare l'ID dell'indirizzo esportato come principale nella scheda cliente in Ariete
    		$addressAlredyUsed = $customerAddress->getId();
    		
		    try
		    {
        		if($item->getData('PI_codice'))
        		{
			        //Verifico la data di ultima modifica del cliente in Magento e la confronto con la data di ultimo import da Ariete
			        //Se il prodotto non è stato modificato in Magento dall'ultimo import da Ariete allora non lo esport verso Ariete
			        /* controllo inutile dopo la modifica del filtro ('updated_at', array('gt' => date('Y-m-d H:i:s', $lastExecutionTime)))
			        $lastImportDate = new DateTime($customer->getArieteLastImport());
			        $updatedAt = new DateTime($customer->getUpdatedAt());
			        if($customer->getArieteLastImport() && $customer->getUpdatedAt() && $lastImportDate >= $updatedAt) continue;
			        */
    			        
    		        $clienteAriete = $this->getLeggiCliente($item->getData('PI_codice'));
        		    if(!$clienteAriete)
        		    {
			            throw new Exception("ID Ariete associato al cliente Magento (ID " . $item->getId() . ") non presente nel DB di Ariete");
        		    }
        		    else
        		    {
        		        $modificato = false;
                       
                        if($customerAddress->getCompany() != '')
                        {
                            $company = $this->_spezzaRagioneSociale($customerAddress->getCompany());
                        }
                        
                        elseif($customer->getCompany() != '')
                        {
                            $company = $this->_spezzaRagioneSociale($customer->getCompany());
                        }
                        elseif($customer->getName() != '')
                        {
                            $company = $this->_spezzaRagioneSociale($customer->getName());
                        }
                        else
                        {
                            $company = $this->_spezzaRagioneSociale($customerAddress->getName());
                        }
        		        $_ragso1 = strtok($company, "\n");
        		        $_ragso2 = strtok("\n");
        		        if($_ragso1 && $_ragso1 != $clienteAriete['_ragso1'])
        		        {
        		            $clienteAriete['_ragso1'] = $_ragso1;
        		            $clienteAriete['_ragso2'] = $_ragso2;
        		            $modificato = true;
        		        }
        		        if($customerAddress->getStreet(1) && $customerAddress->getStreet(1) != $clienteAriete['_indir1'])
        		        {
        		            $clienteAriete['_indir1'] = $customerAddress->getStreet(1);
        		            $modificato = true;
        		        }
        		        if($customerAddress->getStreet(2) && $customerAddress->getStreet(2) != $clienteAriete['_indir2'])
        		        {
        		            $clienteAriete['_indir2'] = $customerAddress->getStreet(2);
        		            $modificato = true;
        		        }
        		        if($customerAddress->getPostcode() && $customerAddress->getPostcode() != $clienteAriete['_codcap'])
        		        {
        		            $clienteAriete['_codcap'] = $customerAddress->getPostcode();
        		            $modificato = true;
        		        }
        		        if($customerAddress->getCity() && $customerAddress->getCity() != $clienteAriete['_citta'])
        		        {
        		            $clienteAriete['_citta'] = $customerAddress->getCity();
        		            $modificato = true;
        		        }
        		        if($customerAddress->getProvincia() && $customerAddress->getProvincia() != $clienteAriete['_provin'])
        		        {
        		            $clienteAriete['_provin'] = $customerAddress->getProvincia();
        		            $modificato = true;
        		        }
        		        else if(!$customerAddress->getProvincia() && $customerAddress->getRegion() && $customerAddress->getRegion() != $clienteAriete['_provin'])
        		        {
        		            $clienteAriete['_provin'] = $customerAddress->getRegion();
        		            $modificato = true;
        		        }
        		        if($customerAddress->getCountryId() && $customerAddress->getCountryId() != $clienteAriete['_staest'])
        		        {
        		            $clienteAriete['_staest'] = $customerAddress->getCountryId();
        		            $modificato = true;
        		        }
        		        if($customerAddress->getTelephone() && $customerAddress->getTelephone() != $clienteAriete['_numtel'])
        		        {
        		            $clienteAriete['_numtel'] = $customerAddress->getTelephone();
        		            $modificato = true;
        		        }
        		        if($customerAddress->getFax() && $customerAddress->getFax() != $clienteAriete['_numfax'])
        		        {
        		            $clienteAriete['_numfax'] = $customerAddress->getFax();
        		            $modificato = true;
        		        }
        		        if($customer->getEmail() && $customer->getEmail() != $clienteAriete['_email'])
        		        {
        		            $clienteAriete['_email'] = $customer->getEmail();
        		            $modificato = true;
        		        }
        		        if($customer->getEmailinvioft() && $customer->getEmailinvioft() != $clienteAriete['_emailinvioft'])
        		        {
        		            $clienteAriete['_emailinvioft'] = $customer->getEmailinvioft();
        		            $modificato = true;
        		        }
        		        if($customer->getEmailpec() && $customer->getEmailpec() != $clienteAriete['_emailpec'])
        		        {
        		            $clienteAriete['_emailpec'] = $customer->getEmailpec();
        		            $modificato = true;
        		        }
        		        $group_id = $this->_decodificaListino($clienteAriete['_numlis']);
        		        if($customer->getGroupId() && $customer->getGroupId() != $group_id)
        		        {
        		            $clienteAriete['_numlis'] = $this->_codificaListino($customer->getGroupId());
        		            $modificato = true;
        		        }
        		        if($customerAddress->getCountryId() && $customerAddress->getCountryId() != $clienteAriete['_codlng'])
        		        {
        		            switch($customerAddress->getCountryId())
        		            {
        		                case 'IT':
        		                    $clienteAriete['_codlng'] = 'IT';
        		                    break;
        		                case 'ES':
        		                    $clienteAriete['_codlng'] = 'ES';
        		                    break;
        		                default:
        		                    $clienteAriete['_codlng'] = 'GB';
        		            }
        		            $modificato = true;
        		        }
        		        if($customer->getTaxvat() && $customer->getTaxvat() != $clienteAriete['_pariva'])
        		        {
        		            $clienteAriete['_pariva'] = $customer->getTaxvat();
        		            $modificato = true;
        		        }
        		        if($customer->getName() && trim($customer->getName()) != trim($clienteAriete['rifper']))
        		        {
        		            $clienteAriete['rifper'] = $customer->getName();
        		            $modificato = true;
        		        }

        		        if($modificato)
                        {
                            $this->modificaClienteAriete($clienteAriete);
                            $this->_saveArieteLastImport($customer->getId());
                            $customersUpdated++;
                        }
        		    }
        		}
        		else
        		{
                    if($customer->getCompany() != '')
                    {
                        $company = $this->_spezzaRagioneSociale($customer->getCompany());
                    }
                    elseif($customerAddress->getCompany() != '')
                    {
                        $company = $this->_spezzaRagioneSociale($customerAddress->getCompany());
                    }
                    elseif($customer->getName() != '')
                    {
                        $company = $this->_spezzaRagioneSociale($customer->getName());
                    }
                    else
                    {
                        $company = $this->_spezzaRagioneSociale($customerAddress->getName());
                    }
                    $_ragso1 = strtok($company, "\n");
    		        $_ragso2 = strtok("\n");
		            switch($customerAddress->getCountryId())
		            {
		                case 'IT':
		                    $_codlng = 'IT';
		                    break;
		                case 'ES':
		                    $_codlng = 'ES';
		                    break;
		                default:
		                    $_codlng = 'GB';
		            }
            		$clienteAriete = array(
        				'PI_codice' => '0',
        		        '_keyrif' => '',
        		        '_codcat' => ($customerAddress->getCountryId() == 'IT' ? '01' : '03'),
        		        '_ragso1' => $_ragso1,
        		        '_ragso2' => $_ragso2,
        		        '_indir1'=> $customerAddress->getStreet(1),
        		        '_indir2'=> $customerAddress->getStreet(2),
        		        '_codcap'=> $customerAddress->getPostcode(),
        		        '_citta'=>$customerAddress->getCity(),
        		        '_provin'=>$customerAddress->getProvincia() ? $customerAddress->getProvincia() : $customerAddress->getRegion(),
        		        '_staest'=>$customerAddress->getCountryId(),
        		        '_numtel'=>$customerAddress->getTelephone(),
        		        '_numfax'=>$customerAddress->getFax(),
        		        '_email'=>$customer->getEmail(),
        		        '_emailinvioft'=>$customer->getEmailinvioft(),
        				'_emailpec'=>$customer->getEmailpec(),
        		        '_codiva' => $this->_getCodiceIvaAriete($customer->getTaxvat(), $customerAddress->getCountryId()),
        		        '_codcve' => 'V01',
        		        '_numlis'=>$this->listinoArieteGen,
        		        '_codlng'=> $_codlng, 
        		        '_pariva'=>$customer->getTaxvat(),
        		        //'numcel'=>'',
        		        'rifper'=>$customer->getName(),
        		        'datdis'=>''
            		);
            		//se il cliente si è registrato come rivenditore da approvare lo associo al listino ariete apposito
            		if ($item->getGroupId() == $this->gruppoMagentoDapprov){ 
            		    $clienteAriete['_numlis'] = $this->listinoArieteDapprov;
            		}
            		else if ($item->getGroupId() == $this->gruppoMagentoAziendaDapprov){ 
            		    $clienteAriete['_numlis'] = $this->listinoArieteAziendaDapprov;
            		}
            		else if ($item->getGroupId() == $this->gruppoMagentoPrivati){
            		    $clienteAriete['_numlis'] = $this->listinoArietePrivati;
            		}
            		
        			$return =  $this->inserisciClienteAriete($clienteAriete);
    			    $customer->setKeyrif($return['_keyrif']);
    			    $customer->setData('PI_codice', $return['PI_codice']);
    			    $customer->save();
    			    $customersInserted++;
        		}//else
        		    
        		//Comincio ad esportare gli indirizzi
        		$customerCodana = $this->getIdContattoDelCliente($customer->getData('PI_codice'));
        		$customerAddresses = $customer->getAddressesCollection();
        		foreach ($customerAddresses As $address)
        		{
        		    /* @var $address Mage_Customer_Model_Address */
        		    
        		    if($address->getId() == $addressAlredyUsed) continue;

        		    //Estraggo il nome dell'azienda o il nome e cognome del contatto
        		    if($address->getCompany() != '')
        		    {
        		        $company = $this->_spezzaRagioneSociale($address->getCompany());
        		    }
        		    else
        		    {
        		        $company = $this->_spezzaRagioneSociale($address->getName());
        		    }
                    $_descri = strtok($company, "\n");
    		        $_desag1 = strtok("\n");
        		    
        		    $datiAriete = array(
        		        'PI_Id'   => $address->getArieteId() ? $address->getArieteId() : 0,
        		        '_codice' => $address->getArieteId() ? $address->getArieteCodice() : $address->getId(),
        		        '_codana' => $customerCodana,
        		        '_descri' => $_descri,
        		        '_desag1' => $_desag1,
        		        '_desag2' => $address->getStreet(1),
        		        '_desag3' => $address->getStreet(2),
        		        '_codcap' => $address->getPostcode(),
        		        '_locali' => $address->getCity(),
        		        '_provin' => $address->getProvincia() ? $address->getProvincia() : $address->getRegion()
        		    );
        		    
        		    if($address->getArieteId())
        		    {
        		        if(!$address->getArieteCodice())
        		        {
        		            //leggo la destinazione ed estraggo il codice dell'indirizzo
        		            $result = $this->getLeggiDestinazioneMerce($address->getArieteId());
        		            if(!isset($result['_codice'])) throw new Exception("Indirizzo destinazione merce (ID " . $address->getArieteId() . ") non presente in Ariete. Impossibile caricare il campo _codice.");
        		            $datiAriete['_codice'] = $result['_codice'];
        		        }
        		        
        		        $return = $this->modificaDestinazioneMerce($datiAriete);
        		        if($return)
        		        {
        		            $addressesUpdated++;
        		            $this->_saveArieteLastImport($address->getCustomerId());
        		        }
        		        else
        		        {
        		            $this->logError("Indirizzo destinazione merce (ID " . $address->getArieteId() . ") non agiornato in Ariete");
        		        }
        		    }
        		    else
        		    {
        		        $return = $this->inserisciDestinazioneMerce($datiAriete);
        		        if($return)
        		        {
        		            $addressesInserted++;

        		            /* @var $customAddress Mage_Customer_Model_Address */
        		            $customAddress = Mage::getSingleton('customer/address')->unsetData();
        		            $customerAddress->load($address->getId())
        		                            ->setArieteId($return)
        		                            ->save();
        		            $this->_saveArieteLastImport($address->getCustomerId());
        		        }
        		        else
        		        {
        		            $this->logError("Indirizzo destinazione merce del cliente Magento (ID " . $customer->getId() . ") non inserito in Ariete");
        		        }
        		    }
        		}//foreach
			}
			catch(Exception $e)
			{
	            $status = 0;
				$this->logError("Errore nell'export cliente verso Ariete (ID " . $customer->getId() . ") in data " . date('Y-m-d H:i:s', $lastExecutionTime) . ": " . $e->getMessage());
			}
		}//foreach
		
		$logMessage = "Clienti interrogati: $customersQueried; clienti inseriti: $customersInserted; clienti aggiornati: $customersUpdated; indirizzi inseriti: $addressesInserted; indirizzi aggiornati: $addressesUpdated";
		$this->logSuccess("Esportati i clienti verso Ariete in data " . date('Y-m-d H:i:s', $lastExecutionTime) . ": $logMessage", true);

		$this->_syncModel->setStatus($status)
                		->setType(self::SYNC_TYPE_CLIENTS)
                		->setDirection(Dotcom_Ariete_Model_Sync::DIRECTION_EXPORT)
                		->setLastEntityId(serialize(array('lastPage' => $lastPage, 'lastExecutionTime' => $lastExecutionTime)))
                		->setLog($logMessage)
                		->save();
	}

	/**
	 * Chiamata al Web-service di Ariete per leggere i dat di un cliente
	 *
	 * @param integer $id_ariete
	 * @throws Exception
	 * @return array
	 */
	public function getLeggiCliente($id_ariete){
		$q = array('Login' => $this->login, 'Id' => $id_ariete);
		$result =  $this->client->call('LeggiCliente', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		
		if(!isset($result['LeggiClienteResult']['_keyrif'])){
			return null;
		}
		else{
			return $result['LeggiClienteResult'];
		}
	}
	
	/**
	 * Chiamata al Web-service di Ariete per l'inserimento di un nuovo cliente
	 * 
	 * @param array $dati
	 * @throws Exception
	 * @return array
	 */
	public function inserisciClienteAriete($dati)
	{
		$q = array('Login' => $this->login, 'Dati' => $dati);
		$result =  $this->client->call('InserisciCliente', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		return $result['InserisciClienteResult'];
	}

	/**
	 * Chiamata al Web-service di Ariete per l'aggiornamento di un cliente
	 *
	 * @param array $dati
	 * @throws Exception
	 * @return boolean
	 */
	public function modificaClienteAriete($dati)
	{
		$q = array('Login' => $this->login, 'Dati' => $dati);
		$result =  $this->client->call('ModificaCliente', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		
		return (boolean) $result['ModificaClienteResult'];
	}
	
	/**
	 * Chiamata al Web-service di Ariete per leggere il codice contatto di un cliente
	 * 
	 * @param integer $idCliente
	 * @throws Exception
	 * @return integer
	 * 
	 */
	public function getIdContattoDelCliente($idCliente)
	{
		$q = array('Login' => $this->login, 'idCliente' => $idCliente);
		$result =  $this->client->call('IdContattoDelCliente', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		
		if(!isset($result['IdContattoDelClienteResult']) || empty($result['IdContattoDelClienteResult'])){
			return null;
		}
		else{
			return $result['IdContattoDelClienteResult'];
		}
	}
	
	/**
	 * Chiamata al Web-service di Ariete per leggere il codice di un indirizzo
	 * 
	 * @param integer $id ID Ariete dell'indirizzo
	 * @throws Exception
	 * @return array
	 * 
	 */
	public function getLeggiDestinazioneMerce($id)
	{
		$q = array('Login' => $this->login, 'Id' => $id);
		$result =  $this->client->call('LeggiDestinazioneMerce', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		
		if(!isset($result['LeggiDestinazioneMerceResult']) || empty($result['LeggiDestinazioneMerceResult']) || empty($result['LeggiDestinazioneMerceResult']['_codana'])){
			return null;
		}
		else{
			return $result['LeggiDestinazioneMerceResult'];
		}
	}
	
	/**
	 * Chiamata al Web-service di Ariete per l'inserimento di una nuova destinazione merce per un cliente
	 * 
	 * @param array $dati
	 * @throws Exception
	 * @return integer
	 */
	public function inserisciDestinazioneMerce($dati)
	{
		$q = array('Login' => $this->login, 'Dati' => $dati);
		$result =  $this->client->call('InserisciDestinazioneMerce', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		return $result['InserisciDestinazioneMerceResult']['PI_Id'];
	}

	/**
	 * Chiamata al Web-service di Ariete per l'aggiornamento di una destinazione merce del cliente cliente
	 *
	 * @param array $dati
	 * @throws Exception
	 * @return boolean
	 */
	public function modificaDestinazioneMerce($dati)
	{
		$q = array('Login' => $this->login, 'Dati' => $dati);
		$result =  $this->client->call('ModificaDestinazioneMerce', $q, $this->namespace);
		$err = $this->client->getError();
		if ($err) {
			throw new Exception($err);
		}
		
		return (boolean) $result['ModificaDestinazioneMerceResult'];
	}
	
	/**
	 * Verifica la presenza di un cliente in Magento controllando l'indirizzo email o la partita IVA.
	 * Come ultima istanza si verifica anche la presenza di un indirizzo email appartenente allo stesso dominio
	 * purché non sia un dominio di tipo pubblico (Gmail, Yahoo, MSN, ecc.)
	 *  
	 * @param string $email
	 * @param string $piva
	 * @throws Exception
	 * @return integer
	 */
	public function esisteClienteInMagento($email, $piva)
	{
	    /* @var $customerCollection Mage_Customer_Model_Resource_Customer_Collection */
		$customerCollection = Mage::getModel('customer/customer')->getCollection();
		
		$items = $customerCollection->addAttributeToFilter('email', $email);
		if($items->count() == 1)
		{
		   return $items->getFirstItem()->getId(); 
		}

		$customerCollection = Mage::getModel('customer/customer')->getCollection();
		$items = $customerCollection->addAttributeToFilter('taxvat', $piva);
		if($items->count() == 1)
		{
		   return $items->getFirstItem()->getId(); 
		}
		else if($items->count() > 1)
		{
		    throw new Exception("La partita IVA $piva è associata a più clienti.");
		}
		
		return 0;

// 		if(preg_match('#^[^@]+(@([^@\.]+)\.[^@]+)$#', $email, $matches))
// 		{
// 		    if(in_array(strtolower($matches[2]), array('yahoo', 'gmail', 'msn', 'hotmail', 'outlook', 'virgiglio', 'tiscali', 'tiscalinet', 'libero', 'tin')))
// 		    {
// 		        //throw new Exception("Indirizzo email $email troppo generico per eseguire la ricerca");
// 		        return 0;
// 		    }

// 		    $customerCollection = Mage::getModel('customer/customer')->getCollection();
//     		$items = $customerCollection->addAttributeToFilter('email', array('like' => '%' . $matches[1]));
//     		if($items->count() == 1)
//     		{
//     		   return $items->getFirstItem()->getId(); 
//     		}
//     		else if($items->count() > 1)
//     		{
//     		    throw new Exception("Il dominio " . $matches[1] . " è associato a più clienti.");
//     		}
//     		else
//     		{
//     		    return 0;
//     		}
// 		}
// 		else
// 		{
// 		    throw new Exception("Indirizzo email $email errato.");
// 		}
	}
	
	/**
	 * Funzione di decodifica dei listini prezzi Ariete
	 * 
	 * @param string $numlistAriete Identificativo del listino in Ariete
	 * @return string Identificativo del listino in Magento
	 * @throws Exception
	 */
	private function _decodificaListino($numlistAriete)
	{
		if($numlistAriete == $this->listinoArieteGen)
			return $this->gruppoMagentoGen;
		if($numlistAriete == $this->listinoArieteRiv)
			return $this->gruppoMagentoRiv;
		if($numlistAriete == $this->listinoArietePar)
			return $this->gruppoMagentoPar;
		if($numlistAriete == $this->listinoArieteDapprov)
		    return $this->gruppoMagentoDapprov;
		if($numlistAriete == $this->listinoArieteAziendaDapprov)
		    return $this->gruppoMagentoAziendaDapprov;
		if($numlistAriete == $this->listinoArietePrivati)
		    return $this->gruppoMagentoPrivati;
		throw new Exception("Listino Ariete ($numlistAriete) non configurato in Magento");
	}
	
	/**
	 * Funzione di codifica dei listini prezzi Ariete
	 * 
	 * @param string $numlistMagento Identificativo del listino in Magento
	 * @return string Identificativo del listino in Ariete
	 * @throws Exception
	 */
	private function _codificaListino($numlistMagento)
	{
		if($numlistMagento == $this->gruppoMagentoGen)
			return $this->listinoArieteGen;
		if($numlistMagento == $this->gruppoMagentoRiv)
			return $this->listinoArieteRiv;
		if($numlistMagento == $this->gruppoMagentoPar)
			return $this->listinoArietePar;
		if($numlistMagento == $this->gruppoMagentoDapprov)
		    return $this->listinoArieteDapprov;
		if($numlistMagento == $this->gruppoMagentoAziendaDapprov)
		    return $this->listinoArieteAziendaDapprov;
		if($numlistMagento == $this->gruppoMagentoPrivati)
		    return $this->listinoArietePrivati;
		throw new Exception("Listino Magento ($numlistMagento) non configurato in Ariete");
	}
	
	/**
	 * Definisce il campo _codiva di Ariete. Se il cliente ha la partita IVA Italiana
	 * restituisce 21, se ha una partita IVA Europea restituisce 41, se la partita IVA
	 * è fuori Europa restituisce 81A, se non ha partita IVA restituisce vuoto.
	 * 
	 * @param string $partIva Partita IVA del cliente
	 * @param string $countryId Paese di appartenenza del cliente
	 * @return string
	 */
	private function _getCodiceIvaAriete($partIva, $countryId)
	{
	    if($partIva)
	    {
	        if($countryId == 'IT')
	        {
	            return '21';
	        }
	        elseif(in_array($countryId, array('AT', 'BE', 'BG', 'CY', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'IE', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'GB', 'CZ', 'RO', 'SK', 'SI', 'ES', 'SE', 'HU')))
	        {
	            return '41';
	        }
	        else
	        {
	            return '81A';
	        }
	    }
	    else
	    {
	        return '';
	    }
	}
	
	/**
	 * Prende il campo company salvato in Magento e lo spezza in due per salvarlo in Ariete.
	 * In Ariete i campi sono due di massimo 40 caratteri ognuno: _ragso1 e _ragso2.
	 * 
	 * @param string $company
	 * @return string
	 */
	private function _spezzaRagioneSociale($company)
	{
	    if(strlen($company) <= 40)
	    {
	        return $company;
	    }
	    else
	    {
	        return wordwrap($company, 40, "\n");
	    }
	}
	
	/**
	 * Verifica l'indirizzo email estratto da Ariete se contine più indirizzi separati da virgola o punto e virgola.
	 * Nel caso estraggo solamente il primo indirizzo e ignoro tutto quanto dopo la prima virgola.
	 *  
	 * @param string $email
	 * @return string
	 */
	private function _verificaIndirizzoEmail(&$email)
	{
	    if(preg_match('#^([^,;]+)([,;][^,;]+)?$#', $email, $matches))
	    {
	        $email = $matches[1];
	    }
	}
	
	private function _saveArieteLastImport($customerId){
	    $resource = Mage::getSingleton('core/resource');
	    $attribute = Mage::getModel('eav/entity_attribute')->loadByCode(1, "ariete_last_import");
	    $table = $attribute->getBackendTable();
	    $attributeId = $attribute->getId();
	    $writeConnection = $resource->getConnection('core_write');
	    
	    $ariete_last_import = date('Y-m-d H:i:s', strtotime('+60 seconds'));
        $query = "UPDATE {$table} SET value = '{$ariete_last_import}' WHERE entity_id = ".$customerId." AND attribute_id = ".$attributeId;
	    $writeConnection->query($query);
	}
	
}