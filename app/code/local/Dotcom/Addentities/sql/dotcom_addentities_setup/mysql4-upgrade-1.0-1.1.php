// <?php
/* @var $this Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/* @var $category Mage_Catalog_Model_Category */
$category = Mage::getModel('catalog/category');
$category->setIsActive(true)
         ->setDisplayMode('PRODUCTS')
         ->setIsAnchor(1)
         ->setIncludeInMenu(1)
         ->setAttributeSetId($category->getDefaultAttributeSetId());

/**
 * Settaggio delle sotto categorie della categoria brand
 */

$parentCategory = $category->loadByAttribute('url_key', "brand");

$category->unsetData('entity_id')->unsetData('url_key')->setName('ACS')->setPath($parentCategory->getPath())->setIdCatAriete("AC")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('AXIS')->setPath($parentCategory->getPath())->setIdCatAriete("AI")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Atmel')->setPath($parentCategory->getPath())->setIdCatAriete("AT")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('AxxessID')->setPath($parentCategory->getPath())->setIdCatAriete("AX")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('CIM')->setPath($parentCategory->getPath())->setIdCatAriete("CM")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Cipi')->setPath($parentCategory->getPath())->setIdCatAriete("CP")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Card Presso')->setPath($parentCategory->getPath())->setIdCatAriete("CPR")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Datacard')->setPath($parentCategory->getPath())->setIdCatAriete("DC")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Datalogic')->setPath($parentCategory->getPath())->setIdCatAriete("DL")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('DNP')->setPath($parentCategory->getPath())->setIdCatAriete("DN")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Evolis')->setPath($parentCategory->getPath())->setIdCatAriete("EV")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Fargo')->setPath($parentCategory->getPath())->setIdCatAriete("FR")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Giga')->setPath($parentCategory->getPath())->setIdCatAriete("GG")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Infineon')->setPath($parentCategory->getPath())->setIdCatAriete("IF")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Infordata')->setPath($parentCategory->getPath())->setIdCatAriete("IS")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('IDTech')->setPath($parentCategory->getPath())->setIdCatAriete("IT")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Javelin')->setPath($parentCategory->getPath())->setIdCatAriete("JV")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Lucky Technology')->setPath($parentCategory->getPath())->setIdCatAriete("LT")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Magic Card')->setPath($parentCategory->getPath())->setIdCatAriete("MC")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Milestone')->setPath($parentCategory->getPath())->setIdCatAriete("MI")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Magtek')->setPath($parentCategory->getPath())->setIdCatAriete("MT")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Martin Yale')->setPath($parentCategory->getPath())->setIdCatAriete("MY")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Opticon')->setPath($parentCategory->getPath())->setIdCatAriete("OC")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Perco')->setPath($parentCategory->getPath())->setIdCatAriete("PE")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('SCM')->setPath($parentCategory->getPath())->setIdCatAriete("SC")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Secugen')->setPath($parentCategory->getPath())->setIdCatAriete("SG")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Tensator')->setPath($parentCategory->getPath())->setIdCatAriete("TE")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('wbe')->setPath($parentCategory->getPath())->setIdCatAriete("WBE")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Zebra')->setPath($parentCategory->getPath())->setIdCatAriete("ZB")->save();

$category->unsetData('entity_id')->unsetData('url_key')->setName('Axess Zucchetti')->setPath($parentCategory->getPath())->setIdCatAriete("ZZ")->save();

$installer->endSetup();