<?php 
class Dotcom_Tracking_TrackController extends Mage_Core_Controller_Front_Action
{
    public function getTntXmlAction()
    {
        $url = $this->getRequest()->getPost();
        $url = $url["url"];
        //$url = "http://www.tnt.it/tracking/getXMLTrack?WT=1&ConsigNos=LR00642573"; //testing url
        $xml = file_get_contents($url);
        try {
            $doc = @simplexml_load_string($xml);
            $valid = true;
        } catch (Exception $e) {
            $valid = false;
        }
        $html = "No result";
        if($valid){
            if($doc->Consignment->StatusDetails){
                $html = "<tr><th>Data</th><th>Descrizione</th><th>Dove si trova</th></tr>";
                foreach ( $doc->Consignment->StatusDetails as $status ){
                    $html .= "<tr>
                            <td class='data'>".(string)$status->StatusDate."</td>
                            <td class='desc'>".(string)$status->StatusDescription."</td>
                            <td class='depo'>".(string)$status->Depot."</td>
                            </tr>";
                }
            }
        }
        echo $html;
    }
    
    public function getGlsXmlAction()
    {
        $url = $this->getRequest()->getPost();
        $url = $url["url"];
        //$url = "http://www.gls-italy.com/scripts/cgiip.exe/get_xml_track.p?locpartenza=TS&bda=1200&CodCli=634"; //testing url
        $xml = file_get_contents($url);
        try {
            $doc = @simplexml_load_string($xml);
            $valid = true;
        } catch (Exception $e) {
            $valid = false;
        }
        $html = "<p>No result</p>";
        if($valid){
            if($doc->SPEDIZIONE->TRACKING){
                $html = "<tr><th>Data</th><th>Descrizione</th><th>Dove si trova</th><th>Stato</th></tr>";
                $tracking = (array)$doc->SPEDIZIONE->TRACKING;
                $count = count($tracking["Data"]);
                for($i=0;$i<$count;$i++){
                    $html .= "<tr>
                            <td class='data'>".(string)$tracking["Data"][$i]." ".$tracking["Ora"][$i]."</td>
                            <td class='desc'>".(string)$tracking["Note"][$i]."</td>
                            <td class='depo'>".(string)$tracking["Luogo"][$i]."</td>
                            <td class='stato'>".(string)$tracking["Stato"][$i]."</td>
                            </tr>";
                }
            }
        }
    
        echo $html;
    }
}