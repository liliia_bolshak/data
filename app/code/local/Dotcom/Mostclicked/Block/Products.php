<?php

class Dotcom_Mostclicked_Block_Products extends Mage_Catalog_Block_Product_List
{
    
    public function getMostClicked(){
        return Mage::getResourceModel('reports/product_collection')
        ->addOrderedQty()
        ->addAttributeToSelect(array('name', 'final_price', 'small_image', 'price'))->addViewsCount()
        ->setPageSize(4)->load();
    }
    
    public function getOfferte($number = 4)
    {
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $special = Mage::getResourceModel('reports/product_collection')
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('visibility', array('neq'=>1))
        ->addAttributeToFilter('status', array('eq'=>1))
        ->addAttributeToFilter('special_price', array('neq'=>''))
        ->setPageSize($number)
        ->addAttributeToFilter('special_from_date', array('date' => true, 'to' => $todayDate))
        ->addAttributeToFilter('special_to_date', array('or'=> array(
                0 => array('date' => true, 'from' => $todayDate),
                1 => array('is' => new Zend_Db_Expr('null')))
        ), 'left')
        ->addAttributeToSort('special_from_date', 'desc');
        return $special;
    }
}

