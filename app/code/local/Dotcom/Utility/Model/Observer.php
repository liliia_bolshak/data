<?php 
class Dotcom_Utility_Model_Observer
{
    public function addMonitoraggioWishlist($observer){
        Mage::getSingleton('core/session')->setAddedtowhishlist(true);
    }    
    
    public function addMonitoraggioCustomerCreate(){
        Mage::getSingleton('core/session')->setCustomercreated(true);
    }

    public function setStatusOnIpnCompleted($observer){
        $order = $observer->getDataObject();
        $paymentMetod = $order->getPayment()->getMethod();
        $state = $order->getState();
        if(($paymentMetod == "hosted_pro" || $paymentMetod == "setefi_cc") && $state == "processing"){
            $order->setState($state, "pagato");
        }
    }
}
	