<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';
class Dotcom_Utility_OnepageController extends Mage_Checkout_OnepageController
{

    /**
     * save checkout billing address
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $vat = $this->getRequest()->getParam("vat_checkout");
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            if($vat){
                $customer->setTaxvat($vat);
                $customer->save();
            }
            
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            //controllo se piva e cf sono stati settati correttamente
            if($customer->getId()){
                $groupId = $customer->getGroupId();
                /*
                if(Mage::helper("dotcom_customer")->isCustomerGroupAzienda($groupId)){ //riveditore
                    $taxvat = $customer->getTaxvat();
                    if(!$taxvat){
                        $result = array();
                        Mage::getSingleton("customer/session")->addError(Mage::helper("core")->__("You have to insert Tax/Vat Number."));
                        $result['redirect'] = Mage::getUrl("customer/account/edit");
                    }
                }
                */
                if(Mage::helper("dotcom_customer")->isCustomerGroupCliente($groupId) && $data["country_id"] == "IT"){ //cliente
                    $cf = $customer->getCf();
                    if(!$cf){
                        $result = array();
                        Mage::getSingleton('customer/session')->addError('Devi inserire il codice fiscale.');
                        $result['redirect'] = Mage::getUrl("customer/account/edit");
                    }
                }
            }
            
            
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
}
