<?php

class Dotcom_ProdottiPersonali_Block_Personali extends Mage_Catalog_Block_Product_List
{
	
	public function getProdottiPersonali(){
	    $visibility = array(
	            Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
	            Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
	    );
		$logged = Mage::getSingleton('customer/session')->isLoggedIn();
		if($logged) {
			$userid = Mage::getSingleton('customer/session')->getCustomer()->getId();
			$userid = '%'.$userid.'%';
			$prodottipersonali = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter('status',1)
			->addFieldToFilter('ariete_allowed_customers', array('like'=>$userid));
		
			return $prodottipersonali;
		}
	}
}

?>