<?php

require_once 'JR/CleverCms/controllers/Adminhtml/Cms/PageController.php';

class Dotcom_RelatedCmsPages_Adminhtml_Cms_PageController extends JR_CleverCms_Adminhtml_Cms_PageController
{
    /**
     * Get related pages grid and serializer block
     */
    public function relatedAction()
    {
        $this->_initRelatedPage();
        $this->loadLayout();
        $this->getLayout()->getBlock('adminhtml.cms.page.edit.tab.related')
            ->setPagesRelated($this->getRequest()->getPost('pages_related', null));
        $this->renderLayout();
    }

    /**
     * Get related pages grid
     */
    public function relatedGridAction()
    {
        $this->_initRelatedPage();
        $this->loadLayout();
        $this->getLayout()->getBlock('adminhtml.cms.page.edit.tab.related')
            ->setPagesRelated($this->getRequest()->getPost('pages_related', null));
        $this->renderLayout();
    }

    /**
     * Initialize page from request parameters
     *
     */
    protected function _initRelatedPage()
    {
    	// 0. Resetto il tema "clever" impostato dal modulo JR_ClefverCms
        Mage::getDesign()->setTheme('default');
		
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('cms/page')->setStoreId($this->getRequest()->getParam('store', 0));

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('cms')->__('Cannot retreave related pages. The parent page no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
		
        // 3. Register model to use later in blocks
        Mage::register('cms_page', $model);
    }
}