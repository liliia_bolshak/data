<?php

class Dotcom_RelatedCmsPages_Block_Adminhtml_Cms_Page_Edit_Tabs extends Mage_Adminhtml_Block_Cms_Page_Edit_Tabs
{	
    protected function _beforeToHtml()
    {		
        $this->addTab('related', array(
            'label'     => Mage::helper('cms')->__('Related Pages'),
            'url'       => $this->getUrl('*/*/related', array('_current' => true)),
            'class'     => 'ajax',
        ));

        return parent::_beforeToHtml();
    }
	
}
