<?php
/**
 * Related pages admin grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Dotcom_RelatedCmsPages_Block_Adminhtml_Cms_Page_Edit_Tab_Related extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('related_pages_grid');
        $this->setDefaultSort('order');
		$this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->_getPage()->getId()) {
            $this->setDefaultFilter(array('in_pages' => 1));
        }
    }

    /**
     * Retirve currently edited page model
     *
     * @return Mage_Cms_Model_Page
     */
    protected function _getPage()
    {
        return Mage::registry('cms_page');
    }

    /**
     * Add filter
     *
     * @param object $column
     * @return Mage_Adminhtml_Block_Cms_Page_Edit_Tab_Related
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in page flag
        if ($column->getId() == 'in_pages') {
            $pageIds = $this->_getSelectedPages();
            if (empty($pageIds)) {
                $pageIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('page_id', array('in' => $pageIds));
            } else {
                if($pageIds) {
                    $this->getCollection()->addFieldToFilter('page_id', array('nin' => $pageIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('cmsrelatedpages/page_link')
            ->getPageCollection()
            ->setPage($this->_getPage());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('in_pages', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'in_pages',
            'values'            => $this->_getSelectedPages(),
            'align'             => 'center',
            'index'             => 'page_id'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('cms')->__('Title'),
            'sortable'  => true,
            'index'     => 'title'
        ));

        $this->addColumn('page_url', array(
            'header'    => Mage::helper('cms')->__('Identifier'),
            'sortable'  => true,
            'index'     => 'identifier'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('cms')->__('Status'),
            'width'     => 90,
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
	            1 => Mage::helper('cms')->__('Enabled'),
	            0 => Mage::helper('cms')->__('Disabled'),
	        ),
        ));

        $this->addColumn('order', array(
            'header'            => Mage::helper('cms')->__('Order'),
            'name'              => 'order',
            'type'              => 'number',
            'validate_class'    => 'validate-number',
            'index'             => 'order',
            'width'             => 60,
            'editable'          => true,
            'edit_only'         => !$this->_getPage()->getId()
        ));

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('*/*/relatedGrid', array('_current' => true));
    }

    /**
     * Retrieve selected related pages
     *
     * @return array
     */
    protected function _getSelectedPages()
    {
        $pages = $this->getPagesRelated();
        if (!is_array($pages)) {
            $pages = array_keys($this->getSelectedRelatedPages());
        }
        return $pages;
    }

    /**
     * Retrieve related pages. Funzione di callback settata nel layout
     *
     * @return array
     */
    public function getSelectedRelatedPages()
    {
        $pages = array();
        foreach ($this->_getPage()->getRelatedPages() as $page) {
            $pages[$page->getId()] = array('order' => $page->getOrder());
        }
        return $pages;
    }
    /**
     * Get children of specified item
     *
     * @param Varien_Object $item
     * @return array
     */
    public function getMultipleRows($item)
    {
		return null;
    }
}
