<?php
/**
 * Cms page related items block
 *
 * @category   Dotcom
 * @package    Dotcom_RelatedCmsPages
 */
class Dotcom_RelatedCmsPages_Block_Cms_Widget_Related extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected $_itemCollection;

    public function __construct()
    {		
        $this->_itemCollection = Mage::getSingleton('cms/page')->getRelatedPageCollection()->setIsStrongMode()
            ->setOrder('order');

        $this->_data['items'] = array();
		
        return $this;
    }
	
	/**
	 * Costruisce l'elenco delle pagine correlate alla pagina corrente,
	 * raggruppandole per identificatore della pagina padre 
	 *
	 * @param string $parentPageIdentifier
	 * @return Dotcom_RelatedCmsPages_Block_Cms_Related
	 */
	public function addRelatedPages($parentPageIdentifier, $title=null)
	{
		/**
		 * @var Mage_Cms_Model_Page
		 */
		$pageModel = Mage::getSingleton('cms/page');
		
		/**
		 * Se l'identificatore di pagina non esiste esco
		 */
		if(null === ($parentPageId = $pageModel->checkIdentifier($parentPageIdentifier, $pageModel->getStoreId())))
		{
			return $this;
		}
		
		if(is_null($title))
		{
			$title = $pageModel->getResource()->getCmsPageTitleById($parentPageId);
		}
		
		foreach($this->_itemCollection->getItems() as $_page)
		{
			if($_page->getParentPage()->getId() == $parentPageId)
			{
				if(!isset($this->_data['items'][$parentPageIdentifier]))
				{
					$this->_data['items'][$parentPageIdentifier] = array(
						'title' => $title,
						'parent_page' => $_page->getParentPage(),
						'pages' => array()
					);
				}
				$this->_data['items'][$parentPageIdentifier]['pages'][] = $_page;
			}
		}
		
        return $this;
	}
}
