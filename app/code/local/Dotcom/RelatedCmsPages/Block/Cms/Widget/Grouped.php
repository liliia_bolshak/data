<?php

/**
 * Cms page related items block
 *
 * @category   Mage
 * @package    Mage_Cms
 */
class Dotcom_RelatedCmsPages_Block_Cms_Widget_Grouped extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected $_itemCollection;

    public function __construct()
    {	
		$this->_data['items'] = array();
        return $this;
    }	
	
	/**
	 * Costruisce l'elenco delle pagine correlate raggruppandole per identificatore della pagina padre 
	 *
	 * @param string $parentPageIdentifier
	 * @return Dotcom_RelatedCmsPages_Block_Cms_Related
	 */
	public function addGroup($identifier, $title=null)
	{
		/**
		 * @var Mage_Cms_Model_Page
		 */
		$currentPage = Mage::getSingleton('cms/page');
		$_itemCollection = Mage::getModel('cms/page')->getCollection()->addChildrenFilter(Mage::getModel('cms/page')->load($identifier, 'identifier'));
		/**
		 * Se l'identificatore di pagina non esiste esco
		 */
		if(null === ($parentPageId = $currentPage->checkIdentifier($identifier, $currentPage->getStoreId())))
		{
			return $this;
		}
		
		if(is_null($title))
		{
			$title = $currentPage->getResource()->getCmsPageTitleById($parentPageId);
		}
		
		foreach($_itemCollection->getItems() as $_page)
		{
			if($_page->getParentPage()->getId() == $parentPageId)
			{
				if(!isset($this->_data['items'][$identifier]))
				{
					$this->_data['items'][$identifier] = array(
						'title' => $title,
						'parent_page' => $_page->getParentPage(),
						'pages' => array()
					);
				}
				$this->_data['items'][$identifier]['pages'][] = $_page;
			}
		}
		
        return $this;
	}
}
