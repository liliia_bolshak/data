<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
/* @var $installer JR_CleverCms_Model_Resource_Setup */
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS `{$this->getTable('cms_related_pages')}`;

    CREATE TABLE `{$this->getTable('cms_related_pages')}` (
      `relation_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `parent_id` int(11) UNSIGNED NOT NULL,
      `child_id` int(11) UNSIGNED NOT NULL,
      `order` int(5) UNSIGNED
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
	ALTER TABLE `{$this->getTable('cms_related_pages')}`
	  ADD UNIQUE (`parent_id`, `child_id`),
	  ADD CONSTRAINT `FK_CMS_RELATED_PAGES_PARENT_CMS_PAGE` FOREIGN KEY (`parent_id`) REFERENCES `cms_page_tree` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
	  ADD CONSTRAINT `FK_CMS_RELATED_PAGES_CHILD_CMS_PAGE` FOREIGN KEY (`child_id`) REFERENCES `cms_page_tree` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$installer->endSetup();
