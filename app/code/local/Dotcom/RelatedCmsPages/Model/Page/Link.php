<?php


/**
 * Cms page link model
 */
class Dotcom_RelatedCmsPages_Model_Page_Link extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     */
    protected function _construct()
    {
        $this->_init('cmsrelatedpages/page_link');
    }
	
    /**
     * Retrieve linked pages collection
     */
    public function getPageCollection()
    {
        return Mage::getResourceModel('cmsrelatedpages/page_link_page_collection');
    }

    /**
     * Save data for page relations
     *
     * @param   Mage_Cms_Model_Page $page
     * @return  Dotcom_RelatedCmsPages_Model_Page_Link
     */
    public function savePageRelations($page)
    {
        $data = $page->getData('relations');
        if (!is_null($data)) {
            $this->_getResource()->savePageRelations($page, Mage::helper('adminhtml/js')->decodeGridSerializedInput($data));
        }
        return $this;
    }
}