<?php

class Dotcom_RelatedCmsPages_Model_Page extends JR_CleverCms_Model_Cms_Page
{
    /**
     * Saving page related data
     *
     * @return Mage_Cms_Model_Page
     */
    protected function _afterSave()
    {
        $this->getRelationInstance()->savePageRelations($this);
        return parent::_afterSave();
    }
		
    /**
     * Retrieve array of related pages
     *
     * @return array
     */
    public function getRelatedPages()
    {
        if (!$this->hasRelatedPages()) {
            $pages = array();
            $collection = $this->getRelatedPageCollection();
            foreach ($collection as $page) {
                $pages[] = $page;
            }
            $this->setRelatedPages($pages);
        }
        return $this->getData('related_pages');
    }

    /**
     * Retrieve related page identifiers
     *
     * @return array
     */
    public function getRelatedPageIds()
    {
        if (!$this->hasRelatedPagestIds()) {
            $ids = array();
            foreach ($this->getRelatedPages() as $page) {
                $ids[] = $page->getId();
            }
            $this->setRelatedPagesIds($ids);
        }
        return $this->getData('related_pages_ids');
    }

    /**
     * Retrieve collection related pages
     */
    public function getRelatedPageCollection()
    {
        $collection = $this->getRelationInstance()
        	->getPageCollection()
            ->setIsStrongMode();
        $collection->setPage($this);
        return $collection;
    }
		

    /**
     * Retrieve link instance
     *
     * @return  Dotcom_RelatedCmsPages_Model_Page_Link
     */
    public function getRelationInstance()
    {
        if (!$this->_linkInstance) {
            $this->_linkInstance = Mage::getSingleton('cmsrelatedpages/page_link');
        }
        return $this->_linkInstance;
    }
}
