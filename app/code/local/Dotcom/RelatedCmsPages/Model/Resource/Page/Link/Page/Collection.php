<?php


/**
 * Cms page linked pages collection
 *
 * @category    Dotcom
 * @package     Dotcom_RelatedCmsPages
 */
class Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection extends Mage_Cms_Model_Resource_Page_Collection
{
    /**
     * Store page model
     *
     * @var Mage_Cms_Model_Page
     */
    protected $_page;
	
    /**
     * Store flag that determine if page filter was enabled
     *
     * @var bool
     */
    protected $_hasLinkFilter  = false;

    /**
     * Store strong mode flag that determine if needed for inner join or left join of linked pages
     *
     * @var bool
     */
    protected $_isStrongMode;
	
    protected function _construct()
    {
        $this->_init('cms/page');
    }
	
    /**
     * Initialize collection parent page and add limitation join
     *
     * @param Mage_Cms_Model_Page $page
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    public function setPage(Mage_Cms_Model_Page $page)
    {
        $this->_page = $page;
        if ($page && $page->getId()) {
            $this->_hasLinkFilter = true;
        }
        return $this;
    }

    /**
     * Retrieve collection base page object
     *
     * @return Mage_Cms_Model_Page
     */
    public function getPage()
    {
        return $this->_page;
    }
	
    /**
     * Join linked products when specified link model
     *
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    protected function _beforeLoad()
    {
	    $this->_joinLinks();
        return parent::_beforeLoad();
    }

    /**
     * Enable strong mode for inner join of linked pages
     *
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    public function setIsStrongMode()
    {
        $this->_isStrongMode = true;
        return $this;
    }

    /**
     * Join linked pages
     *
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    protected function _joinLinks()
    {
        $select  = $this->getSelect();
        $adapter = $select->getAdapter();
        $select->where('store_id = ?', (int)$this->getPage()->getStoreId());
        $joinCondition = array(
            'links.child_id = page_id'
        );
        $joinType = 'join';
        if ($this->getPage() && $this->getPage()->getId()) {
            $pageId = $this->getPage()->getId();
            if ($this->_isStrongMode) {
                $select->where('links.parent_id = ?', (int)$pageId);
            } else {
                $joinType = 'joinLeft';
                $joinCondition[] = $adapter->quoteInto('links.parent_id = ?', $pageId);
            }
            $this->addFieldToFilter('page_id', array('neq' => $pageId));
        } else if ($this->_isStrongMode) {
            $this->addFieldToFilter('page_id', array('eq' => -1));
        }
        if($this->_hasLinkFilter) {
            $select->$joinType(
                array('links' => $this->getTable('cmsrelatedpages/related_pages')),
                implode(' AND ', $joinCondition),
                array('relation_id', 'order')
            );
        }
        return $this;
    }

    /**
     * Exclude pages from filter
     *
     * @param array $pages
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Collection
     */
    public function addExcludePageFilter($pages)
    {
        if (!empty($pages)) {
            if (!is_array($pages)) {
                $pages = array($pages);
            }
            $this->_hasLinkFilter = true;
            $this->getSelect()->where('page_id NOT IN (?)', $pages);
        }
        return $this;
    }

    /**
     * Add pages to filter
     *
     * @param array|int|string $pages
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Collection
     */
    public function addPageFilter($pages)
    {
        if (!empty($pages)) {
            if (!is_array($pages)) {
                $pages = array($pages);
            }
            $this->getSelect()->where('page_id IN (?)', $pages);
            $this->_hasLinkFilter = true;
        }

        return $this;
    }
	
    /**
     * Enable sorting pages by its order
     *
     * @param string $dir sort type asc|desc
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    public function setPositionOrder($dir = self::SORT_ORDER_ASC)
    {
        if ($this->_hasLinkFilter) {
            $this->getSelect()->order('order ' . $dir);
        }
        return $this;
    }
	
    /**
     * Set sorting order
     *
     * $attribute can also be an array of attributes
     *
     * @param string|array $attribute
     * @param string $dir
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link_Page_Collection
     */
    public function setOrder($attribute, $dir = self::SORT_ORDER_ASC)
    {
        if ($attribute == 'order') {
            return $this->setPositionOrder($dir);
        }
        return parent::setOrder($attribute, $dir);
    }
}
