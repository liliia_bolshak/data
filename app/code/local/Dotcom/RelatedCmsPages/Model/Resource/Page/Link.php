<?php

/**
 * Cms page link resource model
 *
 * @category    Dotcom
 * @package     Dotcom_RelatedCmsPages
 */
class Dotcom_RelatedCmsPages_Model_Resource_Page_Link extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table name
     */
    public function _construct()
    {
        $this->_init('cmsrelatedpages/related_pages', 'relation_id');
    }

    /**
     * Save Page Links process
     *
     * @param Mage_Cms_Model_Page $page
     * @param array $data
     * @return Dotcom_RelatedCmsPages_Model_Resource_Page_Link
     */
    public function savePageRelations($page, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }
			
        $adapter = $this->_getWriteAdapter();

        $bind   = array(
            ':page_id'    => (int)$page->getId()
        );
        $select = $adapter->select()
            ->from($this->getMainTable(), array('child_id', 'relation_id', 'order'))
            ->where('parent_id = :page_id');

        $links   = $adapter->fetchAssoc($select, $bind);
		
        $deleteIds = array();
        foreach($links as $linkedPageId => $linkInfo) {
            if (!isset($data[$linkedPageId])) {
                $deleteIds[] = (int)$linkInfo['relation_id'];
            }
        }
        if (!empty($deleteIds)) {
            $adapter->delete($this->getMainTable(), array(
                'relation_id IN (?)' => $deleteIds,
            ));
        }
		
        foreach ($data as $linkedPageId => $linkInfo) {
            $linkId = null;
            if (isset($links[$linkedPageId])) {
                $linkOrder = $links[$linkedPageId]['order'];
                $relation_id = $links[$linkedPageId]['relation_id'];
				if($linkInfo['order'] != $linkOrder)
				{
					$where = $adapter->quoteInto('relation_id = ?', $relation_id);
			        $bind   = array(
			            'order' => (int)$linkInfo['order']
			        );
					$adapter->update($this->getMainTable(), $bind, $where);
				}
                unset($links[$linkedPageId]);
            } else {
                $bind = array(
                    'parent_id'=> $page->getId(),
                    'child_id' => $linkedPageId,
                    'order' => $linkInfo['order']
                );
                $adapter->insert($this->getMainTable(), $bind);
            }
        }
        return $this;
    }

    /**
     * Retrieve Required children ids
     *
     * @param int $parentId
     * @return array
     */
    public function getChildrenIds($parentId)
    {
        $adapter     = $this->_getReadAdapter();
        $bind        = array(
            ':parent_id'    => (int)$parentId
        );
        $select = $adapter->select()
            ->from($this->getMainTable(), array('child_id'))
            ->where('parent_id = :parent_id');
			
        $childrenIds = array();
        $result = $adapter->fetchAll($select, $bind);
        foreach ($result as $row) {
            $childrenIds[$row['child_id']] = $row['child_id'];
        }

        return $childrenIds;
    }

    /**
     * Retrieve parent ids array by required child
     *
     * @param int|array $childId
     * @return array
     */
    public function getParentIdsByChild($childId)
    {
        $parentIds  = array();
        $adapter    = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable(), array('parent_id', 'child_id'))
            ->where('child_id IN(?)', $childId);

        $result = $adapter->fetchAll($select);
        foreach ($result as $row) {
            $parentIds[] = $row['parent_id'];
        }

        return $parentIds;
    }

}
