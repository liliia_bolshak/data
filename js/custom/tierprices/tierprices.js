/**
* MagPleasure Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the EULA
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://www.magpleasure.com/LICENSE.txt
*
* @category   Magpleasure
* @package    Magpleasure_Tierprices
* @copyright  Copyright (c) 2012 MagPleasure Ltd. (http://www.magpleasure.com)
* @license    http://www.magpleasure.com/LICENSE.txt
*/

function applyPercentagePrice(percentage, price_box_id) {
	original_price = document.getElementById('price').value/100;
	document.getElementById(price_box_id).value = original_price*percentage;
}