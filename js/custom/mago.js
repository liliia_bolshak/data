jQuery(function(){

/*
	if (!jQuery.browser.msie || (jQuery.browser.version >= 7 && jQuery.browser.msie))
		jQuery('head').append('<link href="/styles/card.css" media="screen" rel="stylesheet" type="text/css" />');
*/
	
  /*  jQuery('input, select, textarea, button', '#cardListCalculator').keydown(function(e){
        if ( e.which == 9){
            e.preventDefault();
            e.stopImmediatePropagation();
            e.stopPropagation();
        }
    });*/
    
    //jQuery('#cardListCalculator').submit(function(){
    jQuery('#quotation').click(function(){
    	 
        var captcha = jQuery('#captacha_code').val();
        var security_code = jQuery('#security_code').val();
        if (captcha != security_code)
        {
        	alert('Codice di sicurezza errato!');
        	return false;
        }
        jQuery('#cardListCalculator').trigger('submit');
    });

    jQuery('#steptwo, #backtotwo, #tosteptwo').click(function(){
        if ( checkSteps() != true ) return alert(checkSteps());
        jQuery('.cardquotationcustomerwelkome').hide('slow');
        gotoStep('two');
    });
    
    jQuery('#stepthree, #backtothree, #tostepthr').click(function(){
        if ( checkSteps() != true ) return alert(checkSteps());
        jQuery('.cardquotationcustomerwelkome').hide('slow');
        gotoStep('three');
    });
    
    jQuery('#stepfour, #tostepfou').click(function(){
        if ( checkSteps() != true ) return alert(checkSteps());
        jQuery.ajax({
            url:'/preventivocard/index/post',
            data: getFormValues(),
            dataType: 'json',
            type: "POST",
            success: function(response){
                if(response['error'] != undefined)
                {
                    alert(response['error']);
//                } else if (response['status'] == 'login') {
//                    alert('Devi effettuare il login per vedere i prezzi.');
//                    return false;
                } 
                else
                {
                    jQuery('.cardquotationcustomerwelkome').hide('slow');

                    var costo = response['costo'] / 1000;
                    jQuery('#costounitario').val( costo + " €" );

                    var costototale = costo * response['costoCardQuantita'];
                    costototale = Math.round(costototale * Math.pow(10, 2)) / Math.pow(10, 2);
                    jQuery('#costototale').val( costototale + " €" );
                    gotoStep('four');
                    return true;
                }
            },
            error: function(response){
                alert("C'è stato un disservizio, si prega di riprovare più tardi, \r\nnel caso il problema persista si prega di contattarci.");
            }
        });
    });
    
    jQuery('#backtoone, #tostepone').click(function(){
        //jQuery('.cardquotationcustomerwelkome').show('slow');
        gotoStep('one');
    });

	/*
	 * Setup inline help
	 * 
	 */
	jQuery('#cardHelp span').hide();
	jQuery('#hlp').show();
	
	// setup eventi inline help
	jQuery('#costoCardQuantita').click(function(){jQuery('#cardHelp span').hide();jQuery('#qty').show('slow');});
	jQuery('#firma').click(function(){jQuery('#cardHelp span').hide();jQuery('#frm').show('slow');});
	jQuery('#coloreserigrafico').click(function(){jQuery('#cardHelp span').hide();jQuery('#srg').show('slow');});
	
	jQuery('#stampaacaldo').click(function(){jQuery('#cardHelp span').hide();jQuery('#stc').show('slow');});
	
	jQuery('#codificamagnetica').click(function(){jQuery('#cardHelp span').hide();jQuery('#cdm').show('slow');});
	
	jQuery('#colorepantone').click(function(){jQuery('#cardHelp span').hide();jQuery('#pnt').show('slow');});
	jQuery('#punzonatura').click(function(){jQuery('#cardHelp span').hide();jQuery('#pnz').show('slow');});
	jQuery('#asolaturacard').click(function(){jQuery('#cardHelp span').hide();jQuery('#asl').show('slow');});
	jQuery('#nomeprogettocard').click(function(){jQuery('#cardHelp span').hide();jQuery('#nme').show('slow');});
	jQuery('#note').click(function(){jQuery('#cardHelp span').hide();jQuery('#nte').show('slow');});
	jQuery('#chipcontatto').click(
			function() {
					jQuery('#cardHelp span').hide();
					switch(jQuery('#chipcontatto').val()){
						case 'no':jQuery('#nochip').show('slow');break;
						case 'sle4442':jQuery('#chpSLE4442').show('slow');break;
						case 'sle4428':jQuery('#chpSLE4428').show('slow');break;
						case 'sle5542':jQuery('#chpSLE5542').show('slow');break;
						case 'at24c04':jQuery('#chpAT24C04').show('slow');break;
						default: jQuery('#cnt').show('slow');break;
					}
				}
		);
	jQuery('#chipcrfid').click(
			function() {
					jQuery('#cardHelp span').hide();
					switch(jQuery('#chipcrfid').val()){
						case 'no':jQuery('#chpnorfid').show('slow');break;
						case 'em4100':jQuery('#chpEM4100').show('slow');break;
						case 't5567':jQuery('#chpT5567').show('slow');break;
						case 'mifares501k':jQuery('#chpmifare').show('slow');break;
						case 'icode2':jQuery('#chpiCode2').show('slow');break;
						default: jQuery('#rfd').show('slow');break;
					}
				}
		);
	jQuery('#persdativariabili').click(function(){jQuery('#cardHelp span').hide();jQuery('#var').show('slow');});
	jQuery('#persstampafoto').click(function(){jQuery('#cardHelp span').hide();jQuery('#fto').show('slow');});
	jQuery('#colorifronte').click(function(){
		jQuery('#cardHelp span').hide();
		if(jQuery('#colorifronte').val()=='mono'){jQuery('#frn2').show('slow');}
		else if(jQuery('#colorifronte').val()=='quad'){jQuery('#frn1').show('slow');}
		else jQuery('#frn0').show('slow');
	});
	jQuery('#coloriretro').click(function(){
		jQuery('#cardHelp span').hide();
		if(jQuery('#coloriretro').val()=='mono'){jQuery('#rtr2').show('slow');}
		else if(jQuery('#coloriretro').val()=='quad'){jQuery('#rtr1').show('slow');}
		else jQuery('#rtr0').show('slow');
	});
	jQuery('#spessoreCard').click(function(){
		jQuery('#cardHelp span').hide();
		if(jQuery('#spessoreCard').val()=='40'){jQuery('#sps2').show('slow');}
		else if(jQuery('#spessoreCard').val()=='76'){jQuery('#sps1').show('slow');}
		else jQuery('#hlp').show();
	});
	jQuery('#bandamagnetica').click(function(){
		jQuery('#cardHelp span').hide();
		if(jQuery('#bandamagnetica').val()=='no')jQuery('#bnd0').show('slow');
		else if(jQuery('#bandamagnetica').val()=='loco')jQuery('#bnd1').show('slow');
		else jQuery('#bnd2').show('slow');
	});
    jQuery('#tipostampa').click(function(){
        jQuery('#cardHelp span').hide();
        jQuery('#tps').show('slow');
    });
	
    jQuery('#costoCardQuantita').change(function(e) {checkTipoStampa();});
    jQuery('#chipcontatto').change(function(e) {checkDoubleChip();});
    jQuery('#chipcrfid').change(function(e) {checkDoubleChip();});
    jQuery('#spessoreCard').change(function(e) {checkDoubleChip();});

	/*
	 * controllo login
	 */
	jQuery('#quotation').click(function(){
		jQuery('#domath').click();
		jQuery('#costounitario').removeAttr("disabled");
		jQuery('#costototale').removeAttr("disabled");
		jQuery('#cardListCalculator').unbind('submit');
	});

	/*
	 * setup eventi per la preview
	 */
	jQuery('#coloriretro').change(function(){preview();});
	jQuery('#bandamagnetica').change(function(){preview();});
	jQuery('#colorifronte').change(function(){preview();});
	jQuery('#chipcontatto').change(function(){preview();});
	jQuery('#chipcrfid').change(function(){preview();});
	jQuery('#firma').change(function(){preview();});
	//jQuery('#perscodicebarre').change(function(){preview();});
	jQuery('#persdativariabili').change(function(){preview();});
	jQuery('#punzonatura').change(function(){preview();});
	jQuery('#persstampafoto').change(function(){preview();});
	jQuery('#asolaturacard').change(function(){preview();});
	jQuery('#tipostampa').change(function(){preview();checkTipoStampa();});
	jQuery('#tipostampa').click(function(){preview();checkTipoStampa();});

	/*
	 * setup preview
	 */
	preview();

    if ( location.href.substr(-19) == 'card-quotation-send' ) {
        //setTimeout("jQuery('#stepfour').click()", 5000);
        jQuery('#stepfour').click();
    }
});

/*
 * funzione per le combo dati in preview
 */
function preview()
{
	;
	// RETRO
	switch (jQuery('#coloriretro').val()){
		case 'quad':
			jQuery('#cpr').css('background-image', 'url(/images/card/quadricromia-[Convertito].png)');
			break;
		case 'mono':
			jQuery('#cpr').css('background-image', 'url(/images/card/monocromia-[Convertito].png)');
			break;
		default:
			jQuery('#cpr').css('background-image', 'url(/images/card/blank-[Convertito].png)');
			break;
	}

	// FRONTE
	switch (jQuery('#colorifronte').val()){
		case 'quad':
			jQuery('#cpf').css('background-image', 'url(/images/card/quadricromia-[Convertito].png)');
			break;
		case 'mono':
			jQuery('#cpf').css('background-image', 'url(/images/card/monocromia-[Convertito].png)');
			break;
		default:
			jQuery('#cpf').css('background-image', 'url(/images/card/blank-[Convertito].png)');
			break;
	}

	jQuery('.data').hide();
	jQuery('.barc').hide();
	switch (jQuery('#persdativariabili').val()){
		case 'fronte':
			jQuery('#cpf .data').show();
			jQuery('#cpf .barc').show();
			break;
		case 'retro':
			jQuery('#cpr .data').show();
			jQuery('#cpr .barc').show();
			break;
		case 'ambo':
			jQuery('.data').show();
			jQuery('.barc').show();
			break;
	}

	switch (jQuery('#persstampafoto').val()){
		case 'no':
			jQuery('.foto').hide();
			break;
		case 'fronte':
			jQuery('#cpf .foto').show();
			jQuery('#cpr .foto').hide();
			break;
		case 'retro':
			jQuery('#cpf .foto').hide();
			jQuery('#cpr .foto').show();
			break;
	}

	switch (jQuery('#bandamagnetica').val()){
		case 'hico':
			jQuery('#cpr .banh').show();
			jQuery('#cpr .banl').hide();
			break;
		case 'loco':
			jQuery('#cpr .banh').hide();
			jQuery('#cpr .banl').show();
			break;
		default:
			jQuery('#cpr .banh').hide();
			jQuery('#cpr .banl').hide();
	}

	jQuery('#cpf .AT24C04').hide();
	jQuery('#cpf .SLE4428').hide();
	jQuery('#cpf .SLE4442').hide();
	jQuery('#cpf .SLE5542').hide();
	jQuery('#cpf .chip').hide();
	if (jQuery('#chipcontatto').val() != 'no'){
		switch (jQuery('#chipcontatto').val()){
			case 'at24c04':
				jQuery('#cpf .AT24C04').show();
				break;
			case 'sle4428':
				jQuery('#cpf .SLE4428').show();
				break;
			case 'sle4442':
				jQuery('#cpf .SLE4442').show();
				break;
			case 'sle5542':
				jQuery('#cpf .SLE5542').show();
				break;
			default:
				jQuery('#cpf .chip').show();
		}
	} else if (jQuery('#chipcrfid').val() != 'no') jQuery('.chip').show();

	if (jQuery('#firma').attr('checked')) jQuery('#cpr .firm').show();
	else jQuery('#cpr .firm').hide();

	if (jQuery('#punzonatura').attr('checked')) jQuery('.punz').show();
	else jQuery('.punz').hide();

	if (jQuery('#asolaturacard').attr('checked')) jQuery('.asol').show();
	else jQuery('.asol').hide();
}//**

/*
 * getValues x calcolo ajax
 */
function getFormValues(){
	var card = {};
	card.costoCardQuantita = 	jQuery('#costoCardQuantita').val();
	card.spessoreCard = 		jQuery('#spessoreCard').val();
	card.colorifronte = 		jQuery('#colorifronte').val();
	card.coloriretro = 			jQuery('#coloriretro').val();
	card.tipostampa = 			jQuery('#tipostampa').val();
	
	card.bandamagnetica = 		jQuery('#bandamagnetica').val();
	card.chipcontatto = 		jQuery('#chipcontatto').val();
	card.chipcrfid = 			jQuery('#chipcrfid').val();
	card.firma = 				jQuery('#firma').attr('checked') ? 'true' : 'false';
	card.coloreserigrafico = 	jQuery('#coloreserigrafico').attr('checked') ? 'true' : 'false';
	card.stampaacaldo = 	    jQuery('#stampaacaldo').attr('checked') ? 'true' : 'false';
	card.asolaturacard = 		jQuery('#asolaturacard').attr('checked') ? 'true' : 'false';
	
	card.persdativariabili = 	jQuery('#persdativariabili').val();
	card.persstampafoto = 		jQuery('#persstampafoto').val();
	card.punzonatura = 			jQuery('#punzonatura').attr('checked') ? 'true' : 'false';
	card.codificamagnetica = 	jQuery('#codificamagnetica').attr('checked') ? 'true' : 'false';
	card.codificadatichip = 	jQuery('#codificadatichip').attr('checked') ? 'true' : 'false';
	//card.foto =                 jQuery('#foto').val();

	card.nomeprogettocard = 	jQuery('#nomeprogettocard').val();
	card.note = 				jQuery('#note').val();
	//card.perscodicebarre = 		jQuery('#perscodicebarre').val();


	return card;
}//***

/*
 * funzione di controllo qty <-> tipostampa
 */
function checkTipoStampa(){
	jQuery('#costounitario').val('');
	jQuery('#costototale').val('');

    var quantita = jQuery('#costoCardQuantita').val();
    jQuery('option[value=off]').attr('disabled', '');
	jQuery('#tipostampa option').removeAttr('disabled');

    var digits = new RegExp('^[0-9]+$');

    if (quantita == "" || digits.test(jQuery('#costoCardQuantita').val()) == false) {
        return;
    } else if (quantita > 999) {
        if (jQuery('#tipostampa').val() != 'off') {
            if (storeCode == 'dealers_it' ) alert("Per quantità superiori ai 1000 pezzi si deve usare la stampa in offset");
            else alert("For quantities over 1000 pieces you need to use offset printing");
            
            jQuery('option[value=dir], option[value=ind]').attr('disabled', 'disabled');
            jQuery('#tipostampa').val('off');
        }
    } else {
        if (jQuery('#tipostampa').val() == 'off') {
            if ( storeCode == 'dealers_it' ) alert("Per quantità inferiori ai 1000 pezzi non si può usare la stampa in offset");
            else alert("For quantities less than 1000 items you can not use offset printing");

            jQuery('option[value=off]').attr('disabled', 'disabled');
            jQuery('#tipostampa').val('dir');
        }
    }//*****

    jQuery('#cardHelp span').hide();
    jQuery('#qty').show();
}//***

function checkDoubleChip(){
    
    var a = jQuery('#chipcontatto').val();
    var b = jQuery('#chipcrfid').val();
    var c = jQuery('#spessoreCard').val();
   // var locale = location.hostname.substr(-3) == '.it' ? 'it' : 'en';
    //var locale = 'it';
    
    if (c == '40' && (a != 'no' || b != 'no')) {
        switch (storeCode){
            case 'dealers_it':
                alert(storeCode + "\r\nLo spessore di 0,40mm non supporta i chip, utilizzare lo spessore 0,76mm.\r\nHo provveduto a correggere lo spessore!");
                break;
            case 'dealers_en':
                alert("\r\nThickness 0,40mm don't allow the use of a chip, you must use thickness 0,76mm.\r\nThickness updated to 0,76mm!");
                break;
        }// switch
        jQuery('#spessoreCard').val('76');
        return false;
    }// if

    if ( a != 'no' && b != 'no') {
        switch (storeCode){
            case 'dealers_it':
                alert("\r\nPer tessere ibride con doppio chip si prega di richiedere il preventivo.");
                break;
            case 'dealers_en':
                alert("\r\nAbout hybrid dual chip please request a quote.");
                break;
        }// switch
        jQuery('#chipcontatto').val('no');
        jQuery('#chipcrfid').val('no');
        return false;
    } else return true;
}// function


function checkSteps(){
    
    checkTipoStampa();
    checkDoubleChip();
    
    var digits = new RegExp('^[0-9]+$');
    var valid = true;
    var msg = '';
    //var locale = location.hostname.substr(-3) == '.it' ? 'it' : 'en';
    //var locale = 'dealers_it';
    if ( digits.test( jQuery('#costoCardQuantita').val() ) ) valid = valid && true;
    else {
        valid = false;
        switch (storeCode){
            case 'dealers_it':
                msg += "\r\nPerfavore come quantita inserisci il numero in forma decimale.";
                break;
            case 'dealers_en':
                msg += "\r\nPleae as quantity use the decimal format.";
                break;
        }
    }
    
    if ( jQuery('#costoCardQuantita').val() > 99 ) valid = valid && true;
    else {
        valid = false;
        switch (storeCode){
        case 'dealers_it':
            msg += "\r\nLa quantità minima per fare un ordine è di 100 pezzi.";
            break;
        case 'dealers_en':
            msg += "\r\nThe minimum amount for an order is 100 pieces.";
            break;
        }
    }
    
    return valid == true ? true : msg;
}


function gotoStep(N){
    switch(N)
    {
    case 'four':
        jQuery('#end-card-text-wrapper-open').hide();
        
        jQuery('#wizard_one').attr('src', '/images/maga1_select.png');
        jQuery('#wizard_two').attr('src', '/images/maga2_select.png');
        jQuery('#wizard_three').attr('src', '/images/maga3_select.png');
        jQuery('#wizard_four').attr('src', '/images/maga4_select.png');
        
        jQuery('fieldset', '#cardListCalculator').animate({
            opacity: 0
        }, 1000, 'linear', function() {
            jQuery('#fieldset-base').css('display', 'none');
            jQuery('#fieldset-opzioni').css('display', 'none');
            jQuery('#fieldset-personalizzazioni').css('display', 'none');
            jQuery('#fieldset-calc').css('display', 'block');
            jQuery('#fieldset-askquotation').css('display', 'block');
            jQuery('#fieldset-calc, #fieldset-askquotation').animate({
                opacity: 1
            }, 1000, 'linear', function(){});
        });
        break;
    case 'three':
        jQuery('#end-card-text-wrapper-open').show();
        
        jQuery('#wizard_one').attr('src', '/images/maga1_select.png');
        jQuery('#wizard_two').attr('src', '/images/maga2_select.png');
        jQuery('#wizard_three').attr('src', '/images/maga3_select.png');
        jQuery('#wizard_four').attr('src', '/images/maga4.png');
        
        jQuery('fieldset', '#cardListCalculator').animate({
            opacity: 0
        }, 1000, 'linear', function() {
            jQuery('#fieldset-base').css('display', 'none');
            jQuery('#fieldset-opzioni').css('display', 'none');
            jQuery('#fieldset-personalizzazioni').css('display', 'block');
            jQuery('#fieldset-calc').css('display', 'none');
            jQuery('#fieldset-askquotation').css('display', 'none');
            jQuery('#fieldset-personalizzazioni').animate({
                opacity: 1
            }, 1000, 'linear', function(){});
        });
        break;
    case 'two':
        jQuery('#end-card-text-wrapper-open').show();
        
        jQuery('#wizard_one').attr('src', '/images/maga1_select.png');
        jQuery('#wizard_two').attr('src', '/images/maga2_select.png');
        jQuery('#wizard_three').attr('src', '/images/maga3.png');
        jQuery('#wizard_four').attr('src', '/images/maga4.png');
        
        jQuery('fieldset', '#cardListCalculator').animate({
            opacity: 0
        }, 1000, 'linear', function() {
            jQuery('#fieldset-base').css('display', 'none');
            jQuery('#fieldset-opzioni').css('display', 'block');
            jQuery('#fieldset-personalizzazioni').css('display', 'none');
            jQuery('#fieldset-calc').css('display', 'none');
            jQuery('#fieldset-askquotation').css('display', 'none');
            jQuery('#fieldset-opzioni').animate({
                opacity: 1
            }, 1000, 'linear', function(){});
        });
        break;
    case 'one':
    default:
        jQuery('#end-card-text-wrapper-open').show();
    
	    jQuery('#wizard_one').attr('src', '/images/maga1_select.png');
	    jQuery('#wizard_two').attr('src', '/images/maga2.png');
	    jQuery('#wizard_three').attr('src', '/images/maga3.png');
	    jQuery('#wizard_four').attr('src', '/images/maga4.png');

        
        jQuery('fieldset', '#cardListCalculator').animate({
            opacity: 0
        }, 1000, 'linear', function() {
            jQuery('#fieldset-base').css('display', 'block');
            jQuery('#fieldset-opzioni').css('display', 'none');
            jQuery('#fieldset-personalizzazioni').css('display', 'none');
            jQuery('#fieldset-calc').css('display', 'none');
            jQuery('#fieldset-askquotation').css('display', 'none');
            jQuery('#fieldset-base').animate({
                opacity: 1
            }, 1000, 'linear', function(){});
        });
        break;
    }
}