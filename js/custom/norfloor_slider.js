
if (norfloor_slider === undefined) {

	var norfloor_slider = function (options) {
		var instance = {
			"mask_id": null,
			"mask_width": null,
			"prev_id": null,
			"next_id": null,
			"moves_count": 0,
			"moves_width": null,
			"gallery_span": 4,
			"current_index": 0,
			"slider_tag": "ul",
			"children_tag": "li",
			"current_index": 0,
			"enabled": true,
			"_cssDimensionToInt": function(value){
				if (value.slice(-2) === 'px')
					value = value.slice(0, -2);
				return parseInt(value, 10);
			},
			"init": function(options) {
				var self = this;
				if(options.mask !== undefined && options.prev !== undefined && options.next !== undefined) {
					self.mask_id = options.mask;
					self.prev_id = options.prev;
					self.next_id = options.next;
		
					if(options.span !== undefined)     self.gallery_span = options.span;
					if(options.slider !== undefined)   self.slider_tag = options.slider;
					if(options.children !== undefined) self.children_tag = options.children;
		
					self.moves_count = jQuery(self.children_tag, self.mask_id).length - self.gallery_span;
					if(self.moves_count < 1){
						jQuery(self.prev_id).hide();
						jQuery(self.next_id).hide();
						self.enabled = false;
						return false;
					}
					
					self.moves_width = self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('width'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('padding-left'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('padding-right'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('margin-left'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('margin-right'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('border-left-width'))
						+ self._cssDimensionToInt(jQuery(self.children_tag, self.mask_id).css('border-right-width'));
		
					if(options.width !== undefined) {
						self.mask_width = options.width+"px";
						jQuery(self.mask_id).css('width', self.mask_width);
						jQuery(self.mask_id).css('overflow', 'hidden');
						jQuery(self.slider_tag, self.mask_id).css('width', parseInt((self.gallery_span+self.moves_count+1)*self.moves_width, 10)+"px");
					}
					jQuery(self.mask_id).css('position', 'relative');
					jQuery(self.slider_tag, self.mask_id).css('position', 'absolute');
					
					jQuery(self.prev_id).bind('click', function(){self.prev();return false;});
					jQuery(self.next_id).bind('click', function(){self.next();return false;});
				}
			},
			"prev": function(){
				var self = this;
				if (self.enabled && self.current_index > 0) {
					self.enabled = false;
					self.current_index -= 1;
					jQuery(self.slider_tag, self.mask_id).animate({"left": "-"+parseInt(self.moves_width*self.current_index, 10)+"px"}, 500, function(){
						self.enabled = true;
					});
				}
			},
			"next": function(){
				var self = this;
				if (self.enabled && self.current_index < self.moves_count) {
					self.enabled = false;
					self.current_index += 1;
					jQuery(self.slider_tag, self.mask_id).animate({"left": "-"+parseInt(self.moves_width*self.current_index, 10)+"px"}, 500, function(){
						self.enabled = true;
					});
				}
			}
		};
		instance.init(options);
		return instance;
	};
}