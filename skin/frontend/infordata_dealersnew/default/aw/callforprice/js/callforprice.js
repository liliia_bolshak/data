window.addEventListener('load',callforpricebindTooltips,false);
callForPriceTimeoutShow = null;
callForPriceTimeoutHide = null;

function callforpricebindTooltips()
{
    $$('.callforprice_category_text').each(function(el){
        el.addEventListener('mouseover',function(e){

            //his.down(0).style.left = e.pageX+'px';
            //this.down(0).style.top = e.pageY+'px';

            callforpriceDisplayTooltip(this.down(0));
    },false);
        el.addEventListener('mouseout',function(e){

            callforpriceHideTooltip(this);
        },false);
    });

    $$('.callforprice_product_text').each(function(el){
        el.addEventListener('mouseover',function(e){

            //his.down(0).style.left = e.pageX+'px';
            //this.down(0).style.top = e.pageY+'px';

            callforpriceDisplayTooltip(this.down(0));
        },false);
        el.addEventListener('mouseout',function(e){

            callforpriceHideTooltip(this);
        },false);
    });
}


function callforpriceHideTooltip(element) {
    callForPriceTimeoutShow = null;

    callForPriceTimeoutHide = setTimeout(function(){
        if(callForPriceTimeoutHide){
            try {
                element.getElementsByClassName('callforprice_tooltip')[0].style.display = 'none';
            } catch(e) {}
        }
    },100);
}

function callforpriceDisplayTooltip(element) {
    callForPriceTimeoutHide = null;
    callForPriceTimeoutShow = setTimeout(function(){
        if(callForPriceTimeoutShow)
        element.style.display = 'block'
    },500);
}