<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Error reporting
 */
error_reporting(E_ALL | E_STRICT);

/**
 * Compilation includes configuration file
 */
$compilerConfig = 'includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

if (file_exists('maintenance.flag')) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

$includePath = array();
$includePath[] = '/var/www/data/infordata.it';
$includePath[] = '/var/www/data/magento_patch/app';
$includePath[] = '/var/www/data/magento_patch/app/code/local';
$includePath[] = '/var/www/data/magento_patch/app/code/community';
$includePath[] = '/var/www/data/magento-1.6.1.0/app/code/community';
$includePath[] = '/var/www/data/magento-1.6.1.0/app/code/core';
$includePath[] = '/var/www/data/magento_patch/lib';
$includePath[] = '/var/www/data/magento_patch/js';
$includePath[] = '/var/www/data/magento-1.6.1.0/skin';
$includePath[] = '/var/www/data/magento-1.6.1.0/lib';
$includePath[] = '/var/www/data/magento-1.6.1.0/js';

$includePath[] = get_include_path();
$includePath = implode(PATH_SEPARATOR,$includePath);
set_include_path($includePath);

require_once 'app/Mage.php';

function getLanguageURL()
{
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && $_SERVER["REQUEST_URI"]=='/') {

        foreach (explode(",", strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE'])) as $accept) {

            if (preg_match("!([a-z-]+)(;q=([0-9.]+))?!", trim($accept), $found)) {

                $langs[] = $found[1];

                $quality[] = (isset($found[3]) ? (float) $found[3] : 1.0);

            }

        }

        array_multisort($quality, SORT_NUMERIC, SORT_DESC, $langs);

        $lang = substr($langs[0],0,2);

        switch($lang){
            case 'it':
                $selectedLanguage = 'it';
                break;
            case 'es':
                $selectedLanguage = 'es';
                break;
            default:
                $selectedLanguage = 'en';
                break;
        }
        return $selectedLanguage;
    }

    // return 'it';

}

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    #Varien_Profiler::enable();
    Mage::setIsDeveloperMode(true);
    #ini_set('display_errors', 1);
}
ini_set('display_errors', 1);

umask(0);

/* Store or website code */
//$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
//$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
/*
if (!isset($_COOKIE["auto_redirect"]) || $_COOKIE["auto_redirect"]!='1') {
$currentCookieParams = session_get_cookie_params(); 

$rootDomain = '.infordatadealers.com'; 

session_set_cookie_params( 
    $currentCookieParams["lifetime"], 
    $currentCookieParams["path"], 
    $rootDomain, 
    $currentCookieParams["secure"], 
    $currentCookieParams["httponly"] 
); 

session_name('mysessionname'); 
session_start(); 

setcookie("auto_redirect", '1', time() + 3600, '/', $rootDomain); 
$user_language = getLanguageURL();

	switch($user_language){
		    case 'it':
		        $mageRunCode = "2";
			$mageRunType = "group";
		        break;
		    case 'es':
		        $mageRunCode = "dealers_es";
			$mageRunType = "store";
		        break;
		    case 'en':
		        $mageRunCode = "dealers_en";
			$mageRunType = "store";             
		        break;
		    default:
		        $mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';
			$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';              
		        break;
	}
}
else 
{*/
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
//}

Mage::run($mageRunCode, $mageRunType, array(
    'var_dir' 	=> '/var/www/data/infordata.it/var',
    'media_dir' => '/var/www/data/uploads/magento.infordata.it',
    'log_dir'	=> '/var/www/data/infordata.it/var/log'
));


Mage::getModel('ariete/observer')->importOrders();
//Mage::getModel('ariete/observer')->importInvoices();
//Mage::getModel('ariete/observer')->importCustomer();